country_event = {
	id = 10288
	title = "BecomeFailedState_TITLE"
	desc = "BecomeFailedState_DESC"
	major = yes
	trigger = {
		OR = {
			government = insurgency
			government = military_dictatorship
			government = socialist_dictatorship
			government = constitutional_monarchy
			government = absolute_monarchy
		}
		NOT = { stability = 0 }
		num_of_revolts = 1
		revolt_percentage = 0.2
		NOT = { army_size_percentage = 0.7 }
		
	}
	mean_time_to_happen = {
		months = 60
		modifier = {
			factor = 0.4
			has_country_modifier = very_unstable_culture_tm
		}
		modifier = {
			factor = 0.7
			has_country_modifier = unstable_culture_tm
		}
		modifier = {
			factor = 2.0
			has_country_modifier = very_stable_culture_tm
		}
		modifier = {
			factor = 0.75
			NOT = { stability = -1 }
		}
		modifier = {
			factor = 0.75
			NOT = { stability = -2 }
		}
		modifier = {
			factor = 0.8
			revolt_percentage = 0.5
		}
		modifier = {
			factor = 0.8
			revolt_percentage = 0.6
		}
		modifier = {
			factor = 0.8
			revolt_percentage = 0.7
		}
		modifier = {
			factor = 0.8
			revolt_percentage = 0.8
		}
		modifier = {
			factor = 0.8
			revolt_percentage = 0.9
		}
	}
	immediate = {
		random_country = {
			any_country = {
				limit = {
					exists = yes
					any_owned_province = { owned_by = THIS }
					government = insurgency
				}
				government = failed_insurgency
			}	
			any_country = {
				limit = {
					exists = yes
					any_owned_province = { owned_by = THIS }
					OR = {
						government = military_dictatorship
						government = socialist_dictatorship
						government = constitutional_monarchy
						government = absolute_monarchy
					}
				}
				government = failed_state
			}			
		}
	}

	option = {
		name = "EVTOPT_OK"
	}
}
