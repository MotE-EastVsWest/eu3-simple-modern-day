country_event = {
	id = 10016
	#major = yes
	title = "AddFROMCoresOnTHISCores_TITLE"
	desc = "DESC"
	is_triggered_only = yes
	immediate = {
		any_province_region = {
			limit = {
				is_core = THIS
				NOT = { is_core = FROM }
			}
			add_core = FROM
		}
	}
	option = {
		name = "EVTOPT_OK"
	}
}