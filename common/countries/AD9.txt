# Country Name:	San Andres	# Tag:	AD9
#			
graphical_culture = 	southamericagfx		
#			
color = {	155 100 247	}	
historical_ideas = {			
}			
historical_units = {			
	napoleonic_square		
	napoleonic_lancers		
	flying_battery		
}			
monarch_names = {			
	"Martinez #0" = 10		
	"Pomare #0" = 10		
	"Perez #0" = 10		
	"Rodriguez #0" = 10		
	"Bent #0" = 10		
	"Forbes #0" = 10		
	"Archbold #0" = 10		
	"Garcia #0" = 10		
	"Gomez #0" = 10		
	"Hooker #0" = 10		
	"Hernandez #0" = 10		
	"Castro #0" = 10		
	"Gonzalez #0" = 10		
	"Torres #0" = 10		
	"Jiminez #0" = 10		
	"Williams #0" = 10		
	"Herrera #0" = 10		
	"Mendoza #0" = 10		
	"Manuel #0" = 10		
	"James #0" = 10		
	"Bowie #0" = 10		
	"Corpus #0" = 10		
	"Livingston #0" = 10		
	"Cantillo #0" = 10		
	"Gordon #0" = 10		
	"Ramirez #0" = 10		
	"Lopez #0" = 10		
	"Steele #0" = 10		
	"Sanchez #0" = 10		
	"Barrios #0" = 10		
	"Suarez #0" = 10		
	"Sarmiento #0" = 10		
	"Morales #0" = 10		
	"Barker #0" = 10		
	"Hudgson #0" = 10		
	"Lever #0" = 10		
	"Giel #0" = 10		
	"Jacobs #0" = 10		
	"Gomes #0" = 10		
}			
ship_names = {			
	"Coast Guard"		
}			
leader_names			
	Martinez		
	Pomare		
	Perez		
	Rodriguez		
	Bent		
	Forbes		
	Forbes		
	Archbold		
	Garcia		
	Gomez		
	Hooker		
	Hernandez		
	Diaz		
	Castro		
	Gonzalez		
	Torres		
	Jiminez		
	Williams		
	Herrera		
	Mendoza		
	Manuel		
	James		
	Bowie		
	Corpus		
	Livingston		
	Cantillo		
	Gordon		
	Ramirez		
	Lopez		
	Steele		
	Sanchez		
	Barrios		
	Suarez		
	Sarmiento		
	Morales		
	Barker		
	Hudgson		
	Lever		
	Giel		
	Jacobs		
	Gomes		
}			
