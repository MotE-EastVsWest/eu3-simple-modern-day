# Country Name:	United Arab Emirates	# Tag:	AE9
#			
graphical_culture = 	muslimgfx		
#			
color = { 178  103  5 }	
historical_ideas = {			
	ecumenism		
	church_attendance_duty		
	divine_supremacy		
	deus_vult		
	revolution_and_counter		
	press_gangs		
	national_conscripts		
	napoleonic_warfare		
	vice_roys		
	vetting		
	bureaucracy		
}			
historical_units = {			
	napoleonic_square		
	napoleonic_lancers		
	flying_battery		
}			
monarch_names = {			
	"Ali #0" = 10		
	"Ahmed #0" = 10		
	"Khan #0" = 10		
	"Al Shehhi #0" = 10		
	"Kumar #0" = 10		
	"Al Zaabi #0" = 10		
	"Al Mansouri #0" = 10		
	"Saeed #0" = 10		
	"Mohamed #0" = 10		
	"Al Shamsi #0" = 10		
	"Rashid #0" = 10		
	"Allah #0" = 10		
	"Nair #0" = 10		
	"Singh #0" = 10		
	"Salim #0" = 10		
	"Al Balushi #0" = 10		
	"Malik #0" = 10		
	"Ibrahim #0" = 10		
	"Mathew #0" = 10		
	"Shah #0" = 10		
	"Al Mazroui #0" = 10		
	"Al Zaheri #0" = 10		
	"Abbas #0" = 10		
	"El Sewedy #0" = 10		
	"Shaikh #0" = 10		
	"El Amery #0" = 10		
	"Al Habsi #0" = 10		
	"Ahmad #0" = 10		
	"Al Muhairi #0" = 10		
	"El Kutby #0" = 10		
	"Al Mazrouei #0" = 10		
	"Varghese #0" = 10		
	"El Kaaby #0" = 10		
	"Hassan #0" = 10		
	"Al Qasimi #0" = 10		
	"Hanafi #0" = 10		
	"Al Nuaimi #0" = 10		
	"Iqbal #0" = 10		
	"Al Naimi #0" = 10		
}			
ship_names = {			
	"Coast Guard"		
}			
leader_names = {			
	Ali		
	Ahmed		
	Khan		
	Al Shehhi		
	Kumar		
	Al Zaabi		
	Al Zaabi		
	Al Mansouri		
	Saeed		
	Mohamed		
	Al Shamsi		
	Rashid		
	Hussain		
	Allah		
	Nair		
	Singh		
	Salim		
	Al Balushi		
	Malik		
	Ibrahim		
	Mathew		
	Shah		
	Al Mazroui		
	Al Zaheri		
	Abbas		
	El Sewedy		
	Shaikh		
	El Amery		
	Al Habsi		
	Ahmad		
	Al Muhairi		
	El Kutby		
	Al Mazrouei		
	Varghese		
	El Kaaby		
	Hassan		
	Al Qasimi		
	Hanafi		
	Al Nuaimi		
	Iqbal		
	Al Naimi		
}			
