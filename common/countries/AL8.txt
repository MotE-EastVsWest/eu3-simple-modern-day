# Country Name:	Albanian Rebels	# Tag:	AL8
#			
graphical_culture = 	easterngfx		
#			
color = {	151 42 76	}	
historical_ideas = {			
	vetting		
	national_bank		
	national_trade_policy		
	smithian_economics		
	shrewd_commerce_practise		
	scientific_revolution		
	sea_hawks		
	naval_provisioning		
	regimental_system		
	napoleonic_warfare		
	merchant_adventures		
}			
historical_units = {			
	napoleonic_square		
	napoleonic_lancers		
	flying_battery		
}			
monarch_names = {			
	"Hoxha #0" = 10		
	"cela #0" = 10		
	"Kurti #0" = 10		
	"Marku #0" = 10		
	"Muca #0" = 10		
	"Shehu #0" = 10		
	"Dervishi #0" = 10		
	"Kola #0" = 10		
	"Sula #0" = 10		
	"Elezi #0" = 10		
	"Gjoka #0" = 10		
	"Koci #0" = 10		
	"Hysa #0" = 10		
	"Leka #0" = 10		
	"Gjoni #0" = 10		
	"Balla #0" = 10		
	"Meta #0" = 10		
	"Lika #0" = 10		
	"Mema #0" = 10		
	"Hoxhaj #0" = 10		
	"Prifti #0" = 10		
	"Cani #0" = 10		
	"Toska #0" = 10		
	"Hasa #0" = 10		
	"Gjini #0" = 10		
	"Tafa #0" = 10		
	"Karaj #0" = 10		
	"Sinani #0" = 10		
	"Caka #0" = 10		
	"Sulaj #0" = 10		
	"Rama #0" = 10		
	"Murati #0" = 10		
	"Halili #0" = 10		
	"Cenaj #0" = 10		
	"Gega #0" = 10		
	"Aliaj #0" = 10		
	"Beqiri #0" = 10		
	"Doda #0" = 10		
	"Deda #0" = 10		
}			
ship_names = {			
	"Coast Guard"		
}			
leader_names = {			
	Hoxha		
	cela		
	Kurti		
	Marku		
	Muca		
	Shehu		
	Shehu		
	Dervishi		
	Kola		
	Sula		
	Elezi		
	Gjoka		
	Lleshi		
	Koci		
	Hysa		
	Leka		
	Gjoni		
	Balla		
	Meta		
	Lika		
	Mema		
	Hoxhaj		
	Prifti		
	Cani		
	Toska		
	Hasa		
	Gjini		
	Tafa		
	Karaj		
	Sinani		
	Caka		
	Sulaj		
	Rama		
	Murati		
	Halili		
	Cenaj		
	Gega		
	Aliaj		
	Beqiri		
	Doda		
	Deda		
}			
