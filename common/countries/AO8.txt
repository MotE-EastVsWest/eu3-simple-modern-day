# Country Name:	Sao Toman Rebels	# Tag:	AO8
#			
graphical_culture = 	africangfx		
#			
color = {	128 83 101	}	
historical_ideas = {			
	press_gangs		
	grand_navy		
	sea_hawks		
	superior_seamanship		
	naval_glory		
	excellent_shipwrights		
	naval_fighting_instruction		
	naval_provisioning		
	shrewd_commerce_practise		
	merchant_adventures		
	improved_foraging		
}			
historical_units = {			
	napoleonic_square		
	napoleonic_lancers		
	flying_battery		
}			
monarch_names = {			
	"Fernandes #0" = 10		
	"dos Santos #0" = 10		
	"Soares #0" = 10		
	"Afonso #0" = 10		
	"Quaresma #0" = 10		
	"da Costa #0" = 10		
	"Neto #0" = 10		
	"Lima #0" = 10		
	"Pereira #0" = 10		
	"de Sousa #0" = 10		
	"da Conceicão #0" = 10		
	"Santo #0" = 10		
	"Tavares #0" = 10		
	"da Cruz #0" = 10		
	"Lopes #0" = 10		
	"Vaz #0" = 10		
	"Mendes #0" = 10		
	"Dias #0" = 10		
	"Costa #0" = 10		
	"da Trindade #0" = 10		
	"da Graca #0" = 10		
	"Pires #0" = 10		
	"Carvalho #0" = 10		
	"Martins #0" = 10		
	"Gomes #0" = 10		
	"da Silva #0" = 10		
	"Sousa #0" = 10		
	"Das Neves #0" = 10		
	"Santos #0" = 10		
	"Ramos #0" = 10		
	"Monteiro #0" = 10		
	"do Espirito #0" = 10		
	"do Nascimento #0" = 10		
	"Ferreira #0" = 10		
	"Nascimento #0" = 10		
	"do Sacramento #0" = 10		
	"Varela #0" = 10		
	"Silva #0" = 10		
	"Semedo #0" = 10		
}			
ship_names = {			
	"Coast Guard"		
}			
leader_names = {			
	Fernandes		
	dos Santos		
	Soares		
	Afonso		
	Quaresma		
	da Costa		
	da Costa		
	Neto		
	Lima		
	Pereira		
	de Sousa		
	da Conceicão		
	dos Ramos		
	Santo		
	Tavares		
	da Cruz		
	Lopes		
	Vaz		
	Mendes		
	Dias		
	Costa		
	da Trindade		
	da Graca		
	Pires		
	Carvalho		
	Martins		
	Gomes		
	da Silva		
	Sousa		
	Das Neves		
	Santos		
	Ramos		
	Monteiro		
	do Espirito		
	do Nascimento		
	Ferreira		
	Nascimento		
	do Sacramento		
	Varela		
	Silva		
	Semedo		
}			
