# Country Name:	Austria	# Tag:	AT9
#			
graphical_culture = 	latingfx		
#			
color = { 220  220  220 }	
historical_ideas = {			
	ecumenism		
	church_attendance_duty		
	divine_supremacy		
	deus_vult		
	revolution_and_counter		
	press_gangs		
	national_conscripts		
	napoleonic_warfare		
	vice_roys		
	vetting		
	bureaucracy		
}			
historical_units = {			
	napoleonic_square		
	napoleonic_lancers		
	flying_battery		
}			
monarch_names = {			
	"Gruber #0" = 10		
	"Huber #0" = 10		
	"Bauer #0" = 10		
	"Wagner #0" = 10		
	"Muller #0" = 10		
	"Pichler #0" = 10		
	"Steiner #0" = 10		
	"Mayer #0" = 10		
	"Moser #0" = 10		
	"Hofer #0" = 10		
	"Berger #0" = 10		
	"Fuchs #0" = 10		
	"Eder #0" = 10		
	"Fischer #0" = 10		
	"Schmid #0" = 10		
	"Weber #0" = 10		
	"Schneider #0" = 10		
	"Schwarz #0" = 10		
	"Winkler #0" = 10		
	"Maier #0" = 10		
	"Reiter #0" = 10		
	"Schmidt #0" = 10		
	"Mayr #0" = 10		
	"Lang #0" = 10		
	"Baumgartner #0" = 10		
	"Brunner #0" = 10		
	"Wimmer #0" = 10		
	"Auer #0" = 10		
	"Egger #0" = 10		
	"Wolf #0" = 10		
	"Lechner #0" = 10		
	"Wallner #0" = 10		
	"Aigner #0" = 10		
	"Binder #0" = 10		
	"Ebner #0" = 10		
	"Haas #0" = 10		
	"Koller #0" = 10		
	"Lehner #0" = 10		
	"Schuster #0" = 10		
}			
ship_names = {			
	"Coast Guard"		
}			
leader_names = {			
	Gruber		
	Huber		
	Bauer		
	Wagner		
	Muller		
	Pichler		
	Pichler		
	Steiner		
	Mayer		
	Moser		
	Hofer		
	Berger		
	Leitner		
	Fuchs		
	Eder		
	Fischer		
	Schmid		
	Weber		
	Schneider		
	Schwarz		
	Winkler		
	Maier		
	Reiter		
	Schmidt		
	Mayr		
	Lang		
	Baumgartner		
	Brunner		
	Wimmer		
	Auer		
	Egger		
	Wolf		
	Lechner		
	Wallner		
	Aigner		
	Binder		
	Ebner		
	Haas		
	Koller		
	Lehner		
	Schuster		
}			
