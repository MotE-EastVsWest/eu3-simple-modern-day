# Country Name:	Arewa	# Tag:	AW9
#			
graphical_culture = 	africangfx		
#			
color = {	164 247 183	}	
historical_ideas = {			
}			
historical_units = {			
	napoleonic_square		
	napoleonic_lancers		
	flying_battery		
}			
monarch_names = {			
	"Abubakar #0" = 10		
	"Ibrahim #0" = 10		
	"Aliyu #0" = 10		
	"Abdullahi #0" = 10		
	"Bello #0" = 10		
	"Muhammad #0" = 10		
	"Umar #0" = 10		
	"Sani #0" = 10		
	"Shehu #0" = 10		
	"Muhammudu #0" = 10		
	"Garba #0" = 10		
	"Umaru #0" = 10		
	"Usman #0" = 10		
	"Muhammed #0" = 10		
	"Aminu #0" = 10		
	"Ummaru #0" = 10		
	"Isah #0" = 10		
	"Adamu #0" = 10		
	"Yusuf #0" = 10		
	"Hassan #0" = 10		
	"Mamman #0" = 10		
	"Bala #0" = 10		
	"Haruna #0" = 10		
	"Ahmad #0" = 10		
	"Amadu #0" = 10		
	"Yahaya #0" = 10		
	"Lawali #0" = 10		
	"Alhaji #0" = 10		
	"Nasiru #0" = 10		
	"Sahabi #0" = 10		
	"Kabiru #0" = 10		
	"Saidu #0" = 10		
	"Salihu #0" = 10		
	"Malami #0" = 10		
	"Abu #0" = 10		
	"Sanusi #0" = 10		
	"Tukur #0" = 10		
	"Mustapha #0" = 10		
	"Dahiru #0" = 10		
}			
ship_names = {			
	"Coast Guard"		
}			
leader_names			
	Abubakar		
	Ibrahim		
	Aliyu		
	Abdullahi		
	Bello		
	Muhammad		
	Muhammad		
	Umar		
	Sani		
	Shehu		
	Muhammudu		
	Garba		
	Musa		
	Umaru		
	Usman		
	Muhammed		
	Aminu		
	Ummaru		
	Isah		
	Adamu		
	Yusuf		
	Hassan		
	Mamman		
	Bala		
	Haruna		
	Ahmad		
	Amadu		
	Yahaya		
	Lawali		
	Alhaji		
	Nasiru		
	Sahabi		
	Kabiru		
	Saidu		
	Salihu		
	Malami		
	Abu		
	Sanusi		
	Tukur		
	Mustapha		
	Dahiru		
}			
