# Country Name:	British Atlantic Islands	# Tag:	B29
#			
graphical_culture = 	latingfx		
#			
color = {	45 178 128	}	
historical_ideas = {			
}			
historical_units = {			
	napoleonic_square		
	napoleonic_lancers		
	flying_battery		
}			
monarch_names = {			
	"Swain #0" = 10		
	"Green #0" = 10		
	"Repetto #0" = 10		
	"Glass #0" = 10		
	"Rogers #0" = 10		
	"Lavarello #0" = 10		
	"Collins #0" = 10		
	"Hagan #0" = 10		
	"Squibb #0" = 10		
	"Ashworth #0" = 10		
	"Burns #0" = 10		
	"Willemse #0" = 10		
	"Bancroft #0" = 10		
	"Christopher #0" = 10		
	"Conradie #0" = 10		
	"Davis #0" = 10		
	"Gardener #0" = 10		
	"Glass #0" = 10		
	"Hendrikse #0" = 10		
	"Lavarello #0" = 10		
	"Lee #0" = 10		
	"Monahan #0" = 10		
	"Potgieter #0" = 10		
	"Schreier #0" = 10		
	"Clarke #0" = 10		
	"Ford #0" = 10		
	"Jaffray #0" = 10		
	"Short #0" = 10		
	"Smith #0" = 10		
	"Morrison #0" = 10		
	"Goodwin #0" = 10		
	"Berntsen #0" = 10		
	"McKay #0" = 10		
	"Summers #0" = 10		
	"Betts #0" = 10		
	"Goss #0" = 10		
	"McLeod #0" = 10		
	"Bonner #0" = 10		
	"Lee #0" = 10		
}			
ship_names = {			
	"Coast Guard"		
}			
leader_names			
	Swain		
	Green		
	Repetto		
	Glass		
	Rogers		
	Lavarello		
	Lavarello		
	Collins		
	Hagan		
	Squibb		
	Ashworth		
	Burns		
	Mawar		
	Willemse		
	Bancroft		
	Christopher		
	Conradie		
	Davis		
	Gardener		
	Glass		
	Hendrikse		
	Lavarello		
	Lee		
	Monahan		
	Potgieter		
	Schreier		
	Clarke		
	Ford		
	Jaffray		
	Short		
	Smith		
	Morrison		
	Goodwin		
	Berntsen		
	McKay		
	Summers		
	Betts		
	Goss		
	McLeod		
	Bonner		
	Lee		
}			
