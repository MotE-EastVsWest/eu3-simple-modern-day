# Country Name:	Bahraini Rebels	# Tag:	BH8
#			
graphical_culture = 	muslimgfx		
#			
color = {	167 79 79	}	
historical_ideas = {			
	press_gangs		
	grand_navy		
	sea_hawks		
	superior_seamanship		
	naval_glory		
	excellent_shipwrights		
	naval_fighting_instruction		
	naval_provisioning		
	shrewd_commerce_practise		
	merchant_adventures		
	improved_foraging		
}			
historical_units = {			
	napoleonic_square		
	napoleonic_lancers		
	flying_battery		
}			
monarch_names = {			
	"Ali #0" = 10		
	"Ahmed #0" = 10		
	"Kumar #0" = 10		
	"Hassan #0" = 10		
	"Khan #0" = 10		
	"Hussain #0" = 10		
	"Mohamed #0" = 10		
	"Abdulla #0" = 10		
	"Yousif #0" = 10		
	"Ebrahim #0" = 10		
	"Janahi #0" = 10		
	"Nair #0" = 10		
	"Saleh #0" = 10		
	"Mahmood #0" = 10		
	"Mathew #0" = 10		
	"Singh #0" = 10		
	"Abbas #0" = 10		
	"Varghese #0" = 10		
	"Khalil #0" = 10		
	"Jassim #0" = 10		
	"Shaikh #0" = 10		
	"Hasan #0" = 10		
	"Radhi #0" = 10		
	"Haji #0" = 10		
	"Khalaf #0" = 10		
	"Alawi #0" = 10		
	"Khalifa #0" = 10		
	"Malik #0" = 10		
	"Abu #0" = 10		
	"Menon #0" = 10		
	"Nasser #0" = 10		
	"Shetty #0" = 10		
	"Alalawi #0" = 10		
	"Kamal #0" = 10		
	"Alshaikh #0" = 10		
	"Marhoon #0" = 10		
	"Habib #0" = 10		
	"Sharma #0" = 10		
	"Raj #0" = 10		
}			
ship_names = {			
	"Coast Guard"		
}			
leader_names = {			
	Ali		
	Ahmed		
	Kumar		
	Hassan		
	Khan		
	Hussain		
	Hussain		
	Mohamed		
	Abdulla		
	Yousif		
	Ebrahim		
	Janahi		
	Salman		
	Nair		
	Saleh		
	Mahmood		
	Mathew		
	Singh		
	Abbas		
	Varghese		
	Khalil		
	Jassim		
	Shaikh		
	Hasan		
	Radhi		
	Haji		
	Khalaf		
	Alawi		
	Khalifa		
	Malik		
	Abu		
	Menon		
	Nasser		
	Shetty		
	Alalawi		
	Kamal		
	Alshaikh		
	Marhoon		
	Habib		
	Sharma		
	Raj		
}			
