# Country Name:	Burkina Faso Rebels	# Tag:	BK8
#			
graphical_culture = 	africangfx		
#			
color = {	18 126 82	}	
historical_ideas = {			
	ecumenism		
	church_attendance_duty		
	divine_supremacy		
	deus_vult		
	revolution_and_counter		
	press_gangs		
	national_conscripts		
	napoleonic_warfare		
	vice_roys		
	vetting		
	bureaucracy		
}			
historical_units = {			
	napoleonic_square		
	napoleonic_lancers		
	flying_battery		
}			
monarch_names = {			
	"Ouedraogo #0" = 10		
	"Sawadogo #0" = 10		
	"Kabore #0" = 10		
	"Traore #0" = 10		
	"Diallo #0" = 10		
	"Ouattara #0" = 10		
	"Dicko #0" = 10		
	"Zongo #0" = 10		
	"Sanou #0" = 10		
	"Savadogo #0" = 10		
	"Compaore #0" = 10		
	"Barry #0" = 10		
	"Some #0" = 10		
	"Ilboudo #0" = 10		
	"Coulibaly #0" = 10		
	"Ouoba #0" = 10		
	"Konate #0" = 10		
	"Belem #0" = 10		
	"Maiga #0" = 10		
	"Yameogo #0" = 10		
	"Sankara #0" = 10		
	"Zoungrana #0" = 10		
	"Lompo #0" = 10		
	"Sana #0" = 10		
	"Tapsoba #0" = 10		
	"Lankoande #0" = 10		
	"Tamboura #0" = 10		
	"Sore #0" = 10		
	"Nana #0" = 10		
	"Hien #0" = 10		
	"Kabre #0" = 10		
	"Tiendrebeogo #0" = 10		
	"Sidibe #0" = 10		
	"Kafando #0" = 10		
	"Bamogo #0" = 10		
	"Dao #0" = 10		
	"Tindano #0" = 10		
	"Dabire #0" = 10		
	"Kindo #0" = 10		
}			
ship_names = {			
	"Coast Guard"		
}			
leader_names = {			
	Ouedraogo		
	Sawadogo		
	Kabore		
	Traore		
	Diallo		
	Ouattara		
	Ouattara		
	Dicko		
	Zongo		
	Sanou		
	Savadogo		
	Compaore		
	Nikiema		
	Barry		
	Some		
	Ilboudo		
	Coulibaly		
	Ouoba		
	Konate		
	Belem		
	Maiga		
	Yameogo		
	Sankara		
	Zoungrana		
	Lompo		
	Sana		
	Tapsoba		
	Lankoande		
	Tamboura		
	Sore		
	Nana		
	Hien		
	Kabre		
	Tiendrebeogo		
	Sidibe		
	Kafando		
	Bamogo		
	Dao		
	Tindano		
	Dabire		
	Kindo		
}			
