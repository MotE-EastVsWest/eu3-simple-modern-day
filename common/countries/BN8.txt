# Country Name:	Beninese Rebels	# Tag:	BN8
#			
graphical_culture = 	africangfx		
#			
color = {	80 143 99	}	
historical_ideas = {			
	ecumenism		
	church_attendance_duty		
	divine_supremacy		
	deus_vult		
	revolution_and_counter		
	press_gangs		
	national_conscripts		
	napoleonic_warfare		
	vice_roys		
	vetting		
	bureaucracy		
}			
historical_units = {			
	napoleonic_square		
	napoleonic_lancers		
	flying_battery		
}			
monarch_names = {			
	"Bio #0" = 10		
	"Orou #0" = 10		
	"Sabi #0" = 10		
	"Amadou #0" = 10		
	"Dossou #0" = 10		
	"Moussa #0" = 10		
	"Adamou #0" = 10		
	"Amoussou #0" = 10		
	"Assogba #0" = 10		
	"Bani #0" = 10		
	"Chabi #0" = 10		
	"Tossou #0" = 10		
	"Sossou #0" = 10		
	"Boni #0" = 10		
	"Houessou #0" = 10		
	"Zannou #0" = 10		
	"Idrissou #0" = 10		
	"Oumarou #0" = 10		
	"Worou #0" = 10		
	"Agossou #0" = 10		
	"Djossou #0" = 10		
	"Adam #0" = 10		
	"Dansou #0" = 10		
	"Kakpo #0" = 10		
	"Abdoulaye #0" = 10		
	"Kouagou #0" = 10		
	"Boukari #0" = 10		
	"Sanni #0" = 10		
	"Salifou #0" = 10		
	"Yacoubou #0" = 10		
	"Sambieni #0" = 10		
	"N'Tcha #0" = 10		
	"Alassane #0" = 10		
	"Issa #0" = 10		
	"Lokossou #0" = 10		
	"Zinsou #0" = 10		
	"Yarou #0" = 10		
	"Dossa #0" = 10		
	"Gounou #0" = 10		
}			
ship_names = {			
	"Coast Guard"		
}			
leader_names = {			
	Bio		
	Orou		
	Sabi		
	Amadou		
	Dossou		
	Moussa		
	Moussa		
	Adamou		
	Amoussou		
	Assogba		
	Bani		
	Chabi		
	Mama		
	Tossou		
	Sossou		
	Boni		
	Houessou		
	Zannou		
	Idrissou		
	Oumarou		
	Worou		
	Agossou		
	Djossou		
	Adam		
	Dansou		
	Kakpo		
	Abdoulaye		
	Kouagou		
	Boukari		
	Sanni		
	Salifou		
	Yacoubou		
	Sambieni		
	N'Tcha		
	Alassane		
	Issa		
	Lokossou		
	Zinsou		
	Yarou		
	Dossa		
	Gounou		
}			
