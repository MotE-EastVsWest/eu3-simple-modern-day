# Country Name:	Bulgaria	# Tag:	BU9
#			
graphical_culture = 	easterngfx		
#			
color = { 116  107  140 }
historical_ideas = {			
	ecumenism		
	church_attendance_duty		
	divine_supremacy		
	deus_vult		
	revolution_and_counter		
	press_gangs		
	national_conscripts		
	napoleonic_warfare		
	vice_roys		
	vetting		
	bureaucracy		
}			
historical_units = {			
	napoleonic_square		
	napoleonic_lancers		
	flying_battery		
}			
monarch_names = {			
	"Ivanov #0" = 10		
	"Ivanova #0" = 10		
	"Georgieva #0" = 10		
	"Georgiev #0" = 10		
	"Dimitrova #0" = 10		
	"Dimitrov #0" = 10		
	"Petrova #0" = 10		
	"Petrov #0" = 10		
	"Nikolova #0" = 10		
	"Nikolov #0" = 10		
	"Stoyanova #0" = 10		
	"Stoyanov #0" = 10		
	"Todorova #0" = 10		
	"Hristov #0" = 10		
	"Todorov #0" = 10		
	"Ilieva #0" = 10		
	"Angelova #0" = 10		
	"Iliev #0" = 10		
	"Angelov #0" = 10		
	"Atanasova #0" = 10		
	"Atanasov #0" = 10		
	"Vasileva #0" = 10		
	"Vasilev #0" = 10		
	"Yordanova #0" = 10		
	"Yordanov #0" = 10		
	"Petkova #0" = 10		
	"Petkov #0" = 10		
	"Marinova #0" = 10		
	"Koleva #0" = 10		
	"Marinov #0" = 10		
	"Kolev #0" = 10		
	"Stefanova #0" = 10		
	"Stefanov #0" = 10		
	"Mehmed #0" = 10		
	"Mihaylova #0" = 10		
	"Mihaylov #0" = 10		
	"Ahmed #0" = 10		
	"Asenov #0" = 10		
	"Asenova #0" = 10		
}			
ship_names = {			
	"Coast Guard"		
}			
leader_names = {			
	Ivanov		
	Ivanova		
	Georgieva		
	Georgiev		
	Dimitrova		
	Dimitrov		
	Dimitrov		
	Petrova		
	Petrov		
	Nikolova		
	Nikolov		
	Stoyanova		
	Hristova		
	Stoyanov		
	Todorova		
	Hristov		
	Todorov		
	Ilieva		
	Angelova		
	Iliev		
	Angelov		
	Atanasova		
	Atanasov		
	Vasileva		
	Vasilev		
	Yordanova		
	Yordanov		
	Petkova		
	Petkov		
	Marinova		
	Koleva		
	Marinov		
	Kolev		
	Stefanova		
	Stefanov		
	Mehmed		
	Mihaylova		
	Mihaylov		
	Ahmed		
	Asenov		
	Asenova		
}			
