# Country Name:	Belarus	# Tag:	BY9
#			
graphical_culture = 	easterngfx		
#			
color = { 112  157  56 }
historical_ideas = {			
	ecumenism		
	church_attendance_duty		
	divine_supremacy		
	deus_vult		
	revolution_and_counter		
	press_gangs		
	national_conscripts		
	napoleonic_warfare		
	vice_roys		
	vetting		
	bureaucracy		
}			
historical_units = {			
	napoleonic_square		
	napoleonic_lancers		
	flying_battery		
}			
monarch_names = {			
	"Ivanov #0" = 10		
	"Ivanova #0" = 10		
	"Novik #0" = 10		
	"Zhuk #0" = 10		
	"Moroz #0" = 10		
	"Novikova #0" = 10		
	"Kotova #0" = 10		
	"Petrov #0" = 10		
	"Novikov #0" = 10		
	"Volkova #0" = 10		
	"Kravchenko #0" = 10		
	"Petrova #0" = 10		
	"Makarevich #0" = 10		
	"Kovalchuk #0" = 10		
	"Karpovich #0" = 10		
	"Pashkevich #0" = 10		
	"Kozlov #0" = 10		
	"Volkov #0" = 10		
	"Shevchenko #0" = 10		
	"Kotov #0" = 10		
	"Morozova #0" = 10		
	"Klimovich #0" = 10		
	"Morozov #0" = 10		
	"Smirnova #0" = 10		
	"Smirnov #0" = 10		
	"Kozlova #0" = 10		
	"Romanova #0" = 10		
	"Bondarenko #0" = 10		
	"Stankevich #0" = 10		
	"Tarasevich #0" = 10		
	"Mironova #0" = 10		
	"Marchenko #0" = 10		
	"Zaytsev #0" = 10		
	"Lukashevich #0" = 10		
	"Zaytseva #0" = 10		
	"Kot #0" = 10		
	"Savchenko #0" = 10		
	"Kovalyova #0" = 10		
	"Bogdanovich #0" = 10		
}			
ship_names = {			
	"Coast Guard"		
}			
leader_names = {			
	Ivanov		
	Ivanova		
	Novik		
	Zhuk		
	Moroz		
	Novikova		
	Novikova		
	Kotova		
	Petrov		
	Novikov		
	Volkova		
	Kravchenko		
	Kovalenko		
	Petrova		
	Makarevich		
	Kovalchuk		
	Karpovich		
	Pashkevich		
	Kozlov		
	Volkov		
	Shevchenko		
	Kotov		
	Morozova		
	Klimovich		
	Morozov		
	Smirnova		
	Smirnov		
	Kozlova		
	Romanova		
	Bondarenko		
	Stankevich		
	Tarasevich		
	Mironova		
	Marchenko		
	Zaytsev		
	Lukashevich		
	Zaytseva		
	Kot		
	Savchenko		
	Kovalyova		
	Bogdanovich		
}			
