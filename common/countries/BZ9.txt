# Country Name:	Belize	# Tag:	BZ9
#			
graphical_culture = 	africangfx		
#			
color = {	219 177 107	}	
historical_ideas = {			
	press_gangs		
	grand_navy		
	national_trade_policy		
	superior_seamanship		
	naval_glory		
	excellent_shipwrights		
	naval_fighting_instruction		
	naval_provisioning		
	shrewd_commerce_practise		
	merchant_adventures		
	improved_foraging		
}			
historical_units = {			
	napoleonic_square		
	napoleonic_lancers		
	flying_battery		
}			
monarch_names = {			
	"Martinez #0" = 10		
	"Garcia #0" = 10		
	"Castillo #0" = 10		
	"Lopez #0" = 10		
	"Williams #0" = 10		
	"Perez #0" = 10		
	"Cal #0" = 10		
	"Hernandez #0" = 10		
	"Chan #0" = 10		
	"Gonzalez #0" = 10		
	"Flowers #0" = 10		
	"Vasquez #0" = 10		
	"Reyes #0" = 10		
	"Rodriguez #0" = 10		
	"Flores #0" = 10		
	"Smith #0" = 10		
	"Choc #0" = 10		
	"Sanchez #0" = 10		
	"Ramirez #0" = 10		
	"Pop #0" = 10		
	"Coc #0" = 10		
	"Cruz #0" = 10		
	"Jones #0" = 10		
	"Novelo #0" = 10		
	"Mendez #0" = 10		
	"Guerra #0" = 10		
	"Tillett #0" = 10		
	"Torres #0" = 10		
	"Sho #0" = 10		
	"Usher #0" = 10		
	"Gomez #0" = 10		
	"Patt #0" = 10		
	"Nunez #0" = 10		
	"Romero #0" = 10		
	"Jimenez #0" = 10		
	"Tun #0" = 10		
	"Garbutt #0" = 10		
	"Gillett #0" = 10		
	"Mai #0" = 10		
}			
ship_names = {			
	"Coast Guard"		
}			
leader_names = {			
	Martinez		
	Garcia		
	Castillo		
	Lopez		
	Williams		
	Perez		
	Perez		
	Cal		
	Hernandez		
	Chan		
	Gonzalez		
	Flowers		
	Young		
	Vasquez		
	Reyes		
	Rodriguez		
	Flores		
	Smith		
	Choc		
	Sanchez		
	Ramirez		
	Pop		
	Coc		
	Cruz		
	Jones		
	Novelo		
	Mendez		
	Guerra		
	Tillett		
	Torres		
	Sho		
	Usher		
	Gomez		
	Patt		
	Nunez		
	Romero		
	Jimenez		
	Tun		
	Garbutt		
	Gillett		
	Mai		
}			
