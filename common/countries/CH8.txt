# Country Name:	Swiss Rebels	# Tag:	CH8
#			
graphical_culture = 	latingfx		
#			
color = {	123 92 78	}	
historical_ideas = {			
	vetting		
	national_bank		
	national_trade_policy		
	smithian_economics		
	shrewd_commerce_practise		
	scientific_revolution		
	sea_hawks		
	naval_provisioning		
	regimental_system		
	napoleonic_warfare		
	merchant_adventures		
}			
historical_units = {			
	napoleonic_square		
	napoleonic_lancers		
	flying_battery		
}			
monarch_names = {			
	"Muller #0" = 10		
	"Meier #0" = 10		
	"Schmid #0" = 10		
	"Keller #0" = 10		
	"Weber #0" = 10		
	"Schneider #0" = 10		
	"Huber #0" = 10		
	"Meyer #0" = 10		
	"Steiner #0" = 10		
	"Fischer #0" = 10		
	"Baumann #0" = 10		
	"Brunner #0" = 10		
	"Gerber #0" = 10		
	"Widmer #0" = 10		
	"Zimmermann #0" = 10		
	"Moser #0" = 10		
	"Graf #0" = 10		
	"Wyss #0" = 10		
	"Roth #0" = 10		
	"Suter #0" = 10		
	"Baumgartner #0" = 10		
	"Bachmann #0" = 10		
	"Studer #0" = 10		
	"Bucher #0" = 10		
	"Berger #0" = 10		
	"Kaufmann #0" = 10		
	"Kunz #0" = 10		
	"Hofer #0" = 10		
	"Buhler #0" = 10		
	"Luthi #0" = 10		
	"Lehmann #0" = 10		
	"Marti #0" = 10		
	"Frey #0" = 10		
	"Christen #0" = 10		
	"Koch #0" = 10		
	"Egli #0" = 10		
	"Favre #0" = 10		
	"Arnold #0" = 10		
	"Pfister #0" = 10		
}			
ship_names = {			
	"Coast Guard"		
}			
leader_names = {			
	Muller		
	Meier		
	Schmid		
	Keller		
	Weber		
	Schneider		
	Schneider		
	Huber		
	Meyer		
	Steiner		
	Fischer		
	Baumann		
	Frei		
	Brunner		
	Gerber		
	Widmer		
	Zimmermann		
	Moser		
	Graf		
	Wyss		
	Roth		
	Suter		
	Baumgartner		
	Bachmann		
	Studer		
	Bucher		
	Berger		
	Kaufmann		
	Kunz		
	Hofer		
	Buhler		
	Luthi		
	Lehmann		
	Marti		
	Frey		
	Christen		
	Koch		
	Egli		
	Favre		
	Arnold		
	Pfister		
}			
