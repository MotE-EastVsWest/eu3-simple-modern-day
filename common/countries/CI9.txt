# Country Name:	Chile	# Tag:	CI9
#			
graphical_culture = 	latingfx		
#			
color = { 236  159  89 }
historical_ideas = {			
	press_gangs		
	grand_navy		
	sea_hawks		
	superior_seamanship		
	naval_glory		
	excellent_shipwrights		
	naval_fighting_instruction		
	naval_provisioning		
	shrewd_commerce_practise		
	merchant_adventures		
	improved_foraging		
}			
historical_units = {			
	napoleonic_square		
	napoleonic_lancers		
	flying_battery		
}			
monarch_names = {			
	"Gonzalez #0" = 10		
	"Munoz #0" = 10		
	"Rojas #0" = 10		
	"Diaz #0" = 10		
	"Perez #0" = 10		
	"Soto #0" = 10		
	"Contreras #0" = 10		
	"Silva #0" = 10		
	"Martinez #0" = 10		
	"Sepulveda #0" = 10		
	"Morales #0" = 10		
	"Lopez #0" = 10		
	"Araya #0" = 10		
	"Fuentes #0" = 10		
	"Hernandez #0" = 10		
	"Torres #0" = 10		
	"Espinoza #0" = 10		
	"Flores #0" = 10		
	"Castillo #0" = 10		
	"Valenzuela #0" = 10		
	"Ramirez #0" = 10		
	"Reyes #0" = 10		
	"Gutierrez #0" = 10		
	"Castro #0" = 10		
	"Vargas #0" = 10		
	"Alvarez #0" = 10		
	"Vasquez #0" = 10		
	"Tapia #0" = 10		
	"Fernandez #0" = 10		
	"Sanchez #0" = 10		
	"Cortes #0" = 10		
	"Gomez #0" = 10		
	"Herrera #0" = 10		
	"Carrasco #0" = 10		
	"Nunez #0" = 10		
	"Miranda #0" = 10		
	"Jara #0" = 10		
	"Vergara #0" = 10		
	"Rivera #0" = 10		
}			
ship_names = {			
	"Coast Guard"		
}			
leader_names = {			
	Gonzalez		
	Munoz		
	Rojas		
	Diaz		
	Perez		
	Soto		
	Soto		
	Contreras		
	Silva		
	Martinez		
	Sepulveda		
	Morales		
	Rodriguez		
	Lopez		
	Araya		
	Fuentes		
	Hernandez		
	Torres		
	Espinoza		
	Flores		
	Castillo		
	Valenzuela		
	Ramirez		
	Reyes		
	Gutierrez		
	Castro		
	Vargas		
	Alvarez		
	Vasquez		
	Tapia		
	Fernandez		
	Sanchez		
	Cortes		
	Gomez		
	Herrera		
	Carrasco		
	Nunez		
	Miranda		
	Jara		
	Vergara		
	Rivera		
}			
