# Country Name:	Cook Islands	# Tag:	CK9
#			
graphical_culture = 	northamericagfx		
#			
color = {	22 184 217	}	
historical_ideas = {			
	press_gangs		
	grand_navy		
	sea_hawks		
	superior_seamanship		
	naval_glory		
	excellent_shipwrights		
	naval_fighting_instruction		
	naval_provisioning		
	shrewd_commerce_practise		
	merchant_adventures		
	improved_foraging		
}			
historical_units = {			
	napoleonic_square		
	napoleonic_lancers		
	flying_battery		
}			
monarch_names = {			
	"Marsters #0" = 10		
	"Henry #0" = 10		
	"George #0" = 10		
	"William #0" = 10		
	"Williams #0" = 10		
	"Samuel #0" = 10		
	"Heather #0" = 10		
	"Daniel #0" = 10		
	"Napa #0" = 10		
	"Nicholas #0" = 10		
	"Wichman #0" = 10		
	"Mataio #0" = 10		
	"Matapo #0" = 10		
	"Taia #0" = 10		
	"John #0" = 10		
	"Charlie #0" = 10		
	"Ioane #0" = 10		
	"Taripo #0" = 10		
	"Tutai #0" = 10		
	"Mataiti #0" = 10		
	"Maoate #0" = 10		
	"Joseph #0" = 10		
	"Strickland #0" = 10		
	"Short #0" = 10		
	"Nooroa #0" = 10		
	"Tuara #0" = 10		
	"Toka #0" = 10		
	"Ngametua #0" = 10		
	"Nicholls #0" = 10		
	"Bishop #0" = 10		
	"Tou #0" = 10		
	"Manuela #0" = 10		
	"Samuela #0" = 10		
	"Tanga #0" = 10		
	"Brown #0" = 10		
	"Solomona #0" = 10		
	"Ruatoe #0" = 10		
	"Tupou #0" = 10		
	"Koteka #0" = 10		
}			
ship_names = {			
	"Coast Guard"		
}			
leader_names = {			
	Marsters		
	Henry		
	George		
	William		
	Williams		
	Samuel		
	Samuel		
	Heather		
	Daniel		
	Napa		
	Nicholas		
	Wichman		
	Hosking		
	Mataio		
	Matapo		
	Taia		
	John		
	Charlie		
	Ioane		
	Taripo		
	Tutai		
	Mataiti		
	Maoate		
	Joseph		
	Strickland		
	Short		
	Nooroa		
	Tuara		
	Toka		
	Ngametua		
	Nicholls		
	Bishop		
	Tou		
	Manuela		
	Samuela		
	Tanga		
	Brown		
	Solomona		
	Ruatoe		
	Tupou		
	Koteka		
}			
