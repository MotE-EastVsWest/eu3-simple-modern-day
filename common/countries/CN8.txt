# Country Name:	Central African Republic Rebels	# Tag:	CN8
#			
graphical_culture = 	africangfx		
#			
color = {	185 39 77	}	
historical_ideas = {			
	ecumenism		
	church_attendance_duty		
	divine_supremacy		
	deus_vult		
	revolution_and_counter		
	press_gangs		
	national_conscripts		
	napoleonic_warfare		
	vice_roys		
	vetting		
	bureaucracy		
}			
historical_units = {			
	napoleonic_square		
	napoleonic_lancers		
	flying_battery		
}			
monarch_names = {			
	"Pierre #0" = 10		
	"Nestor #0" = 10		
	"Bichara #0" = 10		
	"Honore #0" = 10		
	"Younous #0" = 10		
	"Jean #0" = 10		
	"Christophe #0" = 10		
	"Elysee #0" = 10		
	"Jeremie #0" = 10		
	"Fadoul #0" = 10		
	"Malloum #0" = 10		
	"Prosper #0" = 10		
	"Bouba #0" = 10		
	"Haggar #0" = 10		
	"Joseph #0" = 10		
	"Parfait #0" = 10		
	"Florence #0" = 10		
	"Robert #0" = 10		
	"Bachar #0" = 10		
	"Cherif #0" = 10		
	"Patrice #0" = 10		
	"Paul #0" = 10		
	"Desire #0" = 10		
	"Arnaud #0" = 10		
	"Khamis #0" = 10		
	"Beatrice #0" = 10		
	"Hassaballah #0" = 10		
	"Tidjani #0" = 10		
	"Annour #0" = 10		
	"Gedeon #0" = 10		
	"Appolinaire #0" = 10		
	"Amine #0" = 10		
	"Salomon #0" = 10		
	"Thomas #0" = 10		
	"Jonas #0" = 10		
	"Gloria #0" = 10		
	"Serge #0" = 10		
	"Herve #0" = 10		
	"Carine #0" = 10		
}			
ship_names = {			
	"Coast Guard"		
}			
leader_names = {			
	Pierre		
	Nestor		
	Bichara		
	Honore		
	Younous		
	Jean		
	Jean		
	Christophe		
	Elysee		
	Jeremie		
	Fadoul		
	Malloum		
	Roland		
	Prosper		
	Bouba		
	Haggar		
	Joseph		
	Parfait		
	Florence		
	Robert		
	Bachar		
	Cherif		
	Patrice		
	Paul		
	Desire		
	Arnaud		
	Khamis		
	Beatrice		
	Hassaballah		
	Tidjani		
	Annour		
	Gedeon		
	Appolinaire		
	Amine		
	Salomon		
	Thomas		
	Jonas		
	Gloria		
	Serge		
	Herve		
	Carine		
}			
