# Country Name:	Colombia	# Tag:	CO9
#			
graphical_culture = 	southamericagfx		
#			
color = { 89  140  176 }
historical_ideas = {			
	ecumenism		
	church_attendance_duty		
	divine_supremacy		
	deus_vult		
	revolution_and_counter		
	press_gangs		
	national_conscripts		
	napoleonic_warfare		
	vice_roys		
	vetting		
	bureaucracy		
}			
historical_units = {			
	napoleonic_square		
	napoleonic_lancers		
	flying_battery		
}			
monarch_names = {			
	"Rodriguez #0" = 10		
	"Martinez #0" = 10		
	"Garcia #0" = 10		
	"Gomez #0" = 10		
	"Lopez #0" = 10		
	"Gonzalez #0" = 10		
	"Hernandez #0" = 10		
	"Sanchez #0" = 10		
	"Perez #0" = 10		
	"Ramirez #0" = 10		
	"Diaz #0" = 10		
	"Munoz #0" = 10		
	"Rojas #0" = 10		
	"Moreno #0" = 10		
	"Vargas #0" = 10		
	"Ortiz #0" = 10		
	"Jimenez #0" = 10		
	"Castro #0" = 10		
	"Gutierrez #0" = 10		
	"Alvarez #0" = 10		
	"Valencia #0" = 10		
	"Ruiz #0" = 10		
	"Suarez #0" = 10		
	"Herrera #0" = 10		
	"Romero #0" = 10		
	"Quintero #0" = 10		
	"Morales #0" = 10		
	"Giraldo #0" = 10		
	"Rivera #0" = 10		
	"Arias #0" = 10		
	"Florez #0" = 10		
	"Marin #0" = 10		
	"Castillo #0" = 10		
	"Mejia #0" = 10		
	"Mosquera #0" = 10		
	"Osorio #0" = 10		
	"Cardenas #0" = 10		
	"Cardona #0" = 10		
	"Pena #0" = 10		
}			
ship_names = {			
	"Coast Guard"		
}			
leader_names = {			
	Rodriguez		
	Martinez		
	Garcia		
	Gomez		
	Lopez		
	Gonzalez		
	Gonzalez		
	Hernandez		
	Sanchez		
	Perez		
	Ramirez		
	Diaz		
	Torres		
	Munoz		
	Rojas		
	Moreno		
	Vargas		
	Ortiz		
	Jimenez		
	Castro		
	Gutierrez		
	Alvarez		
	Valencia		
	Ruiz		
	Suarez		
	Herrera		
	Romero		
	Quintero		
	Morales		
	Giraldo		
	Rivera		
	Arias		
	Florez		
	Marin		
	Castillo		
	Mejia		
	Mosquera		
	Osorio		
	Cardenas		
	Cardona		
	Pena		
}			
