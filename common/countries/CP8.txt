# Country Name:	Chiapan Rebels	# Tag:	CP8
#			
graphical_culture = 	southamericagfx		
#			
color = {	128 88 90	}	
historical_ideas = {			
}			
historical_units = {			
	napoleonic_square		
	napoleonic_lancers		
	flying_battery		
}			
monarch_names = {			
	"Lopez #0" = 10		
	"Perez #0" = 10		
	"Hernandez #0" = 10		
	"Gomez #0" = 10		
	"Sanchez #0" = 10		
	"Diaz #0" = 10		
	"Vazquez #0" = 10		
	"Cruz #0" = 10		
	"Jiminez #0" = 10		
	"Morales #0" = 10		
	"Mendez #0" = 10		
	"Gonzalez #0" = 10		
	"Ruiz #0" = 10		
	"Martinez #0" = 10		
	"Aguilar #0" = 10		
	"Gutierrez #0" = 10		
	"Ramirez #0" = 10		
	"Santiz #0" = 10		
	"Rodriguez #0" = 10		
	"Velazquez #0" = 10		
	"de La Cruz #0" = 10		
	"Moreno #0" = 10		
	"Guzman #0" = 10		
	"Roblero #0" = 10		
	"Alvarez #0" = 10		
	"Dominguez #0" = 10		
	"Velasco #0" = 10		
	"Ramos #0" = 10		
	"Espinosa #0" = 10		
	"Mendoza #0" = 10		
	"Torres #0" = 10		
	"Escobar #0" = 10		
	"Reyes #0" = 10		
	"Arcos #0" = 10		
	"Juarez #0" = 10		
	"Flores #0" = 10		
	"Solis #0" = 10		
	"Guillen #0" = 10		
	"Montejo #0" = 10		
}			
ship_names = {			
	"Coast Guard"		
}			
leader_names			
	Lopez		
	Perez		
	Hernandez		
	Gomez		
	Sanchez		
	Diaz		
	Diaz		
	Vazquez		
	Cruz		
	Jiminez		
	Morales		
	Mendez		
	Garcia		
	Gonzalez		
	Ruiz		
	Martinez		
	Aguilar		
	Gutierrez		
	Ramirez		
	Santiz		
	Rodriguez		
	Velazquez		
	de La Cruz		
	Moreno		
	Guzman		
	Roblero		
	Alvarez		
	Dominguez		
	Velasco		
	Ramos		
	Espinosa		
	Mendoza		
	Torres		
	Escobar		
	Reyes		
	Arcos		
	Juarez		
	Flores		
	Solis		
	Guillen		
	Montejo		
}			
