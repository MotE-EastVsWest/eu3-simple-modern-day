# Country Name:	Costa Rican Rebels	# Tag:	CR8
#			
graphical_culture = 	latingfx		
#			
color = {	164 168 149	}	
historical_ideas = {			
	vetting		
	national_bank		
	national_trade_policy		
	smithian_economics		
	shrewd_commerce_practise		
	scientific_revolution		
	sea_hawks		
	naval_provisioning		
	regimental_system		
	napoleonic_warfare		
	merchant_adventures		
}			
historical_units = {			
	napoleonic_square		
	napoleonic_lancers		
	flying_battery		
}			
monarch_names = {			
	"Rodriguez #0" = 10		
	"Vargas #0" = 10		
	"Jimenez #0" = 10		
	"Mora #0" = 10		
	"Rojas #0" = 10		
	"Gonzalez #0" = 10		
	"Sanchez #0" = 10		
	"Hernandez #0" = 10		
	"Ramirez #0" = 10		
	"Castro #0" = 10		
	"Lopez #0" = 10		
	"Solano #0" = 10		
	"Alvarado #0" = 10		
	"Chaves #0" = 10		
	"Perez #0" = 10		
	"Morales #0" = 10		
	"Campos #0" = 10		
	"Quesada #0" = 10		
	"Gomez #0" = 10		
	"Arias #0" = 10		
	"Zuniga #0" = 10		
	"Quiros #0" = 10		
	"Fernandez #0" = 10		
	"Salazar #0" = 10		
	"Villalobos #0" = 10		
	"Brenes #0" = 10		
	"Gutierrez #0" = 10		
	"Garcia #0" = 10		
	"Alfaro #0" = 10		
	"Vega #0" = 10		
	"Aguilar #0" = 10		
	"Calderon #0" = 10		
	"Valverde #0" = 10		
	"Chavarria #0" = 10		
	"Alvarez #0" = 10		
	"Castillo #0" = 10		
	"Salas #0" = 10		
	"Espinoza #0" = 10		
	"Martinez #0" = 10		
}			
ship_names = {			
	"Coast Guard"		
}			
leader_names = {			
	Rodriguez		
	Vargas		
	Jimenez		
	Mora		
	Rojas		
	Gonzalez		
	Gonzalez		
	Sanchez		
	Hernandez		
	Ramirez		
	Castro		
	Lopez		
	Araya		
	Solano		
	Alvarado		
	Chaves		
	Perez		
	Morales		
	Campos		
	Quesada		
	Gomez		
	Arias		
	Zuniga		
	Quiros		
	Fernandez		
	Salazar		
	Villalobos		
	Brenes		
	Gutierrez		
	Garcia		
	Alfaro		
	Vega		
	Aguilar		
	Calderon		
	Valverde		
	Chavarria		
	Alvarez		
	Castillo		
	Salas		
	Espinoza		
	Martinez		
}			
