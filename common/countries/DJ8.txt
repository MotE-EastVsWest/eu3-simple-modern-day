# Country Name:	Djibouti Rebels	# Tag:	DJ8
#			
graphical_culture = 	africangfx		
#			
color = {	52 81 192	}	
historical_ideas = {			
	press_gangs		
	grand_navy		
	sea_hawks		
	superior_seamanship		
	naval_glory		
	excellent_shipwrights		
	naval_fighting_instruction		
	naval_provisioning		
	shrewd_commerce_practise		
	merchant_adventures		
	improved_foraging		
}			
historical_units = {			
	napoleonic_square		
	napoleonic_lancers		
	flying_battery		
}			
monarch_names = {			
	"Mohamed #0" = 10		
	"Ali #0" = 10		
	"Ahmed #0" = 10		
	"Hassan #0" = 10		
	"Abdi #0" = 10		
	"Omar #0" = 10		
	"Abdillahi #0" = 10		
	"Moussa #0" = 10		
	"Ibrahim #0" = 10		
	"Houssein #0" = 10		
	"Aden #0" = 10		
	"Farah #0" = 10		
	"Elmi #0" = 10		
	"Youssouf #0" = 10		
	"Mahamoud #0" = 10		
	"Osman #0" = 10		
	"Robleh #0" = 10		
	"Said #0" = 10		
	"Daher #0" = 10		
	"Ismael #0" = 10		
	"Abdallah #0" = 10		
	"Bouh #0" = 10		
	"Ismail #0" = 10		
	"Houmed #0" = 10		
	"Nour #0" = 10		
	"Awaleh #0" = 10		
	"Darar #0" = 10		
	"Guedi #0" = 10		
	"Waberi #0" = 10		
	"Idriss #0" = 10		
	"Moumin #0" = 10		
	"Guelleh #0" = 10		
	"Kamil #0" = 10		
	"Wais #0" = 10		
	"Daoud #0" = 10		
	"Hamadou #0" = 10		
	"Doualeh #0" = 10		
	"Abdoulkader #0" = 10		
	"Mouhoumed #0" = 10		
}			
ship_names = {			
	"Coast Guard"		
}			
leader_names = {			
	Mohamed		
	Ali		
	Ahmed		
	Hassan		
	Abdi		
	Omar		
	Omar		
	Abdillahi		
	Moussa		
	Ibrahim		
	Houssein		
	Aden		
	Djama		
	Farah		
	Elmi		
	Youssouf		
	Mahamoud		
	Osman		
	Robleh		
	Said		
	Daher		
	Ismael		
	Abdallah		
	Bouh		
	Ismail		
	Houmed		
	Nour		
	Awaleh		
	Darar		
	Guedi		
	Waberi		
	Idriss		
	Moumin		
	Guelleh		
	Kamil		
	Wais		
	Daoud		
	Hamadou		
	Doualeh		
	Abdoulkader		
	Mouhoumed		
}			
