# Country Name:	Denmark	# Tag:	DK9
#			
graphical_culture = 	latingfx		
#			
color = { 190  70  70 }
historical_ideas = {			
	vetting		
	national_bank		
	national_trade_policy		
	smithian_economics		
	shrewd_commerce_practise		
	scientific_revolution		
	sea_hawks		
	naval_provisioning		
	regimental_system		
	napoleonic_warfare		
	merchant_adventures		
}			
historical_units = {			
	napoleonic_square		
	napoleonic_lancers		
	flying_battery		
}			
monarch_names = {			
	"Jensen #0" = 10		
	"Nielsen #0" = 10		
	"Hansen #0" = 10		
	"Pedersen #0" = 10		
	"Andersen #0" = 10		
	"Christensen #0" = 10		
	"Larsen #0" = 10		
	"Sorensen #0" = 10		
	"Rasmussen #0" = 10		
	"Petersen #0" = 10		
	"Jorgensen #0" = 10		
	"Kristensen #0" = 10		
	"Olsen #0" = 10		
	"Thomsen #0" = 10		
	"Christiansen #0" = 10		
	"Poulsen #0" = 10		
	"Johansen #0" = 10		
	"Knudsen #0" = 10		
	"Mortensen #0" = 10		
	"Moller #0" = 10		
	"Jakobsen #0" = 10		
	"Jacobsen #0" = 10		
	"Olesen #0" = 10		
	"Mikkelsen #0" = 10		
	"Lund #0" = 10		
	"Frederiksen #0" = 10		
	"Holm #0" = 10		
	"Laursen #0" = 10		
	"Henriksen #0" = 10		
	"Schmidt #0" = 10		
	"Eriksen #0" = 10		
	"Clausen #0" = 10		
	"Simonsen #0" = 10		
	"Kristiansen #0" = 10		
	"Svendsen #0" = 10		
	"Andreasen #0" = 10		
	"Iversen #0" = 10		
	"Jeppesen #0" = 10		
	"Vestergaard #0" = 10		
}			
ship_names = {			
	"Coast Guard"		
}			
leader_names = {			
	Jensen		
	Nielsen		
	Hansen		
	Pedersen		
	Andersen		
	Christensen		
	Christensen		
	Larsen		
	Sorensen		
	Rasmussen		
	Petersen		
	Jorgensen		
	Madsen		
	Kristensen		
	Olsen		
	Thomsen		
	Christiansen		
	Poulsen		
	Johansen		
	Knudsen		
	Mortensen		
	Moller		
	Jakobsen		
	Jacobsen		
	Olesen		
	Mikkelsen		
	Lund		
	Frederiksen		
	Holm		
	Laursen		
	Henriksen		
	Schmidt		
	Eriksen		
	Clausen		
	Simonsen		
	Kristiansen		
	Svendsen		
	Andreasen		
	Iversen		
	Jeppesen		
	Vestergaard		
}			
