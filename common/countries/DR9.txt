# Country Name:	Dominican Republic	# Tag:	DR9
#			
graphical_culture = 	southamericagfx		
#			
color = {	72 149 120	}	
historical_ideas = {			
	ecumenism		
	church_attendance_duty		
	divine_supremacy		
	deus_vult		
	revolution_and_counter		
	press_gangs		
	national_conscripts		
	napoleonic_warfare		
	vice_roys		
	vetting		
	bureaucracy		
}			
historical_units = {			
	napoleonic_square		
	napoleonic_lancers		
	flying_battery		
}			
monarch_names = {			
	"Rodriguez #0" = 10		
	"Perez #0" = 10		
	"Martinez #0" = 10		
	"Garcia #0" = 10		
	"Sanchez #0" = 10		
	"Ramirez #0" = 10		
	"Diaz #0" = 10		
	"Gonzalez #0" = 10		
	"Reyes #0" = 10		
	"Hernandez #0" = 10		
	"Jimenez #0" = 10		
	"Santana #0" = 10		
	"de La Cruz #0" = 10		
	"Pena #0" = 10		
	"Gomez #0" = 10		
	"Feliz #0" = 10		
	"Guzman #0" = 10		
	"Mejia #0" = 10		
	"Fernandez #0" = 10		
	"Lopez #0" = 10		
	"Cruz #0" = 10		
	"de Los Santos #0" = 10		
	"Baez #0" = 10		
	"Arias #0" = 10		
	"Abreu #0" = 10		
	"Nunez #0" = 10		
	"Matos #0" = 10		
	"Vasquez #0" = 10		
	"Mendez #0" = 10		
	"Medina #0" = 10		
	"Polanco #0" = 10		
	"Valdez #0" = 10		
	"Vargas #0" = 10		
	"Santos #0" = 10		
	"Mateo #0" = 10		
	"Ortiz #0" = 10		
	"de La Rosa #0" = 10		
	"Almonte #0" = 10		
	"Cabrera #0" = 10		
}			
ship_names = {			
	"Coast Guard"		
}			
leader_names = {			
	Rodriguez		
	Perez		
	Martinez		
	Garcia		
	Sanchez		
	Ramirez		
	Ramirez		
	Diaz		
	Gonzalez		
	Reyes		
	Hernandez		
	Jimenez		
	Castillo		
	Santana		
	de La Cruz		
	Pena		
	Gomez		
	Feliz		
	Guzman		
	Mejia		
	Fernandez		
	Lopez		
	Cruz		
	de Los Santos		
	Baez		
	Arias		
	Abreu		
	Nunez		
	Matos		
	Vasquez		
	Mendez		
	Medina		
	Polanco		
	Valdez		
	Vargas		
	Santos		
	Mateo		
	Ortiz		
	de La Rosa		
	Almonte		
	Cabrera		
}			
