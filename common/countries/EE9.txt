# Country Name:	Estonia	# Tag:	EE9
#			
graphical_culture = 	latingfx		
#			
color = {	212 52 19	}	
historical_ideas = {			
	grand_army		
	glorious_arms		
	merchant_adventures		
	scientific_revolution		
	national_bank		
	bill_of_rights		
	smithian_economics		
	liberty_egalite_fraternity		
	patron_of_art		
	humanist_tolerance		
	cabinet		
}			
historical_units = {			
	napoleonic_square		
	napoleonic_lancers		
	flying_battery		
}			
monarch_names = {			
	"Tamm #0" = 10		
	"Saar #0" = 10		
	"Sepp #0" = 10		
	"Kask #0" = 10		
	"Magi #0" = 10		
	"Kukk #0" = 10		
	"Rebane #0" = 10		
	"Koppel #0" = 10		
	"Karu #0" = 10		
	"Ilves #0" = 10		
	"Lepik #0" = 10		
	"Ivanov #0" = 10		
	"Kuusk #0" = 10		
	"Kaasik #0" = 10		
	"Luik #0" = 10		
	"Raudsepp #0" = 10		
	"Vaher #0" = 10		
	"Kallas #0" = 10		
	"Lepp #0" = 10		
	"Laur #0" = 10		
	"Parn #0" = 10		
	"Kuusik #0" = 10		
	"Kivi #0" = 10		
	"Ots #0" = 10		
	"Liiv #0" = 10		
	"Peterson #0" = 10		
	"Teder #0" = 10		
	"Mets #0" = 10		
	"Põder #0" = 10		
	"Lõhmus #0" = 10		
	"Kull #0" = 10		
	"Kutt #0" = 10		
	"Puusepp #0" = 10		
	"Kangur #0" = 10		
	"Jõgi #0" = 10		
	"Jakobson #0" = 10		
	"Rand #0" = 10		
	"Saks #0" = 10		
	"Toom #0" = 10		
}			
ship_names = {			
	"Coast Guard"		
}			
leader_names = {			
	Tamm		
	Saar		
	Sepp		
	Kask		
	Magi		
	Kukk		
	Kukk		
	Rebane		
	Koppel		
	Karu		
	Ilves		
	Lepik		
	Oja		
	Ivanov		
	Kuusk		
	Kaasik		
	Luik		
	Raudsepp		
	Vaher		
	Kallas		
	Lepp		
	Laur		
	Parn		
	Kuusik		
	Kivi		
	Ots		
	Liiv		
	Peterson		
	Teder		
	Mets		
	Põder		
	Lõhmus		
	Kull		
	Kutt		
	Puusepp		
	Kangur		
	Jõgi		
	Jakobson		
	Rand		
	Saks		
	Toom		
}			
