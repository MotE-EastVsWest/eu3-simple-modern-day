# Country Name:	Egypt	# Tag:	EG9
#			
graphical_culture = 	muslimgfx		
#			
color = { 188  166  93 } 	
historical_ideas = {			
	ecumenism		
	church_attendance_duty		
	divine_supremacy		
	deus_vult		
	revolution_and_counter		
	press_gangs		
	national_conscripts		
	napoleonic_warfare		
	vice_roys		
	vetting		
	bureaucracy		
}			
historical_units = {			
	napoleonic_square		
	napoleonic_lancers		
	flying_battery		
}			
monarch_names = {			
	"Mohamed #0" = 10		
	"Ahmed #0" = 10		
	"Ali #0" = 10		
	"Hassan #0" = 10		
	"Mahmoud #0" = 10		
	"Ibrahim #0" = 10		
	"Salah #0" = 10		
	"Mostafa #0" = 10		
	"Adel #0" = 10		
	"Gamal #0" = 10		
	"Saad #0" = 10		
	"Samir #0" = 10		
	"Omar #0" = 10		
	"Hussein #0" = 10		
	"Kamal #0" = 10		
	"Magdy #0" = 10		
	"Salem #0" = 10		
	"Saleh #0" = 10		
	"Ramadan #0" = 10		
	"Hamdy #0" = 10		
	"Elsayed #0" = 10		
	"Khaled #0" = 10		
	"Saeed #0" = 10		
	"Ashraf #0" = 10		
	"Yousef #0" = 10		
	"Fathy #0" = 10		
	"Said #0" = 10		
	"Soliman #0" = 10		
	"Mansour #0" = 10		
	"Kamel #0" = 10		
	"Taha #0" = 10		
	"Mustafa #0" = 10		
	"Abdo #0" = 10		
	"Khalil #0" = 10		
	"Ismail #0" = 10		
	"Nabil #0" = 10		
	"Samy #0" = 10		
	"Ragab #0" = 10		
	"Fouad #0" = 10		
}			
ship_names = {			
	"Coast Guard"		
}			
leader_names = {			
	Mohamed		
	Ahmed		
	Ali		
	Hassan		
	Mahmoud		
	Ibrahim		
	Ibrahim		
	Salah		
	Mostafa		
	Adel		
	Gamal		
	Saad		
	Sayed		
	Samir		
	Omar		
	Hussein		
	Kamal		
	Magdy		
	Salem		
	Saleh		
	Ramadan		
	Hamdy		
	Elsayed		
	Khaled		
	Saeed		
	Ashraf		
	Yousef		
	Fathy		
	Said		
	Soliman		
	Mansour		
	Kamel		
	Taha		
	Mustafa		
	Abdo		
	Khalil		
	Ismail		
	Nabil		
	Samy		
	Ragab		
	Fouad		
}			
