# Country Name:	British Rebels	# Tag:	EN8
#			
graphical_culture = 	latingfx		
#			
color = {	123 0 0	}	
historical_ideas = {			
	grand_army		
	glorious_arms		
	merchant_adventures		
	scientific_revolution		
	national_bank		
	bill_of_rights		
	smithian_economics		
	liberty_egalite_fraternity		
	patron_of_art		
	humanist_tolerance		
	cabinet		
}			
historical_units = {			
	napoleonic_square		
	napoleonic_lancers		
	flying_battery		
}			
monarch_names = {			
	"Smith #0" = 10		
	"Jones #0" = 10		
	"Taylor #0" = 10		
	"Brown #0" = 10		
	"Williams #0" = 10		
	"Wilson #0" = 10		
	"Johnson #0" = 10		
	"Davies #0" = 10		
	"Patel #0" = 10		
	"Robinson #0" = 10		
	"Wright #0" = 10		
	"Evans #0" = 10		
	"Walker #0" = 10		
	"White #0" = 10		
	"Roberts #0" = 10		
	"Green #0" = 10		
	"Hall #0" = 10		
	"Thomas #0" = 10		
	"Clarke #0" = 10		
	"Jackson #0" = 10		
	"Wood #0" = 10		
	"Harris #0" = 10		
	"Edwards #0" = 10		
	"Turner #0" = 10		
	"Martin #0" = 10		
	"Cooper #0" = 10		
	"Hill #0" = 10		
	"Ward #0" = 10		
	"Hughes #0" = 10		
	"Moore #0" = 10		
	"Clark #0" = 10		
	"King #0" = 10		
	"Harrison #0" = 10		
	"Lewis #0" = 10		
	"Baker #0" = 10		
	"Lee #0" = 10		
	"Allen #0" = 10		
	"Morris #0" = 10		
	"Khan #0" = 10		
}			
ship_names = {			
	"Coast Guard"		
}			
leader_names = {			
	Smith		
	Jones		
	Taylor		
	Brown		
	Williams		
	Wilson		
	Wilson		
	Johnson		
	Davies		
	Patel		
	Robinson		
	Wright		
	Thompson		
	Evans		
	Walker		
	White		
	Roberts		
	Green		
	Hall		
	Thomas		
	Clarke		
	Jackson		
	Wood		
	Harris		
	Edwards		
	Turner		
	Martin		
	Cooper		
	Hill		
	Ward		
	Hughes		
	Moore		
	Clark		
	King		
	Harrison		
	Lewis		
	Baker		
	Lee		
	Allen		
	Morris		
	Khan		
}			
