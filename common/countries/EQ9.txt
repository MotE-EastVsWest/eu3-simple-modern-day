# Country Name:	Equatorial Guinea	# Tag:	EQ9
#			
graphical_culture = 	africangfx		
#			
color = {	120 96 100	}	
historical_ideas = {			
	ecumenism		
	church_attendance_duty		
	divine_supremacy		
	deus_vult		
	revolution_and_counter		
	press_gangs		
	national_conscripts		
	napoleonic_warfare		
	vice_roys		
	vetting		
	bureaucracy		
}			
historical_units = {			
	napoleonic_square		
	napoleonic_lancers		
	flying_battery		
}			
monarch_names = {			
	"Nguema #0" = 10		
	"Ndong #0" = 10		
	"Mba #0" = 10		
	"Ondo #0" = 10		
	"Esono #0" = 10		
	"Nsue #0" = 10		
	"Mangue #0" = 10		
	"Nchama #0" = 10		
	"Obiang #0" = 10		
	"Obama #0" = 10		
	"Edu #0" = 10		
	"Ela #0" = 10		
	"Asumu #0" = 10		
	"Obono #0" = 10		
	"Angue #0" = 10		
	"Nzang #0" = 10		
	"Engonga #0" = 10		
	"Nze #0" = 10		
	"Abaga #0" = 10		
	"Micha #0" = 10		
	"Edjang #0" = 10		
	"Andeme #0" = 10		
	"Owono #0" = 10		
	"Mbomio #0" = 10		
	"Ovono #0" = 10		
	"Ncogo #0" = 10		
	"Oyono #0" = 10		
	"Avomo #0" = 10		
	"Mitogo #0" = 10		
	"Alogo #0" = 10		
	"Bindang #0" = 10		
	"Nve #0" = 10		
	"Oyana #0" = 10		
	"Ntutumu #0" = 10		
	"Mikue #0" = 10		
	"Okomo #0" = 10		
	"Ngomo #0" = 10		
	"Sima #0" = 10		
	"Ada #0" = 10		
}			
ship_names = {			
	"Coast Guard"		
}			
leader_names = {			
	Nguema		
	Ndong		
	Mba		
	Ondo		
	Esono		
	Nsue		
	Nsue		
	Mangue		
	Nchama		
	Obiang		
	Obama		
	Edu		
	Abeso		
	Ela		
	Asumu		
	Obono		
	Angue		
	Nzang		
	Engonga		
	Nze		
	Abaga		
	Micha		
	Edjang		
	Andeme		
	Owono		
	Mbomio		
	Ovono		
	Ncogo		
	Oyono		
	Avomo		
	Mitogo		
	Alogo		
	Bindang		
	Nve		
	Oyana		
	Ntutumu		
	Mikue		
	Okomo		
	Ngomo		
	Sima		
	Ada		
}			
