# Country Name:	Eritrea	# Tag:	ER9
#			
graphical_culture = 	africangfx		
#			
color = {	68 197 23	}	
historical_ideas = {			
	ecumenism		
	church_attendance_duty		
	divine_supremacy		
	deus_vult		
	revolution_and_counter		
	press_gangs		
	national_conscripts		
	napoleonic_warfare		
	vice_roys		
	vetting		
	bureaucracy		
}			
historical_units = {			
	napoleonic_square		
	napoleonic_lancers		
	flying_battery		
}			
monarch_names = {			
	"Ali #0" = 10		
	"Haile #0" = 10		
	"Ahmed #0" = 10		
	"Omer #0" = 10		
	"Berhane #0" = 10		
	"Kidane #0" = 10		
	"Osman #0" = 10		
	"Mohamed #0" = 10		
	"Solomon #0" = 10		
	"Hagos #0" = 10		
	"Abraha #0" = 10		
	"Adm #0" = 10		
	"Omar #0" = 10		
	"Yemane #0" = 10		
	"Tekle #0" = 10		
	"Yohannes #0" = 10		
	"Araya #0" = 10		
	"Abdo #0" = 10		
	"Fikru #0" = 10		
	"Abdulkadir #0" = 10		
	"Bahiru #0" = 10		
	"Marta #0" = 10		
	"Workneh #0" = 10		
	"Worke #0" = 10		
	"Mebrat #0" = 10		
	"Birhan #0" = 10		
	"Zinash #0" = 10		
	"Hussein #0" = 10		
	"Tenagne #0" = 10		
	"Adisu #0" = 10		
	"Eshete #0" = 10		
	"Nuredin #0" = 10		
	"Tsegay #0" = 10		
	"Aminat #0" = 10		
	"Chala #0" = 10		
	"Demekech #0" = 10		
	"Haji #0" = 10		
	"Muhammed #0" = 10		
	"Ahemed #0" = 10		
}			
ship_names = {			
	"Coast Guard"		
}			
leader_names = {			
	Ali		
	Haile		
	Ahmed		
	Omer		
	Berhane		
	Kidane		
	Kidane		
	Osman		
	Mohamed		
	Solomon		
	Hagos		
	Abraha		
	Tesfay		
	Adm		
	Omar		
	Yemane		
	Tekle		
	Yohannes		
	Araya		
	Abdo		
	Fikru		
	Abdulkadir		
	Bahiru		
	Marta		
	Workneh		
	Worke		
	Mebrat		
	Birhan		
	Zinash		
	Hussein		
	Tenagne		
	Adisu		
	Eshete		
	Nuredin		
	Tsegay		
	Aminat		
	Chala		
	Demekech		
	Haji		
	Muhammed		
	Ahemed		
}			
