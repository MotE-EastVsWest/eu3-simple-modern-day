# Country Name:	Ethiopia	# Tag:	ET9
#			
graphical_culture = 	africangfx		
#			
color = { 56  120  191 }
historical_ideas = {			
	ecumenism		
	church_attendance_duty		
	divine_supremacy		
	deus_vult		
	revolution_and_counter		
	press_gangs		
	national_conscripts		
	napoleonic_warfare		
	vice_roys		
	vetting		
	bureaucracy		
}			
historical_units = {			
	napoleonic_square		
	napoleonic_lancers		
	flying_battery		
}			
monarch_names = {			
	"Borana #0" = 10		
	"Walaabu #0" = 10		
	"Rayyaa #0" = 10		
	"Karrayyuu #0" = 10		
	"Tulama #0" = 10		
	"Handha #0" = 10		
	"Illuu #0" = 10		
	"Dhakku #0" = 10		
	"Daacci #0" = 10		
	"Oboo #0" = 10		
	"Diigaluu #0" = 10		
	"Guulaalee #0" = 10		
	"Gumbichuu #0" = 10		
	"Konnoo #0" = 10		
	"Yaayee #0" = 10		
	"Galaan #0" = 10		
	"Aabuu #0" = 10		
	"Adaa #0" = 10		
	"Gaduulaa #0" = 10		
	"Jiddaa #0" = 10		
	"Libaan #0" = 10		
	"Warjii #0" = 10		
	"Soddo #0" = 10		
	"Tummee #0" = 10		
	"Garasuu #0" = 10		
	"Illu #0" = 10		
	"Keekuu #0" = 10		
	"Uruu #0" = 10		
	"Waajituu #0" = 10		
	"Meta #0" = 10		
	"Jiillee #0" = 10		
	"Warra Iluu #0" = 10		
	"Oborra #0" = 10		
	"Daga #0" = 10		
	"Baabbile #0" = 10		
	"Ala #0" = 10		
	"Warra Abbayi #0" = 10		
	"Erer #0" = 10		
	"Galaan #0" = 10		
}			
ship_names = {			
	"Coast Guard"		
}			
leader_names = {			
	Borana		
	Walaabu		
	Rayyaa		
	Karrayyuu		
	Tulama		
	Handha		
	Handha		
	Illuu		
	Dhakku		
	Daacci		
	Oboo		
	Diigaluu		
	Eekka		
	Guulaalee		
	Gumbichuu		
	Konnoo		
	Yaayee		
	Galaan		
	Aabuu		
	Adaa		
	Gaduulaa		
	Jiddaa		
	Libaan		
	Warjii		
	Soddo		
	Tummee		
	Garasuu		
	Illu		
	Keekuu		
	Uruu		
	Waajituu		
	Meta		
	Jiillee		
	Warra Iluu		
	Oborra		
	Daga		
	Baabbile		
	Ala		
	Warra Abbayi		
	Erer		
	Galaan		
}			
