# Country Name:	Finland	# Tag:	FI9
#			
graphical_culture = 	latingfx		
#			
color = { 182  134  100 }
historical_ideas = {			
	press_gangs		
	grand_navy		
	sea_hawks		
	superior_seamanship		
	naval_glory		
	excellent_shipwrights		
	naval_fighting_instruction		
	naval_provisioning		
	shrewd_commerce_practise		
	merchant_adventures		
	improved_foraging		
}			
historical_units = {			
	napoleonic_square		
	napoleonic_lancers		
	flying_battery		
}			
monarch_names = {			
	"Korhonen #0" = 10		
	"Virtanen #0" = 10		
	"Nieminen #0" = 10		
	"Makinen #0" = 10		
	"Hamalainen #0" = 10		
	"Makela #0" = 10		
	"Laine #0" = 10		
	"Heikkinen #0" = 10		
	"Koskinen #0" = 10		
	"Lehtonen #0" = 10		
	"Jarvinen #0" = 10		
	"Saarinen #0" = 10		
	"Salminen #0" = 10		
	"Heinonen #0" = 10		
	"Heikkila #0" = 10		
	"Niemi #0" = 10		
	"Salonen #0" = 10		
	"Laitinen #0" = 10		
	"Turunen #0" = 10		
	"Kinnunen #0" = 10		
	"Tuominen #0" = 10		
	"Savolainen #0" = 10		
	"Salo #0" = 10		
	"Rantanen #0" = 10		
	"Jokinen #0" = 10		
	"Miettinen #0" = 10		
	"Mattila #0" = 10		
	"Karjalainen #0" = 10		
	"Rasanen #0" = 10		
	"Ahonen #0" = 10		
	"Lahtinen #0" = 10		
	"Pitkanen #0" = 10		
	"Hiltunen #0" = 10		
	"Ojala #0" = 10		
	"Leppanen #0" = 10		
	"Aaltonen #0" = 10		
	"Leinonen #0" = 10		
	"Kallio #0" = 10		
	"Vaisanen #0" = 10		
}			
ship_names = {			
	"Coast Guard"		
}			
leader_names = {			
	Korhonen		
	Virtanen		
	Nieminen		
	Makinen		
	Hamalainen		
	Makela		
	Makela		
	Laine		
	Heikkinen		
	Koskinen		
	Lehtonen		
	Jarvinen		
	Lehtinen		
	Saarinen		
	Salminen		
	Heinonen		
	Heikkila		
	Niemi		
	Salonen		
	Laitinen		
	Turunen		
	Kinnunen		
	Tuominen		
	Savolainen		
	Salo		
	Rantanen		
	Jokinen		
	Miettinen		
	Mattila		
	Karjalainen		
	Rasanen		
	Ahonen		
	Lahtinen		
	Pitkanen		
	Hiltunen		
	Ojala		
	Leppanen		
	Aaltonen		
	Leinonen		
	Kallio		
	Vaisanen		
}			
