# Country Name:	Fijian Rebels	# Tag:	FJ8
#			
graphical_culture = 	indiangfx		
#			
color = {	182 213 164	}	
historical_ideas = {			
	press_gangs		
	grand_navy		
	sea_hawks		
	superior_seamanship		
	naval_glory		
	excellent_shipwrights		
	naval_fighting_instruction		
	naval_provisioning		
	shrewd_commerce_practise		
	merchant_adventures		
	improved_foraging		
}			
historical_units = {			
	napoleonic_square		
	napoleonic_lancers		
	flying_battery		
}			
monarch_names = {			
	"Kumar #0" = 10		
	"Prasad #0" = 10		
	"Chand #0" = 10		
	"Singh #0" = 10		
	"Lal #0" = 10		
	"Sharma #0" = 10		
	"Narayan #0" = 10		
	"Khan #0" = 10		
	"Ali #0" = 10		
	"Devi #0" = 10		
	"Ram #0" = 10		
	"Chandra #0" = 10		
	"Nand #0" = 10		
	"Lata #0" = 10		
	"Deo #0" = 10		
	"Reddy #0" = 10		
	"Prakash #0" = 10		
	"Raj #0" = 10		
	"Maharaj #0" = 10		
	"Waqa #0" = 10		
	"Goundar #0" = 10		
	"Koroi #0" = 10		
	"Fong #0" = 10		
	"Pillay #0" = 10		
	"Seru #0" = 10		
	"Nair #0" = 10		
	"Gounder #0" = 10		
	"Tawake #0" = 10		
	"Patel #0" = 10		
	"Mani #0" = 10		
	"Nisha #0" = 10		
	"Sami #0" = 10		
	"Smith #0" = 10		
	"Naicker #0" = 10		
	"Krishna #0" = 10		
	"Ledua #0" = 10		
	"Whippy #0" = 10		
	"Lee #0" = 10		
	"Dutt #0" = 10		
}			
ship_names = {			
	"Coast Guard"		
}			
leader_names = {			
	Kumar		
	Prasad		
	Chand		
	Singh		
	Lal		
	Sharma		
	Sharma		
	Narayan		
	Khan		
	Ali		
	Devi		
	Ram		
	Naidu		
	Chandra		
	Nand		
	Lata		
	Deo		
	Reddy		
	Prakash		
	Raj		
	Maharaj		
	Waqa		
	Goundar		
	Koroi		
	Fong		
	Pillay		
	Seru		
	Nair		
	Gounder		
	Tawake		
	Patel		
	Mani		
	Nisha		
	Sami		
	Smith		
	Naicker		
	Krishna		
	Ledua		
	Whippy		
	Lee		
	Dutt		
}			
