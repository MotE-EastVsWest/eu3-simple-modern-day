# Country Name:	Faroe Islander Rebels	# Tag:	FO8
#			
graphical_culture = 	latingfx		
#			
color = {	127 159 147	}	
historical_ideas = {			
}			
historical_units = {			
	napoleonic_square		
	napoleonic_lancers		
	flying_battery		
}			
monarch_names = {			
	"Joensen #0" = 10		
	"Hansen #0" = 10		
	"Jacobsen #0" = 10		
	"Olsen #0" = 10		
	"Poulsen #0" = 10		
	"Petersen #0" = 10		
	"Johannesen #0" = 10		
	"Thomsen #0" = 10		
	"Nielsen #0" = 10		
	"Rasmussen #0" = 10		
	"Johansen #0" = 10		
	"Djurhuus #0" = 10		
	"Danielsen #0" = 10		
	"Mortensen #0" = 10		
	"Jensen #0" = 10		
	"Mikkelsen #0" = 10		
	"Dam #0" = 10		
	"Hojgaard #0" = 10		
	"Sorensen #0" = 10		
	"Andreasen #0" = 10		
	"Hammer #0" = 10		
	"Hentze #0" = 10		
	"Magnussen #0" = 10		
	"Samuelsen #0" = 10		
	"Christiansen #0" = 10		
	"Davidsen #0" = 10		
	"Heinesen #0" = 10		
	"Nicalsen #0" = 10		
	"Holm #0" = 10		
	"Vang #0" = 10		
	"Midjord #0" = 10		
	"Jakobsen #0" = 10		
	"Lamhauge #0" = 10		
	"Egholm #0" = 10		
	"Berg #0" = 10		
	"Justinussen #0" = 10		
	"Johannessen #0" = 10		
	"Dahl #0" = 10		
	"Ellingsgaard #0" = 10		
}			
ship_names = {			
	"Coast Guard"		
}			
leader_names			
	Joensen		
	Hansen		
	Jacobsen		
	Olsen		
	Poulsen		
	Petersen		
	Petersen		
	Johannesen		
	Thomsen		
	Nielsen		
	Rasmussen		
	Johansen		
	Simonsen		
	Djurhuus		
	Danielsen		
	Mortensen		
	Jensen		
	Mikkelsen		
	Dam		
	Hojgaard		
	Sorensen		
	Andreasen		
	Hammer		
	Hentze		
	Magnussen		
	Samuelsen		
	Christiansen		
	Davidsen		
	Heinesen		
	Nicalsen		
	Holm		
	Vang		
	Midjord		
	Jakobsen		
	Lamhauge		
	Egholm		
	Berg		
	Justinussen		
	Johannessen		
	Dahl		
	Ellingsgaard		
}			
