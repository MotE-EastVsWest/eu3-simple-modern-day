# Country Name:	French Rebels	# Tag:	FR8
#			
graphical_culture = 	latingfx		
#			
color = {	9 70 127	}	
historical_ideas = {			
	grand_army		
	glorious_arms		
	merchant_adventures		
	scientific_revolution		
	national_bank		
	bill_of_rights		
	smithian_economics		
	liberty_egalite_fraternity		
	patron_of_art		
	humanist_tolerance		
	cabinet		
}			
historical_units = {			
	napoleonic_square		
	napoleonic_lancers		
	flying_battery		
}			
monarch_names = {			
	"Martin #0" = 10		
	"Bernard #0" = 10		
	"Robert #0" = 10		
	"Richard #0" = 10		
	"Durand #0" = 10		
	"Dubois #0" = 10		
	"Moreau #0" = 10		
	"Simon #0" = 10		
	"Laurent #0" = 10		
	"Michel #0" = 10		
	"Garcia #0" = 10		
	"Leroy #0" = 10		
	"David #0" = 10		
	"Morel #0" = 10		
	"Roux #0" = 10		
	"Girard #0" = 10		
	"Fournier #0" = 10		
	"Lambert #0" = 10		
	"Lefebvre #0" = 10		
	"Mercier #0" = 10		
	"Blanc #0" = 10		
	"Dupont #0" = 10		
	"Faure #0" = 10		
	"Bertrand #0" = 10		
	"Morin #0" = 10		
	"Garnier #0" = 10		
	"Nicolas #0" = 10		
	"Marie #0" = 10		
	"Rousseau #0" = 10		
	"Bonnet #0" = 10		
	"Vincent #0" = 10		
	"Henry #0" = 10		
	"Masson #0" = 10		
	"Robin #0" = 10		
	"Martinez #0" = 10		
	"Boyer #0" = 10		
	"Muller #0" = 10		
	"Chevalier #0" = 10		
	"Denis #0" = 10		
}			
ship_names = {			
	"Coast Guard"		
}			
leader_names = {			
	Martin		
	Bernard		
	Robert		
	Richard		
	Durand		
	Dubois		
	Dubois		
	Moreau		
	Simon		
	Laurent		
	Michel		
	Garcia		
	Thomas		
	Leroy		
	David		
	Morel		
	Roux		
	Girard		
	Fournier		
	Lambert		
	Lefebvre		
	Mercier		
	Blanc		
	Dupont		
	Faure		
	Bertrand		
	Morin		
	Garnier		
	Nicolas		
	Marie		
	Rousseau		
	Bonnet		
	Vincent		
	Henry		
	Masson		
	Robin		
	Martinez		
	Boyer		
	Muller		
	Chevalier		
	Denis		
}			
