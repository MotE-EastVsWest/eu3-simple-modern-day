# Country Name:	Guianese Rebels	# Tag:	GI8
#			
graphical_culture = 	africangfx		
#			
color = {	80 0 189	}	
historical_ideas = {			
	ecumenism		
	church_attendance_duty		
	divine_supremacy		
	deus_vult		
	revolution_and_counter		
	press_gangs		
	national_conscripts		
	napoleonic_warfare		
	vice_roys		
	vetting		
	bureaucracy		
}			
historical_units = {			
	napoleonic_square		
	napoleonic_lancers		
	flying_battery		
}			
monarch_names = {			
	"Ho #0" = 10		
	"Joseph #0" = 10		
	"Pierre #0" = 10		
	"da Silva #0" = 10		
	"Jean #0" = 10		
	"Charles #0" = 10		
	"Masson #0" = 10		
	"Serres #0" = 10		
	"Laffont #0" = 10		
	"Clement #0" = 10		
	"Henry #0" = 10		
	"Aubert #0" = 10		
	"Bru #0" = 10		
	"Cathala #0" = 10		
	"Galy #0" = 10		
	"Arnal #0" = 10		
	"Lefebvre #0" = 10		
	"Delpech #0" = 10		
	"Garnier #0" = 10		
	"Dedieu #0" = 10		
	"Fourcade #0" = 10		
	"Rouanet #0" = 10		
	"Dejean #0" = 10		
	"Bosc #0" = 10		
	"Ferrer #0" = 10		
	"Moulin #0" = 10		
	"Gros #0" = 10		
	"Chevalier #0" = 10		
	"Munoz #0" = 10		
	"Blanchard #0" = 10		
	"Gaubert #0" = 10		
	"Causse #0" = 10		
	"Pech #0" = 10		
	"Torres #0" = 10		
	"Lasserre #0" = 10		
	"Fernandes #0" = 10		
	"dos Santos #0" = 10		
	"Picard #0" = 10		
	"Prat #0" = 10		
}			
ship_names = {			
	"Coast Guard"		
}			
leader_names = {			
	Ho		
	Joseph		
	Pierre		
	da Silva		
	Jean		
	Charles		
	Charles		
	Masson		
	Serres		
	Laffont		
	Clement		
	Henry		
	Moreno		
	Aubert		
	Bru		
	Cathala		
	Galy		
	Arnal		
	Lefebvre		
	Delpech		
	Garnier		
	Dedieu		
	Fourcade		
	Rouanet		
	Dejean		
	Bosc		
	Ferrer		
	Moulin		
	Gros		
	Chevalier		
	Munoz		
	Blanchard		
	Gaubert		
	Causse		
	Pech		
	Torres		
	Lasserre		
	Fernandes		
	dos Santos		
	Picard		
	Prat		
}			
