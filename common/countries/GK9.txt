# Country Name:	Greece	# Tag:	GK9
#			
graphical_culture = 	easterngfx		
#			
color = { 249  206  173 }
historical_ideas = {			
	press_gangs		
	grand_navy		
	sea_hawks		
	superior_seamanship		
	naval_glory		
	excellent_shipwrights		
	naval_fighting_instruction		
	naval_provisioning		
	shrewd_commerce_practise		
	merchant_adventures		
	improved_foraging		
}			
historical_units = {			
	napoleonic_square		
	napoleonic_lancers		
	flying_battery		
}			
monarch_names = {			
	"Papadopoulos #0" = 10		
	"Papadopoulou #0" = 10		
	"Papageorgiou #0" = 10		
	"Oikonomou #0" = 10		
	"Papadimitriou #0" = 10		
	"Georgiou #0" = 10		
	"Papaioannou #0" = 10		
	"Pappas #0" = 10		
	"Vasileiou #0" = 10		
	"Nikolaou #0" = 10		
	"Karagiannis #0" = 10		
	"Antoniou #0" = 10		
	"Makris #0" = 10		
	"Papanikolaou #0" = 10		
	"Dimitriou #0" = 10		
	"Ioannidis #0" = 10		
	"Georgiadis #0" = 10		
	"Triantafyllou #0" = 10		
	"Papadakis #0" = 10		
	"Athanasiou #0" = 10		
	"Konstantinidis #0" = 10		
	"Ioannou #0" = 10		
	"Alexiou #0" = 10		
	"Christodoulou #0" = 10		
	"Theodorou #0" = 10		
	"Giannopoulos #0" = 10		
	"Nikolaidis #0" = 10		
	"Konstantinou #0" = 10		
	"Panagiotopoulos #0" = 10		
	"Michailidis #0" = 10		
	"Papakonstantinou #0" = 10		
	"Papathanasiou #0" = 10		
	"Antonopoulos #0" = 10		
	"Dimopoulos #0" = 10		
	"Karagianni #0" = 10		
	"Anastasiou #0" = 10		
	"Dimitriadis #0" = 10		
	"Pappa #0" = 10		
	"Vlachou #0" = 10		
}			
ship_names = {			
	"Coast Guard"		
}			
leader_names = {			
	Papadopoulos		
	Papadopoulou		
	Papageorgiou		
	Oikonomou		
	Papadimitriou		
	Georgiou		
	Georgiou		
	Papaioannou		
	Pappas		
	Vasileiou		
	Nikolaou		
	Karagiannis		
	Vlachos		
	Antoniou		
	Makris		
	Papanikolaou		
	Dimitriou		
	Ioannidis		
	Georgiadis		
	Triantafyllou		
	Papadakis		
	Athanasiou		
	Konstantinidis		
	Ioannou		
	Alexiou		
	Christodoulou		
	Theodorou		
	Giannopoulos		
	Nikolaidis		
	Konstantinou		
	Panagiotopoulos		
	Michailidis		
	Papakonstantinou		
	Papathanasiou		
	Antonopoulos		
	Dimopoulos		
	Karagianni		
	Anastasiou		
	Dimitriadis		
	Pappa		
	Vlachou		
}			
