# Country Name:	Guyana	# Tag:	GY9
#			
graphical_culture = 	africangfx		
#			
color = {	25 169 160	}	
historical_ideas = {			
	ecumenism		
	church_attendance_duty		
	divine_supremacy		
	deus_vult		
	revolution_and_counter		
	press_gangs		
	national_conscripts		
	napoleonic_warfare		
	vice_roys		
	vetting		
	bureaucracy		
}			
historical_units = {			
	napoleonic_square		
	napoleonic_lancers		
	flying_battery		
}			
monarch_names = {			
	"Persaud #0" = 10		
	"Singh #0" = 10		
	"Williams #0" = 10		
	"Khan #0" = 10		
	"Thomas #0" = 10		
	"Joseph #0" = 10		
	"Henry #0" = 10		
	"James #0" = 10		
	"Mohamed #0" = 10		
	"Smith #0" = 10		
	"Wilson #0" = 10		
	"Fraser #0" = 10		
	"Peters #0" = 10		
	"Narine #0" = 10		
	"George #0" = 10		
	"Benjamin #0" = 10		
	"Lewis #0" = 10		
	"Johnson #0" = 10		
	"John #0" = 10		
	"Adams #0" = 10		
	"Bacchus #0" = 10		
	"Ali #0" = 10		
	"Edwards #0" = 10		
	"Roberts #0" = 10		
	"Charles #0" = 10		
	"Jones #0" = 10		
	"Griffith #0" = 10		
	"Rampersaud #0" = 10		
	"Paul #0" = 10		
	"David #0" = 10		
	"King #0" = 10		
	"Simon #0" = 10		
	"Lall #0" = 10		
	"Samaroo #0" = 10		
	"Ally #0" = 10		
	"Clarke #0" = 10		
	"Fredericks #0" = 10		
	"Rodrigues #0" = 10		
	"Abrams #0" = 10		
}			
ship_names = {			
	"Coast Guard"		
}			
leader_names = {			
	Persaud		
	Singh		
	Williams		
	Khan		
	Thomas		
	Joseph		
	Joseph		
	Henry		
	James		
	Mohamed		
	Smith		
	Wilson		
	Daniels		
	Fraser		
	Peters		
	Narine		
	George		
	Benjamin		
	Lewis		
	Johnson		
	John		
	Adams		
	Bacchus		
	Ali		
	Edwards		
	Roberts		
	Charles		
	Jones		
	Griffith		
	Rampersaud		
	Paul		
	David		
	King		
	Simon		
	Lall		
	Samaroo		
	Ally		
	Clarke		
	Fredericks		
	Rodrigues		
	Abrams		
}			
