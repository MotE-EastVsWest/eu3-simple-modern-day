# Country Name:	Bhutan	# Tag:	HA9
#			
graphical_culture = 	chinesegfx		
#			
color = { 93  180  76 }
historical_ideas = {			
	ecumenism		
	church_attendance_duty		
	divine_supremacy		
	deus_vult		
	revolution_and_counter		
	press_gangs		
	national_conscripts		
	napoleonic_warfare		
	vice_roys		
	vetting		
	bureaucracy		
}			
historical_units = {			
	napoleonic_square		
	napoleonic_lancers		
	flying_battery		
}			
monarch_names = {			
	"Dorji #0" = 10		
	"Wangmo #0" = 10		
	"Rai #0" = 10		
	"Dema #0" = 10		
	"Zangmo #0" = 10		
	"Tshering #0" = 10		
	"Choden #0" = 10		
	"Lhamo #0" = 10		
	"Gurung #0" = 10		
	"Wangchuk #0" = 10		
	"Wangdi #0" = 10		
	"Tamang #0" = 10		
	"Gyeltshen #0" = 10		
	"Tenzin #0" = 10		
	"Tshomo #0" = 10		
	"Lham #0" = 10		
	"Yangzom #0" = 10		
	"Subba #0" = 10		
	"Phuntsho #0" = 10		
	"Penjor #0" = 10		
	"Norbu #0" = 10		
	"Dem #0" = 10		
	"Pem #0" = 10		
	"Mongar #0" = 10		
	"Zam #0" = 10		
	"Monger #0" = 10		
	"Thinley #0" = 10		
	"Jamtsho #0" = 10		
	"Lepcha #0" = 10		
	"Om #0" = 10		
	"Chhetri #0" = 10		
	"Limbu #0" = 10		
	"Choki #0" = 10		
	"Pemo #0" = 10		
	"Tobgay #0" = 10		
	"Pradhan #0" = 10		
	"Tashi #0" = 10		
	"Raini #0" = 10		
	"Doya #0" = 10		
}			
ship_names = {			
	"Coast Guard"		
}			
leader_names = {			
	Dorji		
	Wangmo		
	Rai		
	Dema		
	Zangmo		
	Tshering		
	Tshering		
	Choden		
	Lhamo		
	Gurung		
	Wangchuk		
	Wangdi		
	Ghalley		
	Tamang		
	Gyeltshen		
	Tenzin		
	Tshomo		
	Lham		
	Yangzom		
	Subba		
	Phuntsho		
	Penjor		
	Norbu		
	Dem		
	Pem		
	Mongar		
	Zam		
	Monger		
	Thinley		
	Jamtsho		
	Lepcha		
	Om		
	Chhetri		
	Limbu		
	Choki		
	Pemo		
	Tobgay		
	Pradhan		
	Tashi		
	Raini		
	Doya		
}			
