# Country Name:	Honduran Rebels	# Tag:	HD8
#			
graphical_culture = 	southamericagfx		
#			
color = {	86 92 125	}	
historical_ideas = {			
	ecumenism		
	church_attendance_duty		
	divine_supremacy		
	deus_vult		
	revolution_and_counter		
	press_gangs		
	national_conscripts		
	napoleonic_warfare		
	vice_roys		
	vetting		
	bureaucracy		
}			
historical_units = {			
	napoleonic_square		
	napoleonic_lancers		
	flying_battery		
}			
monarch_names = {			
	"Hernandez #0" = 10		
	"Lopez #0" = 10		
	"Martinez #0" = 10		
	"Rodriguez #0" = 10		
	"Garcia #0" = 10		
	"Mejia #0" = 10		
	"Cruz #0" = 10		
	"Flores #0" = 10		
	"Sanchez #0" = 10		
	"Reyes #0" = 10		
	"Vasquez #0" = 10		
	"Perez #0" = 10		
	"Gomez #0" = 10		
	"Pineda #0" = 10		
	"Diaz #0" = 10		
	"Ramos #0" = 10		
	"Aguilar #0" = 10		
	"Ramirez #0" = 10		
	"Alvarado #0" = 10		
	"Castro #0" = 10		
	"Velasquez #0" = 10		
	"Romero #0" = 10		
	"Castillo #0" = 10		
	"Orellana #0" = 10		
	"Mendoza #0" = 10		
	"Nu #0" = 10		
	"Murillo #0" = 10		
	"Avila #0" = 10		
	"Gonzalez #0" = 10		
	"Zelaya #0" = 10		
	"Padilla #0" = 10		
	"Santos #0" = 10		
	"Paz #0" = 10		
	"Gonzales #0" = 10		
	"Gutierrez #0" = 10		
	"Bonilla #0" = 10		
	"Herrera #0" = 10		
	"Medina #0" = 10		
	"Torres #0" = 10		
}			
ship_names = {			
	"Coast Guard"		
}			
leader_names = {			
	Hernandez		
	Lopez		
	Martinez		
	Rodriguez		
	Garcia		
	Mejia		
	Mejia		
	Cruz		
	Flores		
	Sanchez		
	Reyes		
	Vasquez		
	Rivera		
	Perez		
	Gomez		
	Pineda		
	Diaz		
	Ramos		
	Aguilar		
	Ramirez		
	Alvarado		
	Castro		
	Velasquez		
	Romero		
	Castillo		
	Orellana		
	Mendoza		
	Nu		
	Murillo		
	Avila		
	Gonzalez		
	Zelaya		
	Padilla		
	Santos		
	Paz		
	Gonzales		
	Gutierrez		
	Bonilla		
	Herrera		
	Medina		
	Torres		
}			
