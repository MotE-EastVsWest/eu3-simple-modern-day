# Country Name:	Chechnyan Rebels	# Tag:	HE8
#			
graphical_culture = 	muslimgfx		
#			
color = {	180 57 0	}	
historical_ideas = {			
	ecumenism		
	church_attendance_duty		
	divine_supremacy		
	deus_vult		
	revolution_and_counter		
	press_gangs		
	national_conscripts		
	napoleonic_warfare		
	vice_roys		
	vetting		
	bureaucracy		
}			
historical_units = {			
	napoleonic_square		
	napoleonic_lancers		
	flying_battery		
}			
monarch_names = {			
	"Abubakarov #0" = 10		
	"Magomadov #0" = 10		
	"Khachukaev #0" = 10		
	"Bazurkaev #0" = 10		
	"Khaytaev #0" = 10		
	"Zaytseva #0" = 10		
	"Gaytukaeva #0" = 10		
	"Bunyaev #0" = 10		
	"Khazhmuradov #0" = 10		
	"Tsotsarov #0" = 10		
	"Usumov #0" = 10		
	"Baysuev #0" = 10		
	"Kadyrov #0" = 10		
	"Magomedov #0" = 10		
	"Aliev #0" = 10		
	"Taramov #0" = 10		
	"Edilov #0" = 10		
	"Elimkhanov #0" = 10		
	"Khasukhanov #0" = 10		
	"Sakkazov #0" = 10		
	"Elmurzaev #0" = 10		
	"Yakubov #0" = 10		
	"Ibragimov #0" = 10		
	"Khizriev #0" = 10		
	"Achkhoevsky #0" = 10		
	"Buchaev #0" = 10		
	"Salmanova #0" = 10		
	"Ovcharov #0" = 10		
	"Sivoronov #0" = 10		
	"Chechensky #0" = 10		
	"Bibulatov #0" = 10		
	"Mukhdanov #0" = 10		
	"Umarov #0" = 10		
	"Yusupov #0" = 10		
	"Petrov #0" = 10		
	"Makeeva #0" = 10		
	"Batsalgov #0" = 10		
	"Netkacheva #0" = 10		
	"Taran #0" = 10		
}			
ship_names = {			
	"Coast Guard"		
}			
leader_names = {			
	Abubakarov		
	Magomadov		
	Khachukaev		
	Bazurkaev		
	Khaytaev		
	Zaytseva		
	Zaytseva		
	Gaytukaeva		
	Bunyaev		
	Khazhmuradov		
	Tsotsarov		
	Usumov		
	Bashtaev		
	Baysuev		
	Kadyrov		
	Magomedov		
	Aliev		
	Taramov		
	Edilov		
	Elimkhanov		
	Khasukhanov		
	Sakkazov		
	Elmurzaev		
	Yakubov		
	Ibragimov		
	Khizriev		
	Achkhoevsky		
	Buchaev		
	Salmanova		
	Ovcharov		
	Sivoronov		
	Chechensky		
	Bibulatov		
	Mukhdanov		
	Umarov		
	Yusupov		
	Petrov		
	Makeeva		
	Batsalgov		
	Netkacheva		
	Taran		
}			
