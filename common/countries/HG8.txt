# Country Name:	Hungarian Rebels	# Tag:	HG8
#			
graphical_culture = 	latingfx		
#			
color = {	122 55 62	}	
historical_ideas = {			
	vetting		
	national_bank		
	national_trade_policy		
	smithian_economics		
	shrewd_commerce_practise		
	scientific_revolution		
	sea_hawks		
	naval_provisioning		
	regimental_system		
	napoleonic_warfare		
	merchant_adventures		
}			
historical_units = {			
	napoleonic_square		
	napoleonic_lancers		
	flying_battery		
}			
monarch_names = {			
	"Tóth #0" = 10		
	"Nagy #0" = 10		
	"Szabó #0" = 10		
	"Kovacs #0" = 10		
	"Varga #0" = 10		
	"Horvath #0" = 10		
	"Kiss #0" = 10		
	"Molnar #0" = 10		
	"Németh #0" = 10		
	"Farkas #0" = 10		
	"Takacs #0" = 10		
	"Papp #0" = 10		
	"Juhasz #0" = 10		
	"Szilagyi #0" = 10		
	"Mészaros #0" = 10		
	"Simon #0" = 10		
	"Szűcs #0" = 10		
	"Fekete #0" = 10		
	"TOrOk #0" = 10		
	"Racz #0" = 10		
	"Olah #0" = 10		
	"Szalai #0" = 10		
	"Fehér #0" = 10		
	"Gal #0" = 10		
	"Pintér #0" = 10		
	"Balazs #0" = 10		
	"Kocsis #0" = 10		
	"Lakatos #0" = 10		
	"Fodor #0" = 10		
	"Vincze #0" = 10		
	"Sandor #0" = 10		
	"Veres #0" = 10		
	"Magyar #0" = 10		
	"Kis #0" = 10		
	"Sipos #0" = 10		
	"Katona #0" = 10		
	"Vass #0" = 10		
	"Kiraly #0" = 10		
	"Hegedűs #0" = 10		
}			
ship_names = {			
	"Coast Guard"		
}			
leader_names = {			
	Tóth		
	Nagy		
	Szabó		
	Kovacs		
	Varga		
	Horvath		
	Horvath		
	Kiss		
	Molnar		
	Németh		
	Farkas		
	Takacs		
	Balogh		
	Papp		
	Juhasz		
	Szilagyi		
	Mészaros		
	Simon		
	Szűcs		
	Fekete		
	TOrOk		
	Racz		
	Olah		
	Szalai		
	Fehér		
	Gal		
	Pintér		
	Balazs		
	Kocsis		
	Lakatos		
	Fodor		
	Vincze		
	Sandor		
	Veres		
	Magyar		
	Kis		
	Sipos		
	Katona		
	Vass		
	Kiraly		
	Hegedűs		
}			
