# Country Name:	Hawaiian Rebels	# Tag:	HI8
#			
graphical_culture = 	northamericagfx		
#			
color = {	148 0 111	}	
historical_ideas = {			
	press_gangs		
	grand_navy		
	sea_hawks		
	superior_seamanship		
	naval_glory		
	excellent_shipwrights		
	naval_fighting_instruction		
	naval_provisioning		
	shrewd_commerce_practise		
	merchant_adventures		
	improved_foraging		
}			
historical_units = {			
	napoleonic_square		
	napoleonic_lancers		
	flying_battery		
}			
monarch_names = {			
	"Lee #0" = 10		
	"Wong #0" = 10		
	"Kim #0" = 10		
	"Young #0" = 10		
	"Smith #0" = 10		
	"Chang #0" = 10		
	"Nakamura #0" = 10		
	"Chun #0" = 10		
	"Ching #0" = 10		
	"Lau #0" = 10		
	"Higa #0" = 10		
	"Lum #0" = 10		
	"Tanaka #0" = 10		
	"Johnson #0" = 10		
	"Oshiro #0" = 10		
	"Brown #0" = 10		
	"Watanabe #0" = 10		
	"Williams #0" = 10		
	"Silva #0" = 10		
	"Sato #0" = 10		
	"Miller #0" = 10		
	"Kaneshiro #0" = 10		
	"Ho #0" = 10		
	"Ramos #0" = 10		
	"Garcia #0" = 10		
	"Jones #0" = 10		
	"Fernandez #0" = 10		
	"Anderson #0" = 10		
	"Davis #0" = 10		
	"Souza #0" = 10		
	"Martin #0" = 10		
	"Chong #0" = 10		
	"Matsumoto #0" = 10		
	"Domingo #0" = 10		
	"Leong #0" = 10		
	"Wilson #0" = 10		
	"Medeiros #0" = 10		
	"Chung #0" = 10		
	"Yamada #0" = 10		
}			
ship_names = {			
	"Coast Guard"		
}			
leader_names = {			
	Lee		
	Wong		
	Kim		
	Young		
	Smith		
	Chang		
	Chang		
	Nakamura		
	Chun		
	Ching		
	Lau		
	Higa		
	Yamamoto		
	Lum		
	Tanaka		
	Johnson		
	Oshiro		
	Brown		
	Watanabe		
	Williams		
	Silva		
	Sato		
	Miller		
	Kaneshiro		
	Ho		
	Ramos		
	Garcia		
	Jones		
	Fernandez		
	Anderson		
	Davis		
	Souza		
	Martin		
	Chong		
	Matsumoto		
	Domingo		
	Leong		
	Wilson		
	Medeiros		
	Chung		
	Yamada		
}			
