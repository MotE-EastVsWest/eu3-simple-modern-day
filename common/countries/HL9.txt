# Country Name:	Bangladesh	# Tag:	HL9
#			
graphical_culture = 	indiangfx		
#			
color = { 36  109  194 }
historical_ideas = {			
	ecumenism		
	church_attendance_duty		
	divine_supremacy		
	deus_vult		
	revolution_and_counter		
	press_gangs		
	national_conscripts		
	napoleonic_warfare		
	vice_roys		
	vetting		
	bureaucracy		
}			
historical_units = {			
	napoleonic_square		
	napoleonic_lancers		
	flying_battery		
}			
monarch_names = {			
	"Akter #0" = 10		
	"Islam #0" = 10		
	"Khatun #0" = 10		
	"Hossain #0" = 10		
	"Rahman #0" = 10		
	"Hasan #0" = 10		
	"Ahmed #0" = 10		
	"Begum #0" = 10		
	"Khan #0" = 10		
	"Uddin #0" = 10		
	"Ali #0" = 10		
	"Mia #0" = 10		
	"Sultana #0" = 10		
	"Hossen #0" = 10		
	"Chowdhury #0" = 10		
	"Haque #0" = 10		
	"Rana #0" = 10		
	"Miah #0" = 10		
	"Akhter #0" = 10		
	"Aktar #0" = 10		
	"Akther #0" = 10		
	"Hosen #0" = 10		
	"Das #0" = 10		
	"Mahmud #0" = 10		
	"Biswas #0" = 10		
	"Jahan #0" = 10		
	"Kabir #0" = 10		
	"Sarker #0" = 10		
	"Hoque #0" = 10		
	"Nahar #0" = 10		
	"Hassan #0" = 10		
	"Karim #0" = 10		
	"Parvin #0" = 10		
	"Saha #0" = 10		
	"Roy #0" = 10		
	"Amin #0" = 10		
	"Rashid #0" = 10		
	"Ullah #0" = 10		
	"Al Amin #0" = 10		
}			
ship_names = {			
	"Coast Guard"		
}			
leader_names = {			
	Akter		
	Islam		
	Khatun		
	Hossain		
	Rahman		
	Hasan		
	Hasan		
	Ahmed		
	Begum		
	Khan		
	Uddin		
	Ali		
	Alam		
	Mia		
	Sultana		
	Hossen		
	Chowdhury		
	Haque		
	Rana		
	Miah		
	Akhter		
	Aktar		
	Akther		
	Hosen		
	Das		
	Mahmud		
	Biswas		
	Jahan		
	Kabir		
	Sarker		
	Hoque		
	Nahar		
	Hassan		
	Karim		
	Parvin		
	Saha		
	Roy		
	Amin		
	Rashid		
	Ullah		
	Al Amin		
}			
