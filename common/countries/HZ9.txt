# Country Name:	Herzeg-Bosnia	# Tag:	HZ9
#			
graphical_culture = 	easterngfx		
#			
color = {	203 193 75	}	
historical_ideas = {			
}			
historical_units = {			
	napoleonic_square		
	napoleonic_lancers		
	flying_battery		
}			
monarch_names = {			
	"Maric #0" = 10		
	"Zovko #0" = 10		
	"Coric #0" = 10		
	"Boskovic #0" = 10		
	"Kresic #0" = 10		
	"Pehar #0" = 10		
	"Peric #0" = 10		
	"Pavlovic #0" = 10		
	"Ostojic #0" = 10		
	"Vasilj #0" = 10		
	"Tomic #0" = 10		
	"Bevanda #0" = 10		
	"Martinovic #0" = 10		
	"Kovacevic #0" = 10		
	"Bosnjak #0" = 10		
	"Rozic #0" = 10		
	"Soldo #0" = 10		
	"Ivankovic #0" = 10		
	"Lovric #0" = 10		
	"Kordic #0" = 10		
	"Primorac #0" = 10		
	"Sunjic #0" = 10		
	"Juric #0" = 10		
	"Jozic #0" = 10		
	"Prskalo #0" = 10		
	"Knezovic #0" = 10		
	"Marijanovic #0" = 10		
	"Jurkovic #0" = 10		
	"Brkic #0" = 10		
	"Babic #0" = 10		
	"Cilic #0" = 10		
	"Nikolic #0" = 10		
	"Salcin #0" = 10		
	"Milicevic #0" = 10		
	"Krtalic #0" = 10		
	"Sarcevic #0" = 10		
	"Prce #0" = 10		
	"Tadic #0" = 10		
	"Skoro #0" = 10		
}			
ship_names = {			
	"Coast Guard"		
}			
leader_names			
	Maric		
	Zovko		
	Coric		
	Boskovic		
	Kresic		
	Pehar		
	Pehar		
	Peric		
	Pavlovic		
	Ostojic		
	Vasilj		
	Tomic		
	Raguz		
	Bevanda		
	Martinovic		
	Kovacevic		
	Bosnjak		
	Rozic		
	Soldo		
	Ivankovic		
	Lovric		
	Kordic		
	Primorac		
	Sunjic		
	Juric		
	Jozic		
	Prskalo		
	Knezovic		
	Marijanovic		
	Jurkovic		
	Brkic		
	Babic		
	Cilic		
	Nikolic		
	Salcin		
	Milicevic		
	Krtalic		
	Sarcevic		
	Prce		
	Tadic		
	Skoro		
}			
