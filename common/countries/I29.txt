# Country Name:	Assam	# Tag:	MM9
#			
graphical_culture = 	indiangfx		
#			
color = { 215 87 32 }
}
historical_ideas = {			
	ecumenism		
	church_attendance_duty		
	divine_supremacy		
	deus_vult		
	revolution_and_counter		
	press_gangs		
	national_conscripts		
	napoleonic_warfare		
	vice_roys		
	vetting		
	bureaucracy		
}			
historical_units = {			
	napoleonic_square		
	napoleonic_lancers		
	flying_battery		
}			
monarch_names = {			
	"Ali #0" = 10		
	"Das #0" = 10		
	"Khatun #0" = 10		
	"Basumatary #0" = 10		
	"Uddin #0" = 10		
	"Gogoi #0" = 10		
	"Bibi #0" = 10		
	"Boro #0" = 10		
	"Rabha #0" = 10		
	"Nath #0" = 10		
	"Deka #0" = 10		
	"Islam #0" = 10		
	"Begum #0" = 10		
	"Kalita #0" = 10		
	"Saikia #0" = 10		
	"Hussain #0" = 10		
	"Rahman #0" = 10		
	"Roy #0" = 10		
	"Ray #0" = 10		
	"Brahma #0" = 10		
	"Bora #0" = 10		
	"Hoque #0" = 10		
	"Nessa #0" = 10		
	"Narzary #0" = 10		
	"Borah #0" = 10		
	"Pegu #0" = 10		
	"Laskar #0" = 10		
	"Sheikh #0" = 10		
	"Doley #0" = 10		
	"Devi #0" = 10		
	"Tanti #0" = 10		
	"Bewa #0" = 10		
	"Biswas #0" = 10		
	"Hazarika #0" = 10		
	"Dutta #0" = 10		
	"Sarkar #0" = 10		
	"Sonowal #0" = 10		
	"Ahmed #0" = 10		
	"Mandal #0" = 10		
}			
ship_names = {			
	"Coast Guard"		
}			
leader_names = {			
	Ali		
	Das		
	Khatun		
	Basumatary		
	Uddin		
	Gogoi		
	Gogoi		
	Bibi		
	Boro		
	Rabha		
	Nath		
	Deka		
	Barman		
	Islam		
	Begum		
	Kalita		
	Saikia		
	Hussain		
	Rahman		
	Roy		
	Ray		
	Brahma		
	Bora		
	Hoque		
	Nessa		
	Narzary		
	Borah		
	Pegu		
	Laskar		
	Sheikh		
	Doley		
	Devi		
	Tanti		
	Bewa		
	Biswas		
	Hazarika		
	Dutta		
	Sarkar		
	Sonowal		
	Ahmed		
	Mandal		
}			
