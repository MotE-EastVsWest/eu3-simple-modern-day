# Country Name:	Sistan-Baluchistan	# Tag:	IB9
#			
graphical_culture = 	muslimgfx		
#			
color = {	48 202 184	}	
historical_ideas = {			
}			
historical_units = {			
	napoleonic_square		
	napoleonic_lancers		
	flying_battery		
}			
monarch_names = {			
	"Rigi #0" = 10		
	"Shah #0" = 10		
	"Brahouei #0" = 10		
	"Nouri #0" = 10		
	"Narouei #0" = 10		
	"Baloch #0" = 10		
	"Reyesi #0" = 10		
	"Bameri #0" = 10		
	"Sarani #0" = 10		
	"Mir #0" = 10		
	"Drazadh #0" = 10		
	"Arbabi #0" = 10		
	"Jedghal #0" = 10		
	"Dehvari #0" = 10		
	"Balochi #0" = 10		
	"Sepahi #0" = 10		
	"Shahnavazi #0" = 10		
	"Hossein #0" = 10		
	"Mohammadi #0" = 10		
	"Pudineh #0" = 10		
	"Damni #0" = 10		
	"Rakhshani #0" = 10		
	"Daniel #0" = 10		
	"Sergasi #0" = 10		
	"Hosseini #0" = 10		
	"Dehghani #0" = 10		
	"Chakri #0" = 10		
	"Gorgiyeh #0" = 10		
	"Gargich #0" = 10		
	"Gholami #0" = 10		
	"Gomshadzehi #0" = 10		
	"Hot #0" = 10		
	"Kikha #0" = 10		
	"Jahan #0" = 10		
	"Sanchulli #0" = 10		
	"Malayzai #0" = 10		
	"Heydari #0" = 10		
	"Salari #0" = 10		
	"Mirie #0" = 10		
}			
ship_names = {			
	"Coast Guard"		
}			
leader_names			
	Rigi		
	Shah		
	Brahouei		
	Nouri		
	Narouei		
	Baloch		
	Baloch		
	Reyesi		
	Bameri		
	Sarani		
	Mir		
	Drazadh		
	Shahrakie		
	Arbabi		
	Jedghal		
	Dehvari		
	Balochi		
	Sepahi		
	Shahnavazi		
	Hossein		
	Mohammadi		
	Pudineh		
	Damni		
	Rakhshani		
	Daniel		
	Sergasi		
	Hosseini		
	Dehghani		
	Chakri		
	Gorgiyeh		
	Gargich		
	Gholami		
	Gomshadzehi		
	Hot		
	Kikha		
	Jahan		
	Sanchulli		
	Malayzai		
	Heydari		
	Salari		
	Mirie		
}			
