# Country Name:	Indonesian Rebels	# Tag:	ID8
#			
graphical_culture = 	indiangfx		
#			
color = {	132 104 134	}	
historical_ideas = {			
	vetting		
	national_bank		
	national_trade_policy		
	smithian_economics		
	shrewd_commerce_practise		
	scientific_revolution		
	sea_hawks		
	naval_provisioning		
	regimental_system		
	napoleonic_warfare		
	merchant_adventures		
}			
historical_units = {			
	napoleonic_square		
	napoleonic_lancers		
	flying_battery		
}			
monarch_names = {			
	"Sari #0" = 10		
	"Setiawan #0" = 10		
	"Hidayat #0" = 10		
	"Lestari #0" = 10		
	"Saputra #0" = 10		
	"Wati #0" = 10		
	"Rahayu #0" = 10		
	"Dewi #0" = 10		
	"Kurniawan #0" = 10		
	"Santoso #0" = 10		
	"Putra #0" = 10		
	"Wahyuni #0" = 10		
	"Ningsih #0" = 10		
	"Susanto #0" = 10		
	"Gunawan #0" = 10		
	"Arifin #0" = 10		
	"Siregar #0" = 10		
	"Astuti #0" = 10		
	"Wijaya #0" = 10		
	"Handayani #0" = 10		
	"Rahman #0" = 10		
	"Irawan #0" = 10		
	"Hasanah #0" = 10		
	"Nurhayati #0" = 10		
	"Putri #0" = 10		
	"Wulandari #0" = 10		
	"Wibowo #0" = 10		
	"Aminah #0" = 10		
	"Efendi #0" = 10		
	"Yanti #0" = 10		
	"Maulana #0" = 10		
	"Sinaga #0" = 10		
	"Hadi #0" = 10		
	"Suryani #0" = 10		
	"Fatimah #0" = 10		
	"Wahyudi #0" = 10		
	"Lubis #0" = 10		
	"Pratama #0" = 10		
	"Utami #0" = 10		
}			
ship_names = {			
	"Coast Guard"		
}			
leader_names = {			
	Sari		
	Setiawan		
	Hidayat		
	Lestari		
	Saputra		
	Wati		
	Wati		
	Rahayu		
	Dewi		
	Kurniawan		
	Santoso		
	Putra		
	Susanti		
	Wahyuni		
	Ningsih		
	Susanto		
	Gunawan		
	Arifin		
	Siregar		
	Astuti		
	Wijaya		
	Handayani		
	Rahman		
	Irawan		
	Hasanah		
	Nurhayati		
	Putri		
	Wulandari		
	Wibowo		
	Aminah		
	Efendi		
	Yanti		
	Maulana		
	Sinaga		
	Hadi		
	Suryani		
	Fatimah		
	Wahyudi		
	Lubis		
	Pratama		
	Utami		
}			
