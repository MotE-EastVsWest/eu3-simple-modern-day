# Country Name:	Kurdistan	# Tag:	KD9
#			
graphical_culture = 	muslimgfx		
#			
color = { 32 19  80 }
historical_ideas = {			
	ecumenism		
	church_attendance_duty		
	divine_supremacy		
	deus_vult		
	revolution_and_counter		
	press_gangs		
	national_conscripts		
	napoleonic_warfare		
	vice_roys		
	vetting		
	bureaucracy		
}			
historical_units = {			
	napoleonic_square		
	napoleonic_lancers		
	flying_battery		
}			
monarch_names = {			
	"Demir #0" = 10		
	"celik #0" = 10		
	"Kaya #0" = 10		
	"Aydin #0" = 10		
	"Acar #0" = 10		
	"Dogan #0" = 10		
	"Aslan #0" = 10		
	"Yilmaz #0" = 10		
	"Yildirim #0" = 10		
	"Yildiz #0" = 10		
	"Arslan #0" = 10		
	"Kilic #0" = 10		
	"Tekin #0" = 10		
	"Ay #0" = 10		
	"cetin #0" = 10		
	"Dag #0" = 10		
	"Sahin #0" = 10		
	"Ozturk #0" = 10		
	"Yavuz #0" = 10		
	"Bulut #0" = 10		
	"Aksoy #0" = 10		
	"GumuS #0" = 10		
	"Kilinc #0" = 10		
	"cicek #0" = 10		
	"Deniz #0" = 10		
	"AktaS #0" = 10		
	"Can #0" = 10		
	"Turan #0" = 10		
	"Akan #0" = 10		
	"ErtaS #0" = 10		
	"Akin #0" = 10		
	"Tunc #0" = 10		
	"Ilhan #0" = 10		
	"Agirman #0" = 10		
	"GuneS #0" = 10		
	"Akgul #0" = 10		
	"TaS #0" = 10		
	"Temel #0" = 10		
	"Erdem #0" = 10		
}			
ship_names = {			
	"Coast Guard"		
}			
leader_names = {			
	Demir		
	celik		
	Kaya		
	Aydin		
	Acar		
	Dogan		
	Dogan		
	Aslan		
	Yilmaz		
	Yildirim		
	Yildiz		
	Arslan		
	Ozdemir		
	Kilic		
	Tekin		
	Ay		
	cetin		
	Dag		
	Sahin		
	Ozturk		
	Yavuz		
	Bulut		
	Aksoy		
	GumuS		
	Kilinc		
	cicek		
	Deniz		
	AktaS		
	Can		
	Turan		
	Akan		
	ErtaS		
	Akin		
	Tunc		
	Ilhan		
	Agirman		
	GuneS		
	Akgul		
	TaS		
	Temel		
	Erdem		
}			
