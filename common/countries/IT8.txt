# Country Name:	Italian Rebels	# Tag:	IT8
#			
graphical_culture = 	latingfx		
#			
color = {	95 141 54	}	
historical_ideas = {			
	grand_army		
	glorious_arms		
	merchant_adventures		
	scientific_revolution		
	national_bank		
	bill_of_rights		
	smithian_economics		
	liberty_egalite_fraternity		
	patron_of_art		
	humanist_tolerance		
	cabinet		
}			
historical_units = {			
	napoleonic_square		
	napoleonic_lancers		
	flying_battery		
}			
monarch_names = {			
	"Rossi #0" = 10		
	"Russo #0" = 10		
	"Ferrari #0" = 10		
	"Esposito #0" = 10		
	"Colombo #0" = 10		
	"Bianchi #0" = 10		
	"Romano #0" = 10		
	"Ricci #0" = 10		
	"Gallo #0" = 10		
	"Dal #0" = 10		
	"Bruno #0" = 10		
	"Marino #0" = 10		
	"Conti #0" = 10		
	"Giordano #0" = 10		
	"Rizzo #0" = 10		
	"de Luca #0" = 10		
	"Costa #0" = 10		
	"Mancini #0" = 10		
	"Lombardi #0" = 10		
	"Barbieri #0" = 10		
	"Fontana #0" = 10		
	"Moretti #0" = 10		
	"Mariani #0" = 10		
	"Caruso #0" = 10		
	"Galli #0" = 10		
	"Ferrara #0" = 10		
	"Santoro #0" = 10		
	"Rinaldi #0" = 10		
	"Longo #0" = 10		
	"Villa #0" = 10		
	"Sala #0" = 10		
	"Leone #0" = 10		
	"Martini #0" = 10		
	"Dalla #0" = 10		
	"D'Angelo #0" = 10		
	"Bianco #0" = 10		
	"Martinelli #0" = 10		
	"Gatti #0" = 10		
	"Vitale #0" = 10		
}			
ship_names = {			
	"Coast Guard"		
}			
leader_names = {			
	Rossi		
	Russo		
	Ferrari		
	Esposito		
	Colombo		
	Bianchi		
	Bianchi		
	Romano		
	Ricci		
	Gallo		
	Dal		
	Bruno		
	Greco		
	Marino		
	Conti		
	Giordano		
	Rizzo		
	de Luca		
	Costa		
	Mancini		
	Lombardi		
	Barbieri		
	Fontana		
	Moretti		
	Mariani		
	Caruso		
	Galli		
	Ferrara		
	Santoro		
	Rinaldi		
	Longo		
	Villa		
	Sala		
	Leone		
	Martini		
	Dalla		
	D'Angelo		
	Bianco		
	Martinelli		
	Gatti		
	Vitale		
}			
