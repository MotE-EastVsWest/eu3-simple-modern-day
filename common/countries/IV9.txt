# Country Name:	Ivory Coast	# Tag:	IV9
#			
graphical_culture = 	africangfx		
#			
color = {	125 229 124	}	
historical_ideas = {			
	ecumenism		
	church_attendance_duty		
	divine_supremacy		
	deus_vult		
	revolution_and_counter		
	press_gangs		
	national_conscripts		
	napoleonic_warfare		
	vice_roys		
	vetting		
	bureaucracy		
}			
historical_units = {			
	napoleonic_square		
	napoleonic_lancers		
	flying_battery		
}			
monarch_names = {			
	"Kone #0" = 10		
	"Ouattara #0" = 10		
	"Kouassi #0" = 10		
	"Koffi #0" = 10		
	"Kouadio #0" = 10		
	"Kouame #0" = 10		
	"Yao #0" = 10		
	"Coulibaly #0" = 10		
	"Kouakou #0" = 10		
	"Konan #0" = 10		
	"N'Guessan #0" = 10		
	"Soro #0" = 10		
	"Bamba #0" = 10		
	"Toure #0" = 10		
	"Fofana #0" = 10		
	"Yeo #0" = 10		
	"Diomande #0" = 10		
	"Konate #0" = 10		
	"Silue #0" = 10		
	"Doumbia #0" = 10		
	"Brou #0" = 10		
	"N'Dri #0" = 10		
	"Sanogo #0" = 10		
	"Cisse #0" = 10		
	"Hien #0" = 10		
	"Diarrassouba #0" = 10		
	"Dosso #0" = 10		
	"N'Goran #0" = 10		
	"Sangare #0" = 10		
	"Soumahoro #0" = 10		
	"Bakayoko #0" = 10		
	"Aka #0" = 10		
	"Amani #0" = 10		
	"Sylla #0" = 10		
	"Kra #0" = 10		
	"Kambou #0" = 10		
	"Diabate #0" = 10		
	"Dah #0" = 10		
	"Tuo #0" = 10		
}			
ship_names = {			
	"Coast Guard"		
}			
leader_names = {			
	Kone		
	Ouattara		
	Kouassi		
	Koffi		
	Kouadio		
	Kouame		
	Kouame		
	Yao		
	Coulibaly		
	Kouakou		
	Konan		
	N'Guessan		
	Traore		
	Soro		
	Bamba		
	Toure		
	Fofana		
	Yeo		
	Diomande		
	Konate		
	Silue		
	Doumbia		
	Brou		
	N'Dri		
	Sanogo		
	Cisse		
	Hien		
	Diarrassouba		
	Dosso		
	N'Goran		
	Sangare		
	Soumahoro		
	Bakayoko		
	Aka		
	Amani		
	Sylla		
	Kra		
	Kambou		
	Diabate		
	Dah		
	Tuo		
}			
