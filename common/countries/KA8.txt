# Country Name:	Karelian Rebels	# Tag:	KA8
#			
graphical_culture = 	easterngfx		
#			
color = {	112 47 192	}	
historical_ideas = {			
}			
historical_units = {			
	napoleonic_square		
	napoleonic_lancers		
	flying_battery		
}			
monarch_names = {			
	"Ivanov #0" = 10		
	"Petrov #0" = 10		
	"Smirnov #0" = 10		
	"Bogdanov #0" = 10		
	"Kuznetsov #0" = 10		
	"Pavlov #0" = 10		
	"Vasilev #0" = 10		
	"Popova #0" = 10		
	"Andreeva #0" = 10		
	"Mikhaylova #0" = 10		
	"Makharov #0" = 10		
	"Romanov #0" = 10		
	"Yakovleva #0" = 10		
	"Sergeeva #0" = 10		
	"Nikitina #0" = 10		
	"Sokolova #0" = 10		
	"Vasliev #0" = 10		
	"Egorova #0" = 10		
	"Zakharova #0" = 10		
	"Zaytseva #0" = 10		
	"Andreev #0" = 10		
	"Nikitin #0" = 10		
	"Morozova #0" = 10		
	"Volkova #0" = 10		
	"Novikova #0" = 10		
	"Kuzmina #0" = 10		
	"Egorov #0" = 10		
	"Ilina #0" = 10		
	"Fyodorova #0" = 10		
	"Volkov #0" = 10		
	"Orlova #0" = 10		
	"Antonova #0" = 10		
	"Medvedev #0" = 10		
	"Fomina #0" = 10		
	"Kuzmun #0" = 10		
	"Zaytseva #0" = 10		
	"Titov #0" = 10		
	"Fedotov #0" = 10		
	"Maksimov #0" = 10		
}			
ship_names = {			
	"Coast Guard"		
}			
leader_names			
	Ivanov		
	Petrov		
	Smirnov		
	Bogdanov		
	Kuznetsov		
	Pavlov		
	Pavlov		
	Vasilev		
	Popova		
	Andreeva		
	Mikhaylova		
	Makharov		
	Stepanov		
	Romanov		
	Yakovleva		
	Sergeeva		
	Nikitina		
	Sokolova		
	Vasliev		
	Egorova		
	Zakharova		
	Zaytseva		
	Andreev		
	Nikitin		
	Morozova		
	Volkova		
	Novikova		
	Kuzmina		
	Egorov		
	Ilina		
	Fyodorova		
	Volkov		
	Orlova		
	Antonova		
	Medvedev		
	Fomina		
	Kuzmun		
	Zaytseva		
	Titov		
	Fedotov		
	Maksimov		
}			
