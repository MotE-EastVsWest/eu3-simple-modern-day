# Country Name:	Cambodia	# Tag:	KM9
#			
graphical_culture = 	chinesegfx		
#			
color = { 127  180  60 }	
historical_ideas = {			
	ecumenism		
	church_attendance_duty		
	divine_supremacy		
	deus_vult		
	revolution_and_counter		
	press_gangs		
	national_conscripts		
	napoleonic_warfare		
	vice_roys		
	vetting		
	bureaucracy		
}			
historical_units = {			
	napoleonic_square		
	napoleonic_lancers		
	flying_battery		
}			
monarch_names = {			
	"Sok #0" = 10		
	"Chan #0" = 10		
	"Chea #0" = 10		
	"San #0" = 10		
	"Kong #0" = 10		
	"Sam #0" = 10		
	"Heng #0" = 10		
	"Seng #0" = 10		
	"Li #0" = 10		
	"Phan #0" = 10		
	"Yan #0" = 10		
	"Moau #0" = 10		
	"Chin #0" = 10		
	"Sat #0" = 10		
	"Kim #0" = 10		
	"Lim #0" = 10		
	"Sym #0" = 10		
	"Long #0" = 10		
	"Suan #0" = 10		
	"Mas #0" = 10		
	"Chim #0" = 10		
	"Mom #0" = 10		
	"Cheng #0" = 10		
	"Sun #0" = 10		
	"Touch #0" = 10		
	"Chon #0" = 10		
	"Poe #0" = 10		
	"Uk #0" = 10		
	"Nuon #0" = 10		
	"Ros #0" = 10		
	"Um #0" = 10		
	"Khan #0" = 10		
	"Som #0" = 10		
	"Leng #0" = 10		
	"Sum #0" = 10		
	"Voan #0" = 10		
	"Cum #0" = 10		
	"Sin #0" = 10		
	"An #0" = 10		
}			
ship_names = {			
	"Coast Guard"		
}			
leader_names = {			
	Sok		
	Chan		
	Chea		
	San		
	Kong		
	Sam		
	Sam		
	Heng		
	Seng		
	Li		
	Phan		
	Yan		
	Keo		
	Moau		
	Chin		
	Sat		
	Kim		
	Lim		
	Sym		
	Long		
	Suan		
	Mas		
	Chim		
	Mom		
	Cheng		
	Sun		
	Touch		
	Chon		
	Poe		
	Uk		
	Nuon		
	Ros		
	Um		
	Khan		
	Som		
	Leng		
	Sum		
	Voan		
	Cum		
	Sin		
	An		
}			
