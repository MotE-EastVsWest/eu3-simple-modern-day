# Country Name:	RO Korea	# Tag:	KO9
#			
graphical_culture = 	chinesegfx		
#			
color = { 26  53  177 }
historical_ideas = {			
	vetting		
	national_bank		
	national_trade_policy		
	smithian_economics		
	shrewd_commerce_practise		
	scientific_revolution		
	sea_hawks		
	naval_provisioning		
	regimental_system		
	napoleonic_warfare		
	merchant_adventures		
}			
historical_units = {			
	napoleonic_square		
	napoleonic_lancers		
	flying_battery		
}			
monarch_names = {			
	"Kim #0" = 10		
	"I #0" = 10		
	"Pak #0" = 10		
	"Chong #0" = 10		
	"Choe #0" = 10		
	"Cho #0" = 10		
	"Kang #0" = 10		
	"Yu #0" = 10		
	"Yun #0" = 10		
	"Chang #0" = 10		
	"Im #0" = 10		
	"Song #0" = 10		
	"Chon #0" = 10		
	"So #0" = 10		
	"Han #0" = 10		
	"O #0" = 10		
	"Hwang #0" = 10		
	"Kwon #0" = 10		
	"An #0" = 10		
	"Hong #0" = 10		
	"Yang #0" = 10		
	"Son #0" = 10		
	"Ko #0" = 10		
	"Mun #0" = 10		
	"Pae #0" = 10		
	"Paek #0" = 10		
	"Ho #0" = 10		
	"No #0" = 10		
	"Chu #0" = 10		
	"Nam #0" = 10		
	"Sim #0" = 10		
	"Ha #0" = 10		
	"Ku #0" = 10		
	"Kwak #0" = 10		
	"Cha #0" = 10		
	"U #0" = 10		
	"Na #0" = 10		
	"Chin #0" = 10		
	"Min #0" = 10		
}			
ship_names = {			
	"Coast Guard"		
}			
leader_names = {			
	Kim		
	I		
	Pak		
	Chong		
	Choe		
	Cho		
	Cho		
	Kang		
	Yu		
	Yun		
	Chang		
	Im		
	Sin		
	Song		
	Chon		
	So		
	Han		
	O		
	Hwang		
	Kwon		
	An		
	Hong		
	Yang		
	Son		
	Ko		
	Mun		
	Pae		
	Paek		
	Ho		
	No		
	Chu		
	Nam		
	Sim		
	Ha		
	Ku		
	Kwak		
	Cha		
	U		
	Na		
	Chin		
	Min		
}			
