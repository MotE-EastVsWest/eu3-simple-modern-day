# Country Name:	Karkalpakstani Rebels	# Tag:	KP8
#			
graphical_culture = 	muslimgfx		
#			
color = {	194 195 13	}	
historical_ideas = {			
}			
historical_units = {			
	napoleonic_square		
	napoleonic_lancers		
	flying_battery		
}			
monarch_names = {			
	"Aysary #0" = 10		
	"Baltabay #0" = 10		
	"Biybisara #0" = 10		
	"Gargabay #0" = 10		
	"Geukher #0" = 10		
	"Khinzhigul #0" = 10		
	"Kozybagar #0" = 10		
	"Kunsuluu #0" = 10		
	"Kuuat #0" = 10		
	"Kylyshbay #0" = 10		
	"Miyirgul #0" = 10		
	"Orak #0" = 10		
	"Paluan #0" = 10		
	"Shagalbay #0" = 10		
	"Suluu #0" = 10		
	"Syrga #0" = 10		
	"Tenizbay #0" = 10		
	"Tursyn #0" = 10		
	"Ugylzhan #0" = 10		
	"Zhylkyaydar #0" = 10		
	"Achagul #0" = 10		
	"Abida #0" = 10		
	"Afsona #0" = 10		
	"Adiljan #0" = 10		
	"Ahdiya #0" = 10		
	"Avaz #0" = 10		
	"Bakhriddin #0" = 10		
	"Balqi #0" = 10		
	"Chig'atoy #0" = 10		
	"Cho'qmor #0" = 10		
	"Cho'yan #0" = 10		
	"Dilkash #0" = 10		
	"Gulbonu #0" = 10		
	"Gulbot'son #0" = 10		
	"Tandor #0" = 10		
	"Tanovar #0" = 10		
	"Tansiqa #0" = 10		
	"Tarbiya #0" = 10		
	"Tuzan #0" = 10		
}			
ship_names = {			
	"Coast Guard"		
}			
leader_names			
	Aysary		
	Baltabay		
	Biybisara		
	Gargabay		
	Geukher		
	Khinzhigul		
	Khinzhigul		
	Kozybagar		
	Kunsuluu		
	Kuuat		
	Kylyshbay		
	Miyirgul		
	Nayzabay		
	Orak		
	Paluan		
	Shagalbay		
	Suluu		
	Syrga		
	Tenizbay		
	Tursyn		
	Ugylzhan		
	Zhylkyaydar		
	Achagul		
	Abida		
	Afsona		
	Adiljan		
	Ahdiya		
	Avaz		
	Bakhriddin		
	Balqi		
	Chig'atoy		
	Cho'qmor		
	Cho'yan		
	Dilkash		
	Gulbonu		
	Gulbot'son		
	Tandor		
	Tanovar		
	Tansiqa		
	Tarbiya		
	Tuzan		
}			
