# Country Name:	Kazakhstan	# Tag:	KZ9
#			
graphical_culture = 	easterngfx		
#			
color = { 112  194  63 }	
historical_ideas = {			
	ecumenism		
	church_attendance_duty		
	divine_supremacy		
	deus_vult		
	revolution_and_counter		
	press_gangs		
	national_conscripts		
	napoleonic_warfare		
	vice_roys		
	vetting		
	bureaucracy		
}			
historical_units = {			
	napoleonic_square		
	napoleonic_lancers		
	flying_battery		
}			
monarch_names = {			
	"Kenes #0" = 10		
	"Omarov #0" = 10		
	"Sultanov #0" = 10		
	"Alieva #0" = 10		
	"Barat #0" = 10		
	"Akhmetov #0" = 10		
	"Umarov #0" = 10		
	"Belgibaev #0" = 10		
	"Akhmetova #0" = 10		
	"Kemelov #0" = 10		
	"Bolatbek #0" = 10		
	"Ivanova #0" = 10		
	"Kambarov #0" = 10		
	"Iskakova #0" = 10		
	"Boyko #0" = 10		
	"Smagulov #0" = 10		
	"Aliev #0" = 10		
	"Omarova #0" = 10		
	"Ospanova #0" = 10		
	"Smagulova #0" = 10		
	"Ospanov #0" = 10		
	"Serikov #0" = 10		
	"Tsoy #0" = 10		
	"Suleymenov #0" = 10		
	"Karimov #0" = 10		
	"Ibragimov #0" = 10		
	"Bondarenko #0" = 10		
	"Eleusiz #0" = 10		
	"Asanova #0" = 10		
	"Petrova #0" = 10		
	"Kaliev #0" = 10		
	"Nigmashev #0" = 10		
	"Shevchenko #0" = 10		
	"Popov #0" = 10		
	"Valieva #0" = 10		
	"Suleymenova #0" = 10		
	"Muratov #0" = 10		
	"Romanyuk #0" = 10		
	"Asanov #0" = 10		
}			
ship_names = {			
	"Coast Guard"		
}			
leader_names = {			
	Kenes		
	Omarov		
	Sultanov		
	Alieva		
	Barat		
	Akhmetov		
	Akhmetov		
	Umarov		
	Belgibaev		
	Akhmetova		
	Kemelov		
	Bolatbek		
	Ivanov		
	Ivanova		
	Kambarov		
	Iskakova		
	Boyko		
	Smagulov		
	Aliev		
	Omarova		
	Ospanova		
	Smagulova		
	Ospanov		
	Serikov		
	Tsoy		
	Suleymenov		
	Karimov		
	Ibragimov		
	Bondarenko		
	Eleusiz		
	Asanova		
	Petrova		
	Kaliev		
	Nigmashev		
	Shevchenko		
	Popov		
	Valieva		
	Suleymenova		
	Muratov		
	Romanyuk		
	Asanov		
}			
