# Country Name:	Somalia	# Tag:	LA9
#			
graphical_culture = 	africangfx		
#			
color = {	106 238 78	}	
historical_ideas = {			
	ecumenism		
	church_attendance_duty		
	divine_supremacy		
	deus_vult		
	revolution_and_counter		
	press_gangs		
	national_conscripts		
	napoleonic_warfare		
	vice_roys		
	vetting		
	bureaucracy		
}			
historical_units = {			
	napoleonic_square		
	napoleonic_lancers		
	flying_battery		
}			
monarch_names = {			
	"Ali #0" = 10		
	"Abdi #0" = 10		
	"Mohamed #0" = 10		
	"Ahmed #0" = 10		
	"Hassan #0" = 10		
	"Aden #0" = 10		
	"Abdullahi #0" = 10		
	"Ibrahim #0" = 10		
	"Farah #0" = 10		
	"Omar #0" = 10		
	"Osman #0" = 10		
	"Noor #0" = 10		
	"Dahir #0" = 10		
	"Mahamed #0" = 10		
	"Mohamud #0" = 10		
	"Yusuf #0" = 10		
	"Yussuf #0" = 10		
	"Adan #0" = 10		
	"Jama #0" = 10		
	"Muse #0" = 10		
	"Ismail #0" = 10		
	"Bare #0" = 10		
	"Haji #0" = 10		
	"Abdulahi #0" = 10		
	"Sheikh #0" = 10		
	"Abdirahman #0" = 10		
	"Warsame #0" = 10		
	"Bashir #0" = 10		
	"Daud #0" = 10		
	"Maxamed #0" = 10		
	"Mohmed #0" = 10		
	"Jamac #0" = 10		
	"Hussien #0" = 10		
	"Khalif #0" = 10		
	"Cali #0" = 10		
	"Muhumed #0" = 10		
	"Gedi #0" = 10		
	"Issack #0" = 10		
	"Salah #0" = 10		
}			
ship_names = {			
	"Coast Guard"		
}			
leader_names = {			
	Ali		
	Abdi		
	Mohamed		
	Ahmed		
	Hassan		
	Aden		
	Aden		
	Abdullahi		
	Ibrahim		
	Farah		
	Omar		
	Osman		
	Hussein		
	Noor		
	Dahir		
	Mahamed		
	Mohamud		
	Yusuf		
	Yussuf		
	Adan		
	Jama		
	Muse		
	Ismail		
	Bare		
	Haji		
	Abdulahi		
	Sheikh		
	Abdirahman		
	Warsame		
	Bashir		
	Daud		
	Maxamed		
	Mohmed		
	Jamac		
	Hussien		
	Khalif		
	Cali		
	Muhumed		
	Gedi		
	Issack		
	Salah		
}			
