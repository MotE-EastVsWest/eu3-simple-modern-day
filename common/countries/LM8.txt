# Country Name:	Solomon Islander Rebels	# Tag:	LM8
#			
graphical_culture = 	northamericagfx		
#			
color = {	170 162 181	}	
historical_ideas = {			
	press_gangs		
	grand_navy		
	sea_hawks		
	superior_seamanship		
	naval_glory		
	excellent_shipwrights		
	naval_fighting_instruction		
	naval_provisioning		
	shrewd_commerce_practise		
	merchant_adventures		
	improved_foraging		
}			
historical_units = {			
	napoleonic_square		
	napoleonic_lancers		
	flying_battery		
}			
monarch_names = {			
	"Mae #0" = 10		
	"Ramo #0" = 10		
	"Sau #0" = 10		
	"Taro #0" = 10		
	"Iro #0" = 10		
	"Sale #0" = 10		
	"Hou #0" = 10		
	"John #0" = 10		
	"Leua #0" = 10		
	"Billy #0" = 10		
	"Haga #0" = 10		
	"Bosa #0" = 10		
	"Wale #0" = 10		
	"Sade #0" = 10		
	"Sikua #0" = 10		
	"Wate #0" = 10		
	"Peter #0" = 10		
	"Mani #0" = 10		
	"Talo #0" = 10		
	"Au #0" = 10		
	"Ben #0" = 10		
	"Mary #0" = 10		
	"Jack #0" = 10		
	"Lui #0" = 10		
	"Ngelea #0" = 10		
	"Manu #0" = 10		
	"James #0" = 10		
	"Sam #0" = 10		
	"Junior #0" = 10		
	"Para #0" = 10		
	"Samo #0" = 10		
	"Pegoa #0" = 10		
	"Samani #0" = 10		
	"Ata #0" = 10		
	"Poloso #0" = 10		
	"Mark #0" = 10		
	"Paul #0" = 10		
	"George #0" = 10		
	"Kere #0" = 10		
}			
ship_names = {			
	"Coast Guard"		
}			
leader_names = {			
	Mae		
	Ramo		
	Sau		
	Taro		
	Iro		
	Sale		
	Sale		
	Hou		
	John		
	Leua		
	Billy		
	Haga		
	Tome		
	Bosa		
	Wale		
	Sade		
	Sikua		
	Wate		
	Peter		
	Mani		
	Talo		
	Au		
	Ben		
	Mary		
	Jack		
	Lui		
	Ngelea		
	Manu		
	James		
	Sam		
	Junior		
	Para		
	Samo		
	Pegoa		
	Samani		
	Ata		
	Poloso		
	Mark		
	Paul		
	George		
	Kere		
}			
