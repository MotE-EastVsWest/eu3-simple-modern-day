# Country Name:	Sri Lanka	# Tag:	LN9
#			
graphical_culture = 	indiangfx		
#			
color = { 60  123  65 }	
historical_ideas = {			
	ecumenism		
	church_attendance_duty		
	divine_supremacy		
	deus_vult		
	revolution_and_counter		
	press_gangs		
	national_conscripts		
	napoleonic_warfare		
	vice_roys		
	vetting		
	bureaucracy		
}			
historical_units = {			
	napoleonic_square		
	napoleonic_lancers		
	flying_battery		
}			
monarch_names = {			
	"Perera #0" = 10		
	"Fernando #0" = 10		
	"de Silva #0" = 10		
	"Bandara #0" = 10		
	"Kumara #0" = 10		
	"Dissanayake #0" = 10		
	"Mohamed #0" = 10		
	"Gamage #0" = 10		
	"Liyanage #0" = 10		
	"Jayasinghe #0" = 10		
	"Ranasinghe #0" = 10		
	"Weerasinghe #0" = 10		
	"Peiris #0" = 10		
	"Rathnayake #0" = 10		
	"Wickramasinghe #0" = 10		
	"Wijesinghe #0" = 10		
	"Hettiarachchi #0" = 10		
	"Nanayakkara #0" = 10		
	"Ahamed #0" = 10		
	"Rajapaksha #0" = 10		
	"Mendis #0" = 10		
	"Pathirana #0" = 10		
	"Ekanayake #0" = 10		
	"Gunasekara #0" = 10		
	"Dias #0" = 10		
	"Sampath #0" = 10		
	"Amarasinghe #0" = 10		
	"Ratnayake #0" = 10		
	"Chathuranga #0" = 10		
	"Senanayake #0" = 10		
	"Samarasinghe #0" = 10		
	"Lakmal #0" = 10		
	"Munasinghe #0" = 10		
	"Rodrigo #0" = 10		
	"Seneviratne #0" = 10		
	"Rathnayaka #0" = 10		
	"Edirisinghe #0" = 10		
	"Jayawardena #0" = 10		
	"Fonseka #0" = 10		
}			
ship_names = {			
	"Coast Guard"		
}			
leader_names = {			
	Perera		
	Fernando		
	de Silva		
	Bandara		
	Kumara		
	Dissanayake		
	Dissanayake		
	Mohamed		
	Gamage		
	Liyanage		
	Jayasinghe		
	Ranasinghe		
	Herath		
	Weerasinghe		
	Peiris		
	Rathnayake		
	Wickramasinghe		
	Wijesinghe		
	Hettiarachchi		
	Nanayakkara		
	Ahamed		
	Rajapaksha		
	Mendis		
	Pathirana		
	Ekanayake		
	Gunasekara		
	Dias		
	Sampath		
	Amarasinghe		
	Ratnayake		
	Chathuranga		
	Senanayake		
	Samarasinghe		
	Lakmal		
	Munasinghe		
	Rodrigo		
	Seneviratne		
	Rathnayaka		
	Edirisinghe		
	Jayawardena		
	Fonseka		
}			
