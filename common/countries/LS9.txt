# Country Name:	Lesotho	# Tag:	LS9
#			
graphical_culture = 	africangfx		
#			
color = {	174 64 214	}	
historical_ideas = {			
	ecumenism		
	church_attendance_duty		
	divine_supremacy		
	deus_vult		
	revolution_and_counter		
	press_gangs		
	national_conscripts		
	napoleonic_warfare		
	vice_roys		
	vetting		
	bureaucracy		
}			
historical_units = {			
	napoleonic_square		
	napoleonic_lancers		
	flying_battery		
}			
monarch_names = {			
	"Mohapi #0" = 10		
	"Sello #0" = 10		
	"Mohale #0" = 10		
	"Makhetha #0" = 10		
	"Pitso #0" = 10		
	"Tlali #0" = 10		
	"Molise #0" = 10		
	"Thamae #0" = 10		
	"Letsie #0" = 10		
	"Molapo #0" = 10		
	"Mokone #0" = 10		
	"Matsoso #0" = 10		
	"Moeketsi #0" = 10		
	"Motsamai #0" = 10		
	"Mokete #0" = 10		
	"Mokhethi #0" = 10		
	"Majoro #0" = 10		
	"Moshoeshoe #0" = 10		
	"Mokhele #0" = 10		
	"Posholi #0" = 10		
	"Makara #0" = 10		
	"Moletsane #0" = 10		
	"Makhele #0" = 10		
	"Mosala #0" = 10		
	"Moeti #0" = 10		
	"Sekonyela #0" = 10		
	"Tau #0" = 10		
	"Motseki #0" = 10		
	"Sehloho #0" = 10		
	"Monaheng #0" = 10		
	"Malefane #0" = 10		
	"Mokoena #0" = 10		
	"Monyane #0" = 10		
	"Moleko #0" = 10		
	"Moleleki #0" = 10		
	"Moloi #0" = 10		
	"Mohlomi #0" = 10		
	"Mofolo #0" = 10		
	"Molefe #0" = 10		
}			
ship_names = {			
	"Coast Guard"		
}			
leader_names = {			
	Mohapi		
	Sello		
	Mohale		
	Makhetha		
	Pitso		
	Tlali		
	Tlali		
	Molise		
	Thamae		
	Letsie		
	Molapo		
	Mokone		
	Molefi		
	Matsoso		
	Moeketsi		
	Motsamai		
	Mokete		
	Mokhethi		
	Majoro		
	Moshoeshoe		
	Mokhele		
	Posholi		
	Makara		
	Moletsane		
	Makhele		
	Mosala		
	Moeti		
	Sekonyela		
	Tau		
	Motseki		
	Sehloho		
	Monaheng		
	Malefane		
	Mokoena		
	Monyane		
	Moleko		
	Moleleki		
	Moloi		
	Mohlomi		
	Mofolo		
	Molefe		
}			
