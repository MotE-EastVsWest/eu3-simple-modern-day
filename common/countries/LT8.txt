# Country Name:	Latvian Rebels	# Tag:	LT8
#			
graphical_culture = 	latingfx		
#			
color = {	149 0 70	}	
historical_ideas = {			
	press_gangs		
	grand_navy		
	sea_hawks		
	superior_seamanship		
	naval_glory		
	excellent_shipwrights		
	naval_fighting_instruction		
	naval_provisioning		
	shrewd_commerce_practise		
	merchant_adventures		
	improved_foraging		
}			
historical_units = {			
	napoleonic_square		
	napoleonic_lancers		
	flying_battery		
}			
monarch_names = {			
	"Berzina #0" = 10		
	"Ozola #0" = 10		
	"Ivanova #0" = 10		
	"Kalnina #0" = 10		
	"Ozolina #0" = 10		
	"Berzins #0" = 10		
	"Ozols #0" = 10		
	"Jansone #0" = 10		
	"Kalnins #0" = 10		
	"Ivanovs #0" = 10		
	"Ozolins #0" = 10		
	"Eglite #0" = 10		
	"Arpa #0" = 10		
	"Balode #0" = 10		
	"Krumina #0" = 10		
	"Andersone #0" = 10		
	"Vasiljeva #0" = 10		
	"Dreimanis #0" = 10		
	"Jansons #0" = 10		
	"Rudzite #0" = 10		
	"Petrova #0" = 10		
	"Vasiljevs #0" = 10		
	"Liepins #0" = 10		
	"Zarina #0" = 10		
	"Krumins #0" = 10		
	"Vanaga #0" = 10		
	"Smirnova #0" = 10		
	"Smirnovs #0" = 10		
	"Petersons #0" = 10		
	"Vitola #0" = 10		
	"Balodis #0" = 10		
	"Petersone #0" = 10		
	"Smite #0" = 10		
	"Karklins #0" = 10		
	"Karklina #0" = 10		
	"Fjodorova #0" = 10		
	"Petrovs #0" = 10		
	"Liepa #0" = 10		
	"Klavina #0" = 10		
}			
ship_names = {			
	"Coast Guard"		
}		
leader_names = {		
	Berzina		
	Ozola		
	Ivanova		
	Kalnina		
	Ozolina		
	Berzins		
	Berzins		
	Ozols		
	Jansone		
	Kalnins		
	Ivanovs		
	Ozolins		
	Liepina		
	Eglite		
	Arpa		
	Balode		
	Krumina		
	Andersone		
	Vasiljeva		
	Dreimanis		
	Jansons		
	Rudzite		
	Petrova		
	Vasiljevs		
	Liepins		
	Zarina		
	Krumins		
	Vanaga		
	Smirnova		
	Smirnovs		
	Petersons		
	Vitola		
	Balodis		
	Petersone		
	Smite		
	Karklins		
	Karklina		
	Fjodorova		
	Petrovs		
	Liepa		
	Klavina		
}			
