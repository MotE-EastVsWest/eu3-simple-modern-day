# Country Name:	Madagascar	# Tag:	MG9
#			
graphical_culture = 	africangfx		
#			
color = {	245 193 60	}	
historical_ideas = {			
	press_gangs		
	grand_navy		
	sea_hawks		
	superior_seamanship		
	naval_glory		
	excellent_shipwrights		
	naval_fighting_instruction		
	naval_provisioning		
	shrewd_commerce_practise		
	merchant_adventures		
	improved_foraging		
}			
historical_units = {			
	napoleonic_square		
	napoleonic_lancers		
	flying_battery		
}			
monarch_names = {			
	"Rakotomalala #0" = 10		
	"Rakotonirina #0" = 10		
	"Rakotoarisoa #0" = 10		
	"Rafanomezantsoa #0" = 10		
	"Rakoto #0" = 10		
	"Rakotoniaina #0" = 10		
	"Randrianasolo #0" = 10		
	"Razafindrakoto #0" = 10		
	"Raharimalala #0" = 10		
	"Rakotoarimanana #0" = 10		
	"Ranaivoson #0" = 10		
	"Randrianantenaina #0" = 10		
	"Randrianarisoa #0" = 10		
	"Rakotoarison #0" = 10		
	"Randrianarison #0" = 10		
	"Andrianantenaina #0" = 10		
	"Rakotovao #0" = 10		
	"Rakotoson #0" = 10		
	"Rakotoarivelo #0" = 10		
	"Nomenjanahary #0" = 10		
	"Razafimandimby #0" = 10		
	"Raharison #0" = 10		
	"Ramaroson #0" = 10		
	"Ramanantsoa #0" = 10		
	"Andrianirina #0" = 10		
	"Razafimahatratra #0" = 10		
	"Randriamanantena #0" = 10		
	"Ramamonjisoa #0" = 10		
	"Raveloson #0" = 10		
	"Rajaonarison #0" = 10		
	"Rasoarimalala #0" = 10		
	"Randrianarivelo #0" = 10		
	"Randrianirina #0" = 10		
	"Raharinirina #0" = 10		
	"Rakotobe #0" = 10		
	"Rakotomanana #0" = 10		
	"Rakotondramanana #0" = 10		
	"Rakotondrasoa #0" = 10		
	"Rakotoarivony #0" = 10		
}			
ship_names = {			
	"Coast Guard"		
}			
leader_names = {			
	Rakotomalala		
	Rakotonirina		
	Rakotoarisoa		
	Rafanomezantsoa		
	Rakoto		
	Rakotoniaina		
	Rakotoniaina		
	Randrianasolo		
	Razafindrakoto		
	Raharimalala		
	Rakotoarimanana		
	Ranaivoson		
	Ratsimbazafy		
	Randrianantenaina		
	Randrianarisoa		
	Rakotoarison		
	Randrianarison		
	Andrianantenaina		
	Rakotovao		
	Rakotoson		
	Rakotoarivelo		
	Nomenjanahary		
	Razafimandimby		
	Raharison		
	Ramaroson		
	Ramanantsoa		
	Andrianirina		
	Razafimahatratra		
	Randriamanantena		
	Ramamonjisoa		
	Raveloson		
	Rajaonarison		
	Rasoarimalala		
	Randrianarivelo		
	Randrianirina		
	Raharinirina		
	Rakotobe		
	Rakotomanana		
	Rakotondramanana		
	Rakotondrasoa		
	Rakotoarivony		
}			
