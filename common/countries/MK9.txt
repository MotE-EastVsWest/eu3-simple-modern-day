# Country Name:	Macedonia	# Tag:	MK9
#			
graphical_culture = 	easterngfx		
#			
color = {	138 135 179	}	
historical_ideas = {			
	ecumenism		
	church_attendance_duty		
	divine_supremacy		
	deus_vult		
	revolution_and_counter		
	press_gangs		
	national_conscripts		
	napoleonic_warfare		
	vice_roys		
	vetting		
	bureaucracy		
}			
historical_units = {			
	napoleonic_square		
	napoleonic_lancers		
	flying_battery		
}			
monarch_names = {			
	"Stojanovski #0" = 10		
	"Jovanovska #0" = 10		
	"Jovanovski #0" = 10		
	"Stojanovska #0" = 10		
	"Nikolovska #0" = 10		
	"Nikolovski #0" = 10		
	"Trajkovski #0" = 10		
	"Stojanova #0" = 10		
	"Trajkovska #0" = 10		
	"Ramadani #0" = 10		
	"Stojanov #0" = 10		
	"Petrovska #0" = 10		
	"Krstevski #0" = 10		
	"Gjorgjievska #0" = 10		
	"Ilievska #0" = 10		
	"Jovanov #0" = 10		
	"Krstevska #0" = 10		
	"Bajrami #0" = 10		
	"Atanasov #0" = 10		
	"Jovanova #0" = 10		
	"Dimovska #0" = 10		
	"Nikolova #0" = 10		
	"Ristovski #0" = 10		
	"Nikolov #0" = 10		
	"Petrovski #0" = 10		
	"RedZepi #0" = 10		
	"Stojkovska #0" = 10		
	"Gjorgjievski #0" = 10		
	"Cvetkovski #0" = 10		
	"Petrov #0" = 10		
	"sabani #0" = 10		
	"Ristovska #0" = 10		
	"Ademi #0" = 10		
	"Sulejmani #0" = 10		
	"Petrova #0" = 10		
	"Spasovski #0" = 10		
	"Ivanovska #0" = 10		
	"Todorovska #0" = 10		
	"Spasovska #0" = 10		
}			
ship_names = {			
	"Coast Guard"		
}			
leader_names = {			
	Stojanovski		
	Jovanovska		
	Jovanovski		
	Stojanovska		
	Nikolovska		
	Nikolovski		
	Nikolovski		
	Trajkovski		
	Stojanova		
	Trajkovska		
	Ramadani		
	Stojanov		
	Atanasova		
	Petrovska		
	Krstevski		
	Gjorgjievska		
	Ilievska		
	Jovanov		
	Krstevska		
	Bajrami		
	Atanasov		
	Jovanova		
	Dimovska		
	Nikolova		
	Ristovski		
	Nikolov		
	Petrovski		
	RedZepi		
	Stojkovska		
	Gjorgjievski		
	Cvetkovski		
	Petrov		
	sabani		
	Ristovska		
	Ademi		
	Sulejmani		
	Petrova		
	Spasovski		
	Ivanovska		
	Todorovska		
	Spasovska		
}			
