# Country Name:	Malta	# Tag:	ML9
#			
graphical_culture = 	latingfx		
#			
color = {	109 151 67	}	
historical_ideas = {			
	press_gangs		
	grand_navy		
	sea_hawks		
	superior_seamanship		
	naval_glory		
	excellent_shipwrights		
	naval_fighting_instruction		
	naval_provisioning		
	shrewd_commerce_practise		
	merchant_adventures		
	improved_foraging		
}			
historical_units = {			
	napoleonic_square		
	napoleonic_lancers		
	flying_battery		
}			
monarch_names = {			
	"Borg #0" = 10		
	"Vella #0" = 10		
	"Camilleri #0" = 10		
	"Farrugia #0" = 10		
	"Zammit #0" = 10		
	"Galea #0" = 10		
	"Micallef #0" = 10		
	"Grech #0" = 10		
	"Attard #0" = 10		
	"Cassar #0" = 10		
	"Spiteri #0" = 10		
	"Mifsud #0" = 10		
	"Caruana #0" = 10		
	"Muscat #0" = 10		
	"Agius #0" = 10		
	"Pace #0" = 10		
	"Fenech #0" = 10		
	"Schembri #0" = 10		
	"Aela #0" = 10		
	"Sammut #0" = 10		
	"Gatt #0" = 10		
	"Gauci #0" = 10		
	"Debono #0" = 10		
	"Bugeja #0" = 10		
	"Vassallo #0" = 10		
	"Aquilina #0" = 10		
	"Bonnici #0" = 10		
	"Portelli #0" = 10		
	"Calleja #0" = 10		
	"Scicluna #0" = 10		
	"Cutajar #0" = 10		
	"Falzon #0" = 10		
	"Grima #0" = 10		
	"Mallia #0" = 10		
	"Ellul #0" = 10		
	"Saliba #0" = 10		
	"Bonello #0" = 10		
	"Buttigieg #0" = 10		
	"Xuareb #0" = 10		
}			
ship_names = {			
	"Coast Guard"		
}			
leader_names = {			
	Borg		
	Vella		
	Camilleri		
	Farrugia		
	Zammit		
	Galea		
	Galea		
	Micallef		
	Grech		
	Attard		
	Cassar		
	Spiteri		
	Azzopardi		
	Mifsud		
	Caruana		
	Muscat		
	Agius		
	Pace		
	Fenech		
	Schembri		
	Aela		
	Sammut		
	Gatt		
	Gauci		
	Debono		
	Bugeja		
	Vassallo		
	Aquilina		
	Bonnici		
	Portelli		
	Calleja		
	Scicluna		
	Cutajar		
	Falzon		
	Grima		
	Mallia		
	Ellul		
	Saliba		
	Bonello		
	Buttigieg		
	Xuareb		
}			
