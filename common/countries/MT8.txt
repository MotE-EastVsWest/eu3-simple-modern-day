# Country Name:	Martiniquais Rebels	# Tag:	MT8
#			
graphical_culture = 	africangfx		
#			
color = {	133 66 75	}	
historical_ideas = {			
	grand_army		
	glorious_arms		
	merchant_adventures		
	scientific_revolution		
	national_bank		
	bill_of_rights		
	smithian_economics		
	liberty_egalite_fraternity		
	patron_of_art		
	humanist_tolerance		
	cabinet		
}			
historical_units = {			
	napoleonic_square		
	napoleonic_lancers		
	flying_battery		
}			
monarch_names = {			
	"Jean #0" = 10		
	"Marignan #0" = 10		
	"Marie-Sainte #0" = 10		
	"Jean-Baptiste #0" = 10		
	"Henriol #0" = 10		
	"Mormin #0" = 10		
	"Charles #0" = 10		
	"Nestoret #0" = 10		
	"Babin #0" = 10		
	"Louis #0" = 10		
	"Maizeroi #0" = 10		
	"Michel #0" = 10		
	"Fordant #0" = 10		
	"Joseph #0" = 10		
	"Serbin #0" = 10		
	"Jean-Louis #0" = 10		
	"Martial #0" = 10		
	"Nuissier #0" = 10		
	"Minin #0" = 10		
	"Jean-Marie #0" = 10		
	"Montabord #0" = 10		
	"Daclinat #0" = 10		
	"Jean-Charles #0" = 10		
	"Servius #0" = 10		
	"Amable #0" = 10		
	"Sainte-Rose #0" = 10		
	"Etienne #0" = 10		
	"Marie-Joseph #0" = 10		
	"Moustin #0" = 10		
	"Orville #0" = 10		
	"Jacques #0" = 10		
	"Nallamoutou #0" = 10		
	"Palmont #0" = 10		
	"Marie-Louise #0" = 10		
	"Pain #0" = 10		
	"Jubenot #0" = 10		
	"Louis-Joseph #0" = 10		
	"Bernard #0" = 10		
	"Etinof #0" = 10		
}			
ship_names = {			
	"Coast Guard"		
}			
leader_names = {			
	Jean		
	Marignan		
	Marie-Sainte		
	Jean-Baptiste		
	Henriol		
	Mormin		
	Mormin		
	Charles		
	Nestoret		
	Babin		
	Louis		
	Maizeroi		
	Marie		
	Michel		
	Fordant		
	Joseph		
	Serbin		
	Jean-Louis		
	Martial		
	Nuissier		
	Minin		
	Jean-Marie		
	Montabord		
	Daclinat		
	Jean-Charles		
	Servius		
	Amable		
	Sainte-Rose		
	Etienne		
	Marie-Joseph		
	Moustin		
	Orville		
	Jacques		
	Nallamoutou		
	Palmont		
	Marie-Louise		
	Pain		
	Jubenot		
	Louis-Joseph		
	Bernard		
	Etinof		
}			
