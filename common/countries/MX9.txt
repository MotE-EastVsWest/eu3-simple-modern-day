# Country Name:	Mexico	# Tag:	MX9
#			
graphical_culture = 	southamericagfx		
#			
color = { 219  124  139 }
historical_ideas = {			
	grand_army		
	glorious_arms		
	sea_hawks		
	scientific_revolution		
	national_bank		
	bill_of_rights		
	smithian_economics		
	liberty_egalite_fraternity		
	patron_of_art		
	humanist_tolerance		
	cabinet		
}			
historical_units = {			
	napoleonic_square		
	napoleonic_lancers		
	flying_battery		
}			
monarch_names = {			
	"Hernandez #0" = 10		
	"Garcia #0" = 10		
	"Martinez #0" = 10		
	"Lopez #0" = 10		
	"Gonzalez #0" = 10		
	"Perez #0" = 10		
	"Rodriguez #0" = 10		
	"Sanchez #0" = 10		
	"Ramirez #0" = 10		
	"Cruz #0" = 10		
	"Flores #0" = 10		
	"Morales #0" = 10		
	"Vazquez #0" = 10		
	"Reyes #0" = 10		
	"Jimenez #0" = 10		
	"Torres #0" = 10		
	"Diaz #0" = 10		
	"Gutierrez #0" = 10		
	"Ruiz #0" = 10		
	"Mendoza #0" = 10		
	"Aguilar #0" = 10		
	"Ortiz #0" = 10		
	"Moreno #0" = 10		
	"Castillo #0" = 10		
	"Romero #0" = 10		
	"Alvarez #0" = 10		
	"Mendez #0" = 10		
	"Chavez #0" = 10		
	"Rivera #0" = 10		
	"Juarez #0" = 10		
	"Ramos #0" = 10		
	"Dominguez #0" = 10		
	"Herrera #0" = 10		
	"Medina #0" = 10		
	"Castro #0" = 10		
	"Vargas #0" = 10		
	"Guzman #0" = 10		
	"Velazquez #0" = 10		
	"Munoz #0" = 10		
}			
ship_names = {			
	"Coast Guard"		
}			
leader_names = {			
	Hernandez		
	Garcia		
	Martinez		
	Lopez		
	Gonzalez		
	Perez		
	Perez		
	Rodriguez		
	Sanchez		
	Ramirez		
	Cruz		
	Flores		
	Gomez		
	Morales		
	Vazquez		
	Reyes		
	Jimenez		
	Torres		
	Diaz		
	Gutierrez		
	Ruiz		
	Mendoza		
	Aguilar		
	Ortiz		
	Moreno		
	Castillo		
	Romero		
	Alvarez		
	Mendez		
	Chavez		
	Rivera		
	Juarez		
	Ramos		
	Dominguez		
	Herrera		
	Medina		
	Castro		
	Vargas		
	Guzman		
	Velazquez		
	Munoz		
}			
