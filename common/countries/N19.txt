# Country Name:	Chin	# Tag:	N19
#			
graphical_culture = 	chinesegfx		
#			
color = {	139 151 43	}	
historical_ideas = {			
	grand_army		
	glorious_arms		
	merchant_adventures		
	scientific_revolution		
	national_bank		
	bill_of_rights		
	smithian_economics		
	liberty_egalite_fraternity		
	patron_of_art		
	humanist_tolerance		
	cabinet		
}			
historical_units = {			
	napoleonic_square		
	napoleonic_lancers		
	flying_battery		
}			
monarch_names = {			
	"Htike #0" = 10		
	"Thi #0" = 10		
	"Hla #0" = 10		
	"Le #0" = 10		
	"Sandar #0" = 10		
	"Nyo #0" = 10		
	"Chit #0" = 10		
	"Kathy #0" = 10		
	"Omar #0" = 10		
	"Toe #0" = 10		
	"Nge #0" = 10		
	"Poe #0" = 10		
	"Thwe #0" = 10		
	"Naung #0" = 10		
	"Mo #0" = 10		
	"Minn #0" = 10		
	"Khant #0" = 10		
	"Eaindra #0" = 10		
	"Thida #0" = 10		
	"Nyi #0" = 10		
	"Cora #0" = 10		
	"My #0" = 10		
	"Thaw #0" = 10		
	"Shein #0" = 10		
	"Kham #0" = 10		
	"Yan #0" = 10		
	"Thiha #0" = 10		
	"Sanda #0" = 10		
	"Te #0" = 10		
	"Thar #0" = 10		
	"Thwin #0" = 10		
	"Nam #0" = 10		
	"Lian #0" = 10		
	"Pwint #0" = 10		
	"Sun #0" = 10		
	"Thura #0" = 10		
	"Amy #0" = 10		
	"Ya #0" = 10		
	"U #0" = 10		
}			
ship_names = {			
	"Coast Guard"		
}			
leader_names = {			
	Htike		
	Thi		
	Hla		
	Le		
	Sandar		
	Nyo		
	Nyo		
	Chit		
	Kathy		
	Omar		
	Toe		
	Nge		
	Lynn		
	Poe		
	Thwe		
	Naung		
	Mo		
	Minn		
	Khant		
	Eaindra		
	Thida		
	Nyi		
	Cora		
	My		
	Thaw		
	Shein		
	Kham		
	Yan		
	Thiha		
	Sanda		
	Te		
	Thar		
	Thwin		
	Nam		
	Lian		
	Pwint		
	Sun		
	Thura		
	Amy		
	Ya		
	U		
}			
