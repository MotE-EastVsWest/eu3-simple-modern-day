# Country Name:	PR Chinese Rebels	# Tag:	NA8
#			
graphical_culture = 	chinesegfx		
#			
color = {	90 33 52	}	
historical_ideas = {			
	ecumenism		
	church_attendance_duty		
	divine_supremacy		
	deus_vult		
	revolution_and_counter		
	press_gangs		
	national_conscripts		
	napoleonic_warfare		
	vice_roys		
	vetting		
	bureaucracy		
}			
historical_units = {			
	napoleonic_square		
	napoleonic_lancers		
	flying_battery		
}			
monarch_names = {			
	"Wang #0" = 10		
	"Li #0" = 10		
	"Zhang #0" = 10		
	"Liu #0" = 10		
	"Chen #0" = 10		
	"Yang #0" = 10		
	"Huang #0" = 10		
	"Wu #0" = 10		
	"Xu #0" = 10		
	"Zhao #0" = 10		
	"Zhou #0" = 10		
	"Zhu #0" = 10		
	"Sun #0" = 10		
	"He #0" = 10		
	"Ma #0" = 10		
	"Yu #0" = 10		
	"Hu #0" = 10		
	"Lin #0" = 10		
	"Jiang #0" = 10		
	"Guo #0" = 10		
	"Luo #0" = 10		
	"Gao #0" = 10		
	"Zheng #0" = 10		
	"Liang #0" = 10		
	"Tang #0" = 10		
	"Wei #0" = 10		
	"Shi #0" = 10		
	"Xie #0" = 10		
	"Song #0" = 10		
	"Feng #0" = 10		
	"Yan #0" = 10		
	"Deng #0" = 10		
	"Han #0" = 10		
	"Cao #0" = 10		
	"Tan #0" = 10		
	"Ceng #0" = 10		
	"Peng #0" = 10		
	"Xiao #0" = 10		
	"Yuan #0" = 10		
}			
ship_names = {			
	"Coast Guard"		
}			
leader_names = {			
	Wang		
	Li		
	Zhang		
	Liu		
	Chen		
	Yang		
	Yang		
	Huang		
	Wu		
	Xu		
	Zhao		
	Zhou		
	Lu		
	Zhu		
	Sun		
	He		
	Ma		
	Yu		
	Hu		
	Lin		
	Jiang		
	Guo		
	Luo		
	Gao		
	Zheng		
	Liang		
	Tang		
	Wei		
	Shi		
	Xie		
	Song		
	Feng		
	Yan		
	Deng		
	Han		
	Cao		
	Tan		
	Ceng		
	Peng		
	Xiao		
	Yuan		
}			
