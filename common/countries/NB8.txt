# Country Name:	Namibian Rebels	# Tag:	NB8
#			
graphical_culture = 	africangfx		
#			
color = {	18 15 0	}	
historical_ideas = {			
	vetting		
	national_bank		
	national_trade_policy		
	smithian_economics		
	shrewd_commerce_practise		
	scientific_revolution		
	sea_hawks		
	naval_provisioning		
	regimental_system		
	napoleonic_warfare		
	merchant_adventures		
}			
historical_units = {			
	napoleonic_square		
	napoleonic_lancers		
	flying_battery		
}			
monarch_names = {			
	"Johannes #0" = 10		
	"Shikongo #0" = 10		
	"Paulus #0" = 10		
	"Petrus #0" = 10		
	"Shilongo #0" = 10		
	"Nangolo #0" = 10		
	"Kamati #0" = 10		
	"David #0" = 10		
	"Andreas #0" = 10		
	"Haufiku #0" = 10		
	"Angula #0" = 10		
	"Matheus #0" = 10		
	"Hangula #0" = 10		
	"Hamutenya #0" = 10		
	"Shigwedha #0" = 10		
	"Iipinge #0" = 10		
	"Iyambo #0" = 10		
	"Thomas #0" = 10		
	"van Wyk #0" = 10		
	"Amutenya #0" = 10		
	"Gabriel #0" = 10		
	"Hausiku #0" = 10		
	"Shivute #0" = 10		
	"Haimbodi #0" = 10		
	"Nakale #0" = 10		
	"Iiyambo #0" = 10		
	"Joseph #0" = 10		
	"Beukes #0" = 10		
	"Amunyela #0" = 10		
	"Shaanika #0" = 10		
	"Moses #0" = 10		
	"Immanuel #0" = 10		
	"Lukas #0" = 10		
	"Frans #0" = 10		
	"Cloete #0" = 10		
	"Sakaria #0" = 10		
	"Jonas #0" = 10		
	"Heita #0" = 10		
	"Iita #0" = 10		
}			
ship_names = {			
	"Coast Guard"		
}			
leader_names = {			
	Johannes		
	Shikongo		
	Paulus		
	Petrus		
	Shilongo		
	Nangolo		
	Nangolo		
	Kamati		
	David		
	Andreas		
	Haufiku		
	Angula		
	Simon		
	Matheus		
	Hangula		
	Hamutenya		
	Shigwedha		
	Iipinge		
	Iyambo		
	Thomas		
	van Wyk		
	Amutenya		
	Gabriel		
	Hausiku		
	Shivute		
	Haimbodi		
	Nakale		
	Iiyambo		
	Joseph		
	Beukes		
	Amunyela		
	Shaanika		
	Moses		
	Immanuel		
	Lukas		
	Frans		
	Cloete		
	Sakaria		
	Jonas		
	Heita		
	Iita		
}			
