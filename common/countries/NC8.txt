# Country Name:	Nicaraguan Rebels	# Tag:	NC8
#			
graphical_culture = 	southamericagfx		
#			
color = {	29 195 18	}	
historical_ideas = {			
	ecumenism		
	church_attendance_duty		
	divine_supremacy		
	deus_vult		
	revolution_and_counter		
	press_gangs		
	national_conscripts		
	napoleonic_warfare		
	vice_roys		
	vetting		
	bureaucracy		
}			
historical_units = {			
	napoleonic_square		
	napoleonic_lancers		
	flying_battery		
}			
monarch_names = {			
	"Lopez #0" = 10		
	"Martinez #0" = 10		
	"Garcia #0" = 10		
	"Gonzalez #0" = 10		
	"Hernandez #0" = 10		
	"Perez #0" = 10		
	"Rodriguez #0" = 10		
	"Gutierrez #0" = 10		
	"Sanchez #0" = 10		
	"Espinoza #0" = 10		
	"Flores #0" = 10		
	"Ruiz #0" = 10		
	"Reyes #0" = 10		
	"Cruz #0" = 10		
	"Diaz #0" = 10		
	"Castillo #0" = 10		
	"Gomez #0" = 10		
	"Rivera #0" = 10		
	"Herrera #0" = 10		
	"Morales #0" = 10		
	"Centeno #0" = 10		
	"Urbina #0" = 10		
	"Jarquin #0" = 10		
	"Castro #0" = 10		
	"Ramirez #0" = 10		
	"Chavarria #0" = 10		
	"Blandon #0" = 10		
	"Rivas #0" = 10		
	"Davila #0" = 10		
	"Torrez #0" = 10		
	"Mejia #0" = 10		
	"Aguilar #0" = 10		
	"Mendez #0" = 10		
	"Romero #0" = 10		
	"Obando #0" = 10		
	"Calero #0" = 10		
	"Miranda #0" = 10		
	"Orozco #0" = 10		
	"Velasquez #0" = 10		
}			
ship_names = {			
	"Coast Guard"		
}			
leader_names = {			
	Lopez		
	Martinez		
	Garcia		
	Gonzalez		
	Hernandez		
	Perez		
	Perez		
	Rodriguez		
	Gutierrez		
	Sanchez		
	Espinoza		
	Flores		
	Mendoza		
	Ruiz		
	Reyes		
	Cruz		
	Diaz		
	Castillo		
	Gomez		
	Rivera		
	Herrera		
	Morales		
	Centeno		
	Urbina		
	Jarquin		
	Castro		
	Ramirez		
	Chavarria		
	Blandon		
	Rivas		
	Davila		
	Torrez		
	Mejia		
	Aguilar		
	Mendez		
	Romero		
	Obando		
	Calero		
	Miranda		
	Orozco		
	Velasquez		
}			
