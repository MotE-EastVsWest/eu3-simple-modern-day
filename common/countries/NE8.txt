# Country Name:	Nepali Rebels	# Tag:	NE8
#			
graphical_culture = 	indiangfx		
#			
color = {	107 153 94	}	
historical_ideas = {			
	ecumenism		
	church_attendance_duty		
	divine_supremacy		
	deus_vult		
	revolution_and_counter		
	press_gangs		
	national_conscripts		
	napoleonic_warfare		
	vice_roys		
	vetting		
	bureaucracy		
}			
historical_units = {			
	napoleonic_square		
	napoleonic_lancers		
	flying_battery		
}			
monarch_names = {			
	"Caudhari #0" = 10		
	"Sresth #0" = 10		
	"Yadav #0" = 10		
	"Rai #0" = 10		
	"Thapa #0" = 10		
	"Magar #0" = 10		
	"Devi #0" = 10		
	"Taman #0" = 10		
	"Tharu #0" = 10		
	"Saha #0" = 10		
	"Karki #0" = 10		
	"Khadk #0" = 10		
	"Adhikari #0" = 10		
	"Khatri #0" = 10		
	"Gurung #0" = 10		
	"Khatun #0" = 10		
	"Maharjan #0" = 10		
	"Mahato #0" = 10		
	"Bhandari #0" = 10		
	"Paudel #0" = 10		
	"Tamang #0" = 10		
	"Gurun #0" = 10		
	"Mandal #0" = 10		
	"Sarki #0" = 10		
	"Rana #0" = 10		
	"Sahi #0" = 10		
	"Ksetri #0" = 10		
	"Sunar #0" = 10		
	"Tharuni #0" = 10		
	"Damai #0" = 10		
	"Lama #0" = 10		
	"Basnet #0" = 10		
	"Sarma #0" = 10		
	"Visvakarm #0" = 10		
	"Ghimire #0" = 10		
	"Vika #0" = 10		
	"Giri #0" = 10		
	"Limbu #0" = 10		
	"Budha #0" = 10		
}			
ship_names = {			
	"Coast Guard"		
}			
leader_names = {			
	Caudhari		
	Sresth		
	Yadav		
	Rai		
	Thapa		
	Magar		
	Magar		
	Devi		
	Taman		
	Tharu		
	Saha		
	Karki		
	Kami		
	Khadk		
	Adhikari		
	Khatri		
	Gurung		
	Khatun		
	Maharjan		
	Mahato		
	Bhandari		
	Paudel		
	Tamang		
	Gurun		
	Mandal		
	Sarki		
	Rana		
	Sahi		
	Ksetri		
	Sunar		
	Tharuni		
	Damai		
	Lama		
	Basnet		
	Sarma		
	Visvakarm		
	Ghimire		
	Vika		
	Giri		
	Limbu		
	Budha		
}			
