# Country Name:	Norfolk Island	# Tag:	NF9
#			
graphical_culture = 	northamericagfx		
#			
color = {	20 180 40	}	
historical_ideas = {			
}			
historical_units = {			
	napoleonic_square		
	napoleonic_lancers		
	flying_battery		
}			
monarch_names = {			
	"Christian #0" = 10		
	"Evans #0" = 10		
	"Quintal #0" = 10		
	"Buffett #0" = 10		
	"Anderson #0" = 10		
	"Nobbs #0" = 10		
	"Adams #0" = 10		
	"Snell #0" = 10		
	"McCoy #0" = 10		
	"Brown #0" = 10		
	"Smith #0" = 10		
	"Kiernan #0" = 10		
	"Magri #0" = 10		
	"Williams #0" = 10		
	"Wilson #0" = 10		
	"Christian-Bailey #0" = 10		
	"Gardner #0" = 10		
	"Grube #0" = 10		
	"Jones #0" = 10		
	"Randall #0" = 10		
	"Reeves #0" = 10		
	"Richards #0" = 10		
	"Robinson #0" = 10		
	"Sanders #0" = 10		
	"Davies #0" = 10		
	"Douran #0" = 10		
	"Griffiths #0" = 10		
	"Judd #0" = 10		
	"Menzies #0" = 10		
	"Nicolai #0" = 10		
	"Partridge #0" = 10		
	"Tavener #0" = 10		
	"Bates #0" = 10		
	"Bell #0" = 10		
	"Bigg #0" = 10		
	"McLeod #0" = 10		
	"Murray #0" = 10		
	"Ryves #0" = 10		
	"Sheridan #0" = 10		
}			
ship_names = {			
	"Coast Guard"		
}			
leader_names			
	Christian		
	Evans		
	Quintal		
	Buffett		
	Anderson		
	Nobbs		
	Nobbs		
	Adams		
	Snell		
	McCoy		
	Brown		
	Smith		
	Graham		
	Kiernan		
	Magri		
	Williams		
	Wilson		
	Christian-Bailey		
	Gardner		
	Grube		
	Jones		
	Randall		
	Reeves		
	Richards		
	Robinson		
	Sanders		
	Davies		
	Douran		
	Griffiths		
	Judd		
	Menzies		
	Nicolai		
	Partridge		
	Tavener		
	Bates		
	Bell		
	Bigg		
	McLeod		
	Murray		
	Ryves		
	Sheridan		
}			
