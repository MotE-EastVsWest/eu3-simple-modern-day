# Country Name:	Newfoundland-Labrador Rebels	# Tag:	NL8
#			
graphical_culture = 	latingfx		
#			
color = {	42 135 73	}	
historical_ideas = {			
	press_gangs		
	grand_navy		
	sea_hawks		
	superior_seamanship		
	naval_glory		
	excellent_shipwrights		
	naval_fighting_instruction		
	naval_provisioning		
	shrewd_commerce_practise		
	merchant_adventures		
	improved_foraging		
}			
historical_units = {			
	napoleonic_square		
	napoleonic_lancers		
	flying_battery		
}			
monarch_names = {			
	"Power #0" = 10		
	"Smith #0" = 10		
	"White #0" = 10		
	"Parsons #0" = 10		
	"Walsh #0" = 10		
	"King #0" = 10		
	"Murphy #0" = 10		
	"Penney #0" = 10		
	"Clarke #0" = 10		
	"Mercer #0" = 10		
	"Young #0" = 10		
	"Ryan #0" = 10		
	"Brown #0" = 10		
	"Collins #0" = 10		
	"Sheppard #0" = 10		
	"Bennett #0" = 10		
	"Martin #0" = 10		
	"Williams #0" = 10		
	"Butler #0" = 10		
	"Hynes #0" = 10		
	"Butt #0" = 10		
	"Noseworthy #0" = 10		
	"Pike #0" = 10		
	"Roberts #0" = 10		
	"Taylor #0" = 10		
	"Kelly #0" = 10		
	"Snow #0" = 10		
	"Rideout #0" = 10		
	"Tucker #0" = 10		
	"Dawe #0" = 10		
	"Rose #0" = 10		
	"Green #0" = 10		
	"Hillier #0" = 10		
	"Abbott #0" = 10		
	"Payne #0" = 10		
	"Wells #0" = 10		
	"Kennedy #0" = 10		
	"Saunders #0" = 10		
	"Osmond #0" = 10		
}			
ship_names = {			
	"Coast Guard"		
}			
leader_names = {			
	Power		
	Smith		
	White		
	Parsons		
	Walsh		
	King		
	King		
	Murphy		
	Penney		
	Clarke		
	Mercer		
	Young		
	Reid		
	Ryan		
	Brown		
	Collins		
	Sheppard		
	Bennett		
	Martin		
	Williams		
	Butler		
	Hynes		
	Butt		
	Noseworthy		
	Pike		
	Roberts		
	Taylor		
	Kelly		
	Snow		
	Rideout		
	Tucker		
	Dawe		
	Rose		
	Green		
	Hillier		
	Abbott		
	Payne		
	Wells		
	Kennedy		
	Saunders		
	Osmond		
}			
