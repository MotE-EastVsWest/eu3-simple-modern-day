# Country Name:	Nagorno-Karabakh	# Tag:	NQ9
#			
graphical_culture = 	easterngfx		
#			
color = {	56 194 43	}	
historical_ideas = {			
}			
historical_units = {			
	napoleonic_square		
	napoleonic_lancers		
	flying_battery		
}			
monarch_names = {			
	"Grigoryan #0" = 10		
	"Harutyunyan #0" = 10		
	"Sargsyan #0" = 10		
	"Hayrapetyan #0" = 10		
	"Hovhannisyan #0" = 10		
	"Khachatryan #0" = 10		
	"Hakobyan #0" = 10		
	"Mkrtchyan #0" = 10		
	"Arakelyan #0" = 10		
	"Martirosyan #0" = 10		
	"Petrosyan #0" = 10		
	"Davtyan #0" = 10		
	"Stepanyan #0" = 10		
	"Ghazaryan #0" = 10		
	"Poghosyan #0" = 10		
	"Karapetyan #0" = 10		
	"Baghdasaryan #0" = 10		
	"Vardanyan #0" = 10		
	"Galstyan #0" = 10		
	"Hambardzumyan #0" = 10		
	"Sahakyan #0" = 10		
	"Avetisyan #0" = 10		
	"Babayan #0" = 10		
	"Avagyan #0" = 10		
	"Arzumanyan #0" = 10		
	"Margaryan #0" = 10		
	"Mirzoyan #0" = 10		
	"Hovsepyan #0" = 10		
	"Aleksanyan #0" = 10		
	"Minasyan #0" = 10		
	"Ohanyan #0" = 10		
	"Avanesyan #0" = 10		
	"Gasparyan #0" = 10		
	"Simonyan #0" = 10		
	"Abrahamyan #0" = 10		
	"Danielyan #0" = 10		
	"Zakaryan #0" = 10		
	"Ohanjanyan #0" = 10		
	"Hovakimyan #0" = 10		
}			
ship_names = {			
	"Coast Guard"		
}			
leader_names			
	Grigoryan		
	Harutyunyan		
	Sargsyan		
	Hayrapetyan		
	Hovhannisyan		
	Khachatryan		
	Khachatryan		
	Hakobyan		
	Mkrtchyan		
	Arakelyan		
	Martirosyan		
	Petrosyan		
	Gevorgyan		
	Davtyan		
	Stepanyan		
	Ghazaryan		
	Poghosyan		
	Karapetyan		
	Baghdasaryan		
	Vardanyan		
	Galstyan		
	Hambardzumyan		
	Sahakyan		
	Avetisyan		
	Babayan		
	Avagyan		
	Arzumanyan		
	Margaryan		
	Mirzoyan		
	Hovsepyan		
	Aleksanyan		
	Minasyan		
	Ohanyan		
	Avanesyan		
	Gasparyan		
	Simonyan		
	Abrahamyan		
	Danielyan		
	Zakaryan		
	Ohanjanyan		
	Hovakimyan		
}			
