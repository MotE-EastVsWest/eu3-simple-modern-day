# Country Name:	Sant Martin	# Tag:	NT9
#			
graphical_culture = 	africangfx		
#			
color = {	200 191 163	}	
historical_ideas = {			
}			
historical_units = {			
	napoleonic_square		
	napoleonic_lancers		
	flying_battery		
}			
monarch_names = {			
	"Richardson #0" = 10		
	"Gumbs #0" = 10		
	"Hodge #0" = 10		
	"Williams #0" = 10		
	"Lake #0" = 10		
	"Arrindell #0" = 10		
	"Joseph #0" = 10		
	"Peterson #0" = 10		
	"Carty #0" = 10		
	"James #0" = 10		
	"Brown #0" = 10		
	"Wilson #0" = 10		
	"Brooks #0" = 10		
	"Fleming #0" = 10		
	"Charles #0" = 10		
	"Smith #0" = 10		
	"Jones #0" = 10		
	"York #0" = 10		
	"Pantophlet #0" = 10		
	"Rogers #0" = 10		
	"Henry #0" = 10		
	"Pierre #0" = 10		
	"Webster #0" = 10		
	"Marlin #0" = 10		
	"Jean #0" = 10		
	"Rombley #0" = 10		
	"Flanders #0" = 10		
	"Davis #0" = 10		
	"Halley #0" = 10		
	"Bryson #0" = 10		
	"de Weever #0" = 10		
	"Cannegieter #0" = 10		
	"Daniel #0" = 10		
	"Hazel #0" = 10		
	"Illidge #0" = 10		
	"Goorge #0" = 10		
	"Hodgepodge #0" = 10		
	"Benminster #0" = 10		
	"de Voot #0" = 10		
}			
ship_names = {			
	"Coast Guard"		
}			
leader_names			
	Richardson		
	Gumbs		
	Hodge		
	Williams		
	Lake		
	Arrindell		
	Arrindell		
	Joseph		
	Peterson		
	Carty		
	James		
	Brown		
	Thomas		
	Wilson		
	Brooks		
	Fleming		
	Charles		
	Smith		
	Jones		
	York		
	Pantophlet		
	Rogers		
	Henry		
	Pierre		
	Webster		
	Marlin		
	Jean		
	Rombley		
	Flanders		
	Davis		
	Halley		
	Bryson		
	de Weever		
	Cannegieter		
	Daniel		
	Hazel		
	Illidge		
	Goorge		
	Hodgepodge		
	Benminster		
	de Voot		
}			
