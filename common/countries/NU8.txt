# Country Name:	Niuan Rebels	# Tag:	NU8
#			
graphical_culture = 	northamericagfx		
#			
color = {	210 210 20	}	
historical_ideas = {			
}			
historical_units = {			
	napoleonic_square		
	napoleonic_lancers		
	flying_battery		
}			
monarch_names = {			
	"Fuimaono #0" = 10		
	"Ioane #0" = 10		
	"Tuigamala #0" = 10		
	"Smith #0" = 10		
	"Mauga #0" = 10		
	"Brown #0" = 10		
	"Leota #0" = 10		
	"Mageo #0" = 10		
	"Faumuina #0" = 10		
	"Samuelu #0" = 10		
	"Satele #0" = 10		
	"Kim #0" = 10		
	"Tagaloa #0" = 10		
	"Maiava #0" = 10		
	"Thompson #0" = 10		
	"Lee #0" = 10		
	"Lutu #0" = 10		
	"Scanlan #0" = 10		
	"Young #0" = 10		
	"Hunkin #0" = 10		
	"Liu #0" = 10		
	"Savea #0" = 10		
	"Tasi #0" = 10		
	"Ho #0" = 10		
	"Lui #0" = 10		
	"Pago #0" = 10		
	"Puletasi #0" = 10		
	"Danielson #0" = 10		
	"Reid #0" = 10		
	"Tanielu #0" = 10		
	"Tupua #0" = 10		
	"Allen #0" = 10		
	"Lefiti #0" = 10		
	"Ieremia #0" = 10		
	"Sunia #0" = 10		
	"Tupuola #0" = 10		
	"Moliga #0" = 10		
	"Solaita #0" = 10		
	"Tilo #0" = 10		
}			
ship_names = {			
	"Coast Guard"		
}			
leader_names			
	Fuimaono		
	Ioane		
	Tuigamala		
	Smith		
	Mauga		
	Brown		
	Brown		
	Leota		
	Mageo		
	Faumuina		
	Samuelu		
	Satele		
	Lafaele		
	Kim		
	Tagaloa		
	Maiava		
	Thompson		
	Lee		
	Lutu		
	Scanlan		
	Young		
	Hunkin		
	Liu		
	Savea		
	Tasi		
	Ho		
	Lui		
	Pago		
	Puletasi		
	Danielson		
	Reid		
	Tanielu		
	Tupua		
	Allen		
	Lefiti		
	Ieremia		
	Sunia		
	Tupuola		
	Moliga		
	Solaita		
	Tilo		
}			
