# Country Name:	New Zealander Rebels	# Tag:	NZ8
#			
graphical_culture = 	latingfx		
#			
color = {	201 179 30	}	
historical_ideas = {			
	grand_army		
	glorious_arms		
	merchant_adventures		
	scientific_revolution		
	national_bank		
	bill_of_rights		
	smithian_economics		
	liberty_egalite_fraternity		
	patron_of_art		
	humanist_tolerance		
	cabinet		
}			
historical_units = {			
	napoleonic_square		
	napoleonic_lancers		
	flying_battery		
}			
monarch_names = {			
	"Smith #0" = 10		
	"Williams #0" = 10		
	"Jones #0" = 10		
	"Brown #0" = 10		
	"Taylor #0" = 10		
	"Wilson #0" = 10		
	"Scott #0" = 10		
	"Anderson #0" = 10		
	"Young #0" = 10		
	"Singh #0" = 10		
	"Thompson #0" = 10		
	"Walker #0" = 10		
	"Harris #0" = 10		
	"White #0" = 10		
	"Lee #0" = 10		
	"Robinson #0" = 10		
	"Edwards #0" = 10		
	"Johnson #0" = 10		
	"Stewart #0" = 10		
	"Davis #0" = 10		
	"Robertson #0" = 10		
	"Mitchell #0" = 10		
	"Johnston #0" = 10		
	"Martin #0" = 10		
	"Thomas #0" = 10		
	"Watson #0" = 10		
	"Turner #0" = 10		
	"Simpson #0" = 10		
	"Morgan #0" = 10		
	"Cooper #0" = 10		
	"Clark #0" = 10		
	"Phillips #0" = 10		
	"Murray #0" = 10		
	"Reid #0" = 10		
	"Miller #0" = 10		
	"Jackson #0" = 10		
	"Campbell #0" = 10		
	"McDonald #0" = 10		
	"Moore #0" = 10		
}			
ship_names = {			
	"Coast Guard"		
}			
leader_names = {			
	Smith		
	Williams		
	Jones		
	Brown		
	Taylor		
	Wilson		
	Wilson		
	Scott		
	Anderson		
	Young		
	Singh		
	Thompson		
	King		
	Walker		
	Harris		
	White		
	Lee		
	Robinson		
	Edwards		
	Johnson		
	Stewart		
	Davis		
	Robertson		
	Mitchell		
	Johnston		
	Martin		
	Thomas		
	Watson		
	Turner		
	Simpson		
	Morgan		
	Cooper		
	Clark		
	Phillips		
	Murray		
	Reid		
	Miller		
	Jackson		
	Campbell		
	McDonald		
	Moore		
}			
