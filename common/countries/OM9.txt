# Country Name:	Oman	# Tag:	OM9
#			
graphical_culture = 	muslimgfx		
#			
color = { 98  134  153 }	
historical_ideas = {			
	vetting		
	national_bank		
	national_trade_policy		
	smithian_economics		
	shrewd_commerce_practise		
	scientific_revolution		
	sea_hawks		
	naval_provisioning		
	regimental_system		
	napoleonic_warfare		
	merchant_adventures		
}			
historical_units = {			
	napoleonic_square		
	napoleonic_lancers		
	flying_battery		
}			
monarch_names = {			
	"Ali #0" = 10		
	"Yousuf #0" = 10		
	"Sultan #0" = 10		
	"Khan #0" = 10		
	"Gawas #0" = 10		
	"Al Balushi #0" = 10		
	"Ramadan #0" = 10		
	"Saeed #0" = 10		
	"Singh #0" = 10		
	"Ahmed #0" = 10		
	"Kumar #0" = 10		
	"Araba #0" = 10		
	"Khawar #0" = 10		
	"Hussain #0" = 10		
	"Balochia #0" = 10		
	"Thani #0" = 10		
	"Varghese #0" = 10		
	"Raj #0" = 10		
	"Pillai #0" = 10		
	"Salim #0" = 10		
	"Sharma #0" = 10		
	"Rahman #0" = 10		
	"El Amery #0" = 10		
	"Rashid #0" = 10		
	"Sheikh #0" = 10		
	"Babu #0" = 10		
	"Abbas #0" = 10		
	"Das #0" = 10		
	"Krishnan #0" = 10		
	"Amerieh #0" = 10		
	"Zaidi #0" = 10		
	"Menon #0" = 10		
	"Mathew #0" = 10		
	"Iqbal #0" = 10		
	"Ramadani #0" = 10		
	"Patel #0" = 10		
	"Murad #0" = 10		
	"Rajan #0" = 10		
	"Albalushi #0" = 10		
}			
ship_names = {			
	"Coast Guard"		
}			
leader_names = {			
	Ali		
	Yousuf		
	Sultan		
	Khan		
	Gawas		
	Al Balushi		
	Al Balushi		
	Ramadan		
	Saeed		
	Singh		
	Ahmed		
	Kumar		
	Nair		
	Araba		
	Khawar		
	Hussain		
	Balochia		
	Thani		
	Varghese		
	Raj		
	Pillai		
	Salim		
	Sharma		
	Rahman		
	El Amery		
	Rashid		
	Sheikh		
	Babu		
	Abbas		
	Das		
	Krishnan		
	Amerieh		
	Zaidi		
	Menon		
	Mathew		
	Iqbal		
	Ramadani		
	Patel		
	Murad		
	Rajan		
	Albalushi		
}			
