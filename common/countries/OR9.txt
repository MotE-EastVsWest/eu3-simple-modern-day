# Country Name:	Corsica	# Tag:	OR9
#			
graphical_culture = 	latingfx		
#			
color = { 106  178  46 }
historical_ideas = {			
	vetting		
	national_bank		
	national_trade_policy		
	smithian_economics		
	shrewd_commerce_practise		
	scientific_revolution		
	sea_hawks		
	naval_provisioning		
	regimental_system		
	napoleonic_warfare		
	merchant_adventures		
}			
historical_units = {			
	napoleonic_square		
	napoleonic_lancers		
	flying_battery		
}			
monarch_names = {			
	"Albertini #0" = 10		
	"Mattei #0" = 10		
	"Casanova #0" = 10		
	"Luciani #0" = 10		
	"Pietri #0" = 10		
	"Rossi #0" = 10		
	"Poli #0" = 10		
	"Paoli #0" = 10		
	"Filippi #0" = 10		
	"Santoni #0" = 10		
	"Leca #0" = 10		
	"Colombani #0" = 10		
	"Bartoli #0" = 10		
	"Colonna #0" = 10		
	"Orsini #0" = 10		
	"Graziani #0" = 10		
	"Giudicelli #0" = 10		
	"Mariani #0" = 10		
	"Andreani #0" = 10		
	"Mondoloni #0" = 10		
	"Franceschi #0" = 10		
	"Santini #0" = 10		
	"Grimaldi #0" = 10		
	"Torre #0" = 10		
	"Nicolai #0" = 10		
	"Costa #0" = 10		
	"Acquaviva #0" = 10		
	"Poggi #0" = 10		
	"Peretti #0" = 10		
	"Arrighi #0" = 10		
	"Bernardini #0" = 10		
	"Dominici #0" = 10		
	"Giorgi #0" = 10		
	"Battesti #0" = 10		
	"Moracchini #0" = 10		
	"Moretti #0" = 10		
	"Ottavi #0" = 10		
	"Campana #0" = 10		
	"Raffalli #0" = 10		
}			
ship_names = {			
	"Coast Guard"		
}			
leader_names = {			
	Albertini		
	Mattei		
	Casanova		
	Luciani		
	Pietri		
	Rossi		
	Rossi		
	Poli		
	Paoli		
	Filippi		
	Santoni		
	Leca		
	Agostini		
	Colombani		
	Bartoli		
	Colonna		
	Orsini		
	Graziani		
	Giudicelli		
	Mariani		
	Andreani		
	Mondoloni		
	Franceschi		
	Santini		
	Grimaldi		
	Torre		
	Nicolai		
	Costa		
	Acquaviva		
	Poggi		
	Peretti		
	Arrighi		
	Bernardini		
	Dominici		
	Giorgi		
	Battesti		
	Moracchini		
	Moretti		
	Ottavi		
	Campana		
	Raffalli		
}			
