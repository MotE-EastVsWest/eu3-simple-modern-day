# Country Name:	Panama	# Tag:	PA9
#			
graphical_culture = 	latingfx		
#			
color = {	214 242 155	}	
historical_ideas = {			
	vetting		
	national_bank		
	national_trade_policy		
	smithian_economics		
	shrewd_commerce_practise		
	scientific_revolution		
	sea_hawks		
	naval_provisioning		
	regimental_system		
	napoleonic_warfare		
	merchant_adventures		
}			
historical_units = {			
	napoleonic_square		
	napoleonic_lancers		
	flying_battery		
}			
monarch_names = {			
	"Rodriguez #0" = 10		
	"Gonzalez #0" = 10		
	"Sanchez #0" = 10		
	"Martinez #0" = 10		
	"Castillo #0" = 10		
	"Perez #0" = 10		
	"Abrego #0" = 10		
	"Hernandez #0" = 10		
	"Garcia #0" = 10		
	"Morales #0" = 10		
	"Jimenez #0" = 10		
	"Quintero #0" = 10		
	"Ortega #0" = 10		
	"Lopez #0" = 10		
	"Diaz #0" = 10		
	"Guerra #0" = 10		
	"Miranda #0" = 10		
	"Mendoza #0" = 10		
	"Gomez #0" = 10		
	"Herrera #0" = 10		
	"Cedeno #0" = 10		
	"Batista #0" = 10		
	"Santos #0" = 10		
	"Caballero #0" = 10		
	"Vasquez #0" = 10		
	"Rivera #0" = 10		
	"Valdes #0" = 10		
	"Vega #0" = 10		
	"de Leon #0" = 10		
	"Rios #0" = 10		
	"Nunez #0" = 10		
	"Flores #0" = 10		
	"Ramos #0" = 10		
	"Aguilar #0" = 10		
	"Munoz #0" = 10		
	"Villarreal #0" = 10		
	"Espinosa #0" = 10		
	"Dominguez #0" = 10		
	"Pineda #0" = 10		
}			
ship_names = {			
	"Coast Guard"		
}			
leader_names = {			
	Rodriguez		
	Gonzalez		
	Sanchez		
	Martinez		
	Castillo		
	Perez		
	Perez		
	Abrego		
	Hernandez		
	Garcia		
	Morales		
	Jimenez		
	Moreno		
	Quintero		
	Ortega		
	Lopez		
	Diaz		
	Guerra		
	Miranda		
	Mendoza		
	Gomez		
	Herrera		
	Cedeno		
	Batista		
	Santos		
	Caballero		
	Vasquez		
	Rivera		
	Valdes		
	Vega		
	de Leon		
	Rios		
	Nunez		
	Flores		
	Ramos		
	Aguilar		
	Munoz		
	Villarreal		
	Espinosa		
	Dominguez		
	Pineda		
}			
