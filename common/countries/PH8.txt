# Country Name:	Filipino Rebels	# Tag:	PH8
#			
graphical_culture = 	chinesegfx		
#			
color = {	43 55 46	}	
historical_ideas = {			
	press_gangs		
	grand_navy		
	sea_hawks		
	superior_seamanship		
	naval_glory		
	excellent_shipwrights		
	naval_fighting_instruction		
	naval_provisioning		
	shrewd_commerce_practise		
	merchant_adventures		
	improved_foraging		
}			
historical_units = {			
	napoleonic_square		
	napoleonic_lancers		
	flying_battery		
}			
monarch_names = {			
	"dela Cruz #0" = 10		
	"Garcia #0" = 10		
	"Reyes #0" = 10		
	"Ramos #0" = 10		
	"Mendoza #0" = 10		
	"Santos #0" = 10		
	"Flores #0" = 10		
	"Gonzales #0" = 10		
	"Bautista #0" = 10		
	"Villanueva #0" = 10		
	"Fernandez #0" = 10		
	"de Guzman #0" = 10		
	"Lopez #0" = 10		
	"Perez #0" = 10		
	"Castillo #0" = 10		
	"Francisco #0" = 10		
	"Rivera #0" = 10		
	"Aquino #0" = 10		
	"Castro #0" = 10		
	"Sanchez #0" = 10		
	"Torres #0" = 10		
	"de Leon #0" = 10		
	"Domingo #0" = 10		
	"Martinez #0" = 10		
	"Rodriguez #0" = 10		
	"Santiago #0" = 10		
	"Soriano #0" = 10		
	"Delos Santos #0" = 10		
	"Diaz #0" = 10		
	"Hernandez #0" = 10		
	"Tolentino #0" = 10		
	"Valdez #0" = 10		
	"Ramirez #0" = 10		
	"Morales #0" = 10		
	"Mercado #0" = 10		
	"Tan #0" = 10		
	"Aguilar #0" = 10		
	"Navarro #0" = 10		
	"Manalo #0" = 10		
}			
ship_names = {			
	"Coast Guard"		
}			
leader_names = {			
	dela Cruz		
	Garcia		
	Reyes		
	Ramos		
	Mendoza		
	Santos		
	Santos		
	Flores		
	Gonzales		
	Bautista		
	Villanueva		
	Fernandez		
	Cruz		
	de Guzman		
	Lopez		
	Perez		
	Castillo		
	Francisco		
	Rivera		
	Aquino		
	Castro		
	Sanchez		
	Torres		
	de Leon		
	Domingo		
	Martinez		
	Rodriguez		
	Santiago		
	Soriano		
	Delos Santos		
	Diaz		
	Hernandez		
	Tolentino		
	Valdez		
	Ramirez		
	Morales		
	Mercado		
	Tan		
	Aguilar		
	Navarro		
	Manalo		
}			
