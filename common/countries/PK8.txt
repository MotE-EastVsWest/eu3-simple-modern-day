# Country Name:	Pakistani Rebels	# Tag:	PK8
#			
graphical_culture = 	indiangfx		
#			
color = {	105 74 55	}	
historical_ideas = {			
	ecumenism		
	church_attendance_duty		
	divine_supremacy		
	deus_vult		
	revolution_and_counter		
	press_gangs		
	national_conscripts		
	napoleonic_warfare		
	vice_roys		
	vetting		
	bureaucracy		
}			
historical_units = {			
	napoleonic_square		
	napoleonic_lancers		
	flying_battery		
}			
monarch_names = {			
	"Ali #0" = 10		
	"Ahmad #0" = 10		
	"Hussain #0" = 10		
	"Khan #0" = 10		
	"Mohammad #0" = 10		
	"Bakhash #0" = 10		
	"Bibi #0" = 10		
	"Iqbal #0" = 10		
	"Din #0" = 10		
	"Shah #0" = 10		
	"Khaw #0" = 10		
	"Nawaz #0" = 10		
	"Abbas #0" = 10		
	"Aslam #0" = 10		
	"Akhtar #0" = 10		
	"Mehmood #0" = 10		
	"Ashraf #0" = 10		
	"Muhammad #0" = 10		
	"Akram #0" = 10		
	"Bi #0" = 10		
	"Rasool #0" = 10		
	"Ullah #0" = 10		
	"Ramzan #0" = 10		
	"Hayat #0" = 10		
	"Begum #0" = 10		
	"Siddique #0" = 10		
	"Imran #0" = 10		
	"Maseeh #0" = 10		
	"Parveen #0" = 10		
	"Afzal #0" = 10		
	"Saleem #0" = 10		
	"Sharif #0" = 10		
	"Arshad #0" = 10		
	"Ramadan #0" = 10		
	"Anwar #0" = 10		
	"Javed #0" = 10		
	"Hassan #0" = 10		
	"Shahzad #0" = 10		
	"Allah #0" = 10		
}			
ship_names = {			
	"Coast Guard"		
}			
leader_names = {			
	Ali		
	Ahmad		
	Hussain		
	Khan		
	Mohammad		
	Bakhash		
	Bakhash		
	Bibi		
	Iqbal		
	Din		
	Shah		
	Khaw		
	Ahmed		
	Nawaz		
	Abbas		
	Aslam		
	Akhtar		
	Mehmood		
	Ashraf		
	Muhammad		
	Akram		
	Bi		
	Rasool		
	Ullah		
	Ramzan		
	Hayat		
	Begum		
	Siddique		
	Imran		
	Maseeh		
	Parveen		
	Afzal		
	Saleem		
	Sharif		
	Arshad		
	Ramadan		
	Anwar		
	Javed		
	Hassan		
	Shahzad		
	Allah		
}			
