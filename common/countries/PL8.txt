# Country Name:	Palauan Rebels	# Tag:	PL8
#			
graphical_culture = 	northamericagfx		
#			
color = {	0 169 169	}	
historical_ideas = {			
}			
historical_units = {			
	napoleonic_square		
	napoleonic_lancers		
	flying_battery		
}			
monarch_names = {			
	"Tellei #0" = 10		
	"Abdelbai #0" = 10		
	"Kloulechad #0" = 10		
	"Asanuma #0" = 10		
	"Marcil #0" = 10		
	"Santos #0" = 10		
	"Emesiochel #0" = 10		
	"Khan #0" = 10		
	"Emesiochel #0" = 10		
	"Sakuma #0" = 10		
	"Cruz #0" = 10		
	"Reyes #0" = 10		
	"Aguon #0" = 10		
	"Sugiyama #0" = 10		
	"Andreas #0" = 10		
	"Salii #0" = 10		
	"Thomas #0" = 10		
	"Gutierrez #0" = 10		
	"Yano #0" = 10		
	"Nakamura #0" = 10		
	"Qong #0" = 10		
	"Whipps #0" = 10		
	"Seid #0" = 10		
	"Takada #0" = 10		
	"Kanai #0" = 10		
	"David #0" = 10		
	"Kintaro #0" = 10		
	"Rafael #0" = 10		
	"Alexander #0" = 10		
	"Moses #0" = 10		
	"Morei #0" = 10		
	"Castillo #0" = 10		
	"Fritz #0" = 10		
	"Ngotel #0" = 10		
	"Salvador #0" = 10		
	"Abbas #0" = 10		
	"Rivera #0" = 10		
	"Bai #0" = 10		
	"Gopez #0" = 10		
}			
ship_names = {			
	"Coast Guard"		
}			
leader_names			
	Tellei		
	Abdelbai		
	Kloulechad		
	Asanuma		
	Marcil		
	Santos		
	Santos		
	Emesiochel		
	Khan		
	Emesiochel		
	Sakuma		
	Cruz		
	Smith		
	Reyes		
	Aguon		
	Sugiyama		
	Andreas		
	Salii		
	Thomas		
	Gutierrez		
	Yano		
	Nakamura		
	Qong		
	Whipps		
	Seid		
	Takada		
	Kanai		
	David		
	Kintaro		
	Rafael		
	Alexander		
	Moses		
	Morei		
	Castillo		
	Fritz		
	Ngotel		
	Salvador		
	Abbas		
	Rivera		
	Bai		
	Gopez		
}			
