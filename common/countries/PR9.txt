# Country Name:	Puerto Rico	# Tag:	PR9
#			
graphical_culture = 	southamericagfx		
#			
color = {	103 206 30	}	
historical_ideas = {			
	press_gangs		
	grand_navy		
	sea_hawks		
	superior_seamanship		
	naval_glory		
	excellent_shipwrights		
	naval_fighting_instruction		
	naval_provisioning		
	shrewd_commerce_practise		
	merchant_adventures		
	improved_foraging		
}			
historical_units = {			
	napoleonic_square		
	napoleonic_lancers		
	flying_battery		
}			
monarch_names = {			
	"Sanchez #0" = 10		
	"Rivera #0" = 10		
	"Diaz #0" = 10		
	"Rodriguez #0" = 10		
	"Narvaez #0" = 10		
	"Burgos #0" = 10		
	"Colon #0" = 10		
	"Vazquez #0" = 10		
	"Ramos #0" = 10		
	"Ortiz #0" = 10		
	"Morales #0" = 10		
	"Cruz #0" = 10		
	"Santiago #0" = 10		
	"Reyes #0" = 10		
	"Perez #0" = 10		
	"Agosto #0" = 10		
	"Carrasquillo #0" = 10		
	"Garcia #0" = 10		
	"Ortega #0" = 10		
	"Torres #0" = 10		
	"Canales #0" = 10		
	"Vega #0" = 10		
	"Ayala #0" = 10		
	"Gonzalez #0" = 10		
	"Menendez #0" = 10		
	"Martinez #0" = 10		
	"Lopez #0" = 10		
	"Hernandez #0" = 10		
	"Rosa #0" = 10		
	"Merced #0" = 10		
	"Pagan #0" = 10		
	"Valentin #0" = 10		
	"Delgado #0" = 10		
	"Velez #0" = 10		
	"Quinones #0" = 10		
	"Malave #0" = 10		
	"Rosado #0" = 10		
	"Ogando #0" = 10		
	"Pastrana #0" = 10		
}			
ship_names = {			
	"Coast Guard"		
}			
leader_names = {			
	Sanchez		
	Rivera		
	Diaz		
	Rodriguez		
	Narvaez		
	Burgos		
	Burgos		
	Colon		
	Vazquez		
	Ramos		
	Ortiz		
	Morales		
	Betancourt		
	Cruz		
	Santiago		
	Reyes		
	Perez		
	Agosto		
	Carrasquillo		
	Garcia		
	Ortega		
	Torres		
	Canales		
	Vega		
	Ayala		
	Gonzalez		
	Menendez		
	Martinez		
	Lopez		
	Hernandez		
	Rosa		
	Merced		
	Pagan		
	Valentin		
	Delgado		
	Velez		
	Quinones		
	Malave		
	Rosado		
	Ogando		
	Pastrana		
}			
