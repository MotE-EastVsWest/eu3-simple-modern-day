# Country Name:	Comorian Rebels	# Tag:	QM8
#			
graphical_culture = 	africangfx		
#			
color = {	11 181 170	}	
historical_ideas = {			
}			
historical_units = {			
	napoleonic_square		
	napoleonic_lancers		
	flying_battery		
}			
monarch_names = {			
	"Mohamed #0" = 10		
	"Passable #0" = 10		
	"Ali #0" = 10		
	"Ahmada #0" = 10		
	"Abdou #0" = 10		
	"Said'Ali #0" = 10		
	"Ahmed #0" = 10		
	"Abdallah #0" = 10		
	"Youssouf #0" = 10		
	"Ibrahim #0" = 10		
	"Soilihi #0" = 10		
	"Bacar #0" = 10		
	"Hassani #0" = 10		
	"Moussa #0" = 10		
	"Mbae #0" = 10		
	"Assoumani #0" = 10		
	"Maoulida #0" = 10		
	"Hassane #0" = 10		
	"Salim #0" = 10		
	"Hamidou #0" = 10		
	"Abderemane #0" = 10		
	"Issa #0" = 10		
	"Kassim #0" = 10		
	"Abdillah #0" = 10		
	"Soule #0" = 10		
	"Omar #0" = 10		
	"Mze #0" = 10		
	"Said #0" = 10		
	"Boina #0" = 10		
	"Attoumane #0" = 10		
	"Athoumani #0" = 10		
	"Houmadi #0" = 10		
	"Mdahoma #0" = 10		
	"Mouigni #0" = 10		
	"Allaoui #0" = 10		
	"Madi #0" = 10		
	"Vers #0" = 10		
	"Attoumani #0" = 10		
	"Aboudou #0" = 10		
}			
ship_names = {			
	"Coast Guard"		
}			
leader_names			
	Mohamed		
	Passable		
	Ali		
	Ahmada		
	Abdou		
	Said'Ali		
	Said'Ali		
	Ahmed		
	Abdallah		
	Youssouf		
	Ibrahim		
	Soilihi		
	Mmadi		
	Bacar		
	Hassani		
	Moussa		
	Mbae		
	Assoumani		
	Maoulida		
	Hassane		
	Salim		
	Hamidou		
	Abderemane		
	Issa		
	Kassim		
	Abdillah		
	Soule		
	Omar		
	Mze		
	Said		
	Boina		
	Attoumane		
	Athoumani		
	Houmadi		
	Mdahoma		
	Mouigni		
	Allaoui		
	Madi		
	Vers		
	Attoumani		
	Aboudou		
}			
