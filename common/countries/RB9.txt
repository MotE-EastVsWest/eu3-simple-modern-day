# Country Name:	Aruba	# Tag:	RB9
#			
graphical_culture = 	africangfx		
#			
color = {	109 148 237	}	
historical_ideas = {			
}			
historical_units = {			
	napoleonic_square		
	napoleonic_lancers		
	flying_battery		
}			
monarch_names = {			
	"Tromp #0" = 10		
	"Maduro #0" = 10		
	"Croes #0" = 10		
	"Geerman #0" = 10		
	"Kock #0" = 10		
	"Kelly #0" = 10		
	"Ras #0" = 10		
	"Werleman #0" = 10		
	"Arends #0" = 10		
	"Koolman #0" = 10		
	"Boekhoudt #0" = 10		
	"Wever #0" = 10		
	"de Cuba #0" = 10		
	"Lacle #0" = 10		
	"Vrolijk #0" = 10		
	"Figaroa #0" = 10		
	"Oduber #0" = 10		
	"Rasmijn #0" = 10		
	"Dirksz #0" = 10		
	"Dijkhoff #0" = 10		
	"Lampe #0" = 10		
	"Richardson #0" = 10		
	"Angela #0" = 10		
	"Lopez #0" = 10		
	"London #0" = 10		
	"Thiel #0" = 10		
	"Hernandez #0" = 10		
	"Ruiz #0" = 10		
	"Gomez #0" = 10		
	"Franken #0" = 10		
	"Jansen #0" = 10		
	"Perez #0" = 10		
	"Wernet #0" = 10		
	"Driver #0" = 10		
	"Erasmus #0" = 10		
	"Solognier #0" = 10		
	"Giel #0" = 10		
	"Jacobs #0" = 10		
	"Gomes #0" = 10		
}			
ship_names = {			
	"Coast Guard"		
}			
leader_names			
	Tromp		
	Maduro		
	Croes		
	Geerman		
	Kock		
	Kelly		
	Kelly		
	Ras		
	Werleman		
	Arends		
	Koolman		
	Boekhoudt		
	Henriquez		
	Wever		
	de Cuba		
	Lacle		
	Vrolijk		
	Figaroa		
	Oduber		
	Rasmijn		
	Dirksz		
	Dijkhoff		
	Lampe		
	Richardson		
	Angela		
	Lopez		
	London		
	Thiel		
	Hernandez		
	Ruiz		
	Gomez		
	Franken		
	Jansen		
	Perez		
	Wernet		
	Driver		
	Erasmus		
	Solognier		
	Giel		
	Jacobs		
	Gomes		
}			
