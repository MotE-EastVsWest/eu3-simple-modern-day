# Country Name:	Rifi Rebels	# Tag:	RF8
#			
graphical_culture = 	muslimgfx		
#			
color = {	8 201 80	}	
historical_ideas = {			
}			
historical_units = {			
	napoleonic_square		
	napoleonic_lancers		
	flying_battery		
}			
monarch_names = {			
	"Ait El #0" = 10		
	"El Houssine #0" = 10		
	"Chaouki #0" = 10		
	"Rais #0" = 10		
	"Dahmani #0" = 10		
	"Laaroussi #0" = 10		
	"Chafik #0" = 10		
	"Sami #0" = 10		
	"Hicham #0" = 10		
	"Nait #0" = 10		
	"Nassiri #0" = 10		
	"Youssef #0" = 10		
	"Yousfi #0" = 10		
	"Nouri #0" = 10		
	"Elidrissi #0" = 10		
	"Moumen #0" = 10		
	"Amraoui #0" = 10		
	"Sebti #0" = 10		
	"Ouali #0" = 10		
	"Allali #0" = 10		
	"Rahmouni #0" = 10		
	"Regragui #0" = 10		
	"Samir #0" = 10		
	"Fares #0" = 10		
	"Benslimane #0" = 10		
	"Sekkat #0" = 10		
	"Housni #0" = 10		
	"Guessous #0" = 10		
	"Moussaid #0" = 10		
	"Rahmani #0" = 10		
	"Karam #0" = 10		
	"Nabil #0" = 10		
	"Zefzefi #0" = 10		
	"Benbrahim #0" = 10		
	"Bouzid #0" = 10		
	"Farah #0" = 10		
	"Zouhair #0" = 10		
	"Seadiki #0" = 10		
	"Chamali #0" = 10		
}			
ship_names = {			
	"Coast Guard"		
}			
leader_names			
	Ait El		
	El Houssine		
	Chaouki		
	Rais		
	Dahmani		
	Laaroussi		
	Laaroussi		
	Chafik		
	Sami		
	Hicham		
	Nait		
	Nassiri		
	Lachhab		
	Youssef		
	Yousfi		
	Nouri		
	Elidrissi		
	Moumen		
	Amraoui		
	Sebti		
	Ouali		
	Allali		
	Rahmouni		
	Regragui		
	Samir		
	Fares		
	Benslimane		
	Sekkat		
	Housni		
	Guessous		
	Moussaid		
	Rahmani		
	Karam		
	Nabil		
	Zefzefi		
	Benbrahim		
	Bouzid		
	Farah		
	Zouhair		
	Seadiki		
	Chamali		
}			
