# Country Name:	Mauritius	# Tag:	RI9
#			
graphical_culture = 	africangfx		
#			
color = {	48 192 36	}	
historical_ideas = {			
	press_gangs		
	grand_navy		
	sea_hawks		
	superior_seamanship		
	naval_glory		
	excellent_shipwrights		
	naval_fighting_instruction		
	naval_provisioning		
	shrewd_commerce_practise		
	merchant_adventures		
	improved_foraging		
}			
historical_units = {			
	napoleonic_square		
	napoleonic_lancers		
	flying_battery		
}			
monarch_names = {			
	"Beeharry #0" = 10		
	"Bundhoo #0" = 10		
	"Appadoo #0" = 10		
	"Gopaul #0" = 10		
	"Boodhoo #0" = 10		
	"Li #0" = 10		
	"Bissessur #0" = 10		
	"Ramchurn #0" = 10		
	"Ramsamy #0" = 10		
	"Mungur #0" = 10		
	"Wong #0" = 10		
	"Sookun #0" = 10		
	"Ng #0" = 10		
	"Marie #0" = 10		
	"Joomun #0" = 10		
	"Boodhun #0" = 10		
	"Luchmun #0" = 10		
	"Francois #0" = 10		
	"Perrine #0" = 10		
	"Ramasawmy #0" = 10		
	"Nunkoo #0" = 10		
	"Mohun #0" = 10		
	"Chung #0" = 10		
	"Louis #0" = 10		
	"Luximon #0" = 10		
	"Jean #0" = 10		
	"Mungroo #0" = 10		
	"Lee #0" = 10		
	"Pierre #0" = 10		
	"Teeluck #0" = 10		
	"Mootoosamy #0" = 10		
	"Abdool #0" = 10		
	"Ramessur #0" = 10		
	"Ramphul #0" = 10		
	"Bholah #0" = 10		
	"Armoogum #0" = 10		
	"Aubeeluck #0" = 10		
	"Ramdin #0" = 10		
	"Domun #0" = 10		
}			
ship_names = {			
	"Coast Guard"		
}			
leader_names = {			
	Beeharry		
	Bundhoo		
	Appadoo		
	Gopaul		
	Boodhoo		
	Li		
	Li		
	Bissessur		
	Ramchurn		
	Ramsamy		
	Mungur		
	Wong		
	Chan		
	Sookun		
	Ng		
	Marie		
	Joomun		
	Boodhun		
	Luchmun		
	Francois		
	Perrine		
	Ramasawmy		
	Nunkoo		
	Mohun		
	Chung		
	Louis		
	Luximon		
	Jean		
	Mungroo		
	Lee		
	Pierre		
	Teeluck		
	Mootoosamy		
	Abdool		
	Ramessur		
	Ramphul		
	Bholah		
	Armoogum		
	Aubeeluck		
	Ramdin		
	Domun		
}			
