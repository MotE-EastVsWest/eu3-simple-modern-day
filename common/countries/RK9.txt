# Country Name:	Cherkasia	# Tag:	RK9
#			
graphical_culture = 	muslimgfx		
#			
color = {	177 73 80	}	
historical_ideas = {			
	ecumenism		
	church_attendance_duty		
	divine_supremacy		
	deus_vult		
	revolution_and_counter		
	press_gangs		
	national_conscripts		
	napoleonic_warfare		
	vice_roys		
	vetting		
	bureaucracy		
}			
historical_units = {			
	napoleonic_square		
	napoleonic_lancers		
	flying_battery		
}			
monarch_names = {			
	"Semenova #0" = 10		
	"Uzdenov #0" = 10		
	"Uzdenova #0" = 10		
	"Lebedeva #0" = 10		
	"Kharatokova #0" = 10		
	"Malsugenov #0" = 10		
	"Gukev #0" = 10		
	"Zverev #0" = 10		
	"Radionova #0" = 10		
	"Zamesov #0" = 10		
	"Baychorov #0" = 10		
	"Salpagarov #0" = 10		
	"Bostanov #0" = 10		
	"Batchaev #0" = 10		
	"Bayramukova #0" = 10		
	"Botasheva #0" = 10		
	"Batchaeva #0" = 10		
	"Kyatov #0" = 10		
	"Botashev #0" = 10		
	"Urusova #0" = 10		
	"Semenov #0" = 10		
	"Laypanov #0" = 10		
	"Erkenov #0" = 10		
	"Ivanov #0" = 10		
	"Bidzhieva #0" = 10		
	"Bidzhiev #0" = 10		
	"Bostanova #0" = 10		
	"Chotchaeva #0" = 10		
	"Salpagarova #0" = 10		
	"Shebzukhova #0" = 10		
	"Kipkeeva #0" = 10		
	"Tambiev #0" = 10		
	"Bayramkulova #0" = 10		
	"Bayramkulov #0" = 10		
	"Khubiev #0" = 10		
	"Khapsirokova #0" = 10		
	"Meremukova #0" = 10		
	"Sidakov #0" = 10		
	"Misrokova #0" = 10		
}			
ship_names = {			
	"Coast Guard"		
}			
leader_names = {			
	Semenova		
	Uzdenov		
	Uzdenova		
	Lebedeva		
	Kharatokova		
	Malsugenov		
	Malsugenov		
	Gukev		
	Zverev		
	Radionova		
	Zamesov		
	Baychorov		
	Bayramukov		
	Salpagarov		
	Bostanov		
	Batchaev		
	Bayramukova		
	Botasheva		
	Batchaeva		
	Kyatov		
	Botashev		
	Urusova		
	Semenov		
	Laypanov		
	Erkenov		
	Ivanov		
	Bidzhieva		
	Bidzhiev		
	Bostanova		
	Chotchaeva		
	Salpagarova		
	Shebzukhova		
	Kipkeeva		
	Tambiev		
	Bayramkulova		
	Bayramkulov		
	Khubiev		
	Khapsirokova		
	Meremukova		
	Sidakov		
	Misrokova		
}			
