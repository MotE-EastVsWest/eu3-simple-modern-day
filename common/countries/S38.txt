# Country Name:	Saban Rebels	# Tag:	S38
#			
graphical_culture = 	africangfx		
#			
color = {	113 211 111	}	
historical_ideas = {			
}			
historical_units = {			
	napoleonic_square		
	napoleonic_lancers		
	flying_battery		
}			
monarch_names = {			
	"Hassell #0" = 10		
	"Johnson #0" = 10		
	"Simmons #0" = 10		
	"Zagers #0" = 10		
	"Peterson #0" = 10		
	"Linzey #0" = 10		
	"Heyliger #0" = 10		
	"Wilson #0" = 10		
	"Barnes #0" = 10		
	"Every #0" = 10		
	"Levenstone #0" = 10		
	"Whitfield #0" = 10		
	"Woods #0" = 10		
	"Buchanan #0" = 10		
	"Collins #0" = 10		
	"Geenty #0" = 10		
	"Granger #0" = 10		
	"Holm #0" = 10		
	"Smith #0" = 10		
	"Vant Hof #0" = 10		
	"Anderson #0" = 10		
	"Van Oop #0" = 10		
	"Van der Groot #0" = 10		
	"Van Saba #0" = 10		
	"Van der Hammel #0" = 10		
	"Van Oep #0" = 10		
	"Van der Meer #0" = 10		
	"Van der Bilt #0" = 10		
	"Oop #0" = 10		
	"Oof #0" = 10		
	"de Saba #0" = 10		
	"Mariner #0" = 10		
	"Seahawker #0" = 10		
	"de Saab #0" = 10		
	"de Cock #0" = 10		
	"Van Madi #0" = 10		
	"Van der Clarke #0" = 10		
	"Van Life #0" = 10		
	"Joonson #0" = 10		
}			
ship_names = {			
	"Coast Guard"		
}			
leader_names			
	Hassell		
	Johnson		
	Simmons		
	Zagers		
	Peterson		
	Linzey		
	Linzey		
	Heyliger		
	Wilson		
	Barnes		
	Every		
	Levenstone		
	Sorton		
	Whitfield		
	Woods		
	Buchanan		
	Collins		
	Geenty		
	Granger		
	Holm		
	Smith		
	Vant Hof		
	Anderson		
	Van Oop		
	Van der Groot		
	Van Saba		
	Van der Hammel		
	Van Oep		
	Van der Meer		
	Van der Bilt		
	Oop		
	Oof		
	de Saba		
	Mariner		
	Seahawker		
	de Saab		
	de Cock		
	Van Madi		
	Van der Clarke		
	Van Life		
	Joonson		
}			
