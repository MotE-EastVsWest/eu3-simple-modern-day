# Country Name:	Senegal	# Tag:	SG9
#			
graphical_culture = 	africangfx		
#			
color = {	219 148 194	}	
historical_ideas = {			
	ecumenism		
	church_attendance_duty		
	divine_supremacy		
	deus_vult		
	revolution_and_counter		
	press_gangs		
	national_conscripts		
	napoleonic_warfare		
	vice_roys		
	vetting		
	bureaucracy		
}			
historical_units = {			
	napoleonic_square		
	napoleonic_lancers		
	flying_battery		
}			
monarch_names = {			
	"Ndiaye #0" = 10		
	"Diop #0" = 10		
	"Ba #0" = 10		
	"Diallo #0" = 10		
	"Sow #0" = 10		
	"Diouf #0" = 10		
	"Gueye #0" = 10		
	"Fall #0" = 10		
	"Faye #0" = 10		
	"Sarr #0" = 10		
	"Cisse #0" = 10		
	"Niang #0" = 10		
	"Thiam #0" = 10		
	"Mbaye #0" = 10		
	"Sene #0" = 10		
	"Ka #0" = 10		
	"Dieng #0" = 10		
	"Diagne #0" = 10		
	"Balde #0" = 10		
	"Toure #0" = 10		
	"Dia #0" = 10		
	"Sy #0" = 10		
	"Sall #0" = 10		
	"Ngom #0" = 10		
	"Camara #0" = 10		
	"Ndao #0" = 10		
	"Gaye #0" = 10		
	"Kane #0" = 10		
	"Drame #0" = 10		
	"Diatta #0" = 10		
	"Sane #0" = 10		
	"Lo #0" = 10		
	"Mbengue #0" = 10		
	"Diaw #0" = 10		
	"Dione #0" = 10		
	"Sylla #0" = 10		
	"Samb #0" = 10		
	"Ndour #0" = 10		
	"Mane #0" = 10		
}			
ship_names = {			
	"Coast Guard"		
}			
leader_names = {			
	Ndiaye		
	Diop		
	Ba		
	Diallo		
	Sow		
	Diouf		
	Diouf		
	Gueye		
	Fall		
	Faye		
	Sarr		
	Cisse		
	Seck		
	Niang		
	Thiam		
	Mbaye		
	Sene		
	Ka		
	Dieng		
	Diagne		
	Balde		
	Toure		
	Dia		
	Sy		
	Sall		
	Ngom		
	Camara		
	Ndao		
	Gaye		
	Kane		
	Drame		
	Diatta		
	Sane		
	Lo		
	Mbengue		
	Diaw		
	Dione		
	Sylla		
	Samb		
	Ndour		
	Mane		
}			
