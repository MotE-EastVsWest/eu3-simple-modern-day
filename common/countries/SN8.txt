# Country Name:	Singaporian Rebels	# Tag:	SN8
#			
graphical_culture = 	chinesegfx		
#			
color = {	197 120 165	}	
historical_ideas = {			
}			
historical_units = {			
	napoleonic_square		
	napoleonic_lancers		
	flying_battery		
}			
monarch_names = {			
	"Tan #0" = 10		
	"Chan #0" = 10		
	"Chong #0" = 10		
	"Chen #0" = 10		
	"Sim #0" = 10		
	"Wang #0" = 10		
	"Li #0" = 10		
	"Lim #0" = 10		
	"Tang #0" = 10		
	"Lin #0" = 10		
	"Lau #0" = 10		
	"Cheng #0" = 10		
	"Liu #0" = 10		
	"Lai #0" = 10		
	"Quek #0" = 10		
	"Ng #0" = 10		
	"Huang #0" = 10		
	"Seah #0" = 10		
	"Pang #0" = 10		
	"Yang #0" = 10		
	"Neo #0" = 10		
	"Han #0" = 10		
	"Gan #0" = 10		
	"Wu #0" = 10		
	"Chang #0" = 10		
	"Song #0" = 10		
	"Hong #0" = 10		
	"Wong #0" = 10		
	"Chow #0" = 10		
	"Ling #0" = 10		
	"Wei #0" = 10		
	"Yu #0" = 10		
	"Ong #0" = 10		
	"Kong #0" = 10		
	"Phua #0" = 10		
	"Goh #0" = 10		
	"Teng #0" = 10		
	"Xu #0" = 10		
	"Chua #0" = 10		
}			
ship_names = {			
	"Coast Guard"		
}			
leader_names			
	Tan		
	Chan		
	Chong		
	Chen		
	Sim		
	Wang		
	Wang		
	Li		
	Lim		
	Tang		
	Lin		
	Lau		
	Zhang		
	Cheng		
	Liu		
	Lai		
	Quek		
	Ng		
	Huang		
	Seah		
	Pang		
	Yang		
	Neo		
	Han		
	Gan		
	Wu		
	Chang		
	Song		
	Hong		
	Wong		
	Chow		
	Ling		
	Wei		
	Yu		
	Ong		
	Kong		
	Phua		
	Goh		
	Teng		
	Xu		
	Chua		
}			
