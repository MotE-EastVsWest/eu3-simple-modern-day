# Country Name:	South Sudanese Rebels	# Tag:	SS8
#			
graphical_culture = 	africangfx		
#			
color = {	179 14 96	}	
historical_ideas = {			
	ecumenism		
	church_attendance_duty		
	divine_supremacy		
	deus_vult		
	revolution_and_counter		
	press_gangs		
	national_conscripts		
	napoleonic_warfare		
	vice_roys		
	vetting		
	bureaucracy		
}			
historical_units = {			
	napoleonic_square		
	napoleonic_lancers		
	flying_battery		
}			
monarch_names = {			
	"Deng #0" = 10		
	"Chol #0" = 10		
	"Bol #0" = 10		
	"Chuol #0" = 10		
	"John #0" = 10		
	"Kuol #0" = 10		
	"Majok #0" = 10		
	"Garang #0" = 10		
	"Tut #0" = 10		
	"Peter #0" = 10		
	"Lual #0" = 10		
	"Gatluak #0" = 10		
	"Malual #0" = 10		
	"Lam #0" = 10		
	"James #0" = 10		
	"Dau #0" = 10		
	"Koang #0" = 10		
	"Biel #0" = 10		
	"Jok #0" = 10		
	"Atem #0" = 10		
	"Gatwech #0" = 10		
	"Kong #0" = 10		
	"Dak #0" = 10		
	"Pal #0" = 10		
	"Gatkuoth #0" = 10		
	"Mabior #0" = 10		
	"Jal #0" = 10		
	"Wal #0" = 10		
	"Riek #0" = 10		
	"Both #0" = 10		
	"Ajak #0" = 10		
	"MacHar #0" = 10		
	"Thon #0" = 10		
	"Nhial #0" = 10		
	"Mayen #0" = 10		
	"Riak #0" = 10		
	"Makuach #0" = 10		
	"Paul #0" = 10		
	"Puok #0" = 10		
}			
ship_names = {			
	"Coast Guard"		
}			
leader_names = {			
	Deng		
	Chol		
	Bol		
	Chuol		
	John		
	Kuol		
	Kuol		
	Majok		
	Garang		
	Tut		
	Peter		
	Lual		
	Gai		
	Gatluak		
	Malual		
	Lam		
	James		
	Dau		
	Koang		
	Biel		
	Jok		
	Atem		
	Gatwech		
	Kong		
	Dak		
	Pal		
	Gatkuoth		
	Mabior		
	Jal		
	Wal		
	Riek		
	Both		
	Ajak		
	MacHar		
	Thon		
	Nhial		
	Mayen		
	Riak		
	Makuach		
	Paul		
	Puok		
}			
