# Country Name:	Botswana	# Tag:	SW9
#			
graphical_culture = 	africangfx		
#			
color = {	165 134 96	}	
historical_ideas = {			
	ecumenism		
	church_attendance_duty		
	divine_supremacy		
	deus_vult		
	revolution_and_counter		
	press_gangs		
	national_conscripts		
	napoleonic_warfare		
	vice_roys		
	vetting		
	bureaucracy		
}			
historical_units = {			
	napoleonic_square		
	napoleonic_lancers		
	flying_battery		
}			
monarch_names = {			
	"Molefe #0" = 10		
	"Modise #0" = 10		
	"Mooketsi #0" = 10		
	"Mosweu #0" = 10		
	"Phiri #0" = 10		
	"Moseki #0" = 10		
	"Pule #0" = 10		
	"Moloi #0" = 10		
	"Kabelo #0" = 10		
	"Moalosi #0" = 10		
	"Moeng #0" = 10		
	"Mogotsi #0" = 10		
	"Joseph #0" = 10		
	"Motsumi #0" = 10		
	"Moyo #0" = 10		
	"Tebogo #0" = 10		
	"Marumo #0" = 10		
	"Morapedi #0" = 10		
	"David #0" = 10		
	"Phuthego #0" = 10		
	"Motsamai #0" = 10		
	"Moatshe #0" = 10		
	"Mogapi #0" = 10		
	"Moilwa #0" = 10		
	"Dube #0" = 10		
	"Mokgosi #0" = 10		
	"Thapelo #0" = 10		
	"Montsho #0" = 10		
	"Solomon #0" = 10		
	"Thato #0" = 10		
	"Simon #0" = 10		
	"Letsholo #0" = 10		
	"John #0" = 10		
	"Lesego #0" = 10		
	"Mogorosi #0" = 10		
	"Mothibi #0" = 10		
	"Pilane #0" = 10		
	"Ncube #0" = 10		
	"Moremi #0" = 10		
}			
ship_names = {			
	"Coast Guard"		
}			
leader_names = {			
	Molefe		
	Modise		
	Mooketsi		
	Mosweu		
	Phiri		
	Moseki		
	Moseki		
	Pule		
	Moloi		
	Kabelo		
	Moalosi		
	Moeng		
	Kenosi		
	Mogotsi		
	Joseph		
	Motsumi		
	Moyo		
	Tebogo		
	Marumo		
	Morapedi		
	David		
	Phuthego		
	Motsamai		
	Moatshe		
	Mogapi		
	Moilwa		
	Dube		
	Mokgosi		
	Thapelo		
	Montsho		
	Solomon		
	Thato		
	Simon		
	Letsholo		
	John		
	Lesego		
	Mogorosi		
	Mothibi		
	Pilane		
	Ncube		
	Moremi		
}			
