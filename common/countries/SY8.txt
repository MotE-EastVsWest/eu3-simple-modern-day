# Country Name:	Alawite Syrian Rebels	# Tag:	SY8
#			
graphical_culture = 	muslimgfx		
#			
color = {	173 146 98	}	
historical_ideas = {			
	ecumenism		
	church_attendance_duty		
	divine_supremacy		
	deus_vult		
	revolution_and_counter		
	press_gangs		
	national_conscripts		
	napoleonic_warfare		
	vice_roys		
	vetting		
	bureaucracy		
}			
historical_units = {			
	napoleonic_square		
	napoleonic_lancers		
	flying_battery		
}			
monarch_names = {			
	"Khaled #0" = 10		
	"Mohamed #0" = 10		
	"Al Numan #0" = 10		
	"Ahmed #0" = 10		
	"Ali #0" = 10		
	"Hassan #0" = 10		
	"Hussein #0" = 10		
	"Ibrahim #0" = 10		
	"Mahmoud #0" = 10		
	"Samaan #0" = 10		
	"Saleh #0" = 10		
	"Mostafa #0" = 10		
	"Alththania #0" = 10		
	"Allah #0" = 10		
	"Suleiman #0" = 10		
	"Khalaf #0" = 10		
	"Mohammed #0" = 10		
	"Yousef #0" = 10		
	"Aldaman #0" = 10		
	"Khalil #0" = 10		
	"Ahmad #0" = 10		
	"El Din #0" = 10		
	"Eisaa #0" = 10		
	"Ismail #0" = 10		
	"Hamoud #0" = 10		
	"Abdel #0" = 10		
	"Saeed #0" = 10		
	"Mousa #0" = 10		
	"Omar #0" = 10		
	"Jassim #0" = 10		
	"Rahman #0" = 10		
	"Sheikh #0" = 10		
	"Awad #0" = 10		
	"Deeb #0" = 10		
	"Al-Karim #0" = 10		
	"Azza #0" = 10		
	"Abdo #0" = 10		
	"Abbas #0" = 10		
	"Elkader #0" = 10		
}			
ship_names = {			
	"Coast Guard"		
}			
leader_names = {			
	Khaled		
	Mohamed		
	Al Numan		
	Ahmed		
	Ali		
	Hassan		
	Hassan		
	Hussein		
	Ibrahim		
	Mahmoud		
	Samaan		
	Saleh		
	Alzuhur		
	Mostafa		
	Alththania		
	Allah		
	Suleiman		
	Khalaf		
	Mohammed		
	Yousef		
	Aldaman		
	Khalil		
	Ahmad		
	El Din		
	Eisaa		
	Ismail		
	Hamoud		
	Abdel		
	Saeed		
	Mousa		
	Omar		
	Jassim		
	Rahman		
	Sheikh		
	Awad		
	Deeb		
	Al-Karim		
	Azza		
	Abdo		
	Abbas		
	Elkader		
}			
