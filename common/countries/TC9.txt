# Country Name:	Turks & Caicos	# Tag:	TC9
#			
graphical_culture = 	africangfx		
#			
color = {	21 91 25	}	
historical_ideas = {			
	press_gangs		
	grand_navy		
	sea_hawks		
	superior_seamanship		
	naval_glory		
	excellent_shipwrights		
	naval_fighting_instruction		
	naval_provisioning		
	shrewd_commerce_practise		
	merchant_adventures		
	improved_foraging		
}			
historical_units = {			
	napoleonic_square		
	napoleonic_lancers		
	flying_battery		
}			
monarch_names = {			
	"Williams #0" = 10		
	"Forbes #0" = 10		
	"Smith #0" = 10		
	"Gardiner #0" = 10		
	"Missick #0" = 10		
	"Hall #0" = 10		
	"Rigby #0" = 10		
	"Lightbourne #0" = 10		
	"Robinson #0" = 10		
	"Handfield #0" = 10		
	"Been #0" = 10		
	"Harvey #0" = 10		
	"Simmons #0" = 10		
	"Ewing #0" = 10		
	"Parker #0" = 10		
	"Wilson #0" = 10		
	"Cox #0" = 10		
	"Delancy #0" = 10		
	"Higgs #0" = 10		
	"Taylor #0" = 10		
	"Walkin #0" = 10		
	"Seymour #0" = 10		
	"Malcolm #0" = 10		
	"Swann #0" = 10		
	"Garland #0" = 10		
	"Jennings #0" = 10		
	"Fulford #0" = 10		
	"Hamilton #0" = 10		
	"Thomas #0" = 10		
	"Ingham #0" = 10		
	"Pierre #0" = 10		
	"Adams #0" = 10		
	"Clare #0" = 10		
	"Grant #0" = 10		
	"Gray #0" = 10		
	"Astwood #0" = 10		
	"Lewis #0" = 10		
	"Francis #0" = 10		
	"Penn #0" = 10		
}			
ship_names = {			
	"Coast Guard"		
}			
leader_names = {			
	Williams		
	Forbes		
	Smith		
	Gardiner		
	Missick		
	Hall		
	Hall		
	Rigby		
	Lightbourne		
	Robinson		
	Handfield		
	Been		
	Stubbs		
	Harvey		
	Simmons		
	Ewing		
	Parker		
	Wilson		
	Cox		
	Delancy		
	Higgs		
	Taylor		
	Walkin		
	Seymour		
	Malcolm		
	Swann		
	Garland		
	Jennings		
	Fulford		
	Hamilton		
	Thomas		
	Ingham		
	Pierre		
	Adams		
	Clare		
	Grant		
	Gray		
	Astwood		
	Lewis		
	Francis		
	Penn		
}			
