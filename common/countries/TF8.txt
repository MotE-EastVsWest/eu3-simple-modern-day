# Country Name:	White South African Rebels	# Tag:	TF8
#			
graphical_culture = 	latingfx		
#			
color = {	81 176 193	}	
historical_ideas = {			
	vetting		
	national_bank		
	national_trade_policy		
	smithian_economics		
	shrewd_commerce_practise		
	scientific_revolution		
	sea_hawks		
	naval_provisioning		
	regimental_system		
	napoleonic_warfare		
	merchant_adventures		
}			
historical_units = {			
	napoleonic_square		
	napoleonic_lancers		
	flying_battery		
}			
monarch_names = {			
	"Williams #0" = 10		
	"Jacobs #0" = 10		
	"Adams #0" = 10		
	"Abrahams #0" = 10		
	"Davids #0" = 10		
	"Smith #0" = 10		
	"Hendricks #0" = 10		
	"Petersen #0" = 10		
	"van Wyk #0" = 10		
	"Daniels #0" = 10		
	"Louw #0" = 10		
	"Botha #0" = 10		
	"Fortuin #0" = 10		
	"Booysen #0" = 10		
	"Cloete #0" = 10		
	"Coetzee #0" = 10		
	"Isaacs #0" = 10		
	"van der Merwe #0" = 10		
	"Meyer #0" = 10		
	"Smit #0" = 10		
	"Jansen #0" = 10		
	"Du Toit #0" = 10		
	"Pietersen #0" = 10		
	"van Zyl #0" = 10		
	"Du Plessis #0" = 10		
	"Johnson #0" = 10		
	"Solomons #0" = 10		
	"September #0" = 10		
	"Nel #0" = 10		
	"Muller #0" = 10		
	"Cupido #0" = 10		
	"Fourie #0" = 10		
	"Le Roux #0" = 10		
	"Engelbrecht #0" = 10		
	"Erasmus #0" = 10		
	"van Niekerk #0" = 10		
	"Brown #0" = 10		
	"Martin #0" = 10		
	"Swart #0" = 10		
}			
ship_names = {			
	"Coast Guard"		
}			
leader_names = {			
	Williams		
	Jacobs		
	Adams		
	Abrahams		
	Davids		
	Smith		
	Smith		
	Hendricks		
	Petersen		
	van Wyk		
	Daniels		
	Louw		
	Arendse		
	Botha		
	Fortuin		
	Booysen		
	Cloete		
	Coetzee		
	Isaacs		
	van der Merwe		
	Meyer		
	Smit		
	Jansen		
	Du Toit		
	Pietersen		
	van Zyl		
	Du Plessis		
	Johnson		
	Solomons		
	September		
	Nel		
	Muller		
	Cupido		
	Fourie		
	Le Roux		
	Engelbrecht		
	Erasmus		
	van Niekerk		
	Brown		
	Martin		
	Swart		
}			
