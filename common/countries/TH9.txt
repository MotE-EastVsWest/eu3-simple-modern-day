# Country Name:	Tahiti	# Tag:	TH9
#			
graphical_culture = 	northamericagfx		
#			
color = {	20 19 56	}	
historical_ideas = {			
	press_gangs		
	grand_navy		
	sea_hawks		
	superior_seamanship		
	naval_glory		
	excellent_shipwrights		
	naval_fighting_instruction		
	naval_provisioning		
	shrewd_commerce_practise		
	merchant_adventures		
	improved_foraging		
}			
historical_units = {			
	napoleonic_square		
	napoleonic_lancers		
	flying_battery		
}			
monarch_names = {			
	"Wong #0" = 10		
	"Tetuanui #0" = 10		
	"Richmond #0" = 10		
	"Amaru #0" = 10		
	"Mai #0" = 10		
	"Lucas #0" = 10		
	"Teuira #0" = 10		
	"Lai #0" = 10		
	"Salmon #0" = 10		
	"Chung #0" = 10		
	"Hauata #0" = 10		
	"Chan #0" = 10		
	"Bennett #0" = 10		
	"Martin #0" = 10		
	"Temauri #0" = 10		
	"Flores #0" = 10		
	"Ly #0" = 10		
	"Frogier #0" = 10		
	"Lee #0" = 10		
	"Ebb #0" = 10		
	"Utia #0" = 10		
	"Teihotaata #0" = 10		
	"Temarii #0" = 10		
	"Teriipaia #0" = 10		
	"Bonno #0" = 10		
	"Tepa #0" = 10		
	"Deane #0" = 10		
	"Teheiura #0" = 10		
	"Viriamu #0" = 10		
	"Taerea #0" = 10		
	"Tinorua #0" = 10		
	"Clark #0" = 10		
	"Lau #0" = 10		
	"Mou #0" = 10		
	"Tehei #0" = 10		
	"Barff #0" = 10		
	"Cheung #0" = 10		
	"Doom #0" = 10		
	"Teriierooiterai #0" = 10		
}			
ship_names = {			
	"Coast Guard"		
}			
leader_names = {			
	Wong		
	Tetuanui		
	Richmond		
	Amaru		
	Mai		
	Lucas		
	Lucas		
	Teuira		
	Lai		
	Salmon		
	Chung		
	Hauata		
	Lehartel		
	Chan		
	Bennett		
	Martin		
	Temauri		
	Flores		
	Ly		
	Frogier		
	Lee		
	Ebb		
	Utia		
	Teihotaata		
	Temarii		
	Teriipaia		
	Bonno		
	Tepa		
	Deane		
	Teheiura		
	Viriamu		
	Taerea		
	Tinorua		
	Clark		
	Lau		
	Mou		
	Tehei		
	Barff		
	Cheung		
	Doom		
	Teriierooiterai		
}			
