# Country Name:	Tajik Rebels	# Tag:	TJ8
#			
graphical_culture = 	muslimgfx		
#			
color = {	122 150 96	}	
historical_ideas = {			
	ecumenism		
	church_attendance_duty		
	divine_supremacy		
	deus_vult		
	revolution_and_counter		
	press_gangs		
	national_conscripts		
	napoleonic_warfare		
	vice_roys		
	vetting		
	bureaucracy		
}			
historical_units = {			
	napoleonic_square		
	napoleonic_lancers		
	flying_battery		
}			
monarch_names = {			
	"Sharipova #0" = 10		
	"Sharipov #0" = 10		
	"Saidova #0" = 10		
	"Saidov #0" = 10		
	"Mirzoeva #0" = 10		
	"Mirzoev #0" = 10		
	"Safarova #0" = 10		
	"Safarov #0" = 10		
	"Kurbonova #0" = 10		
	"Kurbonov #0" = 10		
	"Abdulloeva #0" = 10		
	"Davlatova #0" = 10		
	"Davlatov #0" = 10		
	"Boboeva #0" = 10		
	"Boboev #0" = 10		
	"Odinaeva #0" = 10		
	"Radzhabova #0" = 10		
	"Odinaev #0" = 10		
	"Radzhabov #0" = 10		
	"Dzhuraeva #0" = 10		
	"Dzhuraev #0" = 10		
	"Rakhmonova #0" = 10		
	"Rakhmonov #0" = 10		
	"Akhmedova #0" = 10		
	"Murodova #0" = 10		
	"Akhmedov #0" = 10		
	"Murodov #0" = 10		
	"Kholova #0" = 10		
	"Kholov #0" = 10		
	"Alieva #0" = 10		
	"Aliev #0" = 10		
	"Sattorova #0" = 10		
	"Sattorov #0" = 10		
	"Nabieva #0" = 10		
	"Nabiev #0" = 10		
	"Olimova #0" = 10		
	"Olimov #0" = 10		
	"Kamolova #0" = 10		
	"Kamolov #0" = 10		
}			
ship_names = {			
	"Coast Guard"		
}			
leader_names = {			
	Sharipova		
	Sharipov		
	Saidova		
	Saidov		
	Mirzoeva		
	Mirzoev		
	Mirzoev		
	Safarova		
	Safarov		
	Kurbonova		
	Kurbonov		
	Abdulloeva		
	Abdulloev		
	Davlatova		
	Davlatov		
	Boboeva		
	Boboev		
	Odinaeva		
	Radzhabova		
	Odinaev		
	Radzhabov		
	Dzhuraeva		
	Dzhuraev		
	Rakhmonova		
	Rakhmonov		
	Akhmedova		
	Murodova		
	Akhmedov		
	Murodov		
	Kholova		
	Kholov		
	Alieva		
	Aliev		
	Sattorova		
	Sattorov		
	Nabieva		
	Nabiev		
	Olimova		
	Olimov		
	Kamolova		
	Kamolov		
}			
