# Country Name:	Transnistrian Rebels	# Tag:	TN8
#			
graphical_culture = 	easterngfx		
#			
color = {	137 63 167	}	
historical_ideas = {			
	ecumenism		
	church_attendance_duty		
	divine_supremacy		
	deus_vult		
	revolution_and_counter		
	press_gangs		
	national_conscripts		
	napoleonic_warfare		
	vice_roys		
	vetting		
	bureaucracy		
}			
historical_units = {			
	napoleonic_square		
	napoleonic_lancers		
	flying_battery		
}			
monarch_names = {			
	"Cheban #0" = 10		
	"Ivanov #0" = 10		
	"Tsurkan #0" = 10		
	"Ivanova #0" = 10		
	"Muntyan #0" = 10		
	"Melnik #0" = 10		
	"Balan #0" = 10		
	"Tabak #0" = 10		
	"Nikolaeva #0" = 10		
	"Shvets #0" = 10		
	"Popov #0" = 10		
	"Kovalenko #0" = 10		
	"Petrov #0" = 10		
	"Foksha #0" = 10		
	"Nikolaev #0" = 10		
	"Tkachenko #0" = 10		
	"Rusnak #0" = 10		
	"Popova #0" = 10		
	"Chebotar #0" = 10		
	"Shevchenko #0" = 10		
	"Tkach #0" = 10		
	"Rotar #0" = 10		
	"Kushnir #0" = 10		
	"Bondarenko #0" = 10		
	"Sandul #0" = 10		
	"Koval #0" = 10		
	"Petrova #0" = 10		
	"Karaman #0" = 10		
	"Mazur #0" = 10		
	"Kovalchuk #0" = 10		
	"Kolesnik #0" = 10		
	"Russu #0" = 10		
	"Stoyanova #0" = 10		
	"Sokolov #0" = 10		
	"Stoyanov #0" = 10		
	"Chebruchan #0" = 10		
	"Morar #0" = 10		
	"Melnichenko #0" = 10		
	"Sokolova #0" = 10		
}			
ship_names = {			
	"Coast Guard"		
}			
leader_names = {			
	Cheban		
	Ivanov		
	Tsurkan		
	Ivanova		
	Muntyan		
	Melnik		
	Melnik		
	Balan		
	Tabak		
	Nikolaeva		
	Shvets		
	Popov		
	Kozhukhar		
	Kovalenko		
	Petrov		
	Foksha		
	Nikolaev		
	Tkachenko		
	Rusnak		
	Popova		
	Chebotar		
	Shevchenko		
	Tkach		
	Rotar		
	Kushnir		
	Bondarenko		
	Sandul		
	Koval		
	Petrova		
	Karaman		
	Mazur		
	Kovalchuk		
	Kolesnik		
	Russu		
	Stoyanova		
	Sokolov		
	Stoyanov		
	Chebruchan		
	Morar		
	Melnichenko		
	Sokolova		
}			
