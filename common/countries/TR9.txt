# Country Name:	Turkey	# Tag:	TR9
#			
graphical_culture = 	muslimgfx		
#			
color = { 126  203  120 }
historical_ideas = {			
	grand_army		
	glorious_arms		
	merchant_adventures		
	scientific_revolution		
	national_bank		
	bill_of_rights		
	smithian_economics		
	liberty_egalite_fraternity		
	patron_of_art		
	humanist_tolerance		
	cabinet		
}			
historical_units = {			
	napoleonic_square		
	napoleonic_lancers		
	flying_battery		
}			
monarch_names = {			
	"Yilmaz #0" = 10		
	"Kaya #0" = 10		
	"Demir #0" = 10		
	"celik #0" = 10		
	"Sahin #0" = 10		
	"Yildiz #0" = 10		
	"Yildirim #0" = 10		
	"Ozturk #0" = 10		
	"Aydin #0" = 10		
	"Ozdemir #0" = 10		
	"Arslan #0" = 10		
	"Kilic #0" = 10		
	"Aslan #0" = 10		
	"cetin #0" = 10		
	"Kara #0" = 10		
	"Koc #0" = 10		
	"Kurt #0" = 10		
	"Ozkan #0" = 10		
	"Acar #0" = 10		
	"Polat #0" = 10		
	"SimSek #0" = 10		
	"Korkmaz #0" = 10		
	"Ozcan #0" = 10		
	"Erdogan #0" = 10		
	"cakir #0" = 10		
	"Yavuz #0" = 10		
	"Can #0" = 10		
	"Sen #0" = 10		
	"Yalcin #0" = 10		
	"Guler #0" = 10		
	"AktaS #0" = 10		
	"GuneS #0" = 10		
	"Bozkurt #0" = 10		
	"Bulut #0" = 10		
	"ISik #0" = 10		
	"Turan #0" = 10		
	"Keskin #0" = 10		
	"Avci #0" = 10		
	"unal #0" = 10		
}			
ship_names = {			
	"Coast Guard"		
}			
leader_names = {			
	Yilmaz		
	Kaya		
	Demir		
	celik		
	Sahin		
	Yildiz		
	Yildiz		
	Yildirim		
	Ozturk		
	Aydin		
	Ozdemir		
	Arslan		
	Dogan		
	Kilic		
	Aslan		
	cetin		
	Kara		
	Koc		
	Kurt		
	Ozkan		
	Acar		
	Polat		
	SimSek		
	Korkmaz		
	Ozcan		
	Erdogan		
	cakir		
	Yavuz		
	Can		
	Sen		
	Yalcin		
	Guler		
	AktaS		
	GuneS		
	Bozkurt		
	Bulut		
	ISik		
	Turan		
	Keskin		
	Avci		
	unal		
}			
