# Country Name:	Tunisia	# Tag:	TS9
#			
graphical_culture = 	muslimgfx		
#			
color = { 146  134  57 }	
historical_ideas = {			
	grand_army		
	glorious_arms		
	merchant_adventures		
	scientific_revolution		
	national_bank		
	bill_of_rights		
	smithian_economics		
	liberty_egalite_fraternity		
	patron_of_art		
	humanist_tolerance		
	cabinet		
}			
historical_units = {			
	napoleonic_square		
	napoleonic_lancers		
	flying_battery		
}			
monarch_names = {			
	"Trabelsi #0" = 10		
	"Gharbi #0" = 10		
	"Hammami #0" = 10		
	"Mohamed #0" = 10		
	"Dridi #0" = 10		
	"Ayari #0" = 10		
	"Cherif #0" = 10		
	"Sassi #0" = 10		
	"Allah #0" = 10		
	"Ammar #0" = 10		
	"Hamdi #0" = 10		
	"Ali #0" = 10		
	"Al Trabelsi #0" = 10		
	"Ayadi #0" = 10		
	"Riahi #0" = 10		
	"Arfaoui #0" = 10		
	"Abidi #0" = 10		
	"El Hadi #0" = 10		
	"Salem #0" = 10		
	"Saidi #0" = 10		
	"Ahmed #0" = 10		
	"Oueslati #0" = 10		
	"Mansour #0" = 10		
	"Nasri #0" = 10		
	"Ben Ali #0" = 10		
	"Chebbi #0" = 10		
	"Ben Amor #0" = 10		
	"Amri #0" = 10		
	"Abid #0" = 10		
	"Al Hamami #0" = 10		
	"Aloui #0" = 10		
	"Kamoun #0" = 10		
	"Ben-Abdallah #0" = 10		
	"Farhat #0" = 10		
	"Amara #0" = 10		
	"Souissi #0" = 10		
	"Mabrouk #0" = 10		
	"Tlili #0" = 10		
	"Omar #0" = 10		
}			
ship_names = {			
	"Coast Guard"		
}			
leader_names = {			
	Trabelsi		
	Gharbi		
	Hammami		
	Mohamed		
	Dridi		
	Ayari		
	Ayari		
	Cherif		
	Sassi		
	Allah		
	Ammar		
	Hamdi		
	Mejri		
	Ali		
	Al Trabelsi		
	Ayadi		
	Riahi		
	Arfaoui		
	Abidi		
	El Hadi		
	Salem		
	Saidi		
	Ahmed		
	Oueslati		
	Mansour		
	Nasri		
	Ben Ali		
	Chebbi		
	Ben Amor		
	Amri		
	Abid		
	Al Hamami		
	Aloui		
	Kamoun		
	Ben Abdallah		
	Farhat		
	Amara		
	Souissi		
	Mabrouk		
	Tlili		
	Omar		
}			
