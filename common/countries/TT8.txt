# Country Name:	Amazigh Rebels	# Tag:	TT8
#			
graphical_culture = 	muslimgfx		
#			
color = {	0 116 148	}	
historical_ideas = {			
	ecumenism		
	church_attendance_duty		
	divine_supremacy		
	deus_vult		
	revolution_and_counter		
	press_gangs		
	national_conscripts		
	napoleonic_warfare		
	vice_roys		
	vetting		
	bureaucracy		
}			
historical_units = {			
	napoleonic_square		
	napoleonic_lancers		
	flying_battery		
}			
monarch_names = {			
	"Abdouali #0" = 10		
	"Ibba #0" = 10		
	"Elies #0" = 10		
	"Abbassi #0" = 10		
	"Bensalem #0" = 10		
	"Zamaki #0" = 10		
	"Tagabou #0" = 10		
	"Benomar #0" = 10		
	"Ougacem #0" = 10		
	"Gassou #0" = 10		
	"Dahmani #0" = 10		
	"Boughrari #0" = 10		
	"Dahou #0" = 10		
	"Abdoudaoui #0" = 10		
	"Hassani #0" = 10		
	"Adami #0" = 10		
	"Tidjani #0" = 10		
	"Kafi #0" = 10		
	"Korichi #0" = 10		
	"Abbassi #0" = 10		
	"Bensaci #0" = 10		
	"Trabelsi #0" = 10		
	"Boulifa #0" = 10		
	"Boubekri #0" = 10		
	"Daoui #0" = 10		
	"Bourenane #0" = 10		
	"Benhamida #0" = 10		
	"Haoued Mouissa #0" = 10		
	"Abid #0" = 10		
	"Derdouri #0" = 10		
	"Lebssisse #0" = 10		
	"Abbas #0" = 10		
	"Bouziane #0" = 10		
	"Messaoudi #0" = 10		
	"Bada #0" = 10		
	"Rachedi #0" = 10		
	"Benseddik #0" = 10		
	"Selami #0" = 10		
	"Saoud #0" = 10		
}			
ship_names = {			
	"Coast Guard"		
}			
leader_names = {			
	Abdouali		
	Ibba		
	Elies		
	Abbassi		
	Bensalem		
	Zamaki		
	Zamaki		
	Tagabou		
	Benomar		
	Ougacem		
	Gassou		
	Dahmani		
	Marmouri		
	Boughrari		
	Dahou		
	Abdoudaoui		
	Hassani		
	Adami		
	Tidjani		
	Kafi		
	Korichi		
	Abbassi		
	Bensaci		
	Trabelsi		
	Boulifa		
	Boubekri		
	Daoui		
	Bourenane		
	Benhamida		
	Haoued Mouissa		
	Abid		
	Derdouri		
	Lebssisse		
	Abbas		
	Bouziane		
	Messaoudi		
	Bada		
	Rachedi		
	Benseddik		
	Selami		
	Saoud		
}			
