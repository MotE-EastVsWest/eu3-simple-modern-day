# Country Name:	Iliridan Rebels	# Tag:	TV8
#			
graphical_culture = 	muslimgfx		
#			
color = {	209 133 100	}	
historical_ideas = {			
}			
historical_units = {			
	napoleonic_square		
	napoleonic_lancers		
	flying_battery		
}			
monarch_names = {			
	"Jovanovska #0" = 10		
	"Ramadani #0" = 10		
	"Idrizi #0" = 10		
	"Redzepi #0" = 10		
	"Ibraimi #0" = 10		
	"Bajrami #0" = 10		
	"Sulejmani #0" = 10		
	"Ismaili #0" = 10		
	"Jovanovski #0" = 10		
	"Ademi #0" = 10		
	"Petrovska #0" = 10		
	"Sabani #0" = 10		
	"Beciri #0" = 10		
	"Asani #0" = 10		
	"Memeti #0" = 10		
	"Mustafi #0" = 10		
	"Jusufi #0" = 10		
	"Petrovski #0" = 10		
	"Iseni #0" = 10		
	"Ristovski #0" = 10		
	"Stojanovska #0" = 10		
	"Zeciri #0" = 10		
	"Ristovska #0" = 10		
	"Kamberi #0" = 10		
	"Stojanovski #0" = 10		
	"Imeri #0" = 10		
	"Bedzeti #0" = 10		
	"Jovanoska #0" = 10		
	"Rustemi #0" = 10		
	"Simovska #0" = 10		
	"Emini #0" = 10		
	"Saciri #0" = 10		
	"Ameti #0" = 10		
	"Ziberi #0" = 10		
	"Islami #0" = 10		
	"Dauti #0" = 10		
	"Gjorgjevska #0" = 10		
	"Murati #0" = 10		
	"Selimi #0" = 10		
}			
ship_names = {			
	"Coast Guard"		
}			
leader_names			
	Jovanovska		
	Ramadani		
	Idrizi		
	Redzepi		
	Ibraimi		
	Bajrami		
	Bajrami		
	Sulejmani		
	Ismaili		
	Jovanovski		
	Ademi		
	Petrovska		
	Osmani		
	Sabani		
	Beciri		
	Asani		
	Memeti		
	Mustafi		
	Jusufi		
	Petrovski		
	Iseni		
	Ristovski		
	Stojanovska		
	Zeciri		
	Ristovska		
	Kamberi		
	Stojanovski		
	Imeri		
	Bedzeti		
	Jovanoska		
	Rustemi		
	Simovska		
	Emini		
	Saciri		
	Ameti		
	Ziberi		
	Islami		
	Dauti		
	Gjorgjevska		
	Murati		
	Selimi		
}			
