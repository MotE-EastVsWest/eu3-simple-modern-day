# Country Name:	Tanzania	# Tag:	TZ9
#			
graphical_culture = 	africangfx		
#			
color = { 81  141  85 }	
historical_ideas = {			
	ecumenism		
	church_attendance_duty		
	divine_supremacy		
	deus_vult		
	revolution_and_counter		
	press_gangs		
	national_conscripts		
	napoleonic_warfare		
	vice_roys		
	vetting		
	bureaucracy		
}			
historical_units = {			
	napoleonic_square		
	napoleonic_lancers		
	flying_battery		
}			
monarch_names = {			
	"Juma #0" = 10		
	"John #0" = 10		
	"Joseph #0" = 10		
	"Charles #0" = 10		
	"Ally #0" = 10		
	"Mohamed #0" = 10		
	"Ramadhani #0" = 10		
	"Saidi #0" = 10		
	"Peter #0" = 10		
	"Hamisi #0" = 10		
	"Said #0" = 10		
	"Mussa #0" = 10		
	"Mohamedi #0" = 10		
	"Bakari #0" = 10		
	"Shabani #0" = 10		
	"Paul #0" = 10		
	"Michael #0" = 10		
	"Elias #0" = 10		
	"Ali #0" = 10		
	"James #0" = 10		
	"Issa #0" = 10		
	"Athumani #0" = 10		
	"Simon #0" = 10		
	"Emmanuel #0" = 10		
	"Selemani #0" = 10		
	"Samwel #0" = 10		
	"Komba #0" = 10		
	"Omary #0" = 10		
	"Hassan #0" = 10		
	"William #0" = 10		
	"Rashidi #0" = 10		
	"Omari #0" = 10		
	"Julius #0" = 10		
	"Sanga #0" = 10		
	"Mathias #0" = 10		
	"Rajabu #0" = 10		
	"Khamis #0" = 10		
	"Thomas #0" = 10		
	"Daniel #0" = 10		
}			
ship_names = {			
	"Coast Guard"		
}			
leader_names = {			
	Juma		
	John		
	Joseph		
	Charles		
	Ally		
	Mohamed		
	Mohamed		
	Ramadhani		
	Saidi		
	Peter		
	Hamisi		
	Said		
	Abdallah		
	Mussa		
	Mohamedi		
	Bakari		
	Shabani		
	Paul		
	Michael		
	Elias		
	Ali		
	James		
	Issa		
	Athumani		
	Simon		
	Emmanuel		
	Selemani		
	Samwel		
	Komba		
	Omary		
	Hassan		
	William		
	Rashidi		
	Omari		
	Julius		
	Sanga		
	Mathias		
	Rajabu		
	Khamis		
	Thomas		
	Daniel		
}			
