# Country Name:	Ukraine	# Tag:	UA9
#			
graphical_culture = 	easterngfx		
#			
color = { 124  183  151 }
historical_ideas = {			
	vetting		
	national_bank		
	national_trade_policy		
	smithian_economics		
	shrewd_commerce_practise		
	scientific_revolution		
	sea_hawks		
	naval_provisioning		
	regimental_system		
	napoleonic_warfare		
	merchant_adventures		
}			
historical_units = {			
	napoleonic_square		
	napoleonic_lancers		
	flying_battery		
}			
monarch_names = {			
	"Melnyk #0" = 10		
	"Shevchenko #0" = 10		
	"Bondarenko #0" = 10		
	"Kovalenko #0" = 10		
	"Boiko #0" = 10		
	"Tkachenko #0" = 10		
	"Kravchenko #0" = 10		
	"Kovalchuk #0" = 10		
	"Koval #0" = 10		
	"Shevchuk #0" = 10		
	"Polyshchuk #0" = 10		
	"Olyinyk #0" = 10		
	"Yvanova #0" = 10		
	"Lysenko #0" = 10		
	"Moroz #0" = 10		
	"Marchenko #0" = 10		
	"Tkachuk #0" = 10		
	"Savchenko #0" = 10		
	"Rudenko #0" = 10		
	"Petrenko #0" = 10		
	"Klymenko #0" = 10		
	"Pavlenko #0" = 10		
	"Yvanov #0" = 10		
	"Kolesnyk #0" = 10		
	"Kravchuk #0" = 10		
	"Kuzmenko #0" = 10		
	"Ponomarenko #0" = 10		
	"Savchuk #0" = 10		
	"Vasylenko #0" = 10		
	"Levchenko #0" = 10		
	"Sydorenko #0" = 10		
	"Kharchenko #0" = 10		
	"Diachenko #0" = 10		
	"Karpenko #0" = 10		
	"Shvets #0" = 10		
	"Havrylyuk #0" = 10		
	"Lytvynenko #0" = 10		
	"Melnychuk #0" = 10		
	"Myroshnychenko #0" = 10		
}			
ship_names = {			
	"Coast Guard"		
}			
leader_names = {			
	Melnyk		
	Shevchenko		
	Bondarenko		
	Kovalenko		
	Boiko		
	Tkachenko		
	Tkachenko		
	Kravchenko		
	Kovalchuk		
	Koval		
	Shevchuk		
	Polyshchuk		
	Bondar		
	Olyinyk		
	Yvanova		
	Lysenko		
	Moroz		
	Marchenko		
	Tkachuk		
	Savchenko		
	Rudenko		
	Petrenko		
	Klymenko		
	Pavlenko		
	Yvanov		
	Kolesnyk		
	Kravchuk		
	Kuzmenko		
	Ponomarenko		
	Savchuk		
	Vasylenko		
	Levchenko		
	Sydorenko		
	Kharchenko		
	Diachenko		
	Karpenko		
	Shvets		
	Havrylyuk		
	Lytvynenko		
	Melnychuk		
	Myroshnychenko		
}			
