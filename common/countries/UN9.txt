# Country Name:	Uganda	# Tag:	UN9
#			
graphical_culture = 	africangfx		
#			
color = {	100 211 10	}	
historical_ideas = {			
	ecumenism		
	church_attendance_duty		
	divine_supremacy		
	deus_vult		
	revolution_and_counter		
	press_gangs		
	national_conscripts		
	napoleonic_warfare		
	vice_roys		
	vetting		
	bureaucracy		
}			
historical_units = {			
	napoleonic_square		
	napoleonic_lancers		
	flying_battery		
}			
monarch_names = {			
	"Akello #0" = 10		
	"Okello #0" = 10		
	"Mbabazi #0" = 10		
	"Apio #0" = 10		
	"Asiimwe #0" = 10		
	"Auma #0" = 10		
	"Nabirye #0" = 10		
	"Biira #0" = 10		
	"Opio #0" = 10		
	"Tumusiime #0" = 10		
	"Birungi #0" = 10		
	"Babirye #0" = 10		
	"Byaruhanga #0" = 10		
	"Natukunda #0" = 10		
	"Odongo #0" = 10		
	"Kabugho #0" = 10		
	"Kiiza #0" = 10		
	"Adong #0" = 10		
	"Nabwire #0" = 10		
	"Atim #0" = 10		
	"Mugisha #0" = 10		
	"Bwambale #0" = 10		
	"Kato #0" = 10		
	"Ojok #0" = 10		
	"Nangobi #0" = 10		
	"Naigaga #0" = 10		
	"Adongo #0" = 10		
	"Byamukama #0" = 10		
	"Nalubega #0" = 10		
	"Masika #0" = 10		
	"Katushabe #0" = 10		
	"Namatovu #0" = 10		
	"Acen #0" = 10		
	"Ajok #0" = 10		
	"Namutebi #0" = 10		
	"Otim #0" = 10		
	"Matovu #0" = 10		
	"Ogwang #0" = 10		
	"Mbambu #0" = 10		
}			
ship_names = {			
	"Coast Guard"		
}			
leader_names = {			
	Akello		
	Okello		
	Mbabazi		
	Apio		
	Asiimwe		
	Auma		
	Auma		
	Nabirye		
	Biira		
	Opio		
	Tumusiime		
	Birungi		
	Muhindo		
	Babirye		
	Byaruhanga		
	Natukunda		
	Odongo		
	Kabugho		
	Kiiza		
	Adong		
	Nabwire		
	Atim		
	Mugisha		
	Bwambale		
	Kato		
	Ojok		
	Nangobi		
	Naigaga		
	Adongo		
	Byamukama		
	Nalubega		
	Masika		
	Katushabe		
	Namatovu		
	Acen		
	Ajok		
	Namutebi		
	Otim		
	Matovu		
	Ogwang		
	Mbambu		
}			
