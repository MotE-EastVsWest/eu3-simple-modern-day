# Country Name:	Vincentian Rebels	# Tag:	VC8
#			
graphical_culture = 	africangfx		
#			
color = {	89 197 135	}	
historical_ideas = {			
}			
historical_units = {			
	napoleonic_square		
	napoleonic_lancers		
	flying_battery		
}			
monarch_names = {			
	"Williams #0" = 10		
	"John #0" = 10		
	"Browne #0" = 10		
	"Lewis #0" = 10		
	"Charles #0" = 10		
	"Thomas #0" = 10		
	"James #0" = 10		
	"Baptiste #0" = 10		
	"Samuel #0" = 10		
	"Richards #0" = 10		
	"Edwards #0" = 10		
	"Ollivierre #0" = 10		
	"Jack #0" = 10		
	"Peters #0" = 10		
	"Roberts #0" = 10		
	"Alexander #0" = 10		
	"King #0" = 10		
	"Jackson #0" = 10		
	"Matthews #0" = 10		
	"Lavia #0" = 10		
	"Harry #0" = 10		
	"Davis #0" = 10		
	"Simmons #0" = 10		
	"Glasgow #0" = 10		
	"Adams #0" = 10		
	"Phillips #0" = 10		
	"Johnson #0" = 10		
	"George #0" = 10		
	"Nanton #0" = 10		
	"Clarke #0" = 10		
	"Daniel #0" = 10		
	"Jacobs #0" = 10		
	"Grant #0" = 10		
	"Stapleton #0" = 10		
	"Sutherland #0" = 10		
	"Pierre #0" = 10		
	"Young #0" = 10		
	"Bailey #0" = 10		
	"Shallow #0" = 10		
}			
ship_names = {			
	"Coast Guard"		
}			
leader_names			
	Williams		
	John		
	Browne		
	Lewis		
	Charles		
	Thomas		
	Thomas		
	James		
	Baptiste		
	Samuel		
	Richards		
	Edwards		
	Joseph		
	Ollivierre		
	Jack		
	Peters		
	Roberts		
	Alexander		
	King		
	Jackson		
	Matthews		
	Lavia		
	Harry		
	Davis		
	Simmons		
	Glasgow		
	Adams		
	Phillips		
	Johnson		
	George		
	Nanton		
	Clarke		
	Daniel		
	Jacobs		
	Grant		
	Stapleton		
	Sutherland		
	Pierre		
	Young		
	Bailey		
	Shallow		
}			
