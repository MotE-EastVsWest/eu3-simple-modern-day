# Country Name:	Vietnam	# Tag:	VT9
#			
graphical_culture = 	chinesegfx		
#			
color = { 186  14  92 }
historical_ideas = {			
	ecumenism		
	church_attendance_duty		
	divine_supremacy		
	deus_vult		
	revolution_and_counter		
	press_gangs		
	national_conscripts		
	napoleonic_warfare		
	vice_roys		
	vetting		
	bureaucracy		
}			
historical_units = {			
	napoleonic_square		
	napoleonic_lancers		
	flying_battery		
}			
monarch_names = {			
	"Nguyen #0" = 10		
	"Tran #0" = 10		
	"Le #0" = 10		
	"Pham #0" = 10		
	"Hoang #0" = 10		
	"Vu #0" = 10		
	"Phan #0" = 10		
	"Bui #0" = 10		
	"Vo #0" = 10		
	"Huynh #0" = 10		
	"Do #0" = 10		
	"Ho #0" = 10		
	"Truong #0" = 10		
	"Dang #0" = 10		
	"Ha #0" = 10		
	"Duong #0" = 10		
	"Mai #0" = 10		
	"Dinh #0" = 10		
	"Lam #0" = 10		
	"Trinh #0" = 10		
	"O #0" = 10		
	"Anh #0" = 10		
	"Cao #0" = 10		
	"Luong #0" = 10		
	"Dao #0" = 10		
	"Doan #0" = 10		
	"Thanh #0" = 10		
	"Van #0" = 10		
	"Ly #0" = 10		
	"Minh #0" = 10		
	"Luu #0" = 10		
	"Ta #0" = 10		
	"Chu #0" = 10		
	"To #0" = 10		
	"Ngoc #0" = 10		
	"Thai #0" = 10		
	"Ang #0" = 10		
	"Phung #0" = 10		
	"Inh #0" = 10		
}			
ship_names = {			
	"Coast Guard"		
}			
leader_names = {			
	Nguyen		
	Tran		
	Le		
	Pham		
	Hoang		
	Vu		
	Vu		
	Phan		
	Bui		
	Vo		
	Huynh		
	Do		
	Ngo		
	Ho		
	Truong		
	Dang		
	Ha		
	Duong		
	Mai		
	Dinh		
	Lam		
	Trinh		
	O		
	Anh		
	Cao		
	Luong		
	Dao		
	Doan		
	Thanh		
	Van		
	Ly		
	Minh		
	Luu		
	Ta		
	Chu		
	To		
	Ngoc		
	Thai		
	Ang		
	Phung		
	Inh		
}			
