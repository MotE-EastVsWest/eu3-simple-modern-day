# Country Name:	Wales	# Tag:	WA9
#			
graphical_culture = 	latingfx		
#			
color = { 117  127  174 }
historical_ideas = {			
	press_gangs		
	grand_navy		
	sea_hawks		
	superior_seamanship		
	naval_glory		
	excellent_shipwrights		
	naval_fighting_instruction		
	naval_provisioning		
	shrewd_commerce_practise		
	merchant_adventures		
	improved_foraging		
}			
historical_units = {			
	napoleonic_square		
	napoleonic_lancers		
	flying_battery		
}			
monarch_names = {			
	"Jones #0" = 10		
	"Davies #0" = 10		
	"Williams #0" = 10		
	"Evans #0" = 10		
	"Thomas #0" = 10		
	"Roberts #0" = 10		
	"Lewis #0" = 10		
	"Hughes #0" = 10		
	"Morgan #0" = 10		
	"Griffiths #0" = 10		
	"Edwards #0" = 10		
	"James #0" = 10		
	"Rees #0" = 10		
	"Owen #0" = 10		
	"Jenkins #0" = 10		
	"Price #0" = 10		
	"Morris #0" = 10		
	"Phillips #0" = 10		
	"Richards #0" = 10		
	"Lloyd #0" = 10		
	"Harris #0" = 10		
	"Taylor #0" = 10		
	"Parry #0" = 10		
	"Powell #0" = 10		
	"Brown #0" = 10		
	"John #0" = 10		
	"Watkins #0" = 10		
	"Howells #0" = 10		
	"Pritchard #0" = 10		
	"Rogers #0" = 10		
	"Hill #0" = 10		
	"Hopkins #0" = 10		
	"Ellis #0" = 10		
	"Bowen #0" = 10		
	"Johnson #0" = 10		
	"Matthews #0" = 10		
	"White #0" = 10		
	"Wilson #0" = 10		
	"Rowlands #0" = 10		
}			
ship_names = {			
	"Coast Guard"		
}			
leader_names = {			
	Jones		
	Davies		
	Williams		
	Evans		
	Thomas		
	Roberts		
	Roberts		
	Lewis		
	Hughes		
	Morgan		
	Griffiths		
	Edwards		
	Smith		
	James		
	Rees		
	Owen		
	Jenkins		
	Price		
	Morris		
	Phillips		
	Richards		
	Lloyd		
	Harris		
	Taylor		
	Parry		
	Powell		
	Brown		
	John		
	Watkins		
	Howells		
	Pritchard		
	Rogers		
	Hill		
	Hopkins		
	Ellis		
	Bowen		
	Johnson		
	Matthews		
	White		
	Wilson		
	Rowlands		
}			
