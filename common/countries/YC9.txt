# Country Name:	Yucatan	# Tag:	YC9
#			
graphical_culture = 	southamericagfx		
#			
color = { 94  136  191 }	
historical_ideas = {			
	press_gangs		
	grand_navy		
	merchant_adventures		
	superior_seamanship		
	naval_glory		
	excellent_shipwrights		
	naval_fighting_instruction		
	naval_provisioning		
	shrewd_commerce_practise		
	merchant_adventures		
	improved_foraging		
}			
historical_units = {			
	napoleonic_square		
	napoleonic_lancers		
	flying_battery		
}			
monarch_names = {			
	"Chan #0" = 10		
	"Pech #0" = 10		
	"Canul #0" = 10		
	"May #0" = 10		
	"Canche #0" = 10		
	"Dzul #0" = 10		
	"Perez #0" = 10		
	"Gonzalez #0" = 10		
	"Poot #0" = 10		
	"Lopez #0" = 10		
	"Cauich #0" = 10		
	"Caamal #0" = 10		
	"Ku #0" = 10		
	"Uc #0" = 10		
	"Balam #0" = 10		
	"Rodriguez #0" = 10		
	"Ek #0" = 10		
	"Pool #0" = 10		
	"Tun #0" = 10		
	"Sanchez #0" = 10		
	"Puc #0" = 10		
	"Herrera #0" = 10		
	"Dzib #0" = 10		
	"Castillo #0" = 10		
	"Medina #0" = 10		
	"Gomez #0" = 10		
	"Hernandez #0" = 10		
	"Aguilar #0" = 10		
	"Uicab #0" = 10		
	"Moo #0" = 10		
	"Garcia #0" = 10		
	"Martin #0" = 10		
	"Martinez #0" = 10		
	"Noh #0" = 10		
	"Euan #0" = 10		
	"Can #0" = 10		
	"Ake #0" = 10		
	"Pacheco #0" = 10		
	"Diaz #0" = 10		
}			
ship_names = {			
	"Coast Guard"		
}			
leader_names = {			
	Chan		
	Pech		
	Canul		
	May		
	Canche		
	Dzul		
	Dzul		
	Perez		
	Gonzalez		
	Poot		
	Lopez		
	Cauich		
	Chi		
	Caamal		
	Ku		
	Uc		
	Balam		
	Rodriguez		
	Ek		
	Pool		
	Tun		
	Sanchez		
	Puc		
	Herrera		
	Dzib		
	Castillo		
	Medina		
	Gomez		
	Hernandez		
	Aguilar		
	Uicab		
	Moo		
	Garcia		
	Martin		
	Martinez		
	Noh		
	Euan		
	Can		
	Ake		
	Pacheco		
	Diaz		
}			
