# Country Name:	Yemeni Rebels	# Tag:	YE8
#			
graphical_culture = 	muslimgfx		
#			
color = {	183 125 51	}	
historical_ideas = {			
	ecumenism		
	church_attendance_duty		
	divine_supremacy		
	deus_vult		
	revolution_and_counter		
	press_gangs		
	national_conscripts		
	napoleonic_warfare		
	vice_roys		
	vetting		
	bureaucracy		
}			
historical_units = {			
	napoleonic_square		
	napoleonic_lancers		
	flying_battery		
}			
monarch_names = {			
	"Mohamed #0" = 10		
	"Ahmed #0" = 10		
	"Ali #0" = 10		
	"Abdullah #0" = 10		
	"Saleh #0" = 10		
	"Saeed #0" = 10		
	"Hassan #0" = 10		
	"Abdo #0" = 10		
	"Qasim #0" = 10		
	"Hussein #0" = 10		
	"Salem #0" = 10		
	"Nasser #0" = 10		
	"Naji #0" = 10		
	"Saif #0" = 10		
	"Ghaleb #0" = 10		
	"Omar #0" = 10		
	"Mohsen #0" = 10		
	"Kayed #0" = 10		
	"Ibrahim #0" = 10		
	"Osman #0" = 10		
	"Awad #0" = 10		
	"Thabet #0" = 10		
	"Moqbel #0" = 10		
	"Hamoud #0" = 10		
	"Qaid #0" = 10		
	"Saad #0" = 10		
	"Ismail #0" = 10		
	"Mahdi #0" = 10		
	"Faree #0" = 10		
	"Haidar #0" = 10		
	"Noman #0" = 10		
	"Hamid #0" = 10		
	"Abdel Rahman #0" = 10		
	"Hadi #0" = 10		
	"Yousef #0" = 10		
	"Morshed #0" = 10		
	"Muthanna #0" = 10		
	"Al Faqih #0" = 10		
	"Al Wasabi #0" = 10		
}			
ship_names = {			
	"Coast Guard"		
}			
leader_names = {			
	Mohamed		
	Ahmed		
	Ali		
	Abdullah		
	Saleh		
	Saeed		
	Saeed		
	Hassan		
	Abdo		
	Qasim		
	Hussein		
	Salem		
	Yahya		
	Nasser		
	Naji		
	Saif		
	Ghaleb		
	Omar		
	Mohsen		
	Kayed		
	Ibrahim		
	Osman		
	Awad		
	Thabet		
	Moqbel		
	Hamoud		
	Qaid		
	Saad		
	Ismail		
	Mahdi		
	Faree		
	Haidar		
	Noman		
	Hamid		
	Abdel Rahman		
	Hadi		
	Yousef		
	Morshed		
	Muthanna		
	Al Faqih		
	Al Wasabi		
}			
