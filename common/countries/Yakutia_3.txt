#Country Name: Please see filename.

graphical_culture = chinesegfx

color = { 201 61 63 }



historical_ideas = {
	national_conscripts
	glorious_arms
	grand_army
	military_drill
	battlefield_commisions
	shrewd_commerce_practise
	engineer_corps
	merchant_adventures
	national_trade_policy
	espionage
	cabinet
	humanist_tolerance
}

historical_units = {
	east_asian_spearmen
	eastern_bow
	east_mongolian_steppe
	asian_arquebusier
	asian_charge_cavalry
	asian_mass_infantry
	asian_musketeer
	reformed_asian_musketeer
	reformed_asian_cavalry
}

monarch_names = {
	"Dayan #0" = 20
	"Darayisung #0" = 20
	"Mulon #0" = 20
	"Manduulun #0" = 20
	"Sayn #0" = 20
	"Bodi #0" = 20
	"Buyan #0" = 20
	"Ligdan #0" = 20
	"Tayisung #0" = 20
	"Adai #0" = 20
	"Oyiradai #0" = 20
	"Delbeg #0" = 20
	"�ljei #0" = 20
	"G�n #0" = 20
	"�r�g #0" = 15
	"Arigaba #1" = 10
	"Ayurparibhadra #1" = 10
	"Biligt� #1" = 10
	"Elbeg #1" = 10
	"G�y�k #1" = 10
	"Irinchibal #1" = 10
	"Jorightu #1" = 10
	"Kublai #1" = 10
	"M�ngke #1" = 10
	"�gedei #1" = 10
	"Qayshan #1" = 10
	"Qoshila #1" = 10
	"Suddhipala #1" = 10
	"Temujin #1" = 10
	"Tem�r #1" = 10
	"Toghan #1" = 10
	"Tolui #1" = 10
	"Toq #1" = 10
	"Uskhal #1" = 10
	"Yes�n #1" = 10
	"Ariq #0" = 0
	"Batu #0" = 0
	"Eljigidei #0" = 0	
	"Joichi #0" = 0
}

leader_names = { 
	Orsolgene Chagan-cher "Tegus Burilgi" "Quara Temur" "Arigh Arslan"
	"Batu Unegen" "Koke Gal" Nasantai Ulaganqan Qyugungge Maidari
	Delgergene Subeqai
}

ship_names = {
	"Ariq Boke"
	Chengzong
	G�y�k
	Hoelun Hulagu
	Kublai Kiyad K�l�g
	Mongke
	Olkunut �gedei
	Qabul Qaidu
	"Sorghaghtani Beki"
	Tolui
	Yesugei 
}
