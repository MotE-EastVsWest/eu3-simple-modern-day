# Country Name:	Zambian Rebels	# Tag:	ZA8
#			
graphical_culture = 	africangfx		
#			
color = {	151 166 37	}	
historical_ideas = {			
	vetting		
	national_bank		
	national_trade_policy		
	smithian_economics		
	shrewd_commerce_practise		
	scientific_revolution		
	sea_hawks		
	naval_provisioning		
	regimental_system		
	napoleonic_warfare		
	merchant_adventures		
}			
historical_units = {			
	napoleonic_square		
	napoleonic_lancers		
	flying_battery		
}			
monarch_names = {			
	"Phiri #0" = 10		
	"Banda #0" = 10		
	"Mulenga #0" = 10		
	"Zulu #0" = 10		
	"Musonda #0" = 10		
	"Tembo #0" = 10		
	"Mwale #0" = 10		
	"Lungu #0" = 10		
	"Sakala #0" = 10		
	"Daka #0" = 10		
	"Bwalya #0" = 10		
	"Mumba #0" = 10		
	"Mwape #0" = 10		
	"Kunda #0" = 10		
	"Nyirenda #0" = 10		
	"Chanda #0" = 10		
	"Mwila #0" = 10		
	"Zimba #0" = 10		
	"Mutale #0" = 10		
	"Ngoma #0" = 10		
	"Mwewa #0" = 10		
	"Chola #0" = 10		
	"Mubanga #0" = 10		
	"Kabwe #0" = 10		
	"Mwamba #0" = 10		
	"Mwaba #0" = 10		
	"Mweemba #0" = 10		
	"Mwanza #0" = 10		
	"Njobvu #0" = 10		
	"Chama #0" = 10		
	"Mwiinga #0" = 10		
	"Mpundu #0" = 10		
	"Ngosa #0" = 10		
	"Mudenda #0" = 10		
	"Mvula #0" = 10		
	"Mtonga #0" = 10		
	"Moonga #0" = 10		
	"Ngulube #0" = 10		
	"Nyambe #0" = 10		
}			
ship_names = {			
	"Coast Guard"		
}			
leader_names = {			
	Phiri		
	Banda		
	Mulenga		
	Zulu		
	Musonda		
	Tembo		
	Tembo		
	Mwale		
	Lungu		
	Sakala		
	Daka		
	Bwalya		
	Mbewe		
	Mumba		
	Mwape		
	Kunda		
	Nyirenda		
	Chanda		
	Mwila		
	Zimba		
	Mutale		
	Ngoma		
	Mwewa		
	Chola		
	Mubanga		
	Kabwe		
	Mwamba		
	Mwaba		
	Mweemba		
	Mwanza		
	Njobvu		
	Chama		
	Mwiinga		
	Mpundu		
	Ngosa		
	Mudenda		
	Mvula		
	Mtonga		
	Moonga		
	Ngulube		
	Nyambe		
}			
