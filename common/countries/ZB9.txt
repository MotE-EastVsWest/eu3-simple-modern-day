# Country Name:	Zimbabwe	# Tag:	ZB9
#			
graphical_culture = 	africangfx		
#			
color = { 186  164  78 }	
historical_ideas = {			
	ecumenism		
	church_attendance_duty		
	divine_supremacy		
	deus_vult		
	revolution_and_counter		
	press_gangs		
	national_conscripts		
	napoleonic_warfare		
	vice_roys		
	vetting		
	bureaucracy		
}			
historical_units = {			
	napoleonic_square		
	napoleonic_lancers		
	flying_battery		
}			
monarch_names = {			
	"Moyo #0" = 10		
	"Ncube #0" = 10		
	"Sibanda #0" = 10		
	"Dube #0" = 10		
	"Ndlovu #0" = 10		
	"Mpofu #0" = 10		
	"Sithole #0" = 10		
	"Ngwenya #0" = 10		
	"Phiri #0" = 10		
	"Tshuma #0" = 10		
	"Nyoni #0" = 10		
	"Nyathi #0" = 10		
	"Ndhlovu #0" = 10		
	"Mhlanga #0" = 10		
	"Khumalo #0" = 10		
	"Zhou #0" = 10		
	"Banda #0" = 10		
	"Shumba #0" = 10		
	"Gumbo #0" = 10		
	"Ndebele #0" = 10		
	"Muleya #0" = 10		
	"Hove #0" = 10		
	"Shoko #0" = 10		
	"Maphosa #0" = 10		
	"Tembo #0" = 10		
	"Mapfumo #0" = 10		
	"Siziba #0" = 10		
	"Ndou #0" = 10		
	"Mlambo #0" = 10		
	"Dhliwayo #0" = 10		
	"Simango #0" = 10		
	"Maposa #0" = 10		
	"Masuku #0" = 10		
	"Bhebhe #0" = 10		
	"Marufu #0" = 10		
	"Mudenda #0" = 10		
	"Makoni #0" = 10		
	"Mlilo #0" = 10		
	"Zulu #0" = 10		
}			
ship_names = {			
	"Coast Guard"		
}			
leader_names = {			
	Moyo		
	Ncube		
	Sibanda		
	Dube		
	Ndlovu		
	Mpofu		
	Mpofu		
	Sithole		
	Ngwenya		
	Phiri		
	Tshuma		
	Nyoni		
	Nkomo		
	Nyathi		
	Ndhlovu		
	Mhlanga		
	Khumalo		
	Zhou		
	Banda		
	Shumba		
	Gumbo		
	Ndebele		
	Muleya		
	Hove		
	Shoko		
	Maphosa		
	Tembo		
	Mapfumo		
	Siziba		
	Ndou		
	Mlambo		
	Dhliwayo		
	Simango		
	Maposa		
	Masuku		
	Bhebhe		
	Marufu		
	Mudenda		
	Makoni		
	Mlilo		
	Zulu		
}			
