# Country Name:	Mozambiquan Rebels	# Tag:	ZO8
#			
graphical_culture = 	africangfx		
#			
color = {	25 78 128	}	
historical_ideas = {			
	ecumenism		
	church_attendance_duty		
	divine_supremacy		
	deus_vult		
	revolution_and_counter		
	press_gangs		
	national_conscripts		
	napoleonic_warfare		
	vice_roys		
	vetting		
	bureaucracy		
}			
historical_units = {			
	napoleonic_square		
	napoleonic_lancers		
	flying_battery		
}			
monarch_names = {			
	"Langa #0" = 10		
	"Cossa #0" = 10		
	"Tembe #0" = 10		
	"Sitoe #0" = 10		
	"Manjate #0" = 10		
	"Mondlane #0" = 10		
	"Santos #0" = 10		
	"Machava #0" = 10		
	"Nhantumbo #0" = 10		
	"Junior #0" = 10		
	"Macuacua #0" = 10		
	"Manuel #0" = 10		
	"Muchanga #0" = 10		
	"Matsinhe #0" = 10		
	"Domingos #0" = 10		
	"Pereira #0" = 10		
	"António #0" = 10		
	"Muianga #0" = 10		
	"Munguambe #0" = 10		
	"Fernandes #0" = 10		
	"Banze #0" = 10		
	"Ferreira #0" = 10		
	"Bila #0" = 10		
	"Rodriguês #0" = 10		
	"Manhique #0" = 10		
	"Mussa #0" = 10		
	"Simbine #0" = 10		
	"Francisco #0" = 10		
	"Guambe #0" = 10		
	"Dias #0" = 10		
	"Fernando #0" = 10		
	"Martins #0" = 10		
	"Cumbe #0" = 10		
	"Manhica #0" = 10		
	"Joao #0" = 10		
	"Fumo #0" = 10		
	"Mandlate #0" = 10		
	"Almeida #0" = 10		
	"Pedro #0" = 10		
}			
ship_names = {			
	"Coast Guard"		
}			
leader_names = {			
	Langa		
	Cossa		
	Tembe		
	Sitoe		
	Manjate		
	Mondlane		
	Mondlane		
	Santos		
	Machava		
	Nhantumbo		
	Junior		
	Macuacua		
	Macamo		
	Manuel		
	Muchanga		
	Matsinhe		
	Domingos		
	Pereira		
	António		
	Muianga		
	Munguambe		
	Fernandes		
	Banze		
	Ferreira		
	Bila		
	Rodriguês		
	Manhique		
	Mussa		
	Simbine		
	Francisco		
	Guambe		
	Dias		
	Fernando		
	Martins		
	Cumbe		
	Manhica		
	Joao		
	Fumo		
	Mandlate		
	Almeida		
	Pedro		
}			
