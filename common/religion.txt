# If you add religions, and use those tags, do not change them without changing everywhere they are used.

# Uses all 'modifiers' possible thats exported as a Modifier.


#### Religions have been replaced with ideological systems.
### MAINSTREAM RG
# -- Poor --
# Progressivism
# Populism
# Muslim Brotherhood
# -- Rich --
# Liberalism
# Conservatism
# Secular Islamism

### LEFT WING RG
# -- Poor --
# Communism
# Islamic Communism
# -- Rich --
# Syndicalism
# Anarchism
# Environmentalism
# Islamic Socialism

### RIGHT WING RG
# -- Poor --
# Ultranationalism
# Reactionism
# Extremist Islamism
# -- Rich --
# Traditionalism
# Monarchism
# Islamic Monarchism

## UPDATE: this was the old system
### Socialist
### Liberal
### Conservative
### Reactionary


left_wing_rg = {
	socialist_r = {
		color = {
			0.84	#R
			0.08	#G
			0.08	#B
		}
		allowed_conversion = {
			#socialism_r
			liberal_r
			conservative_r
			reactionary_r
		}
		province = {

		}
		country = {
			tolerance_own = 9
			tolerance_heathen = -2

		}
		heretic = { }
	}
}

mainstream_rg = {
	liberal_r = {
		color = {
			0.96	#R
			0.68	#G
			0.16	#B
		}
		allowed_conversion = {
			socialism_r
			#liberal_r
			conservative_r
			reactionary_r
		}
		province = {

		}
		country = {
			tolerance_own = 9
			tolerance_heathen = -8
			tolerance_heretic = -1

		}
		heretic = { }
	}
	conservative_r = {
		color = {
			0.30	#R
			0.74	#G
			0.96	#B
		}
		allowed_conversion = {
			socialism_r
			liberal_r
			#conservative_r
			reactionary_r
		}
		province = {

		}
		country = {
			tolerance_own = 6
			tolerance_heathen = -6
			tolerance_heretic = -2

		}
		heretic = { }
	}
}

right_wing_rg = {
	reactionary_r = {
		color = {
			0.16	#R
			0.18	#G
			0.92	#B
		}
		allowed_conversion = {
			socialism_r
			liberal_r
			conservative_r
			#reactionary_r
		}
		province = {

		}
		country = {
			tolerance_own = 4
			tolerance_heathen = -8

		}
		heretic = { }
	}

}
christian = {

	catholic = {
		color = { 0.8 0.9 0.7 }
	
		allowed_conversion = {
			catholic
			protestant
			reformed
			orthodox
			sunni
			shiite
			hinduism
			buddhism
			confucianism
			shinto
			animism
			judaism
		}
		province = {
			stability_cost = 20
			local_missionary_placement_chance = -0.99
		}
		country = {
			colonists = 1
			colonist_placement_chance = 0.2
			diplomats = 2
			missionaries = -0.99
			missionary_placement_chance = 0.1
			tolerance_heretic = -1
		}
		
		heretic = { BOGOMILIST WALDENSIAN FRATICELLI HUSSITE LOLLARD SOCINIAN }
		
		papacy = yes	#can play with papacy stuff..
		
	}

	protestant = {
		color = { 0 0 0.7 }
		allowed_conversion = {
			catholic
			protestant
			reformed
			orthodox
			sunni
			shiite
			hinduism
			buddhism
			confucianism
			shinto
			animism
			judaism
			}
		province = {
			stability_cost = 30
			local_missionary_placement_chance = -0.99
		}
		country = {
			production_efficiency = 0.1
			global_tax_modifier = 0.1
			colonists =  0.5
			diplomats = 1
			missionaries = -0.99
			missionary_placement_chance = 0.1
			tolerance_heretic = 1
		}
		
		heretic = { PENTECOSTAL PURITAN CONGREGATIONALIST }
		
		date = 1517.10.31
		

	}

	reformed = {
		color = { 0.3 0.3 0.9 }
		allowed_conversion = {
			catholic
			protestant
			reformed
			orthodox
			sunni
			shiite
			hinduism
			buddhism
			confucianism
			shinto
			animism
			judaism
		}
		province = {
			stability_cost = 30
			local_missionary_placement_chance = -0.99
		}
		country = {
			trade_efficiency = 0.1
			global_tax_modifier = -0.1
			colonists = 1
			global_colonial_growth = 0.1	# +10 people per year in colonies.
			diplomats = 1
			missionaries = -0.99
			missionary_placement_chance = 0.09
			tolerance_heretic = 1
		}
		
		heretic = { METHODIST BAPTIST QUAKER }
		
		date = 1536.5.1
	}

	orthodox = {
		allowed_conversion = {
			catholic
			protestant
			reformed
			orthodox
			sunni
			shiite
			hinduism
			buddhism
			confucianism
			shinto
			animism
			judaism
		}
		color = { 0.6 0 0.5 }

		province = {
			stability_cost = 15
			local_missionary_placement_chance = -0.99
		}
		country = {
			colonists = 0.5
			missionaries = -0.99
			missionary_placement_chance = 0.1
		}
		
		heretic = { OLD_BELIEVER MOLOKAN DUKHOBOR KHLYST SKOPTSY ICONOCLAST }
	}
}

muslim = {
	sunni = {
		allowed_conversion = {
			catholic
			protestant
			reformed
			orthodox
			sunni
			shiite
			hinduism
			buddhism
			confucianism
			shinto
			animism
			judaism
			
		}
		color = { 0 0.6 0 }
		province = {
			stability_cost = 20
			local_missionary_placement_chance = -0.99
		}
		country = {
			missionaries = -0.99
		}
		
		heretic = { BEKTASHI AHMADI ZIKRI YAZIDI SIKH }
		
	}

	shiite = {
		allowed_conversion = {
			catholic
			protestant
			reformed
			orthodox
			sunni
			shiite
			hinduism
			buddhism
			confucianism
			shinto
			animism
			judaism
			
		}
		color = { 0 0.8 0 }
		province = {
			stability_cost = 20
			local_missionary_placement_chance = -0.99
		}
		country = {
			global_tax_modifier = -0.2
			land_morale = 0.5
			missionaries = -0.99
			tolerance_heathen = -1
		}
		
		heretic = { DRUZE HURUFI ZAIDI }
	}
}

eastern = {
	buddhism = {
		allowed_conversion = {
			catholic
			protestant
			reformed
			orthodox
			sunni
			shiite
			hinduism
			buddhism
			confucianism
			shinto
			animism
			judaism
			
		}
	
		color = { 0.8 0.3 0 }
		province = {
			stability_cost = 20
			local_missionary_placement_chance = -0.99
		}
		country = {
			global_tax_modifier = -0.2
			missionaries = -0.99
			tolerance_heretic = 2
		}
		
		heretic = { ZEN }
	}

	hinduism = {
		allowed_conversion = {
			catholic
			protestant
			reformed
			orthodox
			sunni
			shiite
			hinduism
			buddhism
			confucianism
			shinto
			animism
			judaism
			
		}
		color = { 0 0.8 0.8 }
		province = {
			stability_cost = 25
			local_missionary_placement_chance = -0.99
		}
		country = {
			global_tax_modifier = 0.05
			missionaries = -0.99
			tolerance_heretic = 2
		}
		
		heretic = { BHAKTI SIKH }
	}

	confucianism = {
		allowed_conversion = {
			catholic
			protestant
			reformed
			orthodox
			sunni
			shiite
			hinduism
			buddhism
			confucianism
			shinto
			animism
			judaism
			
		}
		color = { 0.8 0 0.8 }
		province = {
			stability_cost = 20
			local_missionary_placement_chance = -0.99
		}
		country = {
			global_tax_modifier = -0.5
			missionaries = -0.99
			tolerance_heretic = 2
		}
		heretic = { TAOIST }
	}

	shinto = {
		allowed_conversion = {
			catholic
			protestant
			reformed
			orthodox
			sunni
			shiite
			hinduism
			buddhism
			confucianism
			shinto
			animism
			judaism
			
		}
		color = { 0.8 0 0 }
		province = {
			stability_cost = 15
			local_missionary_placement_chance = -0.99
			}
		country = {
			land_morale = 0.5
			global_tax_modifier = -0.2
			missionaries = -0.99
			tolerance_heathen = -1
		}
		heretic = { SHUGENDO }
	}

}

pagan = {
	animism = {
		allowed_conversion = {
			catholic
			protestant
			reformed
			orthodox
			sunni
			shiite
			hinduism
			buddhism
			confucianism
			shinto
			animism
			judaism
			
		}
		color = { 0.5 0.0 0.0 }
		province = {
			stability_cost = 10
			local_missionary_cost = -0.5
			local_missionary_placement_chance = -0.99
		}
		country = {
			tolerance_heretic = 2
			missionaries = -0.99
		}
		heretic = { BEAR_SPIRIT SNAKE_CLAN }
		
	}
	shamanism = {
		allowed_conversion = {
			
		}
		color = { 0.5 0.3 0.3 }
		province = {
			stability_cost = 10
			local_missionary_cost = -0.5
		}
		country = {
			missionary_placement_chance = 0.167
			tolerance_heretic = 2
			missionaries = -0.99
		}
		
		heretic = { GOAT_SKULL }
			
	}
}

jewish = {
	judaism = {
		allowed_conversion = {
			catholic
			protestant
			reformed
			orthodox
			sunni
			shiite
			hinduism
			buddhism
			confucianism
			shinto
			animism
			judaism
			
		}
		color = { 1.0 0.63 0.12 }
		province = {
			stability_cost = 10
		}
		country = {
			missionaries = -0.99
		}
		heretic = { }
		
	}
}

