# Do not change tags in here without changing every other reference to them.
# If adding new resources, make sure they are uniquely named.
# Please add price definitions to prices.txt as well.

#Base resources
grain = {
	color = { 0.7 0.7 0 }
}

wine = {
	color = { 0.7 0.0 0 }
}

wool = {
	color = { 0.7 0.4 0 }
}

cloth = {
	color = { 0.7 1.0 1.0 }
}

fish = {
	color = { 0.0 1.0 0 }
}

fur = {
	color = { 1.0 1.0 1 }
}

salt = {
	color = { 1.0 0.0 1 }
}

naval_supplies = {
	color = { 1.0 0.4 0.4 }
}


# metals
copper = {
	color = { 0.0 0.4 0.4 }
}

gold = {
	color = { 0.0 0.0 0.4 }
}

iron = {
	color = { 0.0 1.0 0.0 }
}


#African Resources
slaves = {
	color = { 0.0 0.0 0.0 }
}

ivory = {
	color = { 1.0 1.0 1.0 }
}

#Eastern Resources
tea = {
	color = { 1.0 1.0 1.0 }
}

chinaware = {
	color = { 1.0 1.0 1.0 }
}

spices = {
	color = { 1.0 1.0 1.0 }
}


#New World Resources
coffee = {
	color = { 1.0 0.0 1.0 }
}

cotton = {
	color = { 1.0 0.0 0.0 }
}

sugar = {
	color = { 0.1 0.7 0.5 }
}

tobacco = {
	color = { 0.0 0.7 0.5 }
}

unknown = {
	color = { 0.5 0.5 0.5 }
}
