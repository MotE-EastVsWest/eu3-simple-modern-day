# Triggered modifiers are here.
# these are checked for each countries once/month and then applied.
#
# Effects are fully scriptable here.

######################################
# For AI only
######################################
ai_only_tm = {
	trigger = {
		ai = yes
	}
	#land_forcelimit = 50
}

human_only_tm = {
	trigger = {
		NOT = { ai = yes }
		capital_scope = { region = any_province_region }
	}
	build_cost = -0.5
	discipline = -0.2
}
coding_only_tm = {
	trigger = {
		culture_group = coding_g
	}
	global_revolt_risk = -200
	global_tax_modifier = -0.99
	global_manpower_modifier = -0.99
	regiment_recruit_speed = 0.99
}
