# British Hussar

type = cavalry
unit_type = latin

maneuver = 2
offensive_morale = 8
defensive_morale = 7
offensive_fire = 5
defensive_fire = 7
offensive_shock = 5
defensive_shock = 7
