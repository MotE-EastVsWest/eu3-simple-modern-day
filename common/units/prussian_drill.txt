# Prussian Drill

type = infantry
unit_type = latin

maneuver = 1
offensive_morale = 10
defensive_morale = 10
offensive_fire = 10
defensive_fire = 10
offensive_shock = 6
defensive_shock = 6
