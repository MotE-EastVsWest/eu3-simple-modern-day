
country_event = {
	title = "EVT_PASS"
	desc = "DESC"
	#major = yes
	id = 10002
	is_triggered_only = yes
	
	immediate = {
		############################
		## DAILY CODE
		############################
		### clear errant dependencies and flags
		any_country = {
			limit = { has_country_flag = share_common_enemy_dep }
			clr_country_flag = share_common_enemy_dep
		}
		any_country = {
			limit = { has_country_flag = exit_sub }
			clr_country_flag = exit_sub 
		}
		any_country = {
			limit = { has_country_flag = temp_this_flag }
			clr_country_flag = temp_this_flag
		}
		any_country = {
			limit = { 
				OR = {
					has_country_flag = aw_cores
					has_country_flag = aw_wars
					has_country_flag = aw_marriages
					has_country_flag = aw_allies
					has_country_flag = aw_vassals
					has_country_flag = aw_units
					has_country_flag = aw_boats
					has_country_flag = aw_overlord
				}
			}
			clr_country_flag = aw_cores
			clr_country_flag = aw_wars
			clr_country_flag = aw_marriages
			clr_country_flag = aw_allies
			clr_country_flag = aw_vassals
			clr_country_flag = aw_units
			clr_country_flag = aw_boats
			clr_country_flag = aw_overlord
		}		
		## Force flags on 8 countries
		any_country = { limit = { tag = WP8 NOT = { has_country_flag = rebel_tag } } set_country_flag = rebel_tag }
		any_country = { limit = { tag = PP8 NOT = { has_country_flag = rebel_tag } } set_country_flag = rebel_tag }
		any_country = { limit = { tag = KY8 NOT = { has_country_flag = rebel_tag } } set_country_flag = rebel_tag }
		any_country = { limit = { tag = HA8 NOT = { has_country_flag = rebel_tag } } set_country_flag = rebel_tag }
		any_country = { limit = { tag = MO8 NOT = { has_country_flag = rebel_tag } } set_country_flag = rebel_tag }
		any_country = { limit = { tag = TL8 NOT = { has_country_flag = rebel_tag } } set_country_flag = rebel_tag }
		any_country = { limit = { tag = OS8 NOT = { has_country_flag = rebel_tag } } set_country_flag = rebel_tag }
		any_country = { limit = { tag = VT8 NOT = { has_country_flag = rebel_tag } } set_country_flag = rebel_tag }
		any_country = { limit = { tag = TB8 NOT = { has_country_flag = rebel_tag } } set_country_flag = rebel_tag }
		any_country = { limit = { tag = LN8 NOT = { has_country_flag = rebel_tag } } set_country_flag = rebel_tag }
		any_country = { limit = { tag = N18 NOT = { has_country_flag = rebel_tag } } set_country_flag = rebel_tag }
		any_country = { limit = { tag = K18 NOT = { has_country_flag = rebel_tag } } set_country_flag = rebel_tag }
		any_country = { limit = { tag = S18 NOT = { has_country_flag = rebel_tag } } set_country_flag = rebel_tag }
		any_country = { limit = { tag = K28 NOT = { has_country_flag = rebel_tag } } set_country_flag = rebel_tag }
		any_country = { limit = { tag = M18 NOT = { has_country_flag = rebel_tag } } set_country_flag = rebel_tag }
		any_country = { limit = { tag = KM8 NOT = { has_country_flag = rebel_tag } } set_country_flag = rebel_tag }
		any_country = { limit = { tag = MM8 NOT = { has_country_flag = rebel_tag } } set_country_flag = rebel_tag }
		any_country = { limit = { tag = YA8 NOT = { has_country_flag = rebel_tag } } set_country_flag = rebel_tag }
		any_country = { limit = { tag = TE8 NOT = { has_country_flag = rebel_tag } } set_country_flag = rebel_tag }
		any_country = { limit = { tag = MX8 NOT = { has_country_flag = rebel_tag } } set_country_flag = rebel_tag }
		any_country = { limit = { tag = BZ8 NOT = { has_country_flag = rebel_tag } } set_country_flag = rebel_tag }
		any_country = { limit = { tag = EL8 NOT = { has_country_flag = rebel_tag } } set_country_flag = rebel_tag }
		any_country = { limit = { tag = HD8 NOT = { has_country_flag = rebel_tag } } set_country_flag = rebel_tag }
		any_country = { limit = { tag = CR8 NOT = { has_country_flag = rebel_tag } } set_country_flag = rebel_tag }
		any_country = { limit = { tag = PA8 NOT = { has_country_flag = rebel_tag } } set_country_flag = rebel_tag }
		any_country = { limit = { tag = PR8 NOT = { has_country_flag = rebel_tag } } set_country_flag = rebel_tag }
		any_country = { limit = { tag = GL8 NOT = { has_country_flag = rebel_tag } } set_country_flag = rebel_tag }
		any_country = { limit = { tag = GD8 NOT = { has_country_flag = rebel_tag } } set_country_flag = rebel_tag }
		any_country = { limit = { tag = MT8 NOT = { has_country_flag = rebel_tag } } set_country_flag = rebel_tag }
		any_country = { limit = { tag = CC8 NOT = { has_country_flag = rebel_tag } } set_country_flag = rebel_tag }
		any_country = { limit = { tag = CO8 NOT = { has_country_flag = rebel_tag } } set_country_flag = rebel_tag }
		any_country = { limit = { tag = EC8 NOT = { has_country_flag = rebel_tag } } set_country_flag = rebel_tag }
		any_country = { limit = { tag = GI8 NOT = { has_country_flag = rebel_tag } } set_country_flag = rebel_tag }
		any_country = { limit = { tag = BR8 NOT = { has_country_flag = rebel_tag } } set_country_flag = rebel_tag }
		any_country = { limit = { tag = EU8 NOT = { has_country_flag = rebel_tag } } set_country_flag = rebel_tag }
		any_country = { limit = { tag = CI8 NOT = { has_country_flag = rebel_tag } } set_country_flag = rebel_tag }
		any_country = { limit = { tag = AG8 NOT = { has_country_flag = rebel_tag } } set_country_flag = rebel_tag }
		any_country = { limit = { tag = UG8 NOT = { has_country_flag = rebel_tag } } set_country_flag = rebel_tag }
		any_country = { limit = { tag = PG8 NOT = { has_country_flag = rebel_tag } } set_country_flag = rebel_tag }
		any_country = { limit = { tag = TH8 NOT = { has_country_flag = rebel_tag } } set_country_flag = rebel_tag }
		any_country = { limit = { tag = BT8 NOT = { has_country_flag = rebel_tag } } set_country_flag = rebel_tag }
		any_country = { limit = { tag = IE8 NOT = { has_country_flag = rebel_tag } } set_country_flag = rebel_tag }
		any_country = { limit = { tag = ES8 NOT = { has_country_flag = rebel_tag } } set_country_flag = rebel_tag }
		any_country = { limit = { tag = GA8 NOT = { has_country_flag = rebel_tag } } set_country_flag = rebel_tag }
		any_country = { limit = { tag = PT8 NOT = { has_country_flag = rebel_tag } } set_country_flag = rebel_tag }
		any_country = { limit = { tag = VS8 NOT = { has_country_flag = rebel_tag } } set_country_flag = rebel_tag }
		any_country = { limit = { tag = CT8 NOT = { has_country_flag = rebel_tag } } set_country_flag = rebel_tag }
		any_country = { limit = { tag = ML8 NOT = { has_country_flag = rebel_tag } } set_country_flag = rebel_tag }
		any_country = { limit = { tag = SI8 NOT = { has_country_flag = rebel_tag } } set_country_flag = rebel_tag }
		any_country = { limit = { tag = OR8 NOT = { has_country_flag = rebel_tag } } set_country_flag = rebel_tag }
		any_country = { limit = { tag = IT8 NOT = { has_country_flag = rebel_tag } } set_country_flag = rebel_tag }
		any_country = { limit = { tag = HV8 NOT = { has_country_flag = rebel_tag } } set_country_flag = rebel_tag }
		any_country = { limit = { tag = SK8 NOT = { has_country_flag = rebel_tag } } set_country_flag = rebel_tag }
		any_country = { limit = { tag = AO8 NOT = { has_country_flag = rebel_tag } } set_country_flag = rebel_tag }
		any_country = { limit = { tag = EQ8 NOT = { has_country_flag = rebel_tag } } set_country_flag = rebel_tag }
		any_country = { limit = { tag = CV8 NOT = { has_country_flag = rebel_tag } } set_country_flag = rebel_tag }
		any_country = { limit = { tag = PH8 NOT = { has_country_flag = rebel_tag } } set_country_flag = rebel_tag }
		any_country = { limit = { tag = DN8 NOT = { has_country_flag = rebel_tag } } set_country_flag = rebel_tag }
		any_country = { limit = { tag = RE8 NOT = { has_country_flag = rebel_tag } } set_country_flag = rebel_tag }
		any_country = { limit = { tag = TM8 NOT = { has_country_flag = rebel_tag } } set_country_flag = rebel_tag }
		any_country = { limit = { tag = RN8 NOT = { has_country_flag = rebel_tag } } set_country_flag = rebel_tag }
		any_country = { limit = { tag = YC8 NOT = { has_country_flag = rebel_tag } } set_country_flag = rebel_tag }
		any_country = { limit = { tag = GT8 NOT = { has_country_flag = rebel_tag } } set_country_flag = rebel_tag }
		any_country = { limit = { tag = NC8 NOT = { has_country_flag = rebel_tag } } set_country_flag = rebel_tag }
		any_country = { limit = { tag = CB8 NOT = { has_country_flag = rebel_tag } } set_country_flag = rebel_tag }
		any_country = { limit = { tag = JM8 NOT = { has_country_flag = rebel_tag } } set_country_flag = rebel_tag }
		any_country = { limit = { tag = DR8 NOT = { has_country_flag = rebel_tag } } set_country_flag = rebel_tag }
		any_country = { limit = { tag = VZ8 NOT = { has_country_flag = rebel_tag } } set_country_flag = rebel_tag }
		any_country = { limit = { tag = VO8 NOT = { has_country_flag = rebel_tag } } set_country_flag = rebel_tag }
		any_country = { limit = { tag = MG8 NOT = { has_country_flag = rebel_tag } } set_country_flag = rebel_tag }
		any_country = { limit = { tag = AN8 NOT = { has_country_flag = rebel_tag } } set_country_flag = rebel_tag }
		any_country = { limit = { tag = ZZ8 NOT = { has_country_flag = rebel_tag } } set_country_flag = rebel_tag }
		any_country = { limit = { tag = GB8 NOT = { has_country_flag = rebel_tag } } set_country_flag = rebel_tag }
		any_country = { limit = { tag = CM8 NOT = { has_country_flag = rebel_tag } } set_country_flag = rebel_tag }
		any_country = { limit = { tag = BN8 NOT = { has_country_flag = rebel_tag } } set_country_flag = rebel_tag }
		any_country = { limit = { tag = TG8 NOT = { has_country_flag = rebel_tag } } set_country_flag = rebel_tag }
		any_country = { limit = { tag = GU8 NOT = { has_country_flag = rebel_tag } } set_country_flag = rebel_tag }
		any_country = { limit = { tag = BS8 NOT = { has_country_flag = rebel_tag } } set_country_flag = rebel_tag }
		any_country = { limit = { tag = NL8 NOT = { has_country_flag = rebel_tag } } set_country_flag = rebel_tag }
		any_country = { limit = { tag = QC8 NOT = { has_country_flag = rebel_tag } } set_country_flag = rebel_tag }
		any_country = { limit = { tag = FR8 NOT = { has_country_flag = rebel_tag } } set_country_flag = rebel_tag }
		any_country = { limit = { tag = LX8 NOT = { has_country_flag = rebel_tag } } set_country_flag = rebel_tag }
		any_country = { limit = { tag = BG8 NOT = { has_country_flag = rebel_tag } } set_country_flag = rebel_tag }
		any_country = { limit = { tag = AT8 NOT = { has_country_flag = rebel_tag } } set_country_flag = rebel_tag }
		any_country = { limit = { tag = SO8 NOT = { has_country_flag = rebel_tag } } set_country_flag = rebel_tag }
		any_country = { limit = { tag = PO8 NOT = { has_country_flag = rebel_tag } } set_country_flag = rebel_tag }
		any_country = { limit = { tag = CZ8 NOT = { has_country_flag = rebel_tag } } set_country_flag = rebel_tag }
		any_country = { limit = { tag = WL8 NOT = { has_country_flag = rebel_tag } } set_country_flag = rebel_tag }
		any_country = { limit = { tag = HT8 NOT = { has_country_flag = rebel_tag } } set_country_flag = rebel_tag }
		any_country = { limit = { tag = LB8 NOT = { has_country_flag = rebel_tag } } set_country_flag = rebel_tag }
		any_country = { limit = { tag = ZO8 NOT = { has_country_flag = rebel_tag } } set_country_flag = rebel_tag }
		any_country = { limit = { tag = KI8 NOT = { has_country_flag = rebel_tag } } set_country_flag = rebel_tag }
		any_country = { limit = { tag = CN8 NOT = { has_country_flag = rebel_tag } } set_country_flag = rebel_tag }
		any_country = { limit = { tag = IV8 NOT = { has_country_flag = rebel_tag } } set_country_flag = rebel_tag }
		any_country = { limit = { tag = NA8 NOT = { has_country_flag = rebel_tag } } set_country_flag = rebel_tag }
		any_country = { limit = { tag = KO8 NOT = { has_country_flag = rebel_tag } } set_country_flag = rebel_tag }
		any_country = { limit = { tag = TW8 NOT = { has_country_flag = rebel_tag } } set_country_flag = rebel_tag }
		any_country = { limit = { tag = NK8 NOT = { has_country_flag = rebel_tag } } set_country_flag = rebel_tag }
		any_country = { limit = { tag = IN8 NOT = { has_country_flag = rebel_tag } } set_country_flag = rebel_tag }
		any_country = { limit = { tag = NE8 NOT = { has_country_flag = rebel_tag } } set_country_flag = rebel_tag }
		any_country = { limit = { tag = I18 NOT = { has_country_flag = rebel_tag } } set_country_flag = rebel_tag }
		any_country = { limit = { tag = I28 NOT = { has_country_flag = rebel_tag } } set_country_flag = rebel_tag }
		any_country = { limit = { tag = I38 NOT = { has_country_flag = rebel_tag } } set_country_flag = rebel_tag }
		any_country = { limit = { tag = I48 NOT = { has_country_flag = rebel_tag } } set_country_flag = rebel_tag }
		any_country = { limit = { tag = I58 NOT = { has_country_flag = rebel_tag } } set_country_flag = rebel_tag }
		any_country = { limit = { tag = I68 NOT = { has_country_flag = rebel_tag } } set_country_flag = rebel_tag }
		any_country = { limit = { tag = MN8 NOT = { has_country_flag = rebel_tag } } set_country_flag = rebel_tag }
		any_country = { limit = { tag = SB8 NOT = { has_country_flag = rebel_tag } } set_country_flag = rebel_tag }
		any_country = { limit = { tag = MK8 NOT = { has_country_flag = rebel_tag } } set_country_flag = rebel_tag }
		any_country = { limit = { tag = GK8 NOT = { has_country_flag = rebel_tag } } set_country_flag = rebel_tag }
		any_country = { limit = { tag = BU8 NOT = { has_country_flag = rebel_tag } } set_country_flag = rebel_tag }
		any_country = { limit = { tag = RO8 NOT = { has_country_flag = rebel_tag } } set_country_flag = rebel_tag }
		any_country = { limit = { tag = MD8 NOT = { has_country_flag = rebel_tag } } set_country_flag = rebel_tag }
		any_country = { limit = { tag = BY8 NOT = { has_country_flag = rebel_tag } } set_country_flag = rebel_tag }
		any_country = { limit = { tag = UA8 NOT = { has_country_flag = rebel_tag } } set_country_flag = rebel_tag }
		any_country = { limit = { tag = KR8 NOT = { has_country_flag = rebel_tag } } set_country_flag = rebel_tag }
		any_country = { limit = { tag = RU8 NOT = { has_country_flag = rebel_tag } } set_country_flag = rebel_tag }
		any_country = { limit = { tag = RK8 NOT = { has_country_flag = rebel_tag } } set_country_flag = rebel_tag }
		any_country = { limit = { tag = AM8 NOT = { has_country_flag = rebel_tag } } set_country_flag = rebel_tag }
		any_country = { limit = { tag = GE8 NOT = { has_country_flag = rebel_tag } } set_country_flag = rebel_tag }
		any_country = { limit = { tag = IL8 NOT = { has_country_flag = rebel_tag } } set_country_flag = rebel_tag }
		any_country = { limit = { tag = CY8 NOT = { has_country_flag = rebel_tag } } set_country_flag = rebel_tag }
		any_country = { limit = { tag = AK8 NOT = { has_country_flag = rebel_tag } } set_country_flag = rebel_tag }
		any_country = { limit = { tag = OE8 NOT = { has_country_flag = rebel_tag } } set_country_flag = rebel_tag }
		any_country = { limit = { tag = NR8 NOT = { has_country_flag = rebel_tag } } set_country_flag = rebel_tag }
		any_country = { limit = { tag = ER8 NOT = { has_country_flag = rebel_tag } } set_country_flag = rebel_tag }
		any_country = { limit = { tag = ET8 NOT = { has_country_flag = rebel_tag } } set_country_flag = rebel_tag }
		any_country = { limit = { tag = SP8 NOT = { has_country_flag = rebel_tag } } set_country_flag = rebel_tag }
		any_country = { limit = { tag = TN8 NOT = { has_country_flag = rebel_tag } } set_country_flag = rebel_tag }
		any_country = { limit = { tag = BA8 NOT = { has_country_flag = rebel_tag } } set_country_flag = rebel_tag }
		any_country = { limit = { tag = TC8 NOT = { has_country_flag = rebel_tag } } set_country_flag = rebel_tag }
		any_country = { limit = { tag = AB8 NOT = { has_country_flag = rebel_tag } } set_country_flag = rebel_tag }
		any_country = { limit = { tag = DO8 NOT = { has_country_flag = rebel_tag } } set_country_flag = rebel_tag }
		any_country = { limit = { tag = LC8 NOT = { has_country_flag = rebel_tag } } set_country_flag = rebel_tag }
		any_country = { limit = { tag = TD8 NOT = { has_country_flag = rebel_tag } } set_country_flag = rebel_tag }
		any_country = { limit = { tag = CK8 NOT = { has_country_flag = rebel_tag } } set_country_flag = rebel_tag }
		any_country = { limit = { tag = KN8 NOT = { has_country_flag = rebel_tag } } set_country_flag = rebel_tag }
		any_country = { limit = { tag = YL8 NOT = { has_country_flag = rebel_tag } } set_country_flag = rebel_tag }
		any_country = { limit = { tag = SW8 NOT = { has_country_flag = rebel_tag } } set_country_flag = rebel_tag }
		any_country = { limit = { tag = MC8 NOT = { has_country_flag = rebel_tag } } set_country_flag = rebel_tag }
		any_country = { limit = { tag = UR8 NOT = { has_country_flag = rebel_tag } } set_country_flag = rebel_tag }
		any_country = { limit = { tag = LM8 NOT = { has_country_flag = rebel_tag } } set_country_flag = rebel_tag }
		any_country = { limit = { tag = VA8 NOT = { has_country_flag = rebel_tag } } set_country_flag = rebel_tag }
		any_country = { limit = { tag = BB8 NOT = { has_country_flag = rebel_tag } } set_country_flag = rebel_tag }
		any_country = { limit = { tag = UN8 NOT = { has_country_flag = rebel_tag } } set_country_flag = rebel_tag }
		any_country = { limit = { tag = WI8 NOT = { has_country_flag = rebel_tag } } set_country_flag = rebel_tag }
		any_country = { limit = { tag = SZ8 NOT = { has_country_flag = rebel_tag } } set_country_flag = rebel_tag }
		any_country = { limit = { tag = LS8 NOT = { has_country_flag = rebel_tag } } set_country_flag = rebel_tag }
		any_country = { limit = { tag = BF8 NOT = { has_country_flag = rebel_tag } } set_country_flag = rebel_tag }
		any_country = { limit = { tag = NI8 NOT = { has_country_flag = rebel_tag } } set_country_flag = rebel_tag }
		any_country = { limit = { tag = CA8 NOT = { has_country_flag = rebel_tag } } set_country_flag = rebel_tag }
		any_country = { limit = { tag = CL8 NOT = { has_country_flag = rebel_tag } } set_country_flag = rebel_tag }
		any_country = { limit = { tag = HI8 NOT = { has_country_flag = rebel_tag } } set_country_flag = rebel_tag }
		any_country = { limit = { tag = GN8 NOT = { has_country_flag = rebel_tag } } set_country_flag = rebel_tag }
		any_country = { limit = { tag = IC8 NOT = { has_country_flag = rebel_tag } } set_country_flag = rebel_tag }
		any_country = { limit = { tag = SE8 NOT = { has_country_flag = rebel_tag } } set_country_flag = rebel_tag }
		any_country = { limit = { tag = NO8 NOT = { has_country_flag = rebel_tag } } set_country_flag = rebel_tag }
		any_country = { limit = { tag = WA8 NOT = { has_country_flag = rebel_tag } } set_country_flag = rebel_tag }
		any_country = { limit = { tag = DE8 NOT = { has_country_flag = rebel_tag } } set_country_flag = rebel_tag }
		any_country = { limit = { tag = EE8 NOT = { has_country_flag = rebel_tag } } set_country_flag = rebel_tag }
		any_country = { limit = { tag = LT8 NOT = { has_country_flag = rebel_tag } } set_country_flag = rebel_tag }
		any_country = { limit = { tag = LI8 NOT = { has_country_flag = rebel_tag } } set_country_flag = rebel_tag }
		any_country = { limit = { tag = AS8 NOT = { has_country_flag = rebel_tag } } set_country_flag = rebel_tag }
		any_country = { limit = { tag = NZ8 NOT = { has_country_flag = rebel_tag } } set_country_flag = rebel_tag }
		any_country = { limit = { tag = RI8 NOT = { has_country_flag = rebel_tag } } set_country_flag = rebel_tag }
		any_country = { limit = { tag = EN8 NOT = { has_country_flag = rebel_tag } } set_country_flag = rebel_tag }
		any_country = { limit = { tag = FL8 NOT = { has_country_flag = rebel_tag } } set_country_flag = rebel_tag }
		any_country = { limit = { tag = SS8 NOT = { has_country_flag = rebel_tag } } set_country_flag = rebel_tag }
		any_country = { limit = { tag = RW8 NOT = { has_country_flag = rebel_tag } } set_country_flag = rebel_tag }
		any_country = { limit = { tag = BW8 NOT = { has_country_flag = rebel_tag } } set_country_flag = rebel_tag }
		any_country = { limit = { tag = ZB8 NOT = { has_country_flag = rebel_tag } } set_country_flag = rebel_tag }
		any_country = { limit = { tag = LO8 NOT = { has_country_flag = rebel_tag } } set_country_flag = rebel_tag }
		any_country = { limit = { tag = GY8 NOT = { has_country_flag = rebel_tag } } set_country_flag = rebel_tag }
		any_country = { limit = { tag = SR8 NOT = { has_country_flag = rebel_tag } } set_country_flag = rebel_tag }
		any_country = { limit = { tag = SM8 NOT = { has_country_flag = rebel_tag } } set_country_flag = rebel_tag }
		any_country = { limit = { tag = NG8 NOT = { has_country_flag = rebel_tag } } set_country_flag = rebel_tag }
		any_country = { limit = { tag = HG8 NOT = { has_country_flag = rebel_tag } } set_country_flag = rebel_tag }
		any_country = { limit = { tag = TZ8 NOT = { has_country_flag = rebel_tag } } set_country_flag = rebel_tag }
		any_country = { limit = { tag = ZA8 NOT = { has_country_flag = rebel_tag } } set_country_flag = rebel_tag }
		any_country = { limit = { tag = SF8 NOT = { has_country_flag = rebel_tag } } set_country_flag = rebel_tag }
		any_country = { limit = { tag = NB8 NOT = { has_country_flag = rebel_tag } } set_country_flag = rebel_tag }
		any_country = { limit = { tag = SH8 NOT = { has_country_flag = rebel_tag } } set_country_flag = rebel_tag }
		any_country = { limit = { tag = FJ8 NOT = { has_country_flag = rebel_tag } } set_country_flag = rebel_tag }
		any_country = { limit = { tag = TF8 NOT = { has_country_flag = rebel_tag } } set_country_flag = rebel_tag }
		any_country = { limit = { tag = US8 NOT = { has_country_flag = rebel_tag } } set_country_flag = rebel_tag }
		any_country = { limit = { tag = TX8 NOT = { has_country_flag = rebel_tag } } set_country_flag = rebel_tag }
		any_country = { limit = { tag = FI8 NOT = { has_country_flag = rebel_tag } } set_country_flag = rebel_tag }
		any_country = { limit = { tag = DK8 NOT = { has_country_flag = rebel_tag } } set_country_flag = rebel_tag }
		any_country = { limit = { tag = SC8 NOT = { has_country_flag = rebel_tag } } set_country_flag = rebel_tag }
		any_country = { limit = { tag = CH8 NOT = { has_country_flag = rebel_tag } } set_country_flag = rebel_tag }
		any_country = { limit = { tag = ND8 NOT = { has_country_flag = rebel_tag } } set_country_flag = rebel_tag }
		any_country = { limit = { tag = GH8 NOT = { has_country_flag = rebel_tag } } set_country_flag = rebel_tag }
		any_country = { limit = { tag = LR8 NOT = { has_country_flag = rebel_tag } } set_country_flag = rebel_tag }
		any_country = { limit = { tag = AZ8 NOT = { has_country_flag = rebel_tag } } set_country_flag = rebel_tag }
		any_country = { limit = { tag = IR8 NOT = { has_country_flag = rebel_tag } } set_country_flag = rebel_tag }
		any_country = { limit = { tag = SY8 NOT = { has_country_flag = rebel_tag } } set_country_flag = rebel_tag }
		any_country = { limit = { tag = AR8 NOT = { has_country_flag = rebel_tag } } set_country_flag = rebel_tag }
		any_country = { limit = { tag = Q28 NOT = { has_country_flag = rebel_tag } } set_country_flag = rebel_tag }
		any_country = { limit = { tag = JP8 NOT = { has_country_flag = rebel_tag } } set_country_flag = rebel_tag }
		any_country = { limit = { tag = KS8 NOT = { has_country_flag = rebel_tag } } set_country_flag = rebel_tag }
		any_country = { limit = { tag = TR8 NOT = { has_country_flag = rebel_tag } } set_country_flag = rebel_tag }
		any_country = { limit = { tag = JD8 NOT = { has_country_flag = rebel_tag } } set_country_flag = rebel_tag }
		any_country = { limit = { tag = BH8 NOT = { has_country_flag = rebel_tag } } set_country_flag = rebel_tag }
		any_country = { limit = { tag = QT8 NOT = { has_country_flag = rebel_tag } } set_country_flag = rebel_tag }
		any_country = { limit = { tag = OM8 NOT = { has_country_flag = rebel_tag } } set_country_flag = rebel_tag }
		any_country = { limit = { tag = TS8 NOT = { has_country_flag = rebel_tag } } set_country_flag = rebel_tag }
		any_country = { limit = { tag = MA8 NOT = { has_country_flag = rebel_tag } } set_country_flag = rebel_tag }
		any_country = { limit = { tag = DJ8 NOT = { has_country_flag = rebel_tag } } set_country_flag = rebel_tag }
		any_country = { limit = { tag = SG8 NOT = { has_country_flag = rebel_tag } } set_country_flag = rebel_tag }
		any_country = { limit = { tag = GM8 NOT = { has_country_flag = rebel_tag } } set_country_flag = rebel_tag }
		any_country = { limit = { tag = DI8 NOT = { has_country_flag = rebel_tag } } set_country_flag = rebel_tag }
		any_country = { limit = { tag = ID8 NOT = { has_country_flag = rebel_tag } } set_country_flag = rebel_tag }
		any_country = { limit = { tag = TK8 NOT = { has_country_flag = rebel_tag } } set_country_flag = rebel_tag }
		any_country = { limit = { tag = TJ8 NOT = { has_country_flag = rebel_tag } } set_country_flag = rebel_tag }
		any_country = { limit = { tag = YR8 NOT = { has_country_flag = rebel_tag } } set_country_flag = rebel_tag }
		any_country = { limit = { tag = KZ8 NOT = { has_country_flag = rebel_tag } } set_country_flag = rebel_tag }
		any_country = { limit = { tag = UZ8 NOT = { has_country_flag = rebel_tag } } set_country_flag = rebel_tag }
		any_country = { limit = { tag = BI8 NOT = { has_country_flag = rebel_tag } } set_country_flag = rebel_tag }
		any_country = { limit = { tag = KU8 NOT = { has_country_flag = rebel_tag } } set_country_flag = rebel_tag }
		any_country = { limit = { tag = AL8 NOT = { has_country_flag = rebel_tag } } set_country_flag = rebel_tag }
		any_country = { limit = { tag = TY8 NOT = { has_country_flag = rebel_tag } } set_country_flag = rebel_tag }
		any_country = { limit = { tag = BO8 NOT = { has_country_flag = rebel_tag } } set_country_flag = rebel_tag }
		any_country = { limit = { tag = HE8 NOT = { has_country_flag = rebel_tag } } set_country_flag = rebel_tag }
		any_country = { limit = { tag = DG8 NOT = { has_country_flag = rebel_tag } } set_country_flag = rebel_tag }
		any_country = { limit = { tag = EG8 NOT = { has_country_flag = rebel_tag } } set_country_flag = rebel_tag }
		any_country = { limit = { tag = DZ8 NOT = { has_country_flag = rebel_tag } } set_country_flag = rebel_tag }
		any_country = { limit = { tag = EH8 NOT = { has_country_flag = rebel_tag } } set_country_flag = rebel_tag }
		any_country = { limit = { tag = SL8 NOT = { has_country_flag = rebel_tag } } set_country_flag = rebel_tag }
		any_country = { limit = { tag = GR8 NOT = { has_country_flag = rebel_tag } } set_country_flag = rebel_tag }
		any_country = { limit = { tag = AA8 NOT = { has_country_flag = rebel_tag } } set_country_flag = rebel_tag }
		any_country = { limit = { tag = MR8 NOT = { has_country_flag = rebel_tag } } set_country_flag = rebel_tag }
		any_country = { limit = { tag = KH8 NOT = { has_country_flag = rebel_tag } } set_country_flag = rebel_tag }
		any_country = { limit = { tag = PK8 NOT = { has_country_flag = rebel_tag } } set_country_flag = rebel_tag }
		any_country = { limit = { tag = EK8 NOT = { has_country_flag = rebel_tag } } set_country_flag = rebel_tag }
		any_country = { limit = { tag = A18 NOT = { has_country_flag = rebel_tag } } set_country_flag = rebel_tag }
		any_country = { limit = { tag = KD8 NOT = { has_country_flag = rebel_tag } } set_country_flag = rebel_tag }
		any_country = { limit = { tag = IY8 NOT = { has_country_flag = rebel_tag } } set_country_flag = rebel_tag }
		any_country = { limit = { tag = S28 NOT = { has_country_flag = rebel_tag } } set_country_flag = rebel_tag }
		any_country = { limit = { tag = YK8 NOT = { has_country_flag = rebel_tag } } set_country_flag = rebel_tag }
		any_country = { limit = { tag = SA8 NOT = { has_country_flag = rebel_tag } } set_country_flag = rebel_tag }
		any_country = { limit = { tag = AE8 NOT = { has_country_flag = rebel_tag } } set_country_flag = rebel_tag }
		any_country = { limit = { tag = MY8 NOT = { has_country_flag = rebel_tag } } set_country_flag = rebel_tag }
		any_country = { limit = { tag = IQ8 NOT = { has_country_flag = rebel_tag } } set_country_flag = rebel_tag }
		any_country = { limit = { tag = PS8 NOT = { has_country_flag = rebel_tag } } set_country_flag = rebel_tag }
		any_country = { limit = { tag = YE8 NOT = { has_country_flag = rebel_tag } } set_country_flag = rebel_tag }
		any_country = { limit = { tag = LY8 NOT = { has_country_flag = rebel_tag } } set_country_flag = rebel_tag }
		any_country = { limit = { tag = SU8 NOT = { has_country_flag = rebel_tag } } set_country_flag = rebel_tag }
		any_country = { limit = { tag = LA8 NOT = { has_country_flag = rebel_tag } } set_country_flag = rebel_tag }
		any_country = { limit = { tag = CD8 NOT = { has_country_flag = rebel_tag } } set_country_flag = rebel_tag }
		any_country = { limit = { tag = BK8 NOT = { has_country_flag = rebel_tag } } set_country_flag = rebel_tag }
		any_country = { limit = { tag = MI8 NOT = { has_country_flag = rebel_tag } } set_country_flag = rebel_tag }
		any_country = { limit = { tag = HL8 NOT = { has_country_flag = rebel_tag } } set_country_flag = rebel_tag }
		any_country = { limit = { tag = BL8 NOT = { has_country_flag = rebel_tag } } set_country_flag = rebel_tag }
		any_country = { limit = { tag = AF8 NOT = { has_country_flag = rebel_tag } } set_country_flag = rebel_tag }
		any_country = { limit = { tag = FG8 NOT = { has_country_flag = rebel_tag } } set_country_flag = rebel_tag }
		any_country = { limit = { tag = TT8 NOT = { has_country_flag = rebel_tag } } set_country_flag = rebel_tag }
		any_country = { limit = { tag = QK8 NOT = { has_country_flag = rebel_tag } } set_country_flag = rebel_tag }
		any_country = { limit = { tag = IK8 NOT = { has_country_flag = rebel_tag } } set_country_flag = rebel_tag }
		any_country = { limit = { tag = QM8 NOT = { has_country_flag = rebel_tag } } set_country_flag = rebel_tag }
		any_country = { limit = { tag = PL8 NOT = { has_country_flag = rebel_tag } } set_country_flag = rebel_tag }
		any_country = { limit = { tag = CP8 NOT = { has_country_flag = rebel_tag } } set_country_flag = rebel_tag }
		any_country = { limit = { tag = YM8 NOT = { has_country_flag = rebel_tag } } set_country_flag = rebel_tag }
		any_country = { limit = { tag = VI8 NOT = { has_country_flag = rebel_tag } } set_country_flag = rebel_tag }
		any_country = { limit = { tag = NT8 NOT = { has_country_flag = rebel_tag } } set_country_flag = rebel_tag }
		any_country = { limit = { tag = S38 NOT = { has_country_flag = rebel_tag } } set_country_flag = rebel_tag }
		any_country = { limit = { tag = RL8 NOT = { has_country_flag = rebel_tag } } set_country_flag = rebel_tag }
		any_country = { limit = { tag = NS8 NOT = { has_country_flag = rebel_tag } } set_country_flag = rebel_tag }
		any_country = { limit = { tag = VC8 NOT = { has_country_flag = rebel_tag } } set_country_flag = rebel_tag }
		any_country = { limit = { tag = RB8 NOT = { has_country_flag = rebel_tag } } set_country_flag = rebel_tag }
		any_country = { limit = { tag = B18 NOT = { has_country_flag = rebel_tag } } set_country_flag = rebel_tag }
		any_country = { limit = { tag = AD8 NOT = { has_country_flag = rebel_tag } } set_country_flag = rebel_tag }
		any_country = { limit = { tag = FO8 NOT = { has_country_flag = rebel_tag } } set_country_flag = rebel_tag }
		any_country = { limit = { tag = KL8 NOT = { has_country_flag = rebel_tag } } set_country_flag = rebel_tag }
		any_country = { limit = { tag = EI8 NOT = { has_country_flag = rebel_tag } } set_country_flag = rebel_tag }
		any_country = { limit = { tag = HZ8 NOT = { has_country_flag = rebel_tag } } set_country_flag = rebel_tag }
		any_country = { limit = { tag = A28 NOT = { has_country_flag = rebel_tag } } set_country_flag = rebel_tag }
		any_country = { limit = { tag = ZK8 NOT = { has_country_flag = rebel_tag } } set_country_flag = rebel_tag }
		any_country = { limit = { tag = GG8 NOT = { has_country_flag = rebel_tag } } set_country_flag = rebel_tag }
		any_country = { limit = { tag = WF8 NOT = { has_country_flag = rebel_tag } } set_country_flag = rebel_tag }
		any_country = { limit = { tag = GW8 NOT = { has_country_flag = rebel_tag } } set_country_flag = rebel_tag }
		any_country = { limit = { tag = UM8 NOT = { has_country_flag = rebel_tag } } set_country_flag = rebel_tag }
		any_country = { limit = { tag = TV8 NOT = { has_country_flag = rebel_tag } } set_country_flag = rebel_tag }
		any_country = { limit = { tag = IG8 NOT = { has_country_flag = rebel_tag } } set_country_flag = rebel_tag }
		any_country = { limit = { tag = KA8 NOT = { has_country_flag = rebel_tag } } set_country_flag = rebel_tag }
		any_country = { limit = { tag = G28 NOT = { has_country_flag = rebel_tag } } set_country_flag = rebel_tag }
		any_country = { limit = { tag = B28 NOT = { has_country_flag = rebel_tag } } set_country_flag = rebel_tag }
		any_country = { limit = { tag = RM8 NOT = { has_country_flag = rebel_tag } } set_country_flag = rebel_tag }
		any_country = { limit = { tag = PC8 NOT = { has_country_flag = rebel_tag } } set_country_flag = rebel_tag }
		any_country = { limit = { tag = RF8 NOT = { has_country_flag = rebel_tag } } set_country_flag = rebel_tag }
		any_country = { limit = { tag = KB8 NOT = { has_country_flag = rebel_tag } } set_country_flag = rebel_tag }
		any_country = { limit = { tag = BD8 NOT = { has_country_flag = rebel_tag } } set_country_flag = rebel_tag }
		any_country = { limit = { tag = MZ8 NOT = { has_country_flag = rebel_tag } } set_country_flag = rebel_tag }
		any_country = { limit = { tag = KT8 NOT = { has_country_flag = rebel_tag } } set_country_flag = rebel_tag }
		any_country = { limit = { tag = NN8 NOT = { has_country_flag = rebel_tag } } set_country_flag = rebel_tag }
		any_country = { limit = { tag = TI8 NOT = { has_country_flag = rebel_tag } } set_country_flag = rebel_tag }
		any_country = { limit = { tag = SD8 NOT = { has_country_flag = rebel_tag } } set_country_flag = rebel_tag }
		any_country = { limit = { tag = MH8 NOT = { has_country_flag = rebel_tag } } set_country_flag = rebel_tag }
		any_country = { limit = { tag = WT8 NOT = { has_country_flag = rebel_tag } } set_country_flag = rebel_tag }
		any_country = { limit = { tag = ZM8 NOT = { has_country_flag = rebel_tag } } set_country_flag = rebel_tag }
		any_country = { limit = { tag = AW8 NOT = { has_country_flag = rebel_tag } } set_country_flag = rebel_tag }
		any_country = { limit = { tag = CS8 NOT = { has_country_flag = rebel_tag } } set_country_flag = rebel_tag }
		any_country = { limit = { tag = DF8 NOT = { has_country_flag = rebel_tag } } set_country_flag = rebel_tag }
		any_country = { limit = { tag = Y18 NOT = { has_country_flag = rebel_tag } } set_country_flag = rebel_tag }
		any_country = { limit = { tag = IB8 NOT = { has_country_flag = rebel_tag } } set_country_flag = rebel_tag }
		any_country = { limit = { tag = NQ8 NOT = { has_country_flag = rebel_tag } } set_country_flag = rebel_tag }
		any_country = { limit = { tag = HK8 NOT = { has_country_flag = rebel_tag } } set_country_flag = rebel_tag }
		any_country = { limit = { tag = KK8 NOT = { has_country_flag = rebel_tag } } set_country_flag = rebel_tag }
		any_country = { limit = { tag = AC8 NOT = { has_country_flag = rebel_tag } } set_country_flag = rebel_tag }
		any_country = { limit = { tag = MU8 NOT = { has_country_flag = rebel_tag } } set_country_flag = rebel_tag }
		any_country = { limit = { tag = DA8 NOT = { has_country_flag = rebel_tag } } set_country_flag = rebel_tag }
		any_country = { limit = { tag = PN8 NOT = { has_country_flag = rebel_tag } } set_country_flag = rebel_tag }
		any_country = { limit = { tag = KP8 NOT = { has_country_flag = rebel_tag } } set_country_flag = rebel_tag }
		any_country = { limit = { tag = SN8 NOT = { has_country_flag = rebel_tag } } set_country_flag = rebel_tag }
		any_country = { limit = { tag = NF8 NOT = { has_country_flag = rebel_tag } } set_country_flag = rebel_tag }
		any_country = { limit = { tag = GV8 NOT = { has_country_flag = rebel_tag } } set_country_flag = rebel_tag }
		any_country = { limit = { tag = NU8 NOT = { has_country_flag = rebel_tag } } set_country_flag = rebel_tag }
		any_country = { limit = { tag = KE8 NOT = { has_country_flag = rebel_tag } } set_country_flag = rebel_tag }
		## modifiers
		any_province_region = {
			limit = {
				has_owner_culture = yes
				has_owner_religion = yes
				NOT = { has_province_modifier = government_stronghold }
			}
			add_province_modifier = { 
				name = government_stronghold
				duration = -1
			}
		}
		any_province_region = {
			limit = {
				NOT = {
					AND = {
						has_owner_culture = yes
						has_owner_religion = yes
					}
				}
				has_province_modifier = government_stronghold
			}
			remove_province_modifier = government_stronghold
		}
		############################
		## MONTHLY CODE
		############################
		random_country = {
			limit = {
				tag = BNC
				check_variable = {
					which = count_to_one_month
					value = 30
				}
			}
			set_variable = {
				which = count_to_one_month
				value = 0
			}					
		}
								
	}
	option = {
		name = "EVTOPT_OK"		# Ok
		## We put all the EVT things we want here
		# test

		# edit: no
	}
}
