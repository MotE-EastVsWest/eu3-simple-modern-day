country_event = {
	id = 10026
	title = "Add_Culture_Group_Cores_TITLE"
	desc = "Add_Culture_Group_Cores_DESC"
	
	trigger = {
		any_province_region = {
			culture = THIS
			NOT = { is_core = THIS }
		}
	}
	mean_time_to_happen = { days = 1 }
	immediate = {
		any_province_region = {
			limit = { 
				culture = THIS
				NOT = { is_core = THIS }
			}
			add_core = THIS
		}


		#set_revolution_target = BNC #who cares
	}
	option = {
		name = "EVTOPT_OK"
	}
}
