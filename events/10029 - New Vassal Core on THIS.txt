
country_event = {
	title = "BouncerFor10025_TITLE"
	desc = "BouncerFor10025_DESC"
	major = yes
	id = 10029
	is_triggered_only = yes
	
	immediate = {
		FROM = {
			country_event = 10025
		}
	}
	option = {
		name = "EVTOPT_OK"		# Ok
	}
}
