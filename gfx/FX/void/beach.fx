texture tex0 < string name = "Beach.tga"; >;
texture tex1 < string name = "Beach.tga"; >;
texture tex2 < string name = "Beach.tga"; >;
texture tex3 < string name = "Beach.tga"; >;

float4x4 WorldMatrix; 
float4x4 ViewMatrix; 
float4x4 ProjectionMatrix; 
float	 vTime;

float4x4 AbsoluteWorldMatrix;

sampler BaseTexture  =
sampler_state
{
    Texture = <tex0>;
    MinFilter = Linear;
    MagFilter = Linear;
    MipFilter = None;
    AddressU = Wrap;
    AddressV = Wrap;
};

sampler AlphaTexture  =
sampler_state
{
    Texture = <tex1>;
    MinFilter = Linear;
    MagFilter = Linear;
    MipFilter = None;
    AddressU = Clamp;
    AddressV = Clamp;
};

sampler SandTexture  =
sampler_state
{
    Texture = <tex2>;
    MinFilter = Linear;
    MagFilter = Linear;
    MipFilter = None;
    AddressU = Clamp;
    AddressV = Clamp;
};


sampler TerraIncognitaFiltered  =
sampler_state
{
    Texture = <tex3>;
    MinFilter = Linear;
    MagFilter = Linear;
    MipFilter = None;
    AddressU = Clamp;
    AddressV = Clamp;
};



struct VS_INPUT
{
    float4 vPosition  : POSITION;
    float2 vTexCoord0 : TEXCOORD0;
};

struct VS_OUTPUT
{
    float4  vPosition  : POSITION;
    float2  vTexCoord0 : TEXCOORD0;
    float2  vTexCoord1 : TEXCOORD1;
    float2  vTexCoord2 : TEXCOORD2;
    float2  vTexCoord3 : TEXCOORD3;
};


VS_OUTPUT VertexShader_Beach(const VS_INPUT v )
{
	VS_OUTPUT Out = (VS_OUTPUT)0;
	float4x4 WorldView = mul(WorldMatrix, ViewMatrix);
	float3 P = mul(v.vPosition, (float4x3)WorldView);
	Out.vPosition  = mul(float4(P, 1), ProjectionMatrix);

	Out.vTexCoord1 = v.vTexCoord0;
	Out.vTexCoord0 = v.vTexCoord0; //n - float2( 0, vTime* 0.3 );
	Out.vTexCoord3 = v.vTexCoord0;
	Out.vTexCoord3.y = Out.vPosition.w;

	float4 WorldPosition = mul( v.vPosition, AbsoluteWorldMatrix );
	Out.vTexCoord2 = float2( (WorldPosition.x+0.5)/2048.0f, (WorldPosition.z+0.5)/1024.0f );

	return Out;
}

float4 PixelShader_Beach( VS_OUTPUT v ) : COLOR
{	
	float row = frac(8*v.vTexCoord3.y);
	float4 OutColor = float4(0,0,0,0);
	OutColor.a = 0.5*saturate (abs(4*row - 2) - 0.7) * 3 * (v.vTexCoord0.y - 0.1);

	OutColor.a += 0.5*saturate ( 2*saturate(v.vTexCoord0.y) - 0.6);

	
 
	float4 TerraIncognita = tex2D( TerraIncognitaFiltered, v.vTexCoord2 );
	
	if (TerraIncognita.g > 0.25) 
	OutColor.rgb += ( TerraIncognita.g - 0.25 )*1.66;
		
	OutColor.rgba = 0.0;
	
	return OutColor;
}

technique BeachShader
{
	pass p0
	{
		ALPHABLENDENABLE = True;
		ALPHATESTENABLE = True;

		VertexShader = compile vs_1_1 VertexShader_Beach();
		PixelShader = compile ps_2_0 PixelShader_Beach();
	}
}
