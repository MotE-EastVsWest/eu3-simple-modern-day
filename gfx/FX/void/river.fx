#define TAN float3(0.94,0.87,0.68)
#define LIGHTFACTOR 1.1
#define LIGHTSHIFT 0.1
#define FOWFACTOR 1.25

texture tex0 < string name = "River.tga"; >;
texture tex1 < string name = "RiverMovement.tga"; >;
texture tex2 < string name = "TerraIncog.dds"; >;		// Borders texture
texture tex3 < string name = "colormap_water.dds"; >;


float4x4 WorldMatrix; 
float4x4 ViewMatrix; 
float4x4 ProjectionMatrix; 
float4x4 AbsoluteWorldMatrix;
float	 vTime;

sampler BaseTexture  =
sampler_state
{
    Texture = <tex0>;
    MinFilter = Linear;
    MagFilter = Linear;
    MipFilter = None;
    AddressU = Wrap;
    AddressV = Wrap;
};

sampler AnimatedTexture  =
sampler_state
{
    Texture = <tex1>;
    MinFilter = Linear;
    MagFilter = Linear;
    MipFilter = None;
    AddressU = Wrap;
    AddressV = Wrap;
};

sampler TerraIncognitaTexture =
sampler_state
{
    Texture = <tex2>;
    MinFilter = Linear;
    MagFilter = Linear;
    MipFilter = None;
    AddressU = Clamp;
    AddressV = Clamp;
};

sampler Water =
sampler_state
{
    Texture = <tex3>;
    MinFilter = Linear;
    MagFilter = Linear;
    MipFilter = None;
    AddressU = Clamp;
    AddressV = Clamp;
};


struct VS_INPUT
{
    float4 vPosition  : POSITION;
    float2 vTexCoord0 : TEXCOORD0;
    float2 vTexCoord1 : TEXCOORD1;
};

struct VS_OUTPUT
{
    float4  vPosition  : POSITION;
    float2  vTexCoord0 : TEXCOORD0;
    float2  vTexCoord1 : TEXCOORD1;
    float2  vTerrainTexCoord : TEXCOORD2;
    float2  vWaterTexCoord : TEXCOORD3;
};


const float2 off = float2(0.25/1024, 0.25/512);
const float2 offx = float2(0.5/1024, 0.0/512); 
const float2 offy = float2(-0.0/1024, 0.5/512);

VS_OUTPUT VertexShader_River(const VS_INPUT v )
{
	VS_OUTPUT Out = (VS_OUTPUT)0;
	float4x4 WorldView = mul(WorldMatrix, ViewMatrix);
	float4 Q = mul(v.vPosition, WorldMatrix);
	float3 P = mul(Q, (float4x3)ViewMatrix);
	//float3 P = mul(v.vPosition, (float4x3)WorldView);
	Out.vPosition  = mul(float4(P, 1), ProjectionMatrix);

	Out.vTexCoord0 = v.vTexCoord0;
	Out.vTexCoord1 = v.vTexCoord1 * float2(1.0,0.333); 

	float4 WorldPosition = mul( v.vPosition, AbsoluteWorldMatrix );
	Out.vTerrainTexCoord  = float2( (WorldPosition.x+0.5)/2048.0f, (WorldPosition.z+0.5)/1024.0f )+off/2;

	Out.vWaterTexCoord.x = saturate(3*mul( float3(0,1,0) , (float4x3)WorldView).y -0.35); //zoom
	Out.vWaterTexCoord.y = Q.y;

	return Out;
}

float4 PixelShader_River( VS_OUTPUT v ) : COLOR
{
	float4 OutColor = tex2D( BaseTexture, v.vTexCoord0.xy );
	float4 Movement = tex2D( AnimatedTexture, v.vTexCoord1  );

	OutColor.rgb *= float3(0.58,0.58,0.95)*(1-LIGHTSHIFT)*TAN;
	OutColor.rgb *= Movement.rgb;
	OutColor.rgb = lerp(OutColor.rgb, float3(0.375,0.375, 0.6)*TAN, 0.75*(1-v.vWaterTexCoord.x));


	
	// Terra incognita
	float TerraIncognita = (tex2D( TerraIncognitaTexture, v.vTerrainTexCoord ).g - 0.25);
	float ti = saturate(TerraIncognita);
	float4 temp = float4(tex2D( TerraIncognitaTexture, v.vTerrainTexCoord + offx).g,
  	  tex2D( TerraIncognitaTexture, v.vTerrainTexCoord - offx).g,
  	  tex2D( TerraIncognitaTexture, v.vTerrainTexCoord + offy).g,
  	  tex2D( TerraIncognitaTexture, v.vTerrainTexCoord - offy).g );
  float local = saturate(3*(ti+dot(saturate(temp-0.25), float4(1,1,1,1))));
 	OutColor.rgb *= 1+FOWFACTOR*TerraIncognita;
	OutColor.a = lerp(OutColor.a,0.0, local);
	//OutColor.a = lerp(OutColor.a,0.0, saturate(20*(tex2D(Water,v.vTerrainTexCoord).b - 0.35)));
	if (v.vWaterTexCoord.y < 0.45) OutColor.a = 0;


	return OutColor;
}

technique RiverShader
{
	pass p0
	{
		ALPHABLENDENABLE = True;
		ALPHATESTENABLE = True;

		VertexShader = compile vs_1_1 VertexShader_River();
		PixelShader = compile ps_2_0 PixelShader_River();
	}
}
