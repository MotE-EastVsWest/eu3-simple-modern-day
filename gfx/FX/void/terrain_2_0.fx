#define BRIGHTNESS 0.05 //-0.03
#define CONTRAST 2.0
#define DESATURATION 0.4
#define BLACK float4(0.0,0.0,0.0,1.0)
#define TAN float3(0.94,0.87,0.68)
#define LIGHTFACTOR 1.1
#define LIGHTSHIFT 0.1
#define FOWFACTOR 1.25
#define BARE float3( 0.88, 0.83, 0.75 )

#define X_OFFSET 0.5
#define Z_OFFSET 0.5


texture tex0 < string ResourceName = "Base.tga"; >;		// Base texture
texture tex1 < string ResourceName = "Terrain.tga"; >;	// Terrain texture
texture tex2 < string ResourceName = "Color.dds"; >;		// Color texture
texture tex3 < string ResourceName = "Alpha.dds"; >;		// Terrain Alpha Mask
texture tex4 < string ResourceName = "BorderDirection.dds"; >;	// Borders texture
texture tex5 < string ResourceName = "ProvinceBorders.dds"; >;
texture tex6 < string ResourceName = "CountryBorders.dds"; >;
texture tex7 < string ResourceName = "TerraIncog.dds"; >;


float4x4 WorldMatrix		: World; 
float4x4 ViewMatrix		: View; 
float4x4 ProjectionMatrix	: Projection; 
float4x4 AbsoluteWorldMatrix;
float3	 LightDirection;
float	 vAlpha;

sampler BaseTexture  =
sampler_state
{
    Texture = <tex0>;
    MinFilter = Linear; //Point;
    MagFilter = Linear; //Point;
    MipFilter = None; //Linear; //None; /n
    AddressU = Wrap;
    AddressV = Wrap;
};

sampler TreeTexture  =
sampler_state
{
    Texture = <tex0>;
    MinFilter = Linear; //Point;
    MagFilter = Linear; //Point;
    MipFilter = None;
    AddressU = Wrap;
    AddressV = Wrap;
};


sampler MapTexture  =
sampler_state
{
    Texture = <tex1>;
    MinFilter = Linear; //Point;
    MagFilter = Linear; //Point;
    MipFilter = Linear; //None;
    AddressU = Wrap;
    AddressV = Wrap;
};

sampler StripesTexture  =
sampler_state
{
    Texture = <tex1>;
    MinFilter = Linear; //Point;
    MagFilter = Point; //Point;
    MipFilter = Linear;
    AddressU = Wrap;
    AddressV = Wrap;
};


sampler ColorTexture  =
sampler_state
{
    Texture = <tex4>;
    MinFilter = Linear;
    MagFilter = Linear;
    MipFilter = None;
    AddressU = Wrap;
    AddressV = Wrap;
};

// used for political, religious etc etc..
sampler GeneralTexture  =
sampler_state
{
    Texture = <tex2>;
    MinFilter = Point;
    MagFilter = Point;
    MipFilter = None;
    AddressU = Wrap;
    AddressV = Wrap;
};

sampler GeneralTexture2  =
sampler_state
{
    Texture = <tex3>;
    MinFilter = Point;
    MagFilter = Point;
    MipFilter = None;
    AddressU = Wrap;
    AddressV = Wrap;
};


sampler TerrainAlphaTexture  =
sampler_state
{
    Texture = <tex3>;
    MinFilter = Point;
    MagFilter = Point;
    MipFilter = None;
    AddressU = Clamp;
    AddressV = Clamp;
};

sampler BorderDirectionTexture  =
sampler_state
{
    Texture = <tex4>;
    MinFilter = Point;
    MagFilter = Point;
    MipFilter = None;
    AddressU = Clamp;
    AddressV = Clamp;
};

sampler WinterTexture  =
sampler_state
{
    Texture = <tex2>;
    MinFilter = Point;
    MagFilter = Point;
    MipFilter = None;
    AddressU = Clamp;
    AddressV = Clamp;
};


sampler ProvinceBorderTexture  =
sampler_state
{
    Texture = <tex5>;
    MinFilter = Point; //Linear;
    MagFilter = Point; //Linear;
    MipFilter = None;
    AddressU = Clamp;
    AddressV = Clamp;
};

sampler CountryBorderTexture  =
sampler_state
{
    Texture = <tex6>;
    MinFilter = Point; //Linear;
    MagFilter = Point; //Linear;
    MipFilter = None;
    AddressU = Clamp;
    AddressV = Clamp;
};

sampler BorderDiagTexture  =
sampler_state
{
    Texture = <tex1>;
    MinFilter = Point; //Linear;
    MagFilter = Point; //Linear;
    MipFilter = None;
    AddressU = Clamp;
    AddressV = Clamp;
};


sampler TerraIncognitaTextureTerrain =
sampler_state
{
    Texture = <tex7>;
    MinFilter = Linear;
    MagFilter = Linear;
    MipFilter = None;
    AddressU = Clamp;
    AddressV = Clamp;
};

sampler TerraIncognitaTextureTree =
sampler_state
{
    Texture = <tex1>;
    MinFilter = Linear;
    MagFilter = Linear;
    MipFilter = None;
    AddressU = Clamp;
    AddressV = Clamp;
};


struct VS_INPUT
{
    float4 vPosition  : POSITION;
    float3 vNormal    : NORMAL;
    float2 vTexCoord  : TEXCOORD0;
    float2 vProvinceIndexCoord  : TEXCOORD1;
};

struct VS_INPUT_BEACH
{
    float4 vPosition  : POSITION;
    float3 vNormal    : NORMAL;

    float2 vProvinceIndexCoord  : TEXCOORD0;
};

struct VS_OUTPUT
{
    float4  vPosition : POSITION;
    float4  vTexCoord0 : TEXCOORD0;	// third beein' lightIntensity, 4th height
    float2  localTexCoord : TEXCOORD1;
    float2  vColorTexCoord : TEXCOORD2;
    float2  worldTexCoord : TEXCOORD3;
    float2  vBorderTexCoord1 : TEXCOORD4;
    float2  vTerrainTexCoord : TEXCOORD5;
    float2 vProvinceIndexCoord  : TEXCOORD6;
    float4 General		: TEXCOORD7; //n
};

struct VS_OUTPUT_BEACH
{
    float4  vPosition : POSITION;
    float2  vTerrainTexCoord : TEXCOORD0;
    float2  vColorTexCoord : TEXCOORD1;
	// Later put this into ONE texcoord, this is easier for debugging etc..
    float3  vLightIntensity : TEXCOORD2;
    float2 vProvinceIndexCoord  : TEXCOORD3;
	float2 worldTexCoord		: TEXCOORD4;
};

struct VS_INPUT_PTI
{
    float4 vPosition  : POSITION;
};

struct VS_OUTPUT_PTI
{
    float2  worldTexCoord : TEXCOORD3;
    float4  vPosition : POSITION;
};


struct VS_INPUT_TREE
{
    float4 vPosition : POSITION;
    float2 vTexCoord : TEXCOORD0;
};

struct VS_OUTPUT_TREE
{
    float4 vPosition   : POSITION;
    float2 vTexCoord   : TEXCOORD0;
    float2 vTexCoordTI : TEXCOORD1;
};

const float2 off = float2(0.25/1024, 0.25/512);
//const float2 offx = float2(1.0/1024, 0.0); //0.9
//const float2 offy = float2(0.0, 1.0/512);

const float2 offx = float2(0.5/1024, 0.0/512); 
const float2 offy = float2(-0.0/1024, 0.5/512);

/////////////////////////////////////////////////////////////////////////////////



VS_OUTPUT VertexShader_Map_General(const VS_INPUT v )
{
	VS_OUTPUT Out = (VS_OUTPUT)0;

	float4x4 WorldView = mul(WorldMatrix, ViewMatrix);
	float3 P = mul(v.vPosition, (float4x3)WorldView);
	Out.vPosition  = mul(float4(P, 1), ProjectionMatrix);

	float3 VertexNormal = mul( v.vNormal, WorldMatrix );
	float3 direction = normalize( LightDirection );
	Out.vTexCoord0.xy = v.vTexCoord;
	Out.vTexCoord0.z = max( dot( VertexNormal, -direction ), 0.5f )*LIGHTFACTOR-LIGHTSHIFT;
	Out.vTexCoord0.w = v.vPosition.y;


	Out.vProvinceIndexCoord = v.vProvinceIndexCoord;

	float4 WorldPosition = mul( v.vPosition, AbsoluteWorldMatrix );

	float2 TerrainCoord = WorldPosition.xz;
	TerrainCoord += 0.5;
	TerrainCoord /= 8.0;
	Out.vTerrainTexCoord  = TerrainCoord;

	Out.worldTexCoord = float2( WorldPosition.x/2048.0f, WorldPosition.z/1024.0f ) + off;
	Out.General.x = 0.42*saturate(3*mul( float3(0,1,0) , (float4x3)WorldView).y -0.3); //zoom
	Out.localTexCoord  = Out.worldTexCoord*float2(120,60);
	
	return Out;
}



float4 PixelShader_Map2_0_General( VS_OUTPUT v ) : COLOR
{
	float4 WorldColor = tex2D(ColorTexture, v.worldTexCoord.xy);  // World terrain
	float4 BaseColor = tex2D( BaseTexture, v.localTexCoord); // local texture
	float4 Color1 = tex2D( GeneralTexture, v.vProvinceIndexCoord );
	float4 Color2 = tex2D( GeneralTexture2, v.vProvinceIndexCoord );
	float4 stripes = tex2D( StripesTexture, v.vTerrainTexCoord );
	

	float4 Color = lerp(Color2, Color1, stripes.g); //data color
	float3 tmp = Color.rgb-0.5;
	if (dot(tmp,tmp)<0.001) Color.rgb = BARE;	//fix diplomacy map colour
	if (any(Color1.rgb-Color2.rgb)) 		//double printed bits
		Color.rgb *= lerp(1.0, Color2.rgb, stripes.b);



	float4 OutColor = float4( BaseColor.rgb * v.vTexCoord0.z,1.0);  //paper


	WorldColor.r += 0.3*BaseColor.a;// -0.15;
	if (WorldColor.r > 0.70+0.15) OutColor.rgb *= float3( 0.95, 0.95, 0.93); // mountains
	  else if (WorldColor.r > 0.25+0.15) OutColor.rgb *= float3( 0.93, 0.9, 0.82); // hills

	if (WorldColor.g > 0.75) OutColor.rgb *= lerp(float3(0.9,0.96,0.85), float3(0.5,0.7,0.5), 
		v.General.x*stripes.r); // forest
		
  OutColor.rgb *= lerp(1.0, Color.rgb, 0.7 
  		-0.1*(saturate(2*BaseColor.a) -  saturate(35*WorldColor.b))); //colours the map
	
	// 0.35 -- 0.4 is clear -- black Outline at coast
	if (v.vTexCoord0.w < 0.42) OutColor = lerp(OutColor, BLACK, saturate(20*(WorldColor.b - 0.35)) ); 


	
  // Terra incognita and FOW
  float TerraIncognita = tex2D( TerraIncognitaTextureTerrain, v.worldTexCoord ).g-0.25;
  float ti = saturate(TerraIncognita);
  float4 temp = float4(tex2D( TerraIncognitaTextureTerrain, v.worldTexCoord + offx).g,
  	tex2D( TerraIncognitaTextureTerrain, v.worldTexCoord - offx).g,
  	tex2D( TerraIncognitaTextureTerrain, v.worldTexCoord + offy).g,
  	tex2D( TerraIncognitaTextureTerrain, v.worldTexCoord - offy).g );
  float local =  saturate(3*(ti+dot(saturate(temp-0.25), float4(1,1,1,1)))) ;
  OutColor.rgb *= 1+FOWFACTOR*TerraIncognita;
  OutColor.rgb = lerp(OutColor.rgb, (1-FOWFACTOR*0.25)*(1-LIGHTSHIFT)*BaseColor.rgb, local);
  OutColor.rgb *= lerp(1.0,float3(1.0,0.95,0.9),ti);

  return OutColor;
}




VS_OUTPUT_BEACH VertexShader_Beach_General(const VS_INPUT_BEACH v )
{
	VS_OUTPUT_BEACH Out = (VS_OUTPUT_BEACH)0;
	float4x4 WorldView = mul(WorldMatrix, ViewMatrix);
	float3 P = mul(v.vPosition, (float4x3)WorldView);
	Out.vPosition  = mul(float4(P, 1), ProjectionMatrix);

	float3 VertexNormal = mul( v.vNormal, WorldMatrix );

	float3 direction = normalize( LightDirection );
	Out.vLightIntensity.xy = max( dot( VertexNormal, -direction ), 0.5f )*LIGHTFACTOR-LIGHTSHIFT;
	Out.vLightIntensity.z = v.vPosition.y;

	Out.vProvinceIndexCoord = v.vProvinceIndexCoord;

	float4 WorldPosition = mul( v.vPosition, AbsoluteWorldMatrix );
	Out.vColorTexCoord.xy = float2( WorldPosition.x/2048.0f, WorldPosition.z/1024.0f );

	float2 TerrainCoord = WorldPosition.xz;
	TerrainCoord += 0.5;
	TerrainCoord /= 8.0;
	Out.vTerrainTexCoord  = TerrainCoord;

	Out.worldTexCoord = float2( WorldPosition.x/2048.0f, WorldPosition.z/1024.0f ) + off;

	return Out;
}


float4 PixelShader_Beach_General( VS_OUTPUT_BEACH v ) : COLOR
{
	float4 WorldColor = tex2D(ColorTexture, v.worldTexCoord.xy);
	float4 BaseColor = tex2D( BaseTexture, v.worldTexCoord*float2(120,60)); //v.vTerrainTexCoord );
	float4 Color1 = tex2D( GeneralTexture, v.vProvinceIndexCoord ) ;
	float4 Color2 = tex2D( GeneralTexture2, v.vProvinceIndexCoord );
  float4 stripes = tex2D( StripesTexture, v.vTerrainTexCoord );


	float4 Color = lerp(Color2, Color1, stripes.g); //data color
	float3 tmp = Color.rgb-0.5;
	if (dot(tmp,tmp)<0.001) Color.rgb = BARE;	//fix diplomacy map colour
	if (any(Color1.rgb-Color2.rgb)) 		//double printed bits
		Color.rgb *= lerp(1.0, Color2.rgb, stripes.b);



	float4 OutColor = 1.0;
  OutColor.rgb = BaseColor.rgb * v.vLightIntensity.x;
  
  	WorldColor.r += 0.3*BaseColor.a;// -0.15;
  if (WorldColor.r > 0.25+0.15) OutColor.rgb *= float3( 0.93, 0.9, 0.82); // hills
  
 
 //misprint from sea
 if (WorldColor.b *BaseColor.a > 0.17 && v.vLightIntensity.z < 0.45) OutColor.rgb *= float3(0.6,0.6,1.0);

 OutColor.rgb *= lerp(1.0, Color.rgb, 0.7 
  		-0.1*(saturate(2*BaseColor.a) -  saturate(35*WorldColor.b))); //colours the map

  	// 0.35 -- 0.4 is clear -- black
	if (v.vLightIntensity.z < 0.42) OutColor = lerp(OutColor, BLACK, saturate(20*(WorldColor.b - 0.35)) ); 
  



  // Terra incognita and FOW
  float TerraIncognita = tex2D( TerraIncognitaTextureTerrain, v.worldTexCoord ).g-0.25;
  float ti = saturate(TerraIncognita);
  float4 temp = float4(tex2D( TerraIncognitaTextureTerrain, v.worldTexCoord + offx).g,
  	tex2D( TerraIncognitaTextureTerrain, v.worldTexCoord - offx).g,
  	tex2D( TerraIncognitaTextureTerrain, v.worldTexCoord + offy).g,
  	tex2D( TerraIncognitaTextureTerrain, v.worldTexCoord - offy).g );
  float local =  saturate(3*(ti+dot(saturate(temp-0.25), float4(1,1,1,1)))) ;
  OutColor.rgb *= 1+FOWFACTOR*TerraIncognita;
  OutColor.rgb = lerp(OutColor.rgb, (1-FOWFACTOR*0.25)*(1-LIGHTSHIFT)*BaseColor.rgb, local);
  OutColor.rgb *= lerp(1.0,float3(1.0,0.95,0.9),ti);

  return OutColor;
}

	
/////////////////////////////////////////////////////////////////////////////////

VS_OUTPUT VertexShader_Map_Border(const VS_INPUT v )
{
	VS_OUTPUT Out = (VS_OUTPUT)0;

	float4x4 WorldView = mul(WorldMatrix, ViewMatrix);
	float3 P = mul(v.vPosition, (float4x3)WorldView);
	Out.vPosition  = mul(float4(P, 1), ProjectionMatrix);

	float4 WorldPosition = mul( v.vPosition, AbsoluteWorldMatrix );

	float2 TerrainCoord = WorldPosition.xz;
	TerrainCoord += 0.5;
	TerrainCoord /= 8.0;
	Out.vTerrainTexCoord  = TerrainCoord;

	Out.worldTexCoord = float2( ( WorldPosition.x + 0.5 )/2048.0f, ( WorldPosition.z + 0.5 ) /1024.0f );
	Out.vBorderTexCoord1 = float2( WorldPosition.x, WorldPosition.z );

	return Out;
}


float4 PixelShader_Map2_0_Border( VS_OUTPUT v ) : COLOR
{
	float4 BorderDirectionColor = tex2D( BorderDirectionTexture, v.worldTexCoord );
	float2 TexCoord = v.vBorderTexCoord1;

	TexCoord += 0.5;

	TexCoord %= 1;				// 0 => 1 range.. only thing we need is the decimal part.
	TexCoord.y = 1.0 - TexCoord.y;
	TexCoord.x /= 16;
	TexCoord.x *= 0.8; //75;
	float2 TexCoord2 = TexCoord;
	float2 TexCoord3 = TexCoord;

	TexCoord.x += BorderDirectionColor.b;
	float4 ProvinceBorder = tex2D( ProvinceBorderTexture, TexCoord );

	TexCoord2.x += BorderDirectionColor.r;
	float4 CountryBorder = tex2D( CountryBorderTexture, TexCoord2 );

	TexCoord3.x += BorderDirectionColor.a;
	float4 DiagBorder = tex2D( BorderDiagTexture, TexCoord3 );

	//ProvinceBorder.rgb *= ProvinceBorder.a;
	//CountryBorder.rgb *= CountryBorder.a;
	//DiagBorder.rgb *= DiagBorder.a;

	float4 OutColor;
	//OutColor.rgb = ProvinceBorder.rgb*ProvinceBorder.a;
	OutColor.a = max( ProvinceBorder.a, CountryBorder.a );
	OutColor.a = max( OutColor.a, DiagBorder.a );
	//OutColor.rgb = CountryBorder.rgb * CountryBorder.a + OutColor.rgb*( 1.0f - CountryBorder.a );
	//OutColor.rgb = max( OutColor.rgb, DiagBorder.rgb );


	//OutColor.rgb = float3(0.21,0.18,0.05); //5*(0 + max(CountryBorder.a, DiagBorder.a));
	OutColor.rgb = float3(0.19, 0.14, 0.045);
	//OutColor.a -= 0.03; //0.065

	// Terra incognita
	float4 TerraIncognita = tex2D( TerraIncognitaTextureTerrain, v.worldTexCoord )-0.25;
	if ( TerraIncognita.g > 0 )
		OutColor.a -= 10*(TerraIncognita.ggg);
	else OutColor.rgb *= ( 1+1.25*TerraIncognita.g);
	return OutColor;
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////

VS_OUTPUT VertexShader_Map(const VS_INPUT v )
{
	VS_OUTPUT Out = (VS_OUTPUT)0;

	float4x4 WorldView = mul(WorldMatrix, ViewMatrix);
	float3 P = mul(v.vPosition, (float4x3)WorldView);
	Out.vPosition  = mul(float4(P, 1), ProjectionMatrix);

	float3 VertexNormal = mul( v.vNormal, WorldMatrix );

	float3 direction = normalize( LightDirection );
	Out.vTexCoord0.xy = v.vTexCoord;
	Out.vTexCoord0.z = max( dot( VertexNormal, -direction ), 0.5f )*LIGHTFACTOR-LIGHTSHIFT;
//	Out.vTexCoord1  = v.vTexCoord;

	Out.vProvinceIndexCoord = v.vProvinceIndexCoord;

	float4 WorldPosition = mul( v.vPosition, AbsoluteWorldMatrix );
	Out.vColorTexCoord.xy = float2( ( WorldPosition.x + X_OFFSET)/2048.0f, (WorldPosition.z + Z_OFFSET)/1024.0f );

	float2 TerrainCoord = WorldPosition.xz;
	TerrainCoord += 0.5;
	TerrainCoord /= 8.0;
	Out.vTerrainTexCoord  = TerrainCoord;

	Out.worldTexCoord = float2( WorldPosition.x/2048.0f, WorldPosition.z/1024.0f)+off;

	Out.vTexCoord0.w = v.vPosition.y; //height
	Out.General.x = 0.42*saturate(3*mul( float3(0,1,0) , (float4x3)WorldView).y -0.55); //zoom
	return Out;
}


float4 PixelShader_Map2_0( VS_OUTPUT v ) : COLOR
{
	float4 WorldColor = tex2D(ColorTexture, v.worldTexCoord.xy);  // World terrain
	float4 BaseColor = tex2D( BaseTexture, v.worldTexCoord*float2(120,60)); // local texture

	float4 TerrainColor = tex2D( MapTexture, v.vTerrainTexCoord );
	//float4 Color = tex2D( ColorTexture, v.vColorTexCoord );
	float4 Color = 1.0;
	
	// ********* Terrain

    if (WorldColor.g > 0.25) Color.rgb = float3(0.8,0.82,0.6); //plain
		else Color.rgb = float3( 0.86, 0.84, 0.7 ); //desert

	WorldColor.r += 0.3*BaseColor.a;
	if (WorldColor.r > 0.70+0.15) Color.rgb *= float3( 0.95,0.95,1.0); // mountains
	else if (WorldColor.r > 0.25+0.15) Color.rgb *= float3( 0.88, 0.84, 0.81);// hills

	if (WorldColor.g > 0.74) Color.rgb *= float3(0.7, 0.9, 0.6); // forest

	
	Color.rgb = lerp(Color.rgb, 1.0, 0.2*saturate(1-2*BaseColor.a));

	float TerrainAlpha = tex2D( TerrainAlphaTexture, v.vTexCoord0 ).a;
	TerrainColor = lerp(1,TerrainColor,TerrainAlpha);
	float4 OutColor = lerp(Color, Color*TerrainColor, v.General.x);

	float Winter = tex2D( WinterTexture, v.vProvinceIndexCoord ).x;
	if (Winter>0.1) Winter = 0.8*Winter+0.3;
	float WinterTarget = lerp(1.0, 0.05 + TerrainColor.r, (1-TerrainColor.a) * v.General.x);
	OutColor = lerp(OutColor, WinterTarget, Winter) * BaseColor*v.vTexCoord0.z;
	
	//OutColor.rgb *= v.vTexCoord0.z;
	OutColor.a = 1;
	//OutColor.rgb += Color.a*2;

	// 0.35 -- 0.4 is clear -- black
	if (v.vTexCoord0.w < 0.42) OutColor = lerp(OutColor, BLACK, saturate(20*(WorldColor.b - 0.35)) );


  // Terra incognita and FOW
  float TerraIncognita = tex2D( TerraIncognitaTextureTerrain, v.worldTexCoord ).g-0.25;
  float ti = saturate(TerraIncognita);
  float4 temp = float4(tex2D( TerraIncognitaTextureTerrain, v.worldTexCoord + offx).g,
  	tex2D( TerraIncognitaTextureTerrain, v.worldTexCoord - offx).g,
  	tex2D( TerraIncognitaTextureTerrain, v.worldTexCoord + offy).g,
  	tex2D( TerraIncognitaTextureTerrain, v.worldTexCoord - offy).g );
  float local =  saturate(3*(ti+dot(saturate(temp-0.25), float4(1,1,1,1)))) ;
  OutColor.rgb *= 1+FOWFACTOR*TerraIncognita;
  OutColor.rgb = lerp(OutColor.rgb, (1-FOWFACTOR*0.25)*(1-LIGHTSHIFT)*BaseColor.rgb, local);
  OutColor.rgb *= lerp(1.0,float3(1.0,0.95,0.9),ti);
 			
  return OutColor;
}


////////////////////////////////////////////////////////////////////////////////////////////////////////////


VS_OUTPUT_BEACH VertexShader_Beach(const VS_INPUT_BEACH v )
{
	VS_OUTPUT_BEACH Out = (VS_OUTPUT_BEACH)0;
	float4x4 WorldView = mul(WorldMatrix, ViewMatrix);
	float3 P = mul(v.vPosition, (float4x3)WorldView);
	Out.vPosition  = mul(float4(P, 1), ProjectionMatrix);

	float3 VertexNormal = mul( v.vNormal, WorldMatrix );

	float3 direction = normalize( LightDirection );
	Out.vLightIntensity.xy = max( dot( VertexNormal, -direction ), 0.5f )*LIGHTFACTOR-LIGHTSHIFT;
	Out.vLightIntensity.z = v.vPosition.y;

	Out.vProvinceIndexCoord = v.vProvinceIndexCoord;

	float4 WorldPosition = mul( v.vPosition, AbsoluteWorldMatrix );
	Out.vColorTexCoord.xy = float2( ( WorldPosition.x + X_OFFSET )/2048.0f, (WorldPosition.z + Z_OFFSET)/1024.0f );

	float2 TerrainCoord = WorldPosition.xz;
	TerrainCoord += 0.5;
	TerrainCoord /= 8.0;
	Out.vTerrainTexCoord  = TerrainCoord;

	Out.worldTexCoord = float2( WorldPosition.x/2048.0f, WorldPosition.z/1024.0f )+off;
	Out.vLightIntensity.y = 0.42*saturate(3*mul( float3(0,1,0) , (float4x3)WorldView).y -0.55); //zoom

	return Out;
}

float4 PixelShader_Beach( VS_OUTPUT_BEACH v ) : COLOR
{
	float4 WorldColor = tex2D(ColorTexture, v.worldTexCoord.xy);  // World terrain
	float4 BaseColor = tex2D( BaseTexture, v.worldTexCoord*float2(120,60)); // local texture

	float4 OutColor = 1.0;
        if (WorldColor.g > 0.25) OutColor.rgb = float3(0.8,0.82,0.6); //plain
		else OutColor.rgb = float3( 0.86, 0.84, 0.7 ); //desert

	WorldColor.r += 0.3*BaseColor.a;
	if (WorldColor.r > 0.70+0.15) OutColor.rgb *= float3( 0.95,0.95,1.0); // mountains
	else if (WorldColor.r > 0.25+0.15) OutColor.rgb *= float3( 0.88, 0.84, 0.81); // hills

	if (WorldColor.g > 0.74) OutColor.rgb *= float3(0.7, 0.9, 0.6); // forest
	
	
	
	OutColor.rgb = lerp(OutColor.rgb, 1.0, 0.2*saturate(1-2*BaseColor.a));


	float Winter = tex2D( WinterTexture, v.vProvinceIndexCoord ).x;
	OutColor.rgb = lerp(OutColor.rgb, 1.0, Winter) *BaseColor.rgb* v.vLightIntensity.x;

	
	// 0.35 -- 0.4 is clear -- black
	if (v.vLightIntensity.z < 0.42) OutColor = lerp(OutColor, BLACK, saturate(20*(WorldColor.b - 0.35)) );
 if (WorldColor.b *BaseColor.a > 0.17 && v.vLightIntensity.z < 0.45) OutColor.rgb *= float3(0.6,0.6,1.0);


  // Terra incognita and FOW
  float TerraIncognita = tex2D( TerraIncognitaTextureTerrain, v.worldTexCoord ).g-0.25;
  float ti = saturate(TerraIncognita);
  float4 temp = float4(tex2D( TerraIncognitaTextureTerrain, v.worldTexCoord + offx).g,
  	tex2D( TerraIncognitaTextureTerrain, v.worldTexCoord - offx).g,
  	tex2D( TerraIncognitaTextureTerrain, v.worldTexCoord + offy).g,
  	tex2D( TerraIncognitaTextureTerrain, v.worldTexCoord - offy).g );
  float local =  saturate(3*(ti+dot(saturate(temp-0.25), float4(1,1,1,1)))) ;
  OutColor.rgb *= 1+FOWFACTOR*TerraIncognita;
  OutColor.rgb = lerp(OutColor.rgb, (1-FOWFACTOR*0.25)*(1-LIGHTSHIFT)*BaseColor.rgb, local);
  OutColor.rgb *= lerp(1.0,float3(1.0,0.95,0.9),ti);	

return OutColor;
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////


VS_OUTPUT_PTI VertexShader_PTI(const VS_INPUT_PTI v )
{
	VS_OUTPUT_PTI Out = (VS_OUTPUT_PTI)0;
	float4x4 WorldView = mul(WorldMatrix, ViewMatrix);
	float3 P = mul(v.vPosition, (float4x3)WorldView);
	Out.vPosition  = mul(float4(P, 1), ProjectionMatrix);
	float4 WorldPosition = mul( v.vPosition, AbsoluteWorldMatrix );
	Out.worldTexCoord = float2( WorldPosition.x/2048.0f, WorldPosition.z/1024.0f ) + off;
	return Out;
}

float4 PixelShader_PTI( VS_OUTPUT_PTI v ) : COLOR
{
	float4 BaseColor = tex2D( BaseTexture, v.worldTexCoord*float2(120,60)); // local texture
   return float4((1-FOWFACTOR*0.25)*(1-LIGHTSHIFT)*float3(1.0,0.9625,0.925)*BaseColor.rgb, 1.0 ); //0.6875
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////

VS_OUTPUT_TREE VertexShader_TREE(const VS_INPUT_TREE v )
{
	VS_OUTPUT_TREE Out = (VS_OUTPUT_TREE)0;
	float4x4 WorldView = mul(WorldMatrix, ViewMatrix);
	float3 P = mul(v.vPosition, (float4x3)WorldView);
	Out.vPosition  = mul(float4(P, 1), ProjectionMatrix);

	float4 WorldPosition = mul( v.vPosition, AbsoluteWorldMatrix );
	Out.vTexCoordTI = float2( WorldPosition.x/2048.0f, WorldPosition.z/1024.0f );

	Out.vTexCoord = v.vTexCoord;

	return Out;
}

float4 PixelShader_TREE( VS_OUTPUT_TREE v ) : COLOR
{
	float4 OutColor = tex2D( TreeTexture, v.vTexCoord );
	float vFOW = ( tex2D( TerraIncognitaTextureTree, v.vTexCoordTI ).g - 0.25 );
	if ( vFOW < 0 )
		OutColor.rgb *= 1+FOWFACTOR*vFOW;
	//OutColor.rgb *= 0.8;
	OutColor.a *= vAlpha;
	//OutColor.a *= 1.1; //n

	return OutColor;
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////

technique TerrainShader_Graphical
{
	pass p0
	{
				ALPHABLENDENABLE = True;
		ALPHATESTENABLE = True;

		VertexShader = compile vs_1_1 VertexShader_Map();
		PixelShader = compile ps_2_0 PixelShader_Map2_0();
	}
}

technique TerrainShader_General
{
	pass p0
	{
			ALPHABLENDENABLE = True;
		ALPHATESTENABLE = True;

		VertexShader = compile vs_1_1 VertexShader_Map_General();
		PixelShader = compile ps_2_0 PixelShader_Map2_0_General();
	}
}

technique TerrainShader_Border
{
	pass p0
	{
		VertexShader = compile vs_1_1 VertexShader_Map_Border();
		PixelShader = compile ps_2_0 PixelShader_Map2_0_Border();
	}
}


technique BeachShader_Graphical
{
	pass p0
	{
		VertexShader = compile vs_1_1 VertexShader_Beach();
		PixelShader = compile ps_2_0 PixelShader_Beach();
	}
}


technique BeachShader_General
{
	pass p0
	{
		VertexShader = compile vs_1_1 VertexShader_Beach_General();
		PixelShader = compile ps_2_0 PixelShader_Beach_General();
	}
}


technique PTIShader
{
	pass p0
	{
		fvf = XYZ;

		LightEnable[0] = false;
		Lighting = False;

		ALPHABLENDENABLE = True;

		ColorOp[0] = Modulate;
		ColorArg1[0] = Texture;
		ColorArg2[0] = current;
  
		ColorOp[1] = Disable;
		AlphaOp[1] = Disable;

		VertexShader = compile vs_1_1 VertexShader_PTI();
		PixelShader = compile ps_2_0 PixelShader_PTI();
	}
}

technique TreeShader
{
	pass p0
	{
		ALPHABLENDENABLE = True;
		ALPHATESTENABLE = True;

		VertexShader = compile vs_1_1 VertexShader_TREE();
		PixelShader = compile ps_2_0 PixelShader_TREE();
	}
}
