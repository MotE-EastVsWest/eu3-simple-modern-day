// If you don't like the dots in the sea,
// change the line below to
// #define BLUE float4(0.7,0.7,1.0,1.0)
#define BLUE float4(0.6,0.6,1.0,1.0)
#define CLEAR float4(0.0, 0.0, 0.0, 0.0)
#define BLACK float4(0.0,0.0,0.0,1.0)
#define HALFBLUE float4(0.7,0.7,1.0,1.0)

#define TAN float4(0.85,0.7, 0.4, 1.0)
#define DUST float4(0.85,0.7,0.4, 1.0)
#define LIGHTFACTOR 1.1
#define LIGHTSHIFT 0.1
#define FOWFACTOR 1.25

texture tex0 < string name = "Base.tga"; >;	// Base texture
texture tex1 < string name = "Base.tga"; >;	// Base texture
texture tex2 < string name = "Base.tga"; >;	// Base texture
texture tex3 < string name = "Base.tga"; >;	// Base texture
texture tex4 < string name = "Base.tga"; >;	// Base texture

float4x4 WorldViewProjectionMatrix; 

float3	LightPosition;
float3	CameraPosition;
float	Time;
float	vAlpha;

sampler BaseTexture  =
sampler_state
{
    Texture = <tex0>;
    MinFilter = Linear; //Point;
    MagFilter = Point;
    MipFilter = Linear;
    AddressU = Wrap;
    AddressV = Wrap;
//    AddressW = Wrap;
};



sampler TheBackgroundTexture  =
sampler_state
{
    Texture = <tex1>;
    MinFilter = Linear; //Point;
    MagFilter = Point;
    MipFilter = Linear;
    AddressU = Wrap;
    AddressV = Wrap;
};


sampler TerraIncognitaFiltered  =
sampler_state
{
    Texture = <tex2>;
    MinFilter = Linear;
    MagFilter = Linear;
    MipFilter = None;
    AddressU = Clamp;
    AddressV = Clamp;
};

sampler WorldColorTex  =
sampler_state
{
    Texture = <tex3>;
    MinFilter = Linear;
    MagFilter = Linear;
    MipFilter = None;
    AddressU = Wrap;
    AddressV = Wrap;
};

sampler NormalMap  =   //water.dds
sampler_state
{
    Texture = <tex4>;
    MinFilter = Linear; //Point;
    MagFilter = Linear; //Point; //n
    MipFilter = None; //Linear;  //n
    AddressU = Wrap;
    AddressV = Wrap;
};


struct VS_INPUT_WATER
{
    float3 position			: POSITION;
    float4 texCoord0			: TEXCOORD0;
    float4 texCoord1			: TEXCOORD1;
};

struct VS_OUTPUT_WATER
{
    float4 position		: POSITION;
    float4 texCoord0		: TEXCOORD0;

    float3 eyeDirection		: TEXCOORD1;
    float3 lightDirection	: TEXCOORD2;
    float2 localTexCoord	: TEXCOORD4;
    float4 WorldTexture		: TEXCOORD3;
    float2 WorldTextureTI       : TEXCOORD5;
};

const float3 offY = float3(0.11, 0.74, 0.43);
const float3 offZ = float3(0.47, 0.19, 0.78);

const float2 off = float2(0.25/1024, 0.25/512);
const float2 offx = float2(0.5/1024, 0.0/512);
const float2 offy = float2(-0.0/1024, 0.5/512);

////////////////////////////////////////////////////////////////////////////////////////////////////////////

#define X_OFFSET 0.5
#define Z_OFFSET 0.5

float	ColorMapHeight;
float	ColorMapWidth;

float	ColorMapTextureHeight;
float	ColorMapTextureWidth;

float	MapWidth;
float	MapHeight;

float	BorderWidth;
float	BorderHeight;


VS_OUTPUT_WATER VertexShader_Water_2_0(const VS_INPUT_WATER IN )
{
    VS_OUTPUT_WATER OUT = (VS_OUTPUT_WATER)0;
    float4 position = mul( float4(IN.position.xyz , 1.0) , WorldViewProjectionMatrix );
    OUT.position = position;
    OUT.texCoord0 = IN.texCoord0;
    OUT.texCoord0.z = OUT.position.w;

    float WorldPositionX = ( ColorMapWidth * IN.texCoord1.x ) / MapWidth;
    float WorldPositionY = ( ColorMapHeight * IN.texCoord1.y ) / MapHeight;

    OUT.WorldTexture.xy = float2( ( WorldPositionX + X_OFFSET)/ColorMapTextureWidth, (WorldPositionY + Z_OFFSET)/ColorMapTextureHeight );
    OUT.WorldTextureTI	= float2( ( IN.texCoord1.x + 0.25)/BorderWidth, (IN.texCoord1.y + 0.25)/BorderHeight );
    OUT.localTexCoord.xy = OUT.WorldTexture.xy*float2(120,60);

   return OUT;
}


float4 PixelShader_Water_2_0( VS_OUTPUT_WATER IN ) : COLOR
{
  float4 WorldColor = tex2D( WorldColorTex, IN.WorldTexture );

  float4 paper = tex2D(BaseTexture, IN.localTexCoord.xy); //noise-2d (paper, .a is noise)
  float4 noise = tex2D(NormalMap, IN.texCoord0.xy);
  float4 seaTexture = tex2D(TheBackgroundTexture, IN.texCoord0.xy);

  // OUTLINE AT COAST
  // 0.3 -- 0.35 is clear -- black
  float4 OutColor = lerp(CLEAR, BLACK, saturate(20*(WorldColor.b - 0.3)) );
  // 0.4 -- 0.45? is black -- paper texture
  OutColor = lerp(OutColor, float4(paper.rgb, 1.0), saturate(30*(WorldColor.b - 0.4+0.05-0.1*noise.g)) );
	  

  // HATCHING  
  float row = frac(8*IN.texCoord0.z);
  float coast = WorldColor.b - 0.1*noise.g +0.05;
  float ink = 0.5*saturate (abs(4*row-2) - 5*(coast-0.4))*saturate(10*(0.61-coast));
  OutColor.rgb *= (1-ink)*(1.0-LIGHTSHIFT); //correction


  // Water colours
  if (WorldColor.b < 0.6 + 0.15*noise.r) OutColor.rgb *= float3(0.6,0.6,1.0).rgb;
    else OutColor.rgb *=lerp (HALFBLUE.rgb, BLUE.rgb, seaTexture.r*saturate (25*WorldColor.b - 18));
    
  // Terra incognita
  float TerraIncognita = (tex2D( TerraIncognitaFiltered, IN.WorldTextureTI ).g - 0.25);
  float ti = saturate(TerraIncognita);
  float4 temp = float4(tex2D( TerraIncognitaFiltered, IN.WorldTextureTI + offx).g,
   tex2D( TerraIncognitaFiltered, IN.WorldTextureTI - offx).g,
   tex2D( TerraIncognitaFiltered, IN.WorldTextureTI + offy).g,
   tex2D( TerraIncognitaFiltered, IN.WorldTextureTI - offy).g );
  float local = saturate(3*(ti+dot(saturate(temp-0.25), float4(1,1,1,1))));
  OutColor.rgb *= 1+FOWFACTOR*TerraIncognita;
  OutColor.rgba = lerp(OutColor.rgba, float4((1-FOWFACTOR*0.25)*(1-LIGHTSHIFT)*paper.rgb,1.0), local);
  OutColor.rgb *= lerp(1.0,float3(1.0,0.95,0.9),ti);
  OutColor.rgb *= 1- ink*ti*(0.75-ti);
  return OutColor;
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////

VS_OUTPUT_WATER VertexShader_Water_1_1(const VS_INPUT_WATER IN )
{
    VS_OUTPUT_WATER OUT = (VS_OUTPUT_WATER)0;
    float4 position = mul( float4(IN.position.xyz , 1.0) , WorldViewProjectionMatrix );
    OUT.position = position;

    OUT.texCoord0 = float4 (IN.texCoord1 + off,0,0);

  // OUT.WorldTexture = float4 (IN.texCoord1 + off,0,0);


    return OUT;
}

float4 PixelShader_Water_1_1( VS_OUTPUT_WATER IN ) : COLOR
{
 float4 OutColor = float4(0.7, 0.7, 0.95, 1);

    //float4 WorldColor = tex2D( WorldColorTex, IN.WorldTexture.xy  );

  	//OutColor.rgb = lerp(OutColor, float3(0.22, 0.22, 0.4), WorldColor.r);
		
	OutColor.rgb += tex2D( TerraIncognitaFiltered, IN.texCoord0.xy ).g - 0.25;


	//OutColor += WorldColor.b;
	return OutColor;
}


////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Hambut_Bulge, 30/05/2008:
// Paradox use two seperate vertex shaders, WaterShader_2_0 for the terrain map mode, and WaterShader_1_1 for all others.
// Since I couldn't get WaterShader_1_1 to work with TOT, I've rerouted other modes down the terrain route. Seems to work!

technique WaterShader_2_0
{
	pass p0
	{
		ALPHABLENDENABLE = True;
		ALPHATESTENABLE = True;

		VertexShader = compile vs_2_0 VertexShader_Water_2_0();
		PixelShader = compile ps_2_0 PixelShader_Water_2_0();
	}
}

technique WaterShader_1_1
{
	pass p0
	{
		ALPHABLENDENABLE = True;
		ALPHATESTENABLE = True;

//		VertexShader = compile vs_1_1 VertexShader_Water_1_1();
//		PixelShader = compile ps_2_0 PixelShader_Water_1_1();
		VertexShader = compile vs_2_0 VertexShader_Water_2_0();
		PixelShader = compile ps_2_0 PixelShader_Water_2_0();
	}
}
