government = uncivilized_gov			
aristocracy_plutocracy =	0		
centralization_decentralization = 	5	
innovative_narrowminded = 	3		
mercantilism_freetrade = 	-3		
offensive_defensive = 	2		
land_naval = 	2		
quality_quantity = 	1			
serfdom_freesubjects = 	-5		
primary_culture = emirati_nationalist			
#OLDREL religion =
religion = conservatism_r
#religion =  nationalist_r			
technology_group = muslim			
capital = 1846			

1810.1.1 = {
	monarch = {
		name = "Tahnun bin Shakhbut Al Nahyan"
		adm = 5
		dip = 6
		mil = 5
	}
}
