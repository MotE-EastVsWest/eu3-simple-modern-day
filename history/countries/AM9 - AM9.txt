# Country Name:	Armenia	# Tag:	AM9
government = 	dictatorship
govset = no		
primary_culture = 	armenia_culture		
#OLDREL religion =
religion = conservative_r
ideology_set = no
#religion =  	orthodox		
capital = 	419		
#			
aristocracy_plutocracy =	0		
centralization_decentralization = 	0		
innovative_narrowminded = 	0		
mercantilism_freetrade = 	0		
offensive_defensive = 	0		
land_naval = 	0		
quality_quantity = 	0		
serfdom_freesubjects = 	0		

technology_group = latin

1997.1.1 = {
	monarch = {
		name = "Robert Kocharyan"
		adm = 4
		mil = 4
		dip = 4
	}
}