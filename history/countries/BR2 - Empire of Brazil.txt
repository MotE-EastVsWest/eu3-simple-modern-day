government = dictatorship_gov				
aristocracy_plutocracy =	-4			
centralization_decentralization = 	-1			
innovative_narrowminded = 	2			
mercantilism_freetrade = 	-5			
offensive_defensive = 	3			
land_naval = 	-1			
quality_quantity = 	5			
serfdom_freesubjects = 	-4			
primary_culture = brazilian_nationalist				
#OLDREL religion =
religion = conservatism_r
#religion =  nationalist_r		
lowtax_hightax = 3
technology_group = latin				
capital = 763				


1828.1.1 = {
	monarch = {
		name = "Pedro I"
		adm = 6
		dip = 6
		mil = 3
	}
}
