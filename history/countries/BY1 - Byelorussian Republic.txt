#name: BY1 - Byelorussian Republic				
government = dictatorship_gov				
aristocracy_plutocracy =	0			
centralization_decentralization = 	-5			
innovative_narrowminded = 	4			
mercantilism_freetrade = 	-4			
offensive_defensive = 	0			
land_naval = 	-2			
quality_quantity = 	1			
serfdom_freesubjects = 	4			
primary_culture = byelorussian_democratic				
#OLDREL religion =
religion = conservatism_r
#religion =  conservative_r				
technology_group = eastern				
capital = 276				
