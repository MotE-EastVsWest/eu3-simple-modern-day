#name: CB1 - Republic of Cuba			
government = dictatorship_gov			
aristocracy_plutocracy =	-4		
centralization_decentralization = 	-3		
innovative_narrowminded = 	1		
mercantilism_freetrade = 	0		
offensive_defensive = 	0		
land_naval = 	1		
quality_quantity = 	1		
serfdom_freesubjects = 	1		
primary_culture = cuban_democratic			
#OLDREL religion =
religion = conservatism_r
#religion =  conservative_r			
technology_group = latin			
capital = 484			
