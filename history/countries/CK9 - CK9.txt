# Country Name:	Cook Islands	# Tag:	CK9
government = 	democracy
govset = no		
primary_culture = 	cook_islands_culture		
#OLDREL religion =
religion = conservative_r
ideology_set = no
#religion =  	protestant		
capital = 	2137		
#			
aristocracy_plutocracy =	0		
centralization_decentralization = 	0		
innovative_narrowminded = 	0		
mercantilism_freetrade = 	0		
offensive_defensive = 	0		
land_naval = 	0		
quality_quantity = 	0		
serfdom_freesubjects = 	0		

technology_group = latin

1997.1.1 = {
	monarch = {
		name = "Frederick Tutu Goodwin"
		adm = 4
		mil = 4
		dip = 4
	}
}