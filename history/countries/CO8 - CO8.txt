# Country Name:	Colombian Rebels	# Tag:	CO8
government = 	dictatorship
govset = no		
primary_culture = 	colombia_culture		
#OLDREL religion =
religion = socialist_r
ideology_set = no
#religion =  	catholic		
capital = 	825		
technology_group = latin			
aristocracy_plutocracy =	0		
centralization_decentralization = 	0		
innovative_narrowminded = 	0		
mercantilism_freetrade = 	0		
offensive_defensive = 	0		
land_naval = 	0		
quality_quantity = 	0		
serfdom_freesubjects = 	0		


1997.1.1 = {
	monarch = {
		name = "Manuel Velez"
		adm = 4
		mil = 4
		dip = 4
	}
}