#name: GT1 - Republic of Guatemala				
government = dictatorship_gov				
aristocracy_plutocracy =	-4			
centralization_decentralization = 	-1			
innovative_narrowminded = 	1			
mercantilism_freetrade = 	-4			
offensive_defensive = 	-1			
land_naval = 	3			
quality_quantity = 	4			
serfdom_freesubjects = 	-2	
lowtax_hightax = -1		
primary_culture = central-american_democratic				
#OLDREL religion =
religion = conservatism_r
#religion =  conservative_r				
technology_group = latin				
capital = 841				

1815.1.1 = {
	monarch = {
		name = "Jose Francisco Barrundia"
		adm = 5
		dip = 3
		mil = 4
	}
}