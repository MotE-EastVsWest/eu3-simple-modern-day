#name: EL1 - Republic of El Salvador				
government = dictatorship_gov				
aristocracy_plutocracy =	0			
centralization_decentralization = 	-4			
innovative_narrowminded = 	3			
mercantilism_freetrade = 	-4			
offensive_defensive = 	2			
land_naval = 	2			
quality_quantity = 	5			
serfdom_freesubjects = 	2			
primary_culture = salvadorian_democratic				
#OLDREL religion =
religion = conservatism_r
#religion =  conservative_r				
technology_group = latin				
capital = 839				
