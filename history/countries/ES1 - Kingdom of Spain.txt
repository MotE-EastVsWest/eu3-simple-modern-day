#name: ES1 - Kingdom of Spain				
government = dictatorship_gov				
aristocracy_plutocracy =	-1			
centralization_decentralization = 	-2			
innovative_narrowminded = 	3			
mercantilism_freetrade = 	-5			
offensive_defensive = 	-2			
land_naval = 	-1			
quality_quantity = 	1			
serfdom_freesubjects = 	4			
primary_culture = spanish_democratic				
#OLDREL religion =
religion = conservatism_r
#religion =  conservative_r				
technology_group = latin				
capital = 217				
