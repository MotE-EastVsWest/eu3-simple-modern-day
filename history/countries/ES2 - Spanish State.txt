government = dictatorship_gov				
aristocracy_plutocracy =	-1			
centralization_decentralization = 	-3			
innovative_narrowminded = 	3			
mercantilism_freetrade = 	-5			
offensive_defensive = 	-2			
land_naval = 	-1			
quality_quantity = 	1			
serfdom_freesubjects = 	-4			
primary_culture = spanish_nationalist				
#OLDREL religion =
religion = conservatism_r
#religion =  nationalist_r				
technology_group = latin				
capital = 217				

1820.1.1 = {
	monarch = {
		name = "Ferdinand VII"
		adm = 4
		dip = 5
		mil = 4
	}
}
