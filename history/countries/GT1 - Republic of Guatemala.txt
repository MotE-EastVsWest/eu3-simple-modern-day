#name: GT1 - Republic of Guatemala				
government = dictatorship_gov				
aristocracy_plutocracy =	-4			
centralization_decentralization = 	-4			
innovative_narrowminded = 	1			
mercantilism_freetrade = 	-4			
offensive_defensive = 	-1			
land_naval = 	3			
quality_quantity = 	4			
serfdom_freesubjects = 	2			
primary_culture = guatemalan_democratic				
#OLDREL religion =
religion = conservatism_r
#religion =  conservative_r				
technology_group = latin				
capital = 841				
