government = dictatorship_gov				
aristocracy_plutocracy =	-3			
centralization_decentralization = 	-3			
innovative_narrowminded = 	4			
mercantilism_freetrade = 	-5		
offensive_defensive = 	3			
land_naval = 	1			
quality_quantity = 	2			
serfdom_freesubjects = 	-3		
lowtax_hightax = 4
primary_culture = austrian_nationalist				
#OLDREL religion =
religion = conservatism_r
#religion =  nationalist_r				
technology_group = latin				
capital = 134				

1810.1.1 = {
	monarch = {
		name = "Francis II"
		adm = 5
		dip = 6
		mil = 5
	}
}
