#name: IC1 - Republic of Iceland				
government = democracy_gov					
aristocracy_plutocracy =	3				
centralization_decentralization = 	1			
innovative_narrowminded = 	-2			
mercantilism_freetrade = 	3				
offensive_defensive = 	0				
land_naval = 	0			
quality_quantity = 	-1				
serfdom_freesubjects = 	1			
primary_culture = icelandic_democratic				
#OLDREL religion =
religion = conservatism_r
#religion =  liberal_r				
technology_group = latin				
capital = 370				
