# Country Name:	Ivory Coast Rebels	# Tag:	IV8
government = 	dictatorship		
primary_culture = 	ivory_coast_culture		
#OLDREL religion =
religion = conservative_r
ideology_set = no
#religion =  	sunni		
capital = 	1126		
technology_group = latin			
aristocracy_plutocracy =	0		
centralization_decentralization = 	0		
innovative_narrowminded = 	0		
mercantilism_freetrade = 	0		
offensive_defensive = 	0		
land_naval = 	0		
quality_quantity = 	0		
serfdom_freesubjects = 	0		

1997.1.1 = {
	monarch = {
		name = "Guillaume Soro"
		adm = 4
		mil = 4
		dip = 4
	}
}