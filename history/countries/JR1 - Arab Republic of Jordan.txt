#name: JR1 - Arab Republic of Jordan				
government = uncivilized_gov				
aristocracy_plutocracy =	-5			
centralization_decentralization = 	4			
innovative_narrowminded = 	4			
mercantilism_freetrade = 	-4			
offensive_defensive = 	1			
land_naval = 	0			
quality_quantity = 	4			
serfdom_freesubjects = 	0			
primary_culture = jordanian_democratic				
#OLDREL religion =
religion = conservatism_r
#religion =  conservative_r				
technology_group = muslim				
capital = 381				
