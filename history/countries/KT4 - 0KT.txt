government = uncivilized_gov			
aristocracy_plutocracy =	-5			
centralization_decentralization = 	4			
innovative_narrowminded = 	3			
mercantilism_freetrade = 	-4			
offensive_defensive = 	0		
land_naval = 	0		
quality_quantity = 	5			
serfdom_freesubjects = -3		
lowtax_hightax = 2
primary_culture = kalati
#OLDREL religion =
religion = conservatism_r
#religion =  uncivilized_central_asian_1_r
technology_group = indian			
capital = 575			

1810.1.1 = {
	monarch = {
		name = "Khan Ahmadzai II"
		adm = 5
		dip = 6
		mil = 5
	}
}
