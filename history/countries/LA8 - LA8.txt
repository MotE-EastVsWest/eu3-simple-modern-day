# Country Name:	Somali Rebels	# Tag:	LA8
government = 	dictatorship
govset = no		
primary_culture = 	somalia_culture		
#OLDREL religion =
religion = reactionary_r
ideology_set = no
#religion =  	sunni		
capital = 	1204		
technology_group = latin			
aristocracy_plutocracy =	0		
centralization_decentralization = 	0		
innovative_narrowminded = 	0		
mercantilism_freetrade = 	0		
offensive_defensive = 	0		
land_naval = 	0		
quality_quantity = 	0		
serfdom_freesubjects = 	0		


1997.1.1 = {
	monarch = {
		name = "Hussein Mohamed Aidid"
		adm = 4
		mil = 4
		dip = 4
	}
}