#name LB1 - Republic of Liberia				
government = dictatorship_gov				
aristocracy_plutocracy =	-5			
centralization_decentralization = 	-5		
innovative_narrowminded = 	3			
mercantilism_freetrade = 	-5		
offensive_defensive = 	1			
land_naval = 	-3			
quality_quantity = 	1			
serfdom_freesubjects = 	-5	
lowtax_hightax = 2
primary_culture = liberian_democratic				
#OLDREL religion =
religion = conservatism_r
#religion =  conservative_r				
technology_group = latin				
capital = 1119				

1828.1.1 = {
	monarch = {
		name = "Jehudi Ashmun"
		adm = 4
		dip = 6
		mil = 3
	}
}