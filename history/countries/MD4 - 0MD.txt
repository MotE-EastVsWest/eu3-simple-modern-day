government = uncivilized_gov			
aristocracy_plutocracy =	-5			
centralization_decentralization = 	4			
innovative_narrowminded = 	3			
mercantilism_freetrade = 	-4			
offensive_defensive = 	0		
land_naval = 	0		
quality_quantity = 	5			
serfdom_freesubjects = -3		
lowtax_hightax = 3
primary_culture = maldivian	
#OLDREL religion =
religion = conservatism_r
#religion =  uncivilized_central_asian_1_r
technology_group = indian			
capital = 1248			

1810.1.1 = {
	monarch = {
		name = "Abdou al-Razman"
		adm = 5
		dip = 6
		mil = 5
	}
}
