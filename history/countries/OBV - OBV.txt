government = tribal_despotism
aristocracy_plutocracy =	0			
centralization_decentralization = 	0			
innovative_narrowminded = 	0			
mercantilism_freetrade = 	0			
offensive_defensive = 	0		
land_naval = 	0		
quality_quantity = 	0			
serfdom_freesubjects = 0		
primary_culture = observer
religion =  shinto				
technology_group = latin
capital = 2284


1810.1.1 = {
	monarch = {
		name = "OBSERVER"
		adm = 1
		dip = 1
		mil = 1
	}
}

2000.1.1 = {
	decision = starter_decision
}