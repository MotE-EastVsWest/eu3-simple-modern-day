government = dictatorship_gov			
aristocracy_plutocracy =	-5		
centralization_decentralization = 	-2		
innovative_narrowminded = 	5		
mercantilism_freetrade = 	-4		
offensive_defensive = 	-3		
land_naval = 	-1		
quality_quantity = 	2		
serfdom_freesubjects = 	-2		
primary_culture = parmese_nationalist			
#OLDREL religion =
religion = conservatism_r
#religion =  nationalist_r			
technology_group = latin			
capital = 105			

1810.1.1 = {
	monarch = {
		name = "Charles II"
		adm = 5
		dip = 6
		mil = 5
	}
}
