government = uncivilized_gov			
aristocracy_plutocracy =	-5			
centralization_decentralization = 	4			
innovative_narrowminded = 	3			
mercantilism_freetrade = 	-4			
offensive_defensive = 	0		
land_naval = 	0		
quality_quantity = 	5			
serfdom_freesubjects = -3		
lowtax_hightax = 2	
primary_culture = qatari	
#OLDREL religion =
religion = conservatism_r
#religion =  uncivilized_arab_2_r
technology_group = muslim				
capital = 395				

1810.1.1 = {
	monarch = {
		name = "Local Emir Council"
		adm = 5
		dip = 6
		mil = 5
	}
}
