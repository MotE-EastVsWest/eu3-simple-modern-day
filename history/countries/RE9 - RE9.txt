# Country Name:	Reunion	# Tag:	RE9
government = 	administrative_republic		
primary_culture = 	reunion_culture		
#OLDREL religion =
religion = conservatism_r
#religion =  	catholic		
capital = 	1103		
#			
aristocracy_plutocracy =	0		
centralization_decentralization = 	0		
innovative_narrowminded = 	0		
mercantilism_freetrade = 	0		
offensive_defensive = 	0		
land_naval = 	0		
quality_quantity = 	0		
serfdom_freesubjects = 	0		
technology_group = latin
