government = uncivilized_gov				
aristocracy_plutocracy =	-4			
centralization_decentralization = 	5		
innovative_narrowminded = 	4			
mercantilism_freetrade = 	-3			
offensive_defensive = 	2			
land_naval = 	-1			
quality_quantity = 	5			
serfdom_freesubjects = 	-5			
primary_culture = somali_nationalist				
#OLDREL religion =
religion = conservatism_r
#religion =  nationalist_r				
technology_group = muslim				
capital = 1205				

1810.1.1 = {
	monarch = {
		name = "Ali Umar il-Din"
		adm = 5
		dip = 6
		mil = 5
	}
}
