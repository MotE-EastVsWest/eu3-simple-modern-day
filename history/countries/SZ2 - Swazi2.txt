			
government = uncivilized_gov				
aristocracy_plutocracy =	-4				
centralization_decentralization = 	5			
innovative_narrowminded = 	4		
mercantilism_freetrade = 	-2				
offensive_defensive = 	0				
land_naval = 	0			
quality_quantity = 	4				
serfdom_freesubjects = 	-4			
primary_culture = swazi_nationalist		
#OLDREL religion =
religion = conservatism_r
#religion =  nationalist_r				
technology_group = african
capital = 2192

1810.1.1 = {
	monarch = {
		name = "Sobhuza I"
		adm = 5
		dip = 5
		mil = 5
	}
}
