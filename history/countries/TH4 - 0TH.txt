government = uncivilized_gov			
aristocracy_plutocracy =	-5			
centralization_decentralization = 	4			
innovative_narrowminded = 	3			
mercantilism_freetrade = 	-4			
offensive_defensive = 	0		
land_naval = 	0		
quality_quantity = 	5			
serfdom_freesubjects = -3		
lowtax_hightax = 4
primary_culture = siamese	
#OLDREL religion =
religion = conservatism_r
#religion =  uncivilized_east_asian_1_r
technology_group = chinese				
capital = 592				

1810.1.1 = {
	monarch = {
		name = "Rama III"
		adm = 5
		dip = 6
		mil = 5
	}
}
