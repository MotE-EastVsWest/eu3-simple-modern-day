# Country Name:	Turkmenistan	# Tag:	TK9
government = 	dictatorship
govset = no		
primary_culture = 	turkmenistan_culture		
#OLDREL religion =
religion = reactionary_r
ideology_set = no
#religion =  	sunni		
capital = 	445		
#			
aristocracy_plutocracy =	0		
centralization_decentralization = 	0		
innovative_narrowminded = 	0		
mercantilism_freetrade = 	0		
offensive_defensive = 	0		
land_naval = 	0		
quality_quantity = 	0		
serfdom_freesubjects = 	0		

technology_group = latin

1997.1.1 = {
	monarch = {
		name = "Saparmurat Niyazov"
		adm = 4
		mil = 4
		dip = 4
	}
}