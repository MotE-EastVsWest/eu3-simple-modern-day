#name: TP1 - Republic of Libya				
government = uncivilized_gov			
aristocracy_plutocracy =	-2		
centralization_decentralization = 	5
innovative_narrowminded = 	4		
mercantilism_freetrade = 	0		
offensive_defensive = 	1		
land_naval = 	-3		
quality_quantity = 	3			
serfdom_freesubjects = 	-5			
primary_culture = libyan_democratic				
#OLDREL religion =
religion = conservatism_r
#religion =  conservative_r				
technology_group = muslim				
capital = 354				
