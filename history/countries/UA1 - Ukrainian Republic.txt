#name: UA1 - Ukrainian Republic				
government = dictatorship_gov				
aristocracy_plutocracy =	-5			
centralization_decentralization = 	-4			
innovative_narrowminded = 	2			
mercantilism_freetrade = 	-4			
offensive_defensive = 	-1			
land_naval = 	-3			
quality_quantity = 	5			
serfdom_freesubjects = 	1			
primary_culture = ukrainian_democratic				
#OLDREL religion =
religion = conservatism_r
#religion =  conservative_r				
technology_group = eastern				
capital = 280				
