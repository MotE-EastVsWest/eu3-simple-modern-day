# Country Name:	Uganda	# Tag:	UN9
government = 	dictatorship
govset = no		
primary_culture = 	uganda_culture		
#OLDREL religion =
religion = reactionary_r
ideology_set = no
#religion =  	protestant		
capital = 	2032		
#			
aristocracy_plutocracy =	0		
centralization_decentralization = 	0		
innovative_narrowminded = 	0		
mercantilism_freetrade = 	0		
offensive_defensive = 	0		
land_naval = 	0		
quality_quantity = 	0		
serfdom_freesubjects = 	0		

technology_group = latin

1997.1.1 = {
	monarch = {
		name = "Yoweri Museveni"
		adm = 4
		mil = 4
		dip = 4
	}
}