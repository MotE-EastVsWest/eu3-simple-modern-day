government = uncivilized_gov				
aristocracy_plutocracy =	-3			
centralization_decentralization = 	4		
innovative_narrowminded = 	4			
mercantilism_freetrade = 	-3			
offensive_defensive = 	-1			
land_naval = 	1			
quality_quantity = 	1			
serfdom_freesubjects = 	-5		
lowtax_hightax = 3	
primary_culture = vietnamese
#OLDREL religion =
religion = conservatism_r
#religion =  uncivilized_east_asian_1_r		
technology_group = chinese				
capital = 610				

1810.1.1 = {
	monarch = {
		name = "Nguyen"
		adm = 5
		dip = 6
		mil = 5
	}
}
