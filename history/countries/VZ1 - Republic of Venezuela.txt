#name: VZ1 - Republic of Venezuela				
government = dictatorship_gov				
aristocracy_plutocracy =	-5			
centralization_decentralization = 	-5			
innovative_narrowminded = 	0			
mercantilism_freetrade = 	-4			
offensive_defensive = 	-2			
land_naval = 	0			
quality_quantity = 	3			
serfdom_freesubjects = 	-5			
primary_culture = venezuelan_democratic				
#OLDREL religion =
religion = conservatism_r
#religion =  liberal_r				
technology_group = latin				
capital = 831				

1815.1.1 = {
	monarch = {
		name = "Simon Bolivar"
		adm = 9
		dip = 8
		mil = 9
	}
}
