government = uncivilized_gov			
aristocracy_plutocracy =	-2		
centralization_decentralization = 	5	
innovative_narrowminded = 	5		
mercantilism_freetrade = 	-3		
offensive_defensive = 	3		
land_naval = 	3		
quality_quantity = 	0			
serfdom_freesubjects = 	-5		
lowtax_hightax = 3	
primary_culture = yemeni
#OLDREL religion =
religion = conservatism_r
#religion =  uncivilized_arab_1_r
technology_group = muslim			
capital = 390			

1810.1.1 = {
	monarch = {
		name = "Yemeni Tribal Council"
		adm = 5
		dip = 6
		mil = 5
	}
}
