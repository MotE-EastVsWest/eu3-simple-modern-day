# 1016 - Shikoku
culture = japan_culture
#OLDREL religion =
religion = protestant
#religion =  shinto
discovered_by = HAT
discovered_by = latin
discovered_by = eastern
discovered_by = muslim
discovered_by = indian
discovered_by = chinese
capital = "Matsuyama"
terrain = mountain
trade_goods = grain
citysize = 189872
manpower = 1.9
base_tax = 5
smd_prepped = no
hre = no
# dev level = 	Developed
temple = yes
workshop = yes
regimental_camp = yes
constable = yes
courthouse = yes
marketplace = yes
customs_house = no
fort1 = yes
fort2 = yes
fort3 = no
fort4 = no
fort5 = no
fort6 = no
################################################################################
# Anything typed here will be added to all selected province history files
################################################################################
2000.1.1 = { owner = JP9 controller = JP9 add_core = JP9 } 