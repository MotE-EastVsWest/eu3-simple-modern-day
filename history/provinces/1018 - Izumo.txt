# 1018 - Izumo
culture = japan_culture
#OLDREL religion =
religion = protestant
#religion =  buddhism
discovered_by = HAT
discovered_by = latin
discovered_by = eastern
discovered_by = muslim
discovered_by = indian
discovered_by = chinese
capital = "Matsue"
terrain = forested_plain
trade_goods = iron
citysize = 131705
manpower = 1.3
base_tax = 4
smd_prepped = no
hre = no
# dev level = 	Developed
temple = yes
workshop = no
regimental_camp = no
constable = no
courthouse = no
marketplace = yes
customs_house = no
fort1 = yes
fort2 = yes
fort3 = no
fort4 = no
fort5 = no
fort6 = no
################################################################################
# Anything typed here will be added to all selected province history files
################################################################################
2000.1.1 = { owner = JP9 controller = JP9 add_core = JP9 } 