# 103 - Piedmont
culture = italy_culture
#OLDREL religion =
religion = protestant
#religion =  catholic
trade_goods = wine
discovered_by = HAT
discovered_by = latin
discovered_by = eastern
discovered_by = muslim
discovered_by = indian
discovered_by = chinese
capital = "Torino"
citysize = 	442223
manpower = 	4.4
base_tax = 	8
smd_prepped = 	no
hre = 	no
#dev level = 	Developed
temple = 	yes
workshop = 	yes
regimental_camp = 	yes
constable = 	yes
courthouse = 	yes
marketplace = 	yes
customs_house = 	yes
fort1 = 	yes
fort2 = 	yes
fort3 = 	yes
fort4 = 	no
fort5 = 	no
fort6 = 	no
#capital = 	"

################################################################################
# Anything typed here will be added to all selected province history files
################################################################################
2000.1.1 = { owner = IT9 controller = IT9 add_core = IT9 } 