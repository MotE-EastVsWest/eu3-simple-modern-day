# 1041 - Gizhiga
trade_goods = fur
culture = russia_culture
#OLDREL religion =
religion = protestant
#religion =  orthodox
discovered_by = HAT
discovered_by = latin
discovered_by = eastern
discovered_by = muslim
discovered_by = indian
discovered_by = chinese
capital = "Gizhiga"
citysize = 1500
manpower = 0.1
base_tax = 1
smd_prepped = no
hre = no
# dev level = 	Underdeveloped
temple = no
workshop = no
regimental_camp = no
constable = no
courthouse = no
marketplace = no
customs_house = no
fort1 = no
fort2 = no
fort3 = no
fort4 = no
fort5 = no
fort6 = no
################################################################################
# Anything typed here will be added to all selected province history files
################################################################################
2000.1.1 = { owner = RU9 controller = RU9 add_core = RU9 } 