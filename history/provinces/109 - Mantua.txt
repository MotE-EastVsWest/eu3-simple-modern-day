# 109 - Mantua
trade_goods = grain
culture = italy_culture
#OLDREL religion =
religion = protestant
#religion =  catholic
discovered_by = HAT
discovered_by = latin
discovered_by = eastern
discovered_by = muslim
discovered_by = indian
discovered_by = chinese
capital = "Verona"
citysize = 	184729
manpower = 	1.8
base_tax = 	5
smd_prepped = 	no
hre = 	no
#dev level = 	Developed
temple = 	yes
workshop = 	yes
regimental_camp = 	yes
constable = 	yes
courthouse = 	yes
marketplace = 	yes
customs_house = 	no
fort1 = 	yes
fort2 = 	yes
fort3 = 	no
fort4 = 	no
fort5 = 	no
fort6 = 	no
#capital = 	"

################################################################################
# Anything typed here will be added to all selected province history files
################################################################################
2000.1.1 = { owner = IT9 controller = IT9 add_core = IT9 } 