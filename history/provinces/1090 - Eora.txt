# 1090 - Eora
culture = australia_culture
#OLDREL religion =
religion = protestant
#religion =  protestant
discovered_by = HAT
discovered_by = latin
discovered_by = eastern
discovered_by = muslim
discovered_by = indian
discovered_by = chinese
capital = "Canberra"
terrain = mountain
trade_goods = naval_supplies
citysize = 131338
manpower = 1.3
base_tax = 4
smd_prepped = no
hre = no
# dev level = 	Developed
temple = yes
workshop = no
regimental_camp = no
constable = no
courthouse = no
marketplace = yes
customs_house = no
fort1 = yes
fort2 = yes
fort3 = no
fort4 = no
fort5 = no
fort6 = no
################################################################################
# Anything typed here will be added to all selected province history files
################################################################################
2000.1.1 = { owner = AS9 controller = AS9 add_core = AS9 } 