
#1102 - Mauritius



culture = mauritius_culture
religion = protestant


trade_goods = fish    
discovered_by = HAT 
discovered_by = latin 
discovered_by = eastern 
discovered_by = muslim 
discovered_by = indian 
discovered_by = chinese


terrain = steppe

capital = "Diego Garcia"


citysize = 	1100
manpower = 	0.1
base_tax = 	1
smd_prepped = 	no
hre = 	no
#dev level = 	Developing
temple = 	no
workshop = 	no
regimental_camp = 	no
constable = 	no
courthouse = 	no
marketplace = 	no
customs_house = 	no
fort1 = 	no
fort2 = 	no
fort3 = 	no
fort4 = 	no
fort5 = 	no
fort6 = 	no

2000.1.1 = {
	owner = EN9
	controller = EN9
	add_core = RI9
	add_core = EN9
}