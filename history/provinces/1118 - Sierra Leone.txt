#1118 - Sierra Leone



culture = sierra-leone_culture   
#OLDREL religion =
religion = protestant
#religion =  protestant 
trade_goods = naval_supplies

discovered_by = HAT 
discovered_by = latin 
discovered_by = eastern 
discovered_by = muslim 
discovered_by = indian 
discovered_by = chinese

capital = "Freetown"



terrain = forested_plain



citysize = 	300047
manpower = 	3
base_tax = 	3
smd_prepped = 	no
hre = 	no
#dev level = 	Underdeveloped
temple = 	no
workshop = 	no
regimental_camp = 	no
constable = 	no
courthouse = 	no
marketplace = 	no
customs_house = 	no
fort1 = 	yes
fort2 = 	no
fort3 = 	no
fort4 = 	no
fort5 = 	no
fort6 = 	no

2000.1.1 = {
	owner = LO9
	controller = LO9
	add_core = LO9
	add_core = LO8
	revolt = {
		type = gang_rebels
		size = 1
	}
}