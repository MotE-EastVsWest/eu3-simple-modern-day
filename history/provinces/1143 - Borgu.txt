#1143 - Borgu



culture = arewa_culture 
#OLDREL religion =
religion = conservative_r
#religion =  sunni

trade_goods = grain   
discovered_by = HAT 
discovered_by = latin 
discovered_by = eastern 
discovered_by = muslim 
discovered_by = indian 
discovered_by = chinese

capital = "Kaiama"

terrain = forested_plain



citysize = 	24833
manpower = 	0.2
base_tax = 	2
smd_prepped = 	no
hre = 	no
#dev level = 	Underdeveloped
temple = 	no
workshop = 	no
regimental_camp = 	no
constable = 	no
courthouse = 	no
marketplace = 	no
customs_house = 	no
fort1 = 	no
fort2 = 	no
fort3 = 	no
fort4 = 	no
fort5 = 	no
fort6 = 	no


2000.1.1 = {
	owner = NI9
	controller = NI9
	add_core = AW9
	add_core = NI9
}