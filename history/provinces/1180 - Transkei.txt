#1180 - Transkei


culture = south_africa_pluralist_culture 
#OLDREL religion =
religion = protestant
#religion =  protestant   
trade_goods =fish

discovered_by = HAT 
discovered_by = latin 
discovered_by = eastern 
discovered_by = muslim 
discovered_by = indian 
discovered_by = chinese

capital = "East London"

citysize = 	151040
manpower = 	1.5
base_tax = 	3
smd_prepped = 	no
hre = 	no
#dev level = 	Developing
temple = 	yes
workshop = 	no
regimental_camp = 	no
constable = 	no
courthouse = 	no
marketplace = 	no
customs_house = 	no
fort1 = 	yes
fort2 = 	no
fort3 = 	no
fort4 = 	no
fort5 = 	no
fort6 = 	no


2000.1.1 = {
	owner = SF9
	controller = SF9
	add_core = SF9
}