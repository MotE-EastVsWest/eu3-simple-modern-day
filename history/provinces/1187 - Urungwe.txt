#1187 - Urungwe


culture = zimbabwe_culture  
#OLDREL religion =
religion = socialist_r
#religion =  protestant   

discovered_by = HAT 
discovered_by = latin 
discovered_by = eastern 
discovered_by = muslim 
discovered_by = indian 
discovered_by = chinese

capital = "Hwange"
#citysize = 15000


trade_goods =ivory

citysize = 	4260
manpower = 	0.1
base_tax = 	1
smd_prepped = 	no
hre = 	no
#dev level = 	Underdeveloped
temple = 	no
workshop = 	no
regimental_camp = 	no
constable = 	no
courthouse = 	no
marketplace = 	no
customs_house = 	no
fort1 = 	no
fort2 = 	no
fort3 = 	no
fort4 = 	no
fort5 = 	no
fort6 = 	no

2000.1.1 = {
	owner = ZB9
	controller = ZB9
	add_core = ZB9
}