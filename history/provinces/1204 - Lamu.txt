#1204 - Lamu



culture = kenya_culture    
#OLDREL religion =
religion = protestant
#religion =  protestant   

discovered_by = HAT 
discovered_by = latin 
discovered_by = eastern 
discovered_by = muslim 
discovered_by = indian 
discovered_by = chinese

capital = "Lamu"
#citysize = 16000


terrain = steppe

trade_goods =ivory

citysize = 	28784
manpower = 	0.3
base_tax = 	2
smd_prepped = 	no
hre = 	no
#dev level = 	Underdeveloped
temple = 	no
workshop = 	no
regimental_camp = 	no
constable = 	no
courthouse = 	no
marketplace = 	no
customs_house = 	no
fort1 = 	no
fort2 = 	no
fort3 = 	no
fort4 = 	no
fort5 = 	no
fort6 = 	no


2000.1.1 = {
	owner = KN9
	controller = KN9
	add_core = KN9
}