#1221 - Taqali


culture = sudan_culture 
#OLDREL religion =
religion = protestant
#religion =  sunni
trade_goods =grain


discovered_by = HAT 
discovered_by = latin 
discovered_by = eastern 
discovered_by = muslim 
discovered_by = indian 
discovered_by = chinese

capital = "Khartoum"


terrain = steppe



citysize = 	990000
manpower = 	9.9
base_tax = 	6
smd_prepped = 	no
hre = 	no
#dev level = 	Underdeveloped
temple = 	yes
workshop = 	yes
regimental_camp = 	no
constable = 	no
courthouse = 	yes
marketplace = 	yes
customs_house = 	no
fort1 = 	yes
fort2 = 	yes
fort3 = 	no
fort4 = 	no
fort5 = 	no
fort6 = 	no

cot = yes


2000.1.1 = {
	owner = SU9
	controller = SU9
	add_core = SU9
	add_core = SU8
}