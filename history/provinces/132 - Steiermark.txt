# 132 - Steiermark
culture = austria_culture
#OLDREL religion =
religion = protestant
#religion =  catholic
trade_goods = grain
discovered_by = HAT
discovered_by = latin
discovered_by = eastern
discovered_by = muslim
discovered_by = indian
discovered_by = chinese
capital = "Graz"
citysize = 	180000
manpower = 	1.8
base_tax = 	5
smd_prepped = 	no
hre = 	no
#dev level = 	Developed
temple = 	yes
workshop = 	yes
regimental_camp = 	yes
constable = 	yes
courthouse = 	yes
marketplace = 	yes
customs_house = 	no
fort1 = 	yes
fort2 = 	yes
fort3 = 	no
fort4 = 	no
fort5 = 	no
fort6 = 	no
pop_rework = no	


################################################################################
# Anything typed here will be added to all selected province history files
################################################################################
2000.1.1 = { owner = AT9 controller = AT9 add_core = AT9 } 