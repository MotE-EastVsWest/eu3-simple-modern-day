#134 - Wien



culture = austria_culture 
#OLDREL religion =
religion = protestant
#religion =  catholic  
trade_goods =wine 


terrain = forested_plain

discovered_by = HAT 
discovered_by = latin 
discovered_by = eastern 
discovered_by = muslim 
discovered_by = indian 
discovered_by = chinese

capital = "Vienna" 

citysize = 	578120
manpower = 	5.8
base_tax = 	9
smd_prepped = 	no
hre = 	no
#dev level = 	Developed
temple = 	yes
workshop = 	yes
regimental_camp = 	yes
constable = 	yes
courthouse = 	yes
marketplace = 	yes
customs_house = 	yes
fort1 = 	yes
fort2 = 	yes
fort3 = 	yes
fort4 = 	no
fort5 = 	no
fort6 = 	no
pop_rework = no	



2000.1.1 = {
	owner = AT9
	controller = AT9
	add_core = AT9
}