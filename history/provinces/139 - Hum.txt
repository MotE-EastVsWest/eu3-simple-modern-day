#139 - Hum



culture = bosnia_croat_culture
#OLDREL religion =
religion = protestant
#religion =  catholic  

discovered_by = HAT 
discovered_by = latin 
discovered_by = eastern 
discovered_by = muslim 
discovered_by = indian 
discovered_by = chinese

capital = "Mostar"	# Unofficial capital of Herzegovina


terrain = forested_plain

trade_goods =wool
citysize = 	44401
manpower = 	0.4
base_tax = 	2
smd_prepped = 	no
hre = 	no
#dev level = 	Developing
temple = 	no
workshop = 	no
regimental_camp = 	no
constable = 	no
courthouse = 	no
marketplace = 	no
customs_house = 	no
fort1 = 	yes
fort2 = 	no
fort3 = 	no
fort4 = 	no
fort5 = 	no
fort6 = 	no
#capital = 	"


2000.1.1 = {
	owner = HZ9
	controller = HZ9
	add_core = HZ9
	add_core = BO9
	add_core = SP9
}