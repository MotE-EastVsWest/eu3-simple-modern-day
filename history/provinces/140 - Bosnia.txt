#140 - Bosnia



culture = bosnia_bosniak_culture
#OLDREL religion =
religion = protestant
#religion =  sunni  


terrain = mountain

discovered_by = HAT 
discovered_by = latin 
discovered_by = eastern 
discovered_by = muslim 
discovered_by = indian 
discovered_by = chinese

capital = "Sarajevo"

trade_goods =wool
citysize = 	111042
manpower = 	1.1
base_tax = 	3
smd_prepped = 	no
hre = 	no
#dev level = 	Developing
temple = 	yes
workshop = 	no
regimental_camp = 	no
constable = 	no
courthouse = 	no
marketplace = 	yes
customs_house = 	no
fort1 = 	yes
fort2 = 	no
fort3 = 	no
fort4 = 	no
fort5 = 	no
fort6 = 	no

2000.1.1 = {
	owner = BO9
	controller = BO9
	add_core = BO9
	add_core = SP9
	add_core = HZ9
}
