#143 - Albania


culture = albania_culture 
#OLDREL religion =
religion = protestant
#religion =  sunni  

discovered_by = HAT 
discovered_by = latin 
discovered_by = eastern 
discovered_by = muslim 
discovered_by = indian 
discovered_by = chinese

capital = "Tirana"


terrain = forested_plain

trade_goods =wool

citysize = 	209038
manpower = 	2.1
base_tax = 	4
smd_prepped = 	no
hre = 	no
#dev level = 	Developing
temple = 	yes
workshop = 	no
regimental_camp = 	no
constable = 	no
courthouse = 	no
marketplace = 	no
customs_house = 	no
fort1 = 	yes
fort2 = 	yes
fort3 = 	no
fort4 = 	no
fort5 = 	no
fort6 = 	no
pop_rework = no	



2000.1.1 = {
	owner = AL9
	controller = AL9
	add_core = AL9
}