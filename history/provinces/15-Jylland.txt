#Jylland, incl. Aarhus, Aalborg, Viborg and Kolding

trade_goods =grain


culture = denmark_culture
#OLDREL religion =
religion = protestant
#religion =  protestant  
discovered_by = HAT 
discovered_by = latin 
discovered_by = eastern 
discovered_by = muslim 
discovered_by = indian 
discovered_by = chinese

capital = "Aarhus"

citysize = 	180000
manpower = 	1.8
base_tax = 	5
smd_prepped = 	no
hre = 	no
#dev level = 	Developed
temple = 	yes
workshop = 	yes
regimental_camp = 	yes
constable = 	yes
courthouse = 	yes
marketplace = 	yes
customs_house = 	no
fort1 = 	yes
fort2 = 	yes
fort3 = 	no
fort4 = 	no
fort5 = 	no
fort6 = 	no
#capital = 	"


2000.1.1 = {
	owner = DK9
	controller = DK9
	add_core = DK9
}