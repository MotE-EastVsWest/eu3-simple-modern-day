# 155 - Szolnok
culture = hungary_culture
#OLDREL religion =
religion = protestant
#religion =  reformed
discovered_by = HAT
discovered_by = latin
discovered_by = eastern
discovered_by = muslim
discovered_by = indian
discovered_by = chinese
capital = "Szeged"
terrain = forested_plain
trade_goods = naval_supplies
citysize = 	266901
manpower = 	2.7
base_tax = 	7
smd_prepped = 	no
hre = 	no
#dev level = 	Developed
temple = 	yes
workshop = 	yes
regimental_camp = 	yes
constable = 	yes
courthouse = 	yes
marketplace = 	yes
customs_house = 	no
fort1 = 	yes
fort2 = 	yes
fort3 = 	yes
fort4 = 	no
fort5 = 	no
fort6 = 	no
pop_rework = no	


################################################################################
# Anything typed here will be added to all selected province history files
################################################################################
2000.1.1 = { owner = HG9 controller = HG9 add_core = HG9 } 