#Bohusl�n, contains Marstrand, Uddevalla, Str�mstad.


trade_goods =grain

culture = sweden_culture
#OLDREL religion =
religion = protestant
#religion =  protestant  


terrain = forested_plain

discovered_by = HAT 
discovered_by = latin 
discovered_by = eastern 
discovered_by = muslim 
discovered_by = indian 
discovered_by = chinese

capital = "Lidkoping"

citysize = 	282111
manpower = 	2.8
base_tax = 	7
smd_prepped = 	no
hre = 	no
#dev level = 	Developed
temple = 	yes
workshop = 	yes
regimental_camp = 	yes
constable = 	yes
courthouse = 	yes
marketplace = 	yes
customs_house = 	no
fort1 = 	yes
fort2 = 	yes
fort3 = 	yes
fort4 = 	no
fort5 = 	no
fort6 = 	no
#capital = 	"

2000.1.1 = {
	owner = SE9
	controller = SE9
	add_core = SE9
}