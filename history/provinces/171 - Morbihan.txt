# 171 Morbihan - Principal cities: Lorient



culture = france_culture
#OLDREL religion =
religion = protestant
#religion =  catholic     

discovered_by = HAT 
discovered_by = latin 
discovered_by = eastern 
discovered_by = muslim 
discovered_by = indian 
discovered_by = chinese


terrain = forested_plain

capital = "Brest"

trade_goods =fish
citysize = 	183436
manpower = 	1.8
base_tax = 	5
#smd_prepped = 	no
hre = 	no
#dev level = 	Developed
temple = 	yes
workshop = 	yes
regimental_camp = 	yes
constable = 	yes
courthouse = 	yes
marketplace = 	yes
customs_house = 	no
fort1 = 	yes
fort2 = 	yes
fort3 = 	no
fort4 = 	no
fort5 = 	no
fort6 = 	no


2000.1.1 = {
	owner = FR9
	controller = FR9
	add_core = FR9
}