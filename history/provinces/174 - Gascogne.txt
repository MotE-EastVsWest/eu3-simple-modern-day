# 174 Gascogne - Principal cities: Bordeaux



culture = france_culture
#OLDREL religion =
religion = protestant
#religion =  catholic     
discovered_by = HAT 
discovered_by = latin 
discovered_by = eastern 
discovered_by = muslim 
discovered_by = indian 
discovered_by = chinese




capital = "Bordeaux"

trade_goods =wine


citysize = 	330994
manpower = 	3.3
base_tax = 	7
smd_prepped = 	no
hre = 	no
#dev level = 	Developed
temple = 	yes
workshop = 	yes
regimental_camp = 	yes
constable = 	yes
courthouse = 	yes
marketplace = 	yes
customs_house = 	yes
fort1 = 	yes
fort2 = 	yes
fort3 = 	yes
fort4 = 	no
fort5 = 	no
fort6 = 	no
pop_rework = no	



2000.1.1 = {
	owner = FR9
	controller = FR9
	add_core = FR9
}