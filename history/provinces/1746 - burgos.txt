#1746 - Burgos

trade_goods = iron
iron_rgo_building = yes


terrain = steppe


culture = spain_culture
#OLDREL religion =
religion = protestant
#religion =  catholic     


discovered_by = HAT 
discovered_by = latin 
discovered_by = eastern 
discovered_by = muslim 
discovered_by = indian 
discovered_by = chinese

capital = "Burgos"

citysize = 	75000
manpower = 	0.8
base_tax = 	3
smd_prepped = 	no
hre = 	no
#dev level = 	Developed
temple = 	yes
workshop = 	no
regimental_camp = 	no
constable = 	no
courthouse = 	no
marketplace = 	yes
customs_house = 	no
fort1 = 	yes
fort2 = 	no
fort3 = 	no
fort4 = 	no
fort5 = 	no
fort6 = 	no
#capital = 	"



2000.1.1 = {
	owner = ES9
	controller = ES9
	add_core = ES9
}