#1749 - Cadiz

trade_goods =naval_supplies



culture = spain_culture
#OLDREL religion =
religion = protestant
#religion =  catholic     

terrain = forested_plain


discovered_by = HAT 
discovered_by = latin 
discovered_by = eastern 
discovered_by = muslim 
discovered_by = indian 
discovered_by = chinese

capital = "Cadiz" 

citysize = 	248035
manpower = 	2.5
base_tax = 	4
smd_prepped = 	no
hre = 	no
#dev level = 	Developing
temple = 	yes
workshop = 	no
regimental_camp = 	no
constable = 	no
courthouse = 	no
marketplace = 	no
customs_house = 	no
fort1 = 	yes
fort2 = 	yes
fort3 = 	no
fort4 = 	no
fort5 = 	no
fort6 = 	no
#capital = 	"



2000.1.1 = {
	owner = ES9
	controller = ES9
	add_core = ES9
}