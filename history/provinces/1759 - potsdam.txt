# 1759 - Potsdam
culture = germany_culture
#OLDREL religion =
religion = protestant
#religion =  protestant
trade_goods = iron
terrain = forested_plain
discovered_by = HAT
discovered_by = latin
discovered_by = eastern
discovered_by = muslim
discovered_by = indian
discovered_by = chinese
germany = no
capital = "Potsdam"
citysize = 	97150
manpower = 	1
base_tax = 	3
smd_prepped = 	no
hre = 	no
#dev level = 	Developed
temple = 	yes
workshop = 	no
regimental_camp = 	no
constable = 	no
courthouse = 	no
marketplace = 	yes
customs_house = 	no
fort1 = 	yes
fort2 = 	no
fort3 = 	no
fort4 = 	no
fort5 = 	no
fort6 = 	no
pop_rework = no	

################################################################################
# Anything typed here will be added to all selected province history files
################################################################################
2000.1.1 = { owner = DE9 controller = DE9 add_core = DE9 } 