#1765 - Nis



culture = serbia_culture 
#OLDREL religion =
religion = reactionary_r
#religion =  orthodox  

terrain = mountain

discovered_by = HAT 
discovered_by = latin 
discovered_by = eastern 
discovered_by = muslim 
discovered_by = indian 
discovered_by = chinese

capital = "Nis"

trade_goods =copper

citysize = 	186292
manpower = 	1.9
base_tax = 	3
smd_prepped = 	no
hre = 	no
#dev level = 	Developing
temple = 	yes
workshop = 	no
regimental_camp = 	no
constable = 	no
courthouse = 	no
marketplace = 	no
customs_house = 	no
fort1 = 	yes
fort2 = 	no
fort3 = 	no
fort4 = 	no
fort5 = 	no
fort6 = 	no
pop_rework = no	



2000.1.1 = {
	owner = SB9
	controller = SB9
	add_core = SB9
	revolt = {
		type = gang_rebels
		size = 1
	}
}