#377 - Aleppo


culture = syria_alawite_culture 
#OLDREL religion =
religion = socialist_r
#religion =  shiite

discovered_by = HAT 
discovered_by = latin 
discovered_by = eastern 
discovered_by = muslim 
discovered_by = indian 
discovered_by = chinese

capital = "Lattakia"



terrain = mountain

trade_goods =cloth

citysize = 	201600
manpower = 	2
base_tax = 	4
smd_prepped = 	no
hre = 	no
#dev level = 	Developing
temple = 	yes
workshop = 	no
regimental_camp = 	no
constable = 	no
courthouse = 	no
marketplace = 	no
customs_house = 	no
fort1 = 	yes
fort2 = 	yes
fort3 = 	no
fort4 = 	no
fort5 = 	no
fort6 = 	no
pop_rework = no	



2000.1.1 = {
	owner = SY9
	controller = SY9
	add_core = SY9
}