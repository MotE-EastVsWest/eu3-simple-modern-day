# 429 - Fars
culture = iran_culture
#OLDREL religion =
religion = conservative_r
#religion =  shiite
discovered_by = HAT
discovered_by = latin
discovered_by = eastern
discovered_by = muslim
discovered_by = indian
discovered_by = chinese
capital = "Shiraz"
terrain = mountain
trade_goods = spices
citysize = 360000
manpower = 3.6
base_tax = 4
smd_prepped = no
hre = no
# dev level = 	Underdeveloped
temple = yes
workshop = no
regimental_camp = no
constable = no
courthouse = no
marketplace = no
customs_house = no
fort1 = yes
fort2 = no
fort3 = no
fort4 = no
fort5 = no
fort6 = no
################################################################################
# Anything typed here will be added to all selected province history files
################################################################################
2000.1.1 = { owner = IR9 controller = IR9 add_core = IR9 } 