# 158 - Transylvania
culture = romania_culture
#OLDREL religion =
religion = protestant
#religion =  orthodox
discovered_by = HAT
discovered_by = latin
discovered_by = eastern
discovered_by = muslim
discovered_by = indian
discovered_by = chinese
capital = "Timisoara" # Didn't have a capital, traditionally assembled in Turda
terrain = forested_plain
trade_goods = wool
citysize = 	150107
manpower = 	1.5
base_tax = 	3
smd_prepped = 	no
hre = 	no
#dev level = 	Developing
temple = 	yes
workshop = 	no
regimental_camp = 	no
constable = 	no
courthouse = 	no
marketplace = 	no
customs_house = 	no
fort1 = 	yes
fort2 = 	no
fort3 = 	no
fort4 = 	no
fort5 = 	no
fort6 = 	no
pop_rework = no	

################################################################################
# Anything typed here will be added to all selected province history files
################################################################################
2000.1.1 = { owner = RO9 controller = RO9 add_core = RO9 } 