#374 - Leinster

trade_goods =wool



culture = ireland_culture 
#OLDREL religion =
religion = protestant
#religion =  catholic       



terrain = forested_plain

discovered_by = HAT 
discovered_by = latin 
discovered_by = eastern 
discovered_by = muslim 
discovered_by = indian 
discovered_by = chinese

capital = "Limerick"

citysize = 	20457
manpower = 	0.2
base_tax = 	3
smd_prepped = 	no
hre = 	no
#dev level = 	Developed
temple = 	yes
workshop = 	no
regimental_camp = 	no
constable = 	no
courthouse = 	no
marketplace = 	no
customs_house = 	no
fort1 = 	yes
fort2 = 	no
fort3 = 	no
fort4 = 	no
fort5 = 	no
fort6 = 	no


2000.1.1 = {
	owner = IE9
	controller = IE9
	add_core = IE9
}