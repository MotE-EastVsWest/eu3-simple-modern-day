#408 - Basra


culture = kuwait_culture 
#OLDREL religion =
religion = conservative_r
#religion =  sunni

discovered_by = HAT 
discovered_by = latin 
discovered_by = eastern 
discovered_by = muslim 
discovered_by = indian 
discovered_by = chinese

capital = "Kuwait"


terrain = desert

trade_goods =spices

citysize = 	600000
manpower = 	6
base_tax = 	6
smd_prepped = 	no
hre = 	no
#dev level = 	Developing
temple = 	yes
workshop = 	no
regimental_camp = 	yes
constable = 	yes
courthouse = 	yes
marketplace = 	yes
customs_house = 	no
fort1 = 	yes
fort2 = 	yes
fort3 = 	no
fort4 = 	no
fort5 = 	no
fort6 = 	no


2000.1.1 = {
	owner = KU9
	controller = KU9
	add_core = KU9
}