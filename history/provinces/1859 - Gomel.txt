# 292 - Mogilyov
trade_goods = wool
culture = belarus_culture
#OLDREL religion =
religion = protestant
#religion =  orthodox
terrain = forested_plain
discovered_by = HAT
discovered_by = latin
discovered_by = eastern
discovered_by = muslim
discovered_by = indian
discovered_by = chinese
capital = "Gomel"
citysize = 	170000
manpower = 	1.7
base_tax = 	3
smd_prepped = 	no
hre = 	no
#dev level = 	Developing
temple = 	yes
workshop = 	no
regimental_camp = 	no
constable = 	no
courthouse = 	no
marketplace = 	no
customs_house = 	no
fort1 = 	yes
fort2 = 	no
fort3 = 	no
fort4 = 	no
fort5 = 	no
fort6 = 	no
pop_rework = no	

################################################################################
# Anything typed here will be added to all selected province history files
################################################################################
2000.1.1 = { owner = BY9 controller = BY9 add_core = BY9 } 