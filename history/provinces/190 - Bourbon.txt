# 190 Bourbon - Principal cities: Bourges




culture = france_culture
#OLDREL religion =
religion = protestant
#religion =  catholic     
discovered_by = HAT 
discovered_by = latin 
discovered_by = eastern 
discovered_by = muslim 
discovered_by = indian 
discovered_by = chinese


terrain = forested_plain

capital = "Orleans"

trade_goods =iron
citysize = 	76461
manpower = 	0.8
base_tax = 	3
#smd_prepped = 	no
hre = 	no
#dev level = 	Developed
temple = 	yes
workshop = 	no
regimental_camp = 	no
constable = 	no
courthouse = 	no
marketplace = 	yes
customs_house = 	no
fort1 = 	yes
fort2 = 	no
fort3 = 	no
fort4 = 	no
fort5 = 	no
fort6 = 	no

2000.1.1 = {
	owner = FR9
	controller = FR9
	add_core = FR9
}
