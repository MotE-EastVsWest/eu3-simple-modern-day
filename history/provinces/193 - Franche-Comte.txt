# 193 Franche-Comt� - Principal cities: Besan�on



culture = france_culture
#OLDREL religion =
religion = protestant
#religion =  catholic     

discovered_by = HAT 
discovered_by = latin 
discovered_by = eastern 
discovered_by = muslim 
discovered_by = indian 
discovered_by = chinese


terrain = forested_plain

capital = "Besan�on"

trade_goods =salt

citysize = 	109042
manpower = 	1.1
base_tax = 	4
#smd_prepped = 	no
hre = 	no
#dev level = 	Developed
temple = 	yes
workshop = 	no
regimental_camp = 	no
constable = 	no
courthouse = 	no
marketplace = 	yes
customs_house = 	no
fort1 = 	yes
fort2 = 	yes
fort3 = 	no
fort4 = 	no
fort5 = 	no
fort6 = 	no


2000.1.1 = {
	owner = FR9
	controller = FR9
	add_core = FR9
}