


culture = united_states_of_america_culture
#OLDREL religion =
religion = protestant
#religion =  catholic


discovered_by = HAT 
discovered_by = latin 
discovered_by = eastern 
discovered_by = muslim 
discovered_by = indian 
discovered_by = chinese

capital = "Durango"

trade_goods =grain

citysize = 	3814
manpower = 	0.1
base_tax = 	1
smd_prepped = 	no
hre = 	no
#dev level = 	Developing
temple = 	no
workshop = 	no
regimental_camp = 	no
constable = 	no
courthouse = 	no
marketplace = 	no
customs_house = 	no
fort1 = 	no
fort2 = 	no
fort3 = 	no
fort4 = 	no
fort5 = 	no
fort6 = 	no


2000.1.1 = {
	owner = US9
	controller = US9
	add_core = US9
}
