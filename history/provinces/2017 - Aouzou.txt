


culture = chad_culture  
#OLDREL religion =
religion = reactionary_r
#religion =  sunni


terrain = mountain

discovered_by = HAT 
discovered_by = latin 
discovered_by = eastern 
discovered_by = muslim 
discovered_by = indian 
discovered_by = chinese

capital = "Zouar"

trade_goods =salt

citysize = 	8000
manpower = 	0.1
base_tax = 	1
smd_prepped = 	no
hre = 	no
#dev level = 	Underdeveloped
temple = 	no
workshop = 	no
regimental_camp = 	no
constable = 	no
courthouse = 	no
marketplace = 	no
customs_house = 	no
fort1 = 	no
fort2 = 	no
fort3 = 	no
fort4 = 	no
fort5 = 	no
fort6 = 	no

2000.1.1 = {
	owner = CD8
	controller = REB
	add_core = CD9
	add_core = CD8
	revolt = {
		type = gang_rebels
		size = 1
	}
}
