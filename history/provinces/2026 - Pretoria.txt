#1181 - Natal


culture = south_africa_pluralist_culture
#OLDREL religion =
religion = protestant
#religion =  reformed   

discovered_by = HAT 
discovered_by = latin 
discovered_by = eastern 
discovered_by = muslim 
discovered_by = indian 
discovered_by = chinese

trade_goods =grain
capital = "Pretoria"


terrain = steppe


citysize = 	584298
manpower = 	5.8
base_tax = 	6
smd_prepped = 	no
hre = 	no
#dev level = 	Developing
temple = 	yes
workshop = 	no
regimental_camp = 	yes
constable = 	yes
courthouse = 	yes
marketplace = 	yes
customs_house = 	no
fort1 = 	yes
fort2 = 	yes
fort3 = 	no
fort4 = 	no
fort5 = 	no
fort6 = 	no


2000.1.1 = {
	owner = SF9
	controller = SF9
	add_core = SF9
}