


culture = katanga_culture 
#OLDREL religion =
religion = protestant
#religion =  catholic   
trade_goods = grain   


discovered_by = HAT 
discovered_by = latin 
discovered_by = eastern 
discovered_by = muslim 
discovered_by = indian 
discovered_by = chinese


terrain = forested_plain


capital = "Kalemie"

citysize = 	29395
manpower = 	0.3
base_tax = 	2
smd_prepped = 	no
hre = 	no
#dev level = 	Underdeveloped
temple = 	no
workshop = 	no
regimental_camp = 	no
constable = 	no
courthouse = 	no
marketplace = 	no
customs_house = 	no
fort1 = 	no
fort2 = 	no
fort3 = 	no
fort4 = 	no
fort5 = 	no
fort6 = 	no


2000.1.1 = {
	owner = KI8
	controller = KI8
	add_core = KT9
	add_core = KI9
	add_core = KI8
}