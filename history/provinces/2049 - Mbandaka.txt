



culture = dr_congo_culture 
#OLDREL religion =
religion = protestant
#religion =  catholic   

trade_goods = grain   
discovered_by = HAT 
discovered_by = latin 
discovered_by = eastern 
discovered_by = muslim 
discovered_by = indian 
discovered_by = chinese

capital = "Mbandaka"


citysize = 	237567
manpower = 	2.4
base_tax = 	3
smd_prepped = 	no
hre = 	no
#dev level = 	Underdeveloped
temple = 	no
workshop = 	no
regimental_camp = 	no
constable = 	no
courthouse = 	no
marketplace = 	no
customs_house = 	no
fort1 = 	yes
fort2 = 	no
fort3 = 	no
fort4 = 	no
fort5 = 	no
fort6 = 	no


2000.1.1 = {
	owner = KI9
	controller = REB
	add_core = KI9
	add_core = KI8
	revolt = {
		type = gang_rebels
		size = 1
	}
}