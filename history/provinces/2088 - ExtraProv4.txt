# 1026 - Mutsu
culture = japan_culture
#OLDREL religion =
religion = protestant
#religion =  shinto
discovered_by = HAT
discovered_by = latin
discovered_by = eastern
discovered_by = muslim
discovered_by = indian
discovered_by = chinese
capital = "Akita"
terrain = forested_plain
trade_goods = iron
citysize = 61125
manpower = 0.6
base_tax = 3
smd_prepped = no
hre = no
# dev level = 	Developed
temple = yes
workshop = no
regimental_camp = no
constable = no
courthouse = no
marketplace = yes
customs_house = no
fort1 = yes
fort2 = no
fort3 = no
fort4 = no
fort5 = no
fort6 = no
################################################################################
# Anything typed here will be added to all selected province history files
################################################################################
2000.1.1 = { owner = JP9 controller = JP9 add_core = JP9 } 