#668 - Macau

culture = hong_kong_culture 
#OLDREL religion =
religion = protestant
#religion =  confucianism  
discovered_by = HAT 
discovered_by = latin 
discovered_by = eastern 
discovered_by = muslim 
discovered_by = indian 
discovered_by = chinese

capital = "Hong Kong"


terrain = forested_plain


trade_goods =chinaware

citysize = 	990000
manpower = 	9.9
base_tax = 	8
smd_prepped = 	no
hre = 	no
#dev level = 	Developing
temple = 	yes
workshop = 	yes
regimental_camp = 	yes
constable = 	yes
courthouse = 	yes
marketplace = 	yes
customs_house = 	yes
fort1 = 	yes
fort2 = 	yes
fort3 = 	yes
fort4 = 	no
fort5 = 	no
fort6 = 	no

2000.1.1 = { 
    owner = HK9
    controller = HK9
	add_core = HK9
    add_core = NA9
    add_core = TW9
}
