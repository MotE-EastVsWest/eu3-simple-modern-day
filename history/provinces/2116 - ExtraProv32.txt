# 687 - Running
culture = china_people's_republic_culture
#OLDREL religion =
religion = protestant
#religion =  confucianism
discovered_by = HAT
discovered_by = latin
discovered_by = eastern
discovered_by = muslim
discovered_by = indian
discovered_by = chinese
capital = "Linfen"
terrain = forested_plain
trade_goods = grain
citysize = 191840
manpower = 1.9
base_tax = 2
smd_prepped = no
hre = no
# dev level = 	Underdeveloped
temple = no
workshop = no
regimental_camp = no
constable = no
courthouse = no
marketplace = no
customs_house = no
fort1 = yes
fort2 = no
fort3 = no
fort4 = no
fort5 = no
fort6 = no
################################################################################
# Anything typed here will be added to all selected province history files
################################################################################
2000.1.1 = { 
    owner = NA9
    controller = NA9
    add_core = NA9
    add_core = TW9
}
