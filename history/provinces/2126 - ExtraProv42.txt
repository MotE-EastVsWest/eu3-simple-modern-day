# 686 - Anhui
culture = china_people's_republic_culture
#OLDREL religion =
religion = socialist_r
#religion =  confucianism
discovered_by = HAT
discovered_by = latin
discovered_by = eastern
discovered_by = muslim
discovered_by = indian
discovered_by = chinese
capital = "Nanjing"
terrain = forested_plain
trade_goods = tea
citysize = 990000
manpower = 9.9
base_tax = 9
smd_prepped = no
hre = no
# dev level = 	Developing
temple = yes
workshop = yes
regimental_camp = yes
constable = yes
courthouse = yes
marketplace = yes
customs_house = yes
fort1 = yes
fort2 = yes
fort3 = yes
fort4 = yes
fort5 = no
fort6 = no

cot = yes
################################################################################
# Anything typed here will be added to all selected province history files
################################################################################
2000.1.1 = { 
    owner = NA9
    controller = NA9
    add_core = NA9
    add_core = TW9
}
