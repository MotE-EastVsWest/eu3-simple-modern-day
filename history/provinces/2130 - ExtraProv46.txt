# 671 - Changsha
culture = china_people's_republic_culture
#OLDREL religion =
religion = socialist_r
#religion =  confucianism
discovered_by = HAT
discovered_by = latin
discovered_by = eastern
discovered_by = muslim
discovered_by = indian
discovered_by = chinese
capital = "Changsha"
terrain = mountain
trade_goods = iron
citysize = 953258
manpower = 9.5
base_tax = 5
smd_prepped = no
hre = no
# dev level = 	Underdeveloped
temple = yes
workshop = no
regimental_camp = no
constable = no
courthouse = no
marketplace = no
customs_house = no
fort1 = yes
fort2 = yes
fort3 = no
fort4 = no
fort5 = no
fort6 = no
################################################################################
# Anything typed here will be added to all selected province history files
################################################################################
2000.1.1 = { 
    owner = NA9
    controller = NA9
    add_core = NA9
    add_core = TW9
}
