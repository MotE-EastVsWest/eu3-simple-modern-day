#1243 - Samoa


culture = tonga_culture   
#OLDREL religion =
religion = protestant
#religion =  protestant 

discovered_by = HAT 
discovered_by = latin 
discovered_by = eastern 
discovered_by = muslim 
discovered_by = indian 
discovered_by = chinese

capital = "Nuku'alofa"
trade_goods =wool

citysize = 	20042
manpower = 	0.2
base_tax = 	2
smd_prepped = 	no
hre = 	no
#dev level = 	Developing
temple = 	no
workshop = 	no
regimental_camp = 	no
constable = 	no
courthouse = 	no
marketplace = 	no
customs_house = 	no
fort1 = 	yes
fort2 = 	no
fort3 = 	no
fort4 = 	no
fort5 = 	no
fort6 = 	no


2000.1.1 = {
	owner = NG9
	controller = NG9
	add_core = NG9
}