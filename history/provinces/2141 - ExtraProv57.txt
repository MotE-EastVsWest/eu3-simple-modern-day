#1098 - St. Helena

hre = no
capital = "Adamstown"

culture = pitcairn_islands_culture
#OLDREL religion =
religion = protestant
#religion =  reformed

trade_goods =wool

citysize = 	1100
manpower = 	0.1
base_tax = 	1
smd_prepped = 	no
hre = 	no
#dev level = 	Developing
temple = 	no
workshop = 	no
regimental_camp = 	no
constable = 	no
courthouse = 	no
marketplace = 	no
customs_house = 	no
fort1 = 	no
fort2 = 	no
fort3 = 	no
fort4 = 	no
fort5 = 	no
fort6 = 	no
discovered_by = HAT 
discovered_by = latin 
discovered_by = eastern 
discovered_by = muslim 
discovered_by = indian 
discovered_by = chinese

2000.1.1 = {
	owner = EN9
	controller = EN9
	add_core = PC9
	add_core = EN9
}