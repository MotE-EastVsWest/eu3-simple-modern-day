#1098 - St. Helena

culture = british_atlantic_islands_culture
#OLDREL religion =
religion = protestant
#religion =  protestant
discovered_by = HAT 
discovered_by = latin 
discovered_by = eastern 
discovered_by = muslim 
discovered_by = indian 
discovered_by = chinese


trade_goods = naval_supplies
citysize = 	1100
manpower = 	0.1
base_tax = 	1
smd_prepped = 	no
hre = 	no
#dev level = 	Underdeveloped
temple = 	no
workshop = 	no
regimental_camp = 	no
constable = 	no
courthouse = 	no
marketplace = 	no
customs_house = 	no
fort1 = 	no
fort2 = 	no
fort3 = 	no
fort4 = 	no
fort5 = 	no
fort6 = 	no


2000.1.1 = {
	owner = EN9
	controller = EN9
	add_core = B29
	add_core = EN9
	add_core = AG9
}