#1113 - Cayor

culture = guinea-bissau_culture 
#OLDREL religion =
religion = protestant
#religion =  sunni


discovered_by = HAT 
discovered_by = latin 
discovered_by = eastern 
discovered_by = muslim 
discovered_by = indian 
discovered_by = chinese

capital = "Bissau"

terrain = jungle


trade_goods = slaves

citysize = 	415764
manpower = 	4.2
base_tax = 	4
smd_prepped = 	no
hre = 	no
#dev level = 	Underdeveloped
temple = 	yes
workshop = 	no
regimental_camp = 	no
constable = 	no
courthouse = 	no
marketplace = 	no
customs_house = 	no
fort1 = 	yes
fort2 = 	no
fort3 = 	no
fort4 = 	no
fort5 = 	no
fort6 = 	no

2000.1.1 = {
	owner = BS9
	controller = BS9
	add_core = BS9
}
