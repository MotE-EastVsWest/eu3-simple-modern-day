#1116 - Wolof



culture = nigeria_culture 
#OLDREL religion =
religion = protestant
#religion =  protestant 


discovered_by = HAT 
discovered_by = latin 
discovered_by = eastern 
discovered_by = muslim 
discovered_by = indian 
discovered_by = chinese

capital = "Ibadan"


trade_goods = grain   

terrain = forested_plain

citysize = 	990000
manpower = 	9.9
base_tax = 	6
smd_prepped = 	no
hre = 	no
#dev level = 	Underdeveloped
temple = 	yes
workshop = 	yes
regimental_camp = 	no
constable = 	no
courthouse = 	yes
marketplace = 	yes
customs_house = 	no
fort1 = 	yes
fort2 = 	yes
fort3 = 	no
fort4 = 	no
fort5 = 	no
fort6 = 	no

cot = yes


2000.1.1 = {
	owner = NI9
	controller = NI9
	add_core = NI9
}