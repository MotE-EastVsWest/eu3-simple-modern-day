#1181 - Natal



culture = south_africa_pluralist_culture 
#OLDREL religion =
religion = protestant
#religion =  protestant   
discovered_by = HAT 
discovered_by = latin 
discovered_by = eastern 
discovered_by = muslim 
discovered_by = indian 
discovered_by = chinese

trade_goods =grain
capital = "Mbombela"

terrain = mountain



citysize = 	22032
manpower = 	0.2
base_tax = 	2
smd_prepped = 	no
hre = 	no
#dev level = 	Underdeveloped
temple = 	no
workshop = 	no
regimental_camp = 	no
constable = 	no
courthouse = 	no
marketplace = 	no
customs_house = 	no
fort1 = 	no
fort2 = 	no
fort3 = 	no
fort4 = 	no
fort5 = 	no
fort6 = 	no


2000.1.1 = {
	owner = SF9
	controller = SF9
	add_core = SF9
}