#777 - Parana




culture = argentina_culture 
#OLDREL religion =
religion = protestant
#religion =  catholic 
discovered_by = HAT 
discovered_by = latin 
discovered_by = eastern 
discovered_by = muslim 
discovered_by = indian 
discovered_by = chinese

capital = "Mar de la Plata"


terrain = steppe

trade_goods =grain

citysize = 	136521
manpower = 	1.4
base_tax = 	3
smd_prepped = 	no
hre = 	no
#dev level = 	Developing
temple = 	yes
workshop = 	no
regimental_camp = 	no
constable = 	no
courthouse = 	no
marketplace = 	yes
customs_house = 	no
fort1 = 	yes
fort2 = 	no
fort3 = 	no
fort4 = 	no
fort5 = 	no
fort6 = 	no


2000.1.1 = {
	owner = AG9
	controller = AG9
	add_core = AG9
}