#795 - Chichas

culture = bolivia_culture
#OLDREL religion =
religion = protestant
#religion =  catholic
discovered_by = HAT 
discovered_by = latin 
discovered_by = eastern 
discovered_by = muslim 
discovered_by = indian 
discovered_by = chinese

capital = "Sucre"

terrain = mountain


trade_goods =grain

citysize = 	78000
manpower = 	0.8
base_tax = 	2
smd_prepped = 	no
hre = 	no
#dev level = 	Developing
temple = 	no
workshop = 	no
regimental_camp = 	no
constable = 	no
courthouse = 	no
marketplace = 	no
customs_house = 	no
fort1 = 	yes
fort2 = 	no
fort3 = 	no
fort4 = 	no
fort5 = 	no
fort6 = 	no


2000.1.1 = {
	owner = VO9
	controller = VO9
	add_core = VO9
}