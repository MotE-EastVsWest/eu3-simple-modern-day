#232 - Bragan�a



culture = portugal_culture
#OLDREL religion =
religion = protestant
#religion =  catholic     
discovered_by = HAT 
discovered_by = latin 
discovered_by = eastern 
discovered_by = muslim 
discovered_by = indian 
discovered_by = chinese

capital = "Bragan�a"

terrain = mountain


trade_goods =wine

citysize = 	44561
manpower = 	0.4
base_tax = 	3
#smd_prepped = 	no
hre = 	no
#dev level = 	Developed
temple = 	yes
workshop = 	no
regimental_camp = 	no
constable = 	no
courthouse = 	no
marketplace = 	no
customs_house = 	no
fort1 = 	yes
fort2 = 	no
fort3 = 	no
fort4 = 	no
fort5 = 	no
fort6 = 	no


2000.1.1 = {
	owner = PT9
	controller = PT9
	add_core = PT9
}