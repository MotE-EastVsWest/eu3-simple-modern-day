#233 - Cornwall

trade_goods =copper


culture = united_kingdom_culture
#OLDREL religion =
religion = protestant
#religion =  protestant       

terrain = forested_plain

discovered_by = HAT 
discovered_by = latin 
discovered_by = eastern 
discovered_by = muslim 
discovered_by = indian 
discovered_by = chinese

capital = "Plymouth"

citysize = 	155595
manpower = 	1.6
base_tax = 	5
smd_prepped = 	no
hre = 	no
#dev level = 	Developed
temple = 	yes
workshop = 	yes
regimental_camp = 	yes
constable = 	yes
courthouse = 	yes
marketplace = 	yes
customs_house = 	no
fort1 = 	yes
fort2 = 	yes
fort3 = 	no
fort4 = 	no
fort5 = 	no
fort6 = 	no
#capital = 	"


2000.1.1 = {
	owner = EN9
	controller = EN9
	add_core = EN9
}