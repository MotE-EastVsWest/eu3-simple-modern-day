#234 - Wessex

trade_goods =fish

culture = united_kingdom_culture
#OLDREL religion =
religion = protestant
#religion =  protestant       

terrain = forested_plain

discovered_by = HAT 
discovered_by = latin 
discovered_by = eastern 
discovered_by = muslim 
discovered_by = indian 
discovered_by = chinese

capital = "Southampton"

citysize = 	375583
manpower = 	3.8
base_tax = 	8
smd_prepped = 	no
hre = 	no
#dev level = 	Developed
temple = 	yes
workshop = 	yes
regimental_camp = 	yes
constable = 	yes
courthouse = 	yes
marketplace = 	yes
customs_house = 	yes
fort1 = 	yes
fort2 = 	yes
fort3 = 	yes
fort4 = 	no
fort5 = 	no
fort6 = 	no
#capital = 	"


2000.1.1 = {
	owner = EN9
	controller = EN9
	add_core = EN9
}
