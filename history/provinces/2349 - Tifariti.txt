
culture = western_sahara_culture 
#OLDREL religion =
religion = socialist_r
#religion =  sunni
capital = "Tifariti"
discovered_by = HAT 
discovered_by = latin 
discovered_by = eastern 
discovered_by = muslim 
discovered_by = indian 
discovered_by = chinese

trade_goods = unknown
hre = no

trade_goods =salt
africa_rename = no

citysize = 	3000
manpower = 	0.1
base_tax = 	1
smd_prepped = 	no
hre = 	no
#dev level = 	Underdeveloped
temple = 	no
workshop = 	no
regimental_camp = 	no
constable = 	no
courthouse = 	no
marketplace = 	no
customs_house = 	no
fort1 = 	no
fort2 = 	no
fort3 = 	no
fort4 = 	no
fort5 = 	no
fort6 = 	no


2000.1.1 = {
	owner = EH9
	controller = EH9
	add_core = EH9
	add_core = MA9
}