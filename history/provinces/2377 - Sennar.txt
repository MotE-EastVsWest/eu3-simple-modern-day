
capital = "el-Gadarif"
culture = sudan_culture 
#OLDREL religion =
religion = protestant
#religion =  sunni
trade_goods =grain

discovered_by = HAT 
discovered_by = latin 
discovered_by = eastern 
discovered_by = muslim 
discovered_by = indian 
discovered_by = chinese
hre = no
africa_rename  = no

citysize = 	280062
manpower = 	2.8
base_tax = 	3
smd_prepped = 	no
hre = 	no
#dev level = 	Underdeveloped
temple = 	no
workshop = 	no
regimental_camp = 	no
constable = 	no
courthouse = 	no
marketplace = 	no
customs_house = 	no
fort1 = 	yes
fort2 = 	no
fort3 = 	no
fort4 = 	no
fort5 = 	no
fort6 = 	no


2000.1.1 = {
	owner = SU9
	controller = SU9
	add_core = SU9
	add_core = SU8
}