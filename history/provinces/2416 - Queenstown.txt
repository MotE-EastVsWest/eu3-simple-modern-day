

culture = south_africa_pluralist_culture 
#OLDREL religion =
religion = protestant
#religion =  protestant   
trade_goods =grain
capital = "Queenstown"
discovered_by = HAT 
discovered_by = latin 
discovered_by = eastern 
discovered_by = muslim 
discovered_by = indian 
discovered_by = chinese


hre = no

africa_rename  = no

citysize = 	13774
manpower = 	0.1
base_tax = 	2
smd_prepped = 	no
hre = 	no
#dev level = 	Developing
temple = 	no
workshop = 	no
regimental_camp = 	no
constable = 	no
courthouse = 	no
marketplace = 	no
customs_house = 	no
fort1 = 	yes
fort2 = 	no
fort3 = 	no
fort4 = 	no
fort5 = 	no
fort6 = 	no


2000.1.1 = {
	owner = SF9
	controller = SF9
	add_core = SF9
}