

culture = namibia_culture 
#OLDREL religion =
religion = protestant
#religion =  protestant   
capital = "Walvis Bay"
discovered_by = HAT 
discovered_by = latin 
discovered_by = eastern 
discovered_by = muslim 
discovered_by = indian 
discovered_by = chinese

trade_goods =fish
hre = no

africa_rename  = no

citysize = 	12419
manpower = 	0.1
base_tax = 	2
smd_prepped = 	no
hre = 	no
#dev level = 	Developing
temple = 	no
workshop = 	no
regimental_camp = 	no
constable = 	no
courthouse = 	no
marketplace = 	no
customs_house = 	no
fort1 = 	yes
fort2 = 	no
fort3 = 	no
fort4 = 	no
fort5 = 	no
fort6 = 	no


2000.1.1 = {
	owner = NB9
	controller = NB9
	add_core = NB9
}