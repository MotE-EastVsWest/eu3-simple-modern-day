
capital = "Yaounde"

culture = cameroon_culture 
#OLDREL religion =
religion = protestant
#religion =  protestant    
discovered_by = HAT 
discovered_by = latin 
discovered_by = eastern 
discovered_by = muslim 
discovered_by = indian 
discovered_by = chinese

trade_goods =grain
hre = no

africa_rename  = no

citysize = 	553120
manpower = 	5.5
base_tax = 	6
smd_prepped = 	no
hre = 	no
#dev level = 	Developing
temple = 	yes
workshop = 	no
regimental_camp = 	yes
constable = 	yes
courthouse = 	yes
marketplace = 	yes
customs_house = 	no
fort1 = 	yes
fort2 = 	yes
fort3 = 	no
fort4 = 	no
fort5 = 	no
fort6 = 	no


2000.1.1 = {
	owner = CM9
	controller = CM9
	add_core = CM9
}