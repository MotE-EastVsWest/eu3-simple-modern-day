
capital = "Atakpame"

culture = togo_culture 
#OLDREL religion =
religion = protestant
#religion =  catholic 
discovered_by = HAT 
discovered_by = latin 
discovered_by = eastern 
discovered_by = muslim 
discovered_by = indian 
discovered_by = chinese

trade_goods = grain   
hre = no

africa_rename  = no

citysize = 	16996
manpower = 	0.2
base_tax = 	1
smd_prepped = 	no
hre = 	no
#dev level = 	Underdeveloped
temple = 	no
workshop = 	no
regimental_camp = 	no
constable = 	no
courthouse = 	no
marketplace = 	no
customs_house = 	no
fort1 = 	no
fort2 = 	no
fort3 = 	no
fort4 = 	no
fort5 = 	no
fort6 = 	no

2000.1.1 = {
	owner = TG9
	controller = TG9
	add_core = TG9
}
