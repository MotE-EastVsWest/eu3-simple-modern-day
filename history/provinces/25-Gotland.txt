#Gotland- Visby etc


trade_goods =wool

culture = sweden_culture
#OLDREL religion =
religion = protestant
#religion =  protestant  

discovered_by = HAT 
discovered_by = latin 
discovered_by = eastern 
discovered_by = muslim 
discovered_by = indian 
discovered_by = chinese

capital = "Visby"

citysize = 	12206
manpower = 	0.1
base_tax = 	2
smd_prepped = 	no
hre = 	no
#dev level = 	Developed
temple = 	yes
workshop = 	no
regimental_camp = 	no
constable = 	no
courthouse = 	no
marketplace = 	no
customs_house = 	no
fort1 = 	yes
fort2 = 	no
fort3 = 	no
fort4 = 	no
fort5 = 	no
fort6 = 	no
#capital = 	"


2000.1.1 = {
	owner = SE9
	controller = SE9
	add_core = SE9
}