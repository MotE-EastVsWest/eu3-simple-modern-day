
capital = "Daloa"

culture = ivory_coast_culture 
#OLDREL religion =
religion = protestant
#religion =  animism
discovered_by = HAT 
discovered_by = latin 
discovered_by = eastern 
discovered_by = muslim 
discovered_by = indian 
discovered_by = chinese

trade_goods = grain   
hre = no

africa_rename  = no

citysize = 	84374
manpower = 	0.8
base_tax = 	2
smd_prepped = 	no
hre = 	no
#dev level = 	Underdeveloped
temple = 	no
workshop = 	no
regimental_camp = 	no
constable = 	no
courthouse = 	no
marketplace = 	no
customs_house = 	no
fort1 = 	yes
fort2 = 	no
fort3 = 	no
fort4 = 	no
fort5 = 	no
fort6 = 	no


2000.1.1 = {
	owner = IV9
	controller = IV9
	add_core = IV9
	add_core = IV8
}
