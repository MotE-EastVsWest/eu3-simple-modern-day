
capital = "Gazimaguza"


culture = cyprus_turkish_culture 
#OLDREL religion =
religion = protestant
#religion =  sunni  
trade_goods = fish   
discovered_by = HAT 
discovered_by = latin 
discovered_by = eastern 
discovered_by = muslim 
discovered_by = indian 
discovered_by = chinese
hre = no
citysize = 	76572
manpower = 	0.8
base_tax = 	2
smd_prepped = 	no
hre = 	no
#dev level = 	Developing
temple = 	no
workshop = 	no
regimental_camp = 	no
constable = 	no
courthouse = 	no
marketplace = 	no
customs_house = 	no
fort1 = 	yes
fort2 = 	no
fort3 = 	no
fort4 = 	no
fort5 = 	no
fort6 = 	no
pop_rework = no	


2000.1.1 = {
	owner = TY9
	controller = TY9
	add_core = TY9
	add_core = CY9
}
