
capital = "Mersin"

culture = turkey_culture
#OLDREL religion =
religion = protestant
#religion =  sunni

trade_goods = fish   
discovered_by = HAT 
discovered_by = latin 
discovered_by = eastern 
discovered_by = muslim 
discovered_by = indian 
discovered_by = chinese
hre = no
italy_rename = no

citysize = 	383280
manpower = 	3.8
base_tax = 	5
smd_prepped = 	no
hre = 	no
#dev level = 	Developing
temple = 	yes
workshop = 	no
regimental_camp = 	no
constable = 	no
courthouse = 	no
marketplace = 	yes
customs_house = 	no
fort1 = 	yes
fort2 = 	yes
fort3 = 	no
fort4 = 	no
fort5 = 	no
fort6 = 	no
pop_rework = no	



2000.1.1 = {
	owner = TR9
	controller = TR9
	add_core = TR9
}