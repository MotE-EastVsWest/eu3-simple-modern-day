
capital = "Yevlakh"


culture = azerbaijan_culture
#OLDREL religion =
religion = protestant
#religion =  shiite
trade_goods = grain   
discovered_by = HAT 
discovered_by = latin 
discovered_by = eastern 
discovered_by = muslim 
discovered_by = indian 
discovered_by = chinese
hre = no
europe_rename = no

citysize = 	146080
manpower = 	1.5
base_tax = 	3
smd_prepped = 	no
hre = 	no
#dev level = 	Developing
temple = 	yes
workshop = 	no
regimental_camp = 	no
constable = 	no
courthouse = 	no
marketplace = 	yes
customs_house = 	no
fort1 = 	yes
fort2 = 	no
fort3 = 	no
fort4 = 	no
fort5 = 	no
fort6 = 	no
pop_rework = no	


2000.1.1 = {
	owner = AZ9
	controller = AZ9
	add_core = AZ9
}