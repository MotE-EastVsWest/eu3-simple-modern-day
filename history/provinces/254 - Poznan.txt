# 254 - Poznan
culture = poland_culture
#OLDREL religion =
religion = protestant
#religion =  catholic
discovered_by = HAT
discovered_by = latin
discovered_by = eastern
discovered_by = muslim
discovered_by = indian
discovered_by = chinese
terrain = forested_plain
capital = "Poznan"
# culture = polish
# #OLDREL religion =
religion = protestant
#religion =  catholic
trade_goods = salt
citysize = 	380000
manpower = 	3.8
base_tax = 	8
smd_prepped = 	no
hre = 	no
#dev level = 	Developed
temple = 	yes
workshop = 	yes
regimental_camp = 	yes
constable = 	yes
courthouse = 	yes
marketplace = 	yes
customs_house = 	yes
fort1 = 	yes
fort2 = 	yes
fort3 = 	yes
fort4 = 	no
fort5 = 	no
fort6 = 	no
pop_rework = no	

################################################################################
# Anything typed here will be added to all selected province history files
################################################################################
2000.1.1 = { owner = PO9 controller = PO9 add_core = PO9 } 