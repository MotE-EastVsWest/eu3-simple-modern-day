

culture = spain_culture
#OLDREL religion =
religion = protestant
#religion =  catholic     
capital = "Ciudad Real"
discovered_by = HAT 
discovered_by = latin 
discovered_by = eastern 
discovered_by = muslim 
discovered_by = indian 
discovered_by = chinese

trade_goods = grain
hre = no

citysize = 	104992
manpower = 	1
base_tax = 	4
#smd_prepped = 	no
hre = 	no
#dev level = 	Developed
temple = 	yes
workshop = 	no
regimental_camp = 	no
constable = 	no
courthouse = 	no
marketplace = 	yes
customs_house = 	no
fort1 = 	yes
fort2 = 	yes
fort3 = 	no
fort4 = 	no
fort5 = 	no
fort6 = 	no


2000.1.1 = {
	owner = ES9
	controller = ES9
	add_core = ES9
}