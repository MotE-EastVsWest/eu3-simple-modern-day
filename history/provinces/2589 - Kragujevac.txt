
capital = "Kragujevac"


culture = serbia_culture 
#OLDREL religion =
religion = protestant
#religion =  orthodox  
discovered_by = HAT 
discovered_by = latin 
discovered_by = eastern 
discovered_by = muslim 
discovered_by = indian 
discovered_by = chinese
trade_goods = grain   
hre = no

citysize = 	93946
manpower = 	0.9
base_tax = 	2
smd_prepped = 	no
hre = 	no
#dev level = 	Developing
temple = 	no
workshop = 	no
regimental_camp = 	no
constable = 	no
courthouse = 	no
marketplace = 	no
customs_house = 	no
fort1 = 	yes
fort2 = 	no
fort3 = 	no
fort4 = 	no
fort5 = 	no
fort6 = 	no
pop_rework = no	



2000.1.1 = {
	owner = SB9
	controller = SB9
	add_core = SB9
}