
capital = "Grevena"

culture = greece_culture 
#OLDREL religion =
religion = protestant
#religion =  orthodox  
trade_goods = grain   

discovered_by = HAT 
discovered_by = latin 
discovered_by = eastern 
discovered_by = muslim 
discovered_by = indian 
discovered_by = chinese
hre = no

citysize = 	51011
manpower = 	0.5
base_tax = 	2
smd_prepped = 	no
hre = 	no
#dev level = 	Developing
temple = 	no
workshop = 	no
regimental_camp = 	no
constable = 	no
courthouse = 	no
marketplace = 	no
customs_house = 	no
fort1 = 	yes
fort2 = 	no
fort3 = 	no
fort4 = 	no
fort5 = 	no
fort6 = 	no
#capital = 	"




2000.1.1 = {
	owner = GK9
	controller = GK9
	add_core = GK9
}
