culture = italy_culture
#OLDREL religion =
religion = protestant
#religion =  catholic
capital = "Aosta"
discovered_by = HAT
discovered_by = latin
discovered_by = eastern
discovered_by = muslim
discovered_by = indian
discovered_by = chinese
trade_goods = grain
hre = no
italy_rename = no
citysize = 	25387
manpower = 	0.3
base_tax = 	3
smd_prepped = 	no
hre = 	no
#dev level = 	Developed
temple = 	yes
workshop = 	no
regimental_camp = 	no
constable = 	no
courthouse = 	no
marketplace = 	no
customs_house = 	no
fort1 = 	yes
fort2 = 	no
fort3 = 	no
fort4 = 	no
fort5 = 	no
fort6 = 	no
#capital = 	"

################################################################################
# Anything typed here will be added to all selected province history files
################################################################################
2000.1.1 = { owner = IT9 controller = IT9 add_core = IT9 } 