capital = "Siedlce"
culture = poland_culture
#OLDREL religion =
religion = reactionary_r
#religion =  catholic
trade_goods = grain
discovered_by = HAT
discovered_by = latin
discovered_by = eastern
discovered_by = muslim
discovered_by = indian
discovered_by = chinese
hre = no
europe_rename = no
citysize = 	160000
manpower = 	1.6
base_tax = 	3
smd_prepped = 	no
hre = 	no
#dev level = 	Developing
temple = 	yes
workshop = 	no
regimental_camp = 	no
constable = 	no
courthouse = 	no
marketplace = 	no
customs_house = 	no
fort1 = 	yes
fort2 = 	no
fort3 = 	no
fort4 = 	no
fort5 = 	no
fort6 = 	no
pop_rework = no	

################################################################################
# Anything typed here will be added to all selected province history files
################################################################################
2000.1.1 = { owner = PO9 controller = PO9 add_core = PO9 } 