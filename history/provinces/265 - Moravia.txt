# 265 - Moravia
culture = czech_republic_culture
#OLDREL religion =
religion = protestant
#religion =  protestant
discovered_by = HAT
discovered_by = latin
discovered_by = eastern
discovered_by = muslim
discovered_by = indian
discovered_by = chinese
terrain = forested_plain
capital = "Brno"
trade_goods = wine
citysize = 	400000
manpower = 	4
base_tax = 	8
smd_prepped = 	no
hre = 	no
#dev level = 	Developed
temple = 	yes
workshop = 	yes
regimental_camp = 	yes
constable = 	yes
courthouse = 	yes
marketplace = 	yes
customs_house = 	yes
fort1 = 	yes
fort2 = 	yes
fort3 = 	yes
fort4 = 	no
fort5 = 	no
fort6 = 	no
pop_rework = no	

################################################################################
# Anything typed here will be added to all selected province history files
################################################################################
2000.1.1 = { owner = CZ9 controller = CZ9 add_core = CZ9 } 