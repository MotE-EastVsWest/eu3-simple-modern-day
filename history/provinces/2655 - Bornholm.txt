# No previous file for Bornholm


culture = denmark_culture
#OLDREL religion =
religion = protestant
#religion =  protestant  
discovered_by = HAT 
discovered_by = latin 
discovered_by = eastern 
discovered_by = muslim 
discovered_by = indian 
discovered_by = chinese

trade_goods = fish

capital = "Ronne"

citysize = 	7866
manpower = 	0.1
base_tax = 	2
smd_prepped = 	no
hre = 	no
#dev level = 	Developed
temple = 	yes
workshop = 	no
regimental_camp = 	no
constable = 	no
courthouse = 	no
marketplace = 	no
customs_house = 	no
fort1 = 	yes
fort2 = 	no
fort3 = 	no
fort4 = 	no
fort5 = 	no
fort6 = 	no
#capital = 	"


2000.1.1 = {
	owner = DK9
	controller = DK9
	add_core = DK9
}