


culture = switzerland_culture  
#OLDREL religion =
religion = protestant
#religion =  reformed  
capital = "Lugano"
discovered_by = HAT 
discovered_by = latin 
discovered_by = eastern 
discovered_by = muslim 
discovered_by = indian 
discovered_by = chinese

hre = no
trade_goods = grain   

europe_rename = no

citysize = 	12637
manpower = 	0.1
base_tax = 	2
smd_prepped = 	no
hre = 	no
#dev level = 	Developed
temple = 	yes
workshop = 	no
regimental_camp = 	no
constable = 	no
courthouse = 	no
marketplace = 	no
customs_house = 	no
fort1 = 	yes
fort2 = 	no
fort3 = 	no
fort4 = 	no
fort5 = 	no
fort6 = 	no

2000.1.1 = {
	owner = CH9
	controller = CH9
	add_core = CH9
}
