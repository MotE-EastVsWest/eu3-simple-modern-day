

culture = switzerland_culture 
#OLDREL religion =
religion = protestant
#religion =  reformed
capital = "Geneva"
discovered_by = HAT 
discovered_by = latin 
discovered_by = eastern 
discovered_by = muslim 
discovered_by = indian 
discovered_by = chinese

hre = no
trade_goods = grain   

europe_rename = no

citysize = 	208953
manpower = 	2.1
base_tax = 	6
smd_prepped = 	no
hre = 	no
#dev level = 	Developed
temple = 	yes
workshop = 	yes
regimental_camp = 	yes
constable = 	yes
courthouse = 	yes
marketplace = 	yes
customs_house = 	no
fort1 = 	yes
fort2 = 	yes
fort3 = 	yes
fort4 = 	no
fort5 = 	no
fort6 = 	no


2000.1.1 = {
	owner = CH9
	controller = CH9
	add_core = CH9
}