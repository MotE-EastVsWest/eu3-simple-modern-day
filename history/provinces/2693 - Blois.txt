

culture = france_culture
#OLDREL religion =
religion = protestant
#religion =  catholic     
capital = "Blois"
discovered_by = HAT 
discovered_by = latin 
discovered_by = eastern 
discovered_by = muslim 
discovered_by = indian 
discovered_by = chinese
trade_goods = grain   

citysize = 	65907
manpower = 	0.7
base_tax = 	3
#smd_prepped = 	no
hre = 	no
#dev level = 	Developed
temple = 	yes
workshop = 	no
regimental_camp = 	no
constable = 	no
courthouse = 	no
marketplace = 	yes
customs_house = 	no
fort1 = 	yes
fort2 = 	no
fort3 = 	no
fort4 = 	no
fort5 = 	no
fort6 = 	no


2000.1.1 = {
	owner = FR9
	controller = FR9
	add_core = FR9
}