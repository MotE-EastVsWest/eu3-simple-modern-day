capital = "Vijayawada"
culture = india_culture
#OLDREL religion =
religion = protestant
#religion =  hinduism
trade_goods = fish
discovered_by = HAT
discovered_by = latin
discovered_by = eastern
discovered_by = muslim
discovered_by = indian
discovered_by = chinese
hre = no
citysize = 990000
manpower = 9.9
base_tax = 6
smd_prepped = no
hre = no
# dev level = 	Underdeveloped
temple = yes
workshop = no
regimental_camp = no
constable = no
courthouse = no
marketplace = yes
customs_house = no
fort1 = yes
fort2 = yes
fort3 = no
fort4 = no
fort5 = no
fort6 = no

cot = yes
################################################################################
# Anything typed here will be added to all selected province history files
################################################################################
2000.1.1 = { owner = IN9 controller = IN9 add_core = IN9 } 