
capital = "Agartala"

culture = tripura_culture  
#OLDREL religion =
religion = protestant
#religion =  hinduism
trade_goods = grain
discovered_by = HAT 
discovered_by = latin 
discovered_by = eastern 
discovered_by = muslim 
discovered_by = indian 
discovered_by = chinese
hre = no

citysize = 	429400
manpower = 	4.3
base_tax = 	4
smd_prepped = 	no
hre = 	no
#dev level = 	Underdeveloped
temple = 	yes
workshop = 	no
regimental_camp = 	no
constable = 	no
courthouse = 	no
marketplace = 	no
customs_house = 	no
fort1 = 	yes
fort2 = 	no
fort3 = 	no
fort4 = 	no
fort5 = 	no
fort6 = 	no


2000.1.1 = {
	owner = IN9
	controller = IN9
	add_core = I59
	add_core = IN9
	revolt = {
		type = nationalist_rebels
		size = 1
	}
}