
capital = "Itanagar"
culture = assam_culture  
#OLDREL religion =
religion = protestant
#religion =  hinduism

trade_goods = copper  
discovered_by = HAT 
discovered_by = latin 
discovered_by = eastern 
discovered_by = muslim 
discovered_by = indian 
discovered_by = chinese
hre = no

citysize = 	11988
manpower = 	0.1
base_tax = 	1
smd_prepped = 	no
hre = 	no
#dev level = 	Underdeveloped
temple = 	no
workshop = 	no
regimental_camp = 	no
constable = 	no
courthouse = 	no
marketplace = 	no
customs_house = 	no
fort1 = 	no
fort2 = 	no
fort3 = 	no
fort4 = 	no
fort5 = 	no
fort6 = 	no


2000.1.1 = {
	owner = IN9
	controller = IN9
	add_core = MM9
	add_core = IN9
	add_core = NA9
	add_core = TW9
}