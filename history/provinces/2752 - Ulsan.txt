

culture = korea_republic_culture  
#OLDREL religion =
religion = protestant
#religion =  protestant  

discovered_by = HAT 
discovered_by = latin 
discovered_by = eastern 
discovered_by = muslim 
discovered_by = indian 
discovered_by = chinese

capital = "Ulsan"



terrain = forested_plain


trade_goods =chinaware

citysize = 	222341
manpower = 	2.2
base_tax = 	6
#smd_prepped = 	no
hre = 	no
#dev level = 	Developed
temple = 	yes
workshop = 	yes
regimental_camp = 	yes
constable = 	yes
courthouse = 	yes
marketplace = 	yes
customs_house = 	no
fort1 = 	yes
fort2 = 	yes
fort3 = 	yes
fort4 = 	no
fort5 = 	no
fort6 = 	no



2000.1.1 = {
	owner = KO9
	controller = KO9
	add_core = KO9
	add_core = NK9
}