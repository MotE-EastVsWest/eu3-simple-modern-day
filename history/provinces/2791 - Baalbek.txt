
culture = lebanon_culture  
#OLDREL religion =
religion = reactionary_r
#religion =  sunni  

discovered_by = HAT 
discovered_by = latin 
discovered_by = eastern 
discovered_by = muslim 
discovered_by = indian 
discovered_by = chinese

capital = "Baalbek"

terrain = mountain


trade_goods = wool

citysize = 	108760
manpower = 	1.1
base_tax = 	2
smd_prepped = 	no
hre = 	no
#dev level = 	Underdeveloped
temple = 	no
workshop = 	no
regimental_camp = 	no
constable = 	no
courthouse = 	no
marketplace = 	no
customs_house = 	no
fort1 = 	yes
fort2 = 	no
fort3 = 	no
fort4 = 	no
fort5 = 	no
fort6 = 	no


2000.1.1 = {
	owner = LB8
	controller = LB8
	add_core = LB9
	add_core = LB8
}