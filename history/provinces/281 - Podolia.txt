# 281 - Podolia
culture = ukraine_culture
#OLDREL religion =
religion = protestant
#religion =  catholic
trade_goods = grain
terrain = forested_plain
discovered_by = HAT
discovered_by = latin
discovered_by = eastern
discovered_by = muslim
discovered_by = indian
discovered_by = chinese
capital = "Lviv"
citysize = 	503000
manpower = 	5
base_tax = 	6
smd_prepped = 	no
hre = 	no
#dev level = 	Developing
temple = 	yes
workshop = 	no
regimental_camp = 	yes
constable = 	yes
courthouse = 	yes
marketplace = 	yes
customs_house = 	no
fort1 = 	yes
fort2 = 	yes
fort3 = 	no
fort4 = 	no
fort5 = 	no
fort6 = 	no
pop_rework = no	

################################################################################
# Anything typed here will be added to all selected province history files
################################################################################
2000.1.1 = { owner = UA9 controller = UA9 add_core = UA9 } 