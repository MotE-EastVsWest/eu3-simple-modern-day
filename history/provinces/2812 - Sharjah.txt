#398 - Qawasim

culture = united_arab_emirates_culture
#OLDREL religion =
religion = conservative_r
#religion =  sunni


discovered_by = HAT 
discovered_by = latin 
discovered_by = eastern 
discovered_by = muslim 
discovered_by = indian 
discovered_by = chinese

capital = "Sharjah"



terrain = desert

trade_goods =wool

citysize = 	361400
manpower = 	3.6
base_tax = 	8
smd_prepped = 	no
hre = 	no
#dev level = 	Developed
temple = 	yes
workshop = 	yes
regimental_camp = 	yes
constable = 	yes
courthouse = 	yes
marketplace = 	yes
customs_house = 	yes
fort1 = 	yes
fort2 = 	yes
fort3 = 	yes
fort4 = 	no
fort5 = 	no
fort6 = 	no


2000.1.1 = {
	owner = AE9
	controller = AE9
	add_core = AE9
}