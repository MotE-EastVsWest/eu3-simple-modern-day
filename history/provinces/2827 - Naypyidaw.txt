culture = myanmar_culture  
#OLDREL religion =
religion = protestant
#religion =  buddhism  

discovered_by = HAT 
discovered_by = latin 
discovered_by = eastern 
discovered_by = muslim 
discovered_by = indian 
discovered_by = chinese

capital = "Naypyidaw"

terrain = forested_plain


trade_goods =spices

citysize = 	37955
manpower = 	0.4
base_tax = 	2
smd_prepped = 	no
hre = 	no
#dev level = 	Underdeveloped
temple = 	no
workshop = 	no
regimental_camp = 	no
constable = 	no
courthouse = 	no
marketplace = 	no
customs_house = 	no
fort1 = 	no
fort2 = 	no
fort3 = 	no
fort4 = 	no
fort5 = 	no
fort6 = 	no


2000.1.1 = {
	owner = YA9
	controller = YA9
	add_core = YA9
}