capital = "Medicine Hat"
culture = canada_culture
religion = protestant
trade_goods = grain
citysize = 12604
manpower = 0.1
base_tax = 2
# smd_prepped = 	no
hre = no
# dev level = 	Developed
temple = yes
workshop = no
regimental_camp = no
constable = no
courthouse = no
marketplace = no
customs_house = no
fort1 = yes
fort2 = no
fort3 = no
fort4 = no
fort5 = no
fort6 = no
discovered_by = HAT 
discovered_by = latin 
discovered_by = eastern 
discovered_by = muslim 
discovered_by = indian 
discovered_by = chinese

################################################################################
# Anything typed here will be added to all selected province history files
################################################################################
2000.1.1 = { owner = CA9 controller = CA9 add_core = CA9 } 