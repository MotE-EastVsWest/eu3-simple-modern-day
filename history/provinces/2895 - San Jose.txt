capital = "San Jose"
religion = protestant
culture = california_culture
trade_goods = naval_supplies
citysize = 202648
manpower = 2
base_tax = 6
# smd_prepped = 	no
hre = no
# dev level = 	Developed
temple = yes
workshop = yes
regimental_camp = yes
constable = yes
courthouse = yes
marketplace = yes
customs_house = no
fort1 = yes
fort2 = yes
fort3 = yes
fort4 = no
fort5 = no
fort6 = no
discovered_by = HAT 
discovered_by = latin 
discovered_by = eastern 
discovered_by = muslim 
discovered_by = indian 
discovered_by = chinese

################################################################################
# Anything typed here will be added to all selected province history files
################################################################################
2000.1.1 = { 
    owner = US9
    controller = US9
    add_core = US9
    add_core = CL9
}
