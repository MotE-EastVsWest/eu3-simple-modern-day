capital = "Neodesha"
religion = protestant
culture = united_states_of_america_culture
trade_goods = grain
citysize = 1100
manpower = 0.1
base_tax = 1
# smd_prepped = 	no
hre = no
# dev level = 	Developed
temple = no
workshop = no
regimental_camp = no
constable = no
courthouse = no
marketplace = no
customs_house = no
fort1 = no
fort2 = no
fort3 = no
fort4 = no
fort5 = no
fort6 = no
discovered_by = HAT 
discovered_by = latin 
discovered_by = eastern 
discovered_by = muslim 
discovered_by = indian 
discovered_by = chinese

################################################################################
# Anything typed here will be added to all selected province history files
################################################################################
2000.1.1 = { owner = US9 controller = US9 add_core = US9 } 