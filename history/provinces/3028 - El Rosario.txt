capital = "El Rosario"
trade_goods = fish
citysize = 1800
manpower = 0.1
base_tax = 1
# smd_prepped = 	no
hre = no
# dev level = 	Underdeveloped
temple = no
workshop = no
regimental_camp = no
constable = no
courthouse = no
marketplace = no
customs_house = no
fort1 = no
fort2 = no
fort3 = no
fort4 = no
fort5 = no
fort6 = no
discovered_by = HAT 
discovered_by = latin 
discovered_by = eastern 
discovered_by = muslim 
discovered_by = indian 
discovered_by = chinese

# Anything typed here will be added to all selected province history files
religion = protestant
culture = mexico_culture
################################################################################
# Anything typed here will be added to all selected province history files
################################################################################
2000.1.1 = { owner = MX9 controller = MX9 add_core = MX9 } 