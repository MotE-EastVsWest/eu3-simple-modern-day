capital = "Saltillo"
trade_goods = wool
citysize = 184727
manpower = 1.8
base_tax = 2
# smd_prepped = 	no
hre = no
# dev level = 	Underdeveloped
temple = no
workshop = no
regimental_camp = no
constable = no
courthouse = no
marketplace = no
customs_house = no
fort1 = yes
fort2 = no
fort3 = no
fort4 = no
fort5 = no
fort6 = no
discovered_by = HAT 
discovered_by = latin 
discovered_by = eastern 
discovered_by = muslim 
discovered_by = indian 
discovered_by = chinese

# Anything typed here will be added to all selected province history files
religion = protestant
culture = mexico_culture
################################################################################
# Anything typed here will be added to all selected province history files
################################################################################
2000.1.1 = { owner = MX9 controller = MX9 add_core = MX9 } 