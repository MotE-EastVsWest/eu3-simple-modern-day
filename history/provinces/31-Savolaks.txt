#Savolaks, incl. Nyslott(Olofsborg).



culture = finland_culture  
#OLDREL religion =
religion = protestant
#religion =  protestant  
trade_goods =iron


terrain = forested_plain

discovered_by = HAT 
discovered_by = latin 
discovered_by = eastern 
discovered_by = muslim 
discovered_by = indian 
discovered_by = chinese

capital = "Joensuu"
citysize = 	57400
manpower = 	0.6
base_tax = 	2
smd_prepped = 	no
hre = 	no
#dev level = 	Developing
temple = 	no
workshop = 	no
regimental_camp = 	no
constable = 	no
courthouse = 	no
marketplace = 	no
customs_house = 	no
fort1 = 	yes
fort2 = 	no
fort3 = 	no
fort4 = 	no
fort5 = 	no
fort6 = 	no
#capital = 	"



2000.1.1 = {
	owner = FI9
	controller = FI9
	add_core = FI9
}