
culture = brazil_culture 
religion = protestant

discovered_by = HAT 
discovered_by = latin 
discovered_by = eastern 
discovered_by = muslim 
discovered_by = indian 
discovered_by = chinese

citysize = 	26528
manpower = 	0.3
base_tax = 	2
#smd_prepped = 	no
hre = 	no
#dev level = 	Underdeveloped
temple = 	no
workshop = 	no
regimental_camp = 	no
constable = 	no
courthouse = 	no
marketplace = 	no
customs_house = 	no
fort1 = 	no
fort2 = 	no
fort3 = 	no
fort4 = 	no
fort5 = 	no
fort6 = 	no
capital = 	"Sao Mateus"

trade_goods = fish
2000.1.1 = {
	owner = BR9
	controller = BR9
	add_core = BR9
}