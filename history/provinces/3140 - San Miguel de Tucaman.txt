
culture = argentina_culture 
religion = protestant

discovered_by = HAT 
discovered_by = latin 
discovered_by = eastern 
discovered_by = muslim 
discovered_by = indian 
discovered_by = chinese
trade_goods = grain

citysize = 	166000
manpower = 	1.7
base_tax = 	2
#smd_prepped = 	no
hre = 	no
#dev level = 	Underdeveloped
temple = 	no
workshop = 	no
regimental_camp = 	no
constable = 	no
courthouse = 	no
marketplace = 	no
customs_house = 	no
fort1 = 	yes
fort2 = 	no
fort3 = 	no
fort4 = 	no
fort5 = 	no
fort6 = 	no
capital = 	"San Miguel de Tucaman"

2000.1.1 = {
	owner = AG9
	controller = AG9
	add_core = AG9
}