
culture = nigeria_culture 
religion = protestant

discovered_by = HAT 
discovered_by = latin 
discovered_by = eastern 
discovered_by = muslim 
discovered_by = indian 
discovered_by = chinese

citysize = 	90321
manpower = 	0.9
base_tax = 	2
#smd_prepped = 	no
hre = 	no
#dev level = 	Underdeveloped
temple = 	no
workshop = 	no
regimental_camp = 	no
constable = 	no
courthouse = 	no
marketplace = 	no
customs_house = 	no
fort1 = 	yes
fort2 = 	no
fort3 = 	no
fort4 = 	no
fort5 = 	no
fort6 = 	no
capital = 	"Abeokuta"
trade_goods = ivory

2000.1.1 = {
	owner = NI9
	controller = NI9
	add_core = NI9
}