
culture = ethiopia_culture 
religion = conservative_r

discovered_by = HAT 
discovered_by = latin 
discovered_by = eastern 
discovered_by = muslim 
discovered_by = indian 
discovered_by = chinese

trade_goods = wool

citysize = 	9000
manpower = 	0.1
base_tax = 	1
#smd_prepped = 	no
hre = 	no
#dev level = 	Underdeveloped
temple = 	no
workshop = 	no
regimental_camp = 	no
constable = 	no
courthouse = 	no
marketplace = 	no
customs_house = 	no
fort1 = 	no
fort2 = 	no
fort3 = 	no
fort4 = 	no
fort5 = 	no
fort6 = 	no
capital = 	"Kebri Dehar"

2000.1.1 = {
	owner = ET9
	controller = ET9
	add_core = LA9
	add_core = LA8
	add_core = ET9
}