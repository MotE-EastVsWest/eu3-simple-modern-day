#322 - Anatolia


culture = turkey_culture
#OLDREL religion =
religion = protestant
#religion =  sunni


discovered_by = HAT 
discovered_by = latin 
discovered_by = eastern 
discovered_by = muslim 
discovered_by = indian 
discovered_by = chinese

capital = "Eskisehir"


terrain = steppe

trade_goods =cloth

citysize = 	181323
manpower = 	1.8
base_tax = 	3
smd_prepped = 	no
hre = 	no
#dev level = 	Developing
temple = 	yes
workshop = 	no
regimental_camp = 	no
constable = 	no
courthouse = 	no
marketplace = 	no
customs_house = 	no
fort1 = 	yes
fort2 = 	no
fort3 = 	no
fort4 = 	no
fort5 = 	no
fort6 = 	no
pop_rework = no	




2000.1.1 = {
	owner = TR9
	controller = TR9
	add_core = TR9
}