culture = pakistan_culture
religion = protestant

citysize = 	809800
manpower = 	8.1
base_tax = 	5
smd_prepped = 	no
hre = 	no
#dev level = 	Underdeveloped
temple = 	yes
workshop = 	no
regimental_camp = 	no
constable = 	no
courthouse = 	no
marketplace = 	no
customs_house = 	no
fort1 = 	yes
fort2 = 	yes
fort3 = 	no
fort4 = 	no
fort5 = 	no
fort6 = 	no
pop_rework = no	

trade_goods = grain

capital = "Sheikhupura"
discovered_by = HAT
discovered_by = latin
discovered_by = eastern
discovered_by = muslim
discovered_by = indian
discovered_by = chinese

2000.1.1 = {
	owner = PK9
	controller = PK9
	add_core = PK9
}