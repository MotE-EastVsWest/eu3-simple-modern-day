# N�set + the land around the river Neva, incl. N�teborg, Nyen and St.Petersburg.
trade_goods = fur
culture = russia_culture
#OLDREL religion =
religion = protestant
#religion =  orthodox
terrain = forested_plain
discovered_by = HAT
discovered_by = latin
discovered_by = eastern
discovered_by = muslim
discovered_by = indian
discovered_by = chinese
capital = "St. Petersburg"
citysize = 990000
manpower = 9.9
base_tax = 10
smd_prepped = no
hre = no
# dev level = 	Developed
temple = yes
workshop = yes
regimental_camp = yes
constable = yes
courthouse = yes
marketplace = yes
customs_house = yes
fort1 = yes
fort2 = yes
fort3 = yes
fort4 = yes
fort5 = no
fort6 = no

cot = yes
################################################################################
# Anything typed here will be added to all selected province history files
################################################################################
2000.1.1 = { owner = RU9 controller = RU9 add_core = RU9 } 