culture = china_people's_republic_culture
religion = protestant

citysize = 	70000
manpower = 	0.7
base_tax = 	2
smd_prepped = 	no
hre = 	no
#dev level = 	Underdeveloped
temple = 	no
workshop = 	no
regimental_camp = 	no
constable = 	no
courthouse = 	no
marketplace = 	no
customs_house = 	no
fort1 = 	yes
fort2 = 	no
fort3 = 	no
fort4 = 	no
fort5 = 	no
fort6 = 	no
#capital = 	"

trade_goods = wool

capital = "Xilingol"
discovered_by = HAT
discovered_by = latin
discovered_by = eastern
discovered_by = muslim
discovered_by = indian
discovered_by = chinese
2000.1.1 = { 
    owner = NA9
    controller = NA9
    add_core = NA9
    add_core = TW9
}