culture = tibet_culture
religion = protestant

citysize = 	19097
manpower = 	0.2
base_tax = 	1
smd_prepped = 	no
hre = 	no
#dev level = 	Underdeveloped
temple = 	no
workshop = 	no
regimental_camp = 	no
constable = 	no
courthouse = 	no
marketplace = 	no
customs_house = 	no
fort1 = 	no
fort2 = 	no
fort3 = 	no
fort4 = 	no
fort5 = 	no
fort6 = 	no
#capital = 	"

trade_goods = iron

capital = "Ngari"
discovered_by = HAT
discovered_by = latin
discovered_by = eastern
discovered_by = muslim
discovered_by = indian
discovered_by = chinese


2000.1.1 = { 
    owner = NA9
    controller = NA9
    add_core = TB9
    add_core = NA9
    add_core = TW9
}
