culture = russia_culture 
religion = protestant

citysize = 	112781
manpower = 	1.1
base_tax = 	3
smd_prepped = 	no
hre = 	no
#dev level = 	Developing
temple = 	yes
workshop = 	no
regimental_camp = 	no
constable = 	no
courthouse = 	no
marketplace = 	yes
customs_house = 	no
fort1 = 	yes
fort2 = 	no
fort3 = 	no
fort4 = 	no
fort5 = 	no
fort6 = 	no
#capital = 	"

trade_goods = fur

capital = "Komsomolsk"
discovered_by = HAT
discovered_by = latin
discovered_by = eastern
discovered_by = muslim
discovered_by = indian
discovered_by = chinese

2000.1.1 = {
	owner = RU9
	controller = RU9
	add_core = RU9
}