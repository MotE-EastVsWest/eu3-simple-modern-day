
culture = china_republic_culture
religion = protestant
trade_goods = fish

discovered_by = HAT
discovered_by = latin
discovered_by = eastern
discovered_by = muslim
discovered_by = indian
discovered_by = chinese
capital = "Kinmen"

citysize = 	50000
manpower = 	0.5
base_tax = 	3
smd_prepped = 	no
hre = 	no
#dev level = 	Developed
temple = 	yes
workshop = 	no
regimental_camp = 	no
constable = 	no
courthouse = 	no
marketplace = 	yes
customs_house = 	no
fort1 = 	yes
fort2 = 	yes
fort3 = 	no
fort4 = 	no
fort5 = 	no
fort6 = 	no
#capital = 	"




2000.1.1 = { 
    owner = TW9
    controller = TW9
    add_core = TW9
    add_core = NA9
}