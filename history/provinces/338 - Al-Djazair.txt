#338 - Al-Djazair


culture = algeria_culture
#OLDREL religion =
religion = liberal_r
#religion =  sunni

discovered_by = HAT 
discovered_by = latin 
discovered_by = eastern 
discovered_by = muslim 
discovered_by = indian 
discovered_by = chinese


terrain = mountain

capital = "Algiers"

trade_goods =grain

citysize = 	902000
manpower = 	9
base_tax = 	7
smd_prepped = 	no
hre = 	no
#dev level = 	Developing
temple = 	yes
workshop = 	yes
regimental_camp = 	yes
constable = 	yes
courthouse = 	yes
marketplace = 	yes
customs_house = 	yes
fort1 = 	yes
fort2 = 	yes
fort3 = 	yes
fort4 = 	no
fort5 = 	no
fort6 = 	no

2000.1.1 = {
	owner = DZ9
	controller = DZ9
	add_core = DZ9
	add_core = DZ8
}
