culture = algeria_culture

religion = protestant
discovered_by = HAT
discovered_by = latin
discovered_by = eastern
discovered_by = muslim
discovered_by = indian
discovered_by = chinese
capital = "Djelfa"

citysize = 	98050
manpower = 	1
base_tax = 	2
smd_prepped = 	no
hre = 	no
#dev level = 	Underdeveloped
temple = 	no
workshop = 	no
regimental_camp = 	no
constable = 	no
courthouse = 	no
marketplace = 	no
customs_house = 	no
fort1 = 	yes
fort2 = 	no
fort3 = 	no
fort4 = 	no
fort5 = 	no
fort6 = 	no
#capital = 	"
trade_goods = grain

2000.1.1 = {
	owner = DZ9
	controller = DZ9
	add_core = DZ9
	add_core = DZ8
}