culture = vietnam_culture
religion = protestant
capital = "Truong Sa"

discovered_by = HAT 
discovered_by = latin 
discovered_by = eastern 
discovered_by = muslim 
discovered_by = indian 
discovered_by = chinese

citysize = 	1100
manpower = 	0.1
base_tax = 	1
smd_prepped = 	no
hre = 	no
#dev level = 	Underdeveloped
temple = 	no
workshop = 	no
regimental_camp = 	no
constable = 	no
courthouse = 	no
marketplace = 	no
customs_house = 	no
fort1 = 	no
fort2 = 	no
fort3 = 	no
fort4 = 	no
fort5 = 	no
fort6 = 	no
pop_rework = no	

trade_goods = fish



2000.1.1 = {
	owner = VT9
	controller = VT9
	add_core = VT9
	add_core = NA9
	add_core = TW9
	add_core = PH9
	add_core = MY9
	add_core = BI9
}