#356 - Cyrenaica


culture = libya_republic_culture 
#OLDREL religion =
religion = conservative_r
#religion =  sunni


discovered_by = HAT 
discovered_by = latin 
discovered_by = eastern 
discovered_by = muslim 
discovered_by = indian 
discovered_by = chinese

terrain = mountain


capital = "Benghazi"

trade_goods =wool

citysize = 	360000
manpower = 	3.6
base_tax = 	4
smd_prepped = 	no
hre = 	no
#dev level = 	Underdeveloped
temple = 	yes
workshop = 	no
regimental_camp = 	no
constable = 	no
courthouse = 	no
marketplace = 	no
customs_house = 	no
fort1 = 	yes
fort2 = 	no
fort3 = 	no
fort4 = 	no
fort5 = 	no
fort6 = 	no
pop_rework = no	


2000.1.1 = {
	owner = LY9
	controller = LY9
	add_core = IY9
	add_core = LY9
}
