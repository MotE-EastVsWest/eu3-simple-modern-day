#364 - Gaza


culture = palestine_culture 
#OLDREL religion =
religion = conservative_r
#religion =  sunni

discovered_by = HAT 
discovered_by = latin 
discovered_by = eastern 
discovered_by = muslim 
discovered_by = indian 
discovered_by = chinese

capital = "Hebron"


terrain = steppe


trade_goods =salt

citysize = 	449849
manpower = 	4.5
base_tax = 	5
smd_prepped = 	no
hre = 	no
#dev level = 	Developing
temple = 	yes
workshop = 	no
regimental_camp = 	no
constable = 	yes
courthouse = 	no
marketplace = 	yes
customs_house = 	no
fort1 = 	yes
fort2 = 	yes
fort3 = 	no
fort4 = 	no
fort5 = 	no
fort6 = 	no

2000.1.1 = {
	owner = PS9
	controller = PS9
	add_core = PS9
	add_core = IL9
}