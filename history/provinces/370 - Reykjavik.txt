#370 - Reykjavik

trade_goods =fish



culture = iceland_culture  
#OLDREL religion =
religion = protestant
#religion =  protestant


discovered_by = HAT 
discovered_by = latin 
discovered_by = eastern 
discovered_by = muslim 
discovered_by = indian 
discovered_by = chinese

capital = "Reykjavik"

citysize = 	68176
manpower = 	0.7
base_tax = 	3
smd_prepped = 	no
hre = 	no
#dev level = 	Developed
temple = 	yes
workshop = 	no
regimental_camp = 	no
constable = 	no
courthouse = 	no
marketplace = 	yes
customs_house = 	no
fort1 = 	yes
fort2 = 	no
fort3 = 	no
fort4 = 	no
fort5 = 	no
fort6 = 	no
#capital = 	"

2000.1.1 = {
	owner = IC9
	controller = IC9
	add_core = IC9
}