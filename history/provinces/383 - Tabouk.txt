#383 - Tabouk


culture = saudi_arabia_culture  
#OLDREL religion =
religion = conservative_r
#religion =  sunni

terrain = mountain

discovered_by = HAT 
discovered_by = latin 
discovered_by = eastern 
discovered_by = muslim 
discovered_by = indian 
discovered_by = chinese

capital = "Tabouk"

trade_goods =wool

citysize = 	182006
manpower = 	1.8
base_tax = 	3
smd_prepped = 	no
hre = 	no
#dev level = 	Developing
temple = 	yes
workshop = 	no
regimental_camp = 	no
constable = 	no
courthouse = 	no
marketplace = 	no
customs_house = 	no
fort1 = 	yes
fort2 = 	no
fort3 = 	no
fort4 = 	no
fort5 = 	no
fort6 = 	no
pop_rework = no	


2000.1.1 = {
	owner = SA9
	controller = SA9
	add_core = SA9
}