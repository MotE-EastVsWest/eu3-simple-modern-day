#388 - Aden

#owner = YM4
culture = south_yemen_culture 
#OLDREL religion =
religion = protestant
#religion =  sunni

discovered_by = HAT 
discovered_by = latin 
discovered_by = eastern 
discovered_by = muslim 
discovered_by = indian 
discovered_by = chinese

terrain = mountain


capital = "Aden"

trade_goods =spices

citysize = 	215934
manpower = 	2.2
base_tax = 	3
smd_prepped = 	no
hre = 	no
#dev level = 	Underdeveloped
temple = 	no
workshop = 	no
regimental_camp = 	no
constable = 	no
courthouse = 	no
marketplace = 	no
customs_house = 	no
fort1 = 	yes
fort2 = 	no
fort3 = 	no
fort4 = 	no
fort5 = 	no
fort6 = 	no

2000.1.1 = {
	owner = YE9
	controller = YE9
	add_core = Y19
	add_core = YE9
}