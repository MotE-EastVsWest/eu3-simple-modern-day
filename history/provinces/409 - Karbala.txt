#409 - Karbala


culture = iraq_shia_culture 
#OLDREL religion =
religion = reactionary_r
#religion =  shiite

discovered_by = HAT 
discovered_by = latin 
discovered_by = eastern 
discovered_by = muslim 
discovered_by = indian 
discovered_by = chinese


terrain = steppe

capital = "Karbala"

trade_goods =spices

citysize = 	243746
manpower = 	2.4
base_tax = 	3
smd_prepped = 	no
hre = 	no
#dev level = 	Underdeveloped
temple = 	no
workshop = 	no
regimental_camp = 	no
constable = 	no
courthouse = 	no
marketplace = 	no
customs_house = 	no
fort1 = 	yes
fort2 = 	no
fort3 = 	no
fort4 = 	no
fort5 = 	no
fort6 = 	no


2000.1.1 = {
	owner = IQ9
	controller = IQ9
	add_core = Q29
	add_core = IQ9
}