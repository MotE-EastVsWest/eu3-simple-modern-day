#411 - Mosul


culture = iraq_sunni_culture 
#OLDREL religion =
religion = conservative_r
#religion =  sunni

discovered_by = HAT 
discovered_by = latin 
discovered_by = eastern 
discovered_by = muslim 
discovered_by = indian 
discovered_by = chinese

capital = "Mosul"


terrain = steppe

trade_goods =grain

citysize = 	546000
manpower = 	5.5
base_tax = 	4
smd_prepped = 	no
hre = 	no
#dev level = 	Underdeveloped
temple = 	yes
workshop = 	no
regimental_camp = 	no
constable = 	no
courthouse = 	no
marketplace = 	no
customs_house = 	no
fort1 = 	yes
fort2 = 	no
fort3 = 	no
fort4 = 	no
fort5 = 	no
fort6 = 	no
pop_rework = no	



2000.1.1 = {
	owner = IQ9
	controller = IQ9
	add_core = IQ9
}