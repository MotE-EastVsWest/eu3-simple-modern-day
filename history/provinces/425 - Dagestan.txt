#425 - Dagestan


culture = dagestan_culture 
#OLDREL religion =
religion = reactionary_r
#religion =  sunni  

discovered_by = HAT 
discovered_by = latin 
discovered_by = eastern 
discovered_by = muslim 
discovered_by = indian 
discovered_by = chinese

capital = "Kochubei"


terrain = steppe

trade_goods =wool

citysize = 	80000
manpower = 	0.8
base_tax = 	2
smd_prepped = 	no
hre = 	no
#dev level = 	Developing
temple = 	no
workshop = 	no
regimental_camp = 	no
constable = 	no
courthouse = 	no
marketplace = 	no
customs_house = 	no
fort1 = 	yes
fort2 = 	no
fort3 = 	no
fort4 = 	no
fort5 = 	no
fort6 = 	no
pop_rework = no	


2000.1.1 = {
	owner = RU9
	controller = RU9
	add_core = DG9
	add_core = RU9
	revolt = {
		type = nationalist_rebels
		size = 1
	}
}