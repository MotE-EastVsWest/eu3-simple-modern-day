# 441 - Khiva
culture = uzbekistan_culture
#OLDREL religion =
religion = protestant
#religion =  sunni
discovered_by = HAT
discovered_by = latin
discovered_by = eastern
discovered_by = muslim
discovered_by = indian
discovered_by = chinese
capital = "Urgench"
terrain = desert
trade_goods = wool
citysize = 29000
manpower = 0.3
base_tax = 2
smd_prepped = no
hre = no
# dev level = 	Underdeveloped
temple = no
workshop = no
regimental_camp = no
constable = no
courthouse = no
marketplace = no
customs_house = no
fort1 = no
fort2 = no
fort3 = no
fort4 = no
fort5 = no
fort6 = no
################################################################################
# Anything typed here will be added to all selected province history files
################################################################################
2000.1.1 = { owner = UZ9 controller = UZ9 add_core = UZ9 } 