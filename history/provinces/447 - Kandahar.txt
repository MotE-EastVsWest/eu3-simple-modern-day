#447 - Kandahar

culture = afghanistan_emirate_culture
#OLDREL religion =
religion = conservative_r
#religion =  sunni
discovered_by = HAT 
discovered_by = latin 
discovered_by = eastern 
discovered_by = muslim 
discovered_by = indian 
discovered_by = chinese

capital = "Kandahar"


terrain = desert

trade_goods =wool

citysize = 	170297
manpower = 	1.7
base_tax = 	2
smd_prepped = 	no
hre = 	no
#dev level = 	Underdeveloped
temple = 	no
workshop = 	no
regimental_camp = 	no
constable = 	no
courthouse = 	no
marketplace = 	no
customs_house = 	no
fort1 = 	yes
fort2 = 	no
fort3 = 	no
fort4 = 	no
fort5 = 	no
fort6 = 	no


2000.1.1 = {
	owner = AF9
	controller = AF9
	add_core = AF9
	add_core = FG9
}