#448 - Gazni


culture = afghanistan_emirate_culture  
#OLDREL religion =
religion = reactionary_r
#religion =  sunni

discovered_by = HAT 
discovered_by = latin 
discovered_by = eastern 
discovered_by = muslim 
discovered_by = indian 
discovered_by = chinese

capital = "Ghazni"


terrain = desert

trade_goods =grain

citysize = 	277353
manpower = 	2.8
base_tax = 	3
smd_prepped = 	no
hre = 	no
#dev level = 	Underdeveloped
temple = 	no
workshop = 	no
regimental_camp = 	no
constable = 	no
courthouse = 	no
marketplace = 	no
customs_house = 	no
fort1 = 	yes
fort2 = 	no
fort3 = 	no
fort4 = 	no
fort5 = 	no
fort6 = 	no
pop_rework = no	



2000.1.1 = {
	owner = AF9
	controller = REB
	add_core = AF9
	add_core = FG9
	revolt = {
		type = gang_rebels
		size = 1
	}
}