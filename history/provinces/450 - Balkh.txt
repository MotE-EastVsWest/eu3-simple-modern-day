#450 - Balkh

culture = afghanistan_republic_culture
#OLDREL religion =
religion = conservative_r
#religion =  sunni

discovered_by = HAT 
discovered_by = latin 
discovered_by = eastern 
discovered_by = muslim 
discovered_by = indian 
discovered_by = chinese

capital = "Mazar-i-Sharif"


terrain = mountain

trade_goods =wool


citysize = 	120041
manpower = 	1.2
base_tax = 	2
smd_prepped = 	no
hre = 	no
#dev level = 	Underdeveloped
temple = 	no
workshop = 	no
regimental_camp = 	no
constable = 	no
courthouse = 	no
marketplace = 	no
customs_house = 	no
fort1 = 	yes
fort2 = 	no
fort3 = 	no
fort4 = 	no
fort5 = 	no
fort6 = 	no

2000.1.1 = {
	owner = AF9
	controller = AF9
	add_core = AF9
	add_core = FG9
}
