#460 - Tian Shan


culture = kyrgyzstan_culture   
#OLDREL religion =
religion = protestant
#religion =  sunni

discovered_by = HAT 
discovered_by = latin 
discovered_by = eastern 
discovered_by = muslim 
discovered_by = indian 
discovered_by = chinese

capital = "Bishkek"

terrain = mountain


trade_goods =wool

citysize = 	229000
manpower = 	2.3
base_tax = 	4
smd_prepped = 	no
hre = 	no
#dev level = 	Developing
temple = 	yes
workshop = 	no
regimental_camp = 	no
constable = 	no
courthouse = 	no
marketplace = 	no
customs_house = 	no
fort1 = 	yes
fort2 = 	yes
fort3 = 	no
fort4 = 	no
fort5 = 	no
fort6 = 	no

2000.1.1 = {
	owner = YR9
	controller = YR9
	add_core = YR9
}