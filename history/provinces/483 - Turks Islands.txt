#483 - Turks Islands



culture = turks_n_caicos_culture   
#OLDREL religion =
religion = protestant
#religion =  protestant

discovered_by = HAT 
discovered_by = latin 
discovered_by = eastern 
discovered_by = muslim 
discovered_by = indian 
discovered_by = chinese

capital = "Grand Turk"

trade_goods = sugar 

citysize = 	8908
manpower = 	0.1
base_tax = 	1
smd_prepped = 	no
hre = 	no
#dev level = 	Developing
temple = 	no
workshop = 	no
regimental_camp = 	no
constable = 	no
courthouse = 	no
marketplace = 	no
customs_house = 	no
fort1 = 	yes
fort2 = 	no
fort3 = 	no
fort4 = 	no
fort5 = 	no
fort6 = 	no


2000.1.1 = {
	owner = EN9
	controller = EN9
	add_core = TC9
	add_core = EN9
}