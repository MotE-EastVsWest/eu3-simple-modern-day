#496 - Antigua

culture = antigua_and_barbuda_culture 
#OLDREL religion =
religion = protestant
#religion =  protestant 

discovered_by = HAT 
discovered_by = latin 
discovered_by = eastern 
discovered_by = muslim 
discovered_by = indian 
discovered_by = chinese



capital = "St. John"

		
trade_goods =sugar 

citysize = 	20154
manpower = 	0.2
base_tax = 	2
smd_prepped = 	no
hre = 	no
#dev level = 	Developing
temple = 	no
workshop = 	no
regimental_camp = 	no
constable = 	no
courthouse = 	no
marketplace = 	no
customs_house = 	no
fort1 = 	yes
fort2 = 	no
fort3 = 	no
fort4 = 	no
fort5 = 	no
fort6 = 	no

2000.1.1 = {
	owner = AB9
	controller = AB9
	add_core = AB9
}