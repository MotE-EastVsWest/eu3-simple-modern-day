#500 - St. Lucia


culture = st._lucia_culture    
#OLDREL religion =
religion = protestant
#religion =  protestant
discovered_by = HAT 
discovered_by = latin 
discovered_by = eastern 
discovered_by = muslim 
discovered_by = indian 
discovered_by = chinese

capital = "Castries"


terrain = mountain

trade_goods =sugar

citysize = 	35739
manpower = 	0.4
base_tax = 	2
smd_prepped = 	no
hre = 	no
#dev level = 	Developing
temple = 	no
workshop = 	no
regimental_camp = 	no
constable = 	no
courthouse = 	no
marketplace = 	no
customs_house = 	no
fort1 = 	yes
fort2 = 	no
fort3 = 	no
fort4 = 	no
fort5 = 	no
fort6 = 	no

2000.1.1 = {
	owner = LC9
	controller = LC9
	add_core = LC9
}