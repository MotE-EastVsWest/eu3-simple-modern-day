#522 - Delhi


culture = kashmir_culture 
#OLDREL religion =
religion = protestant
#religion =  sunni  

discovered_by = HAT 
discovered_by = latin 
discovered_by = eastern 
discovered_by = muslim 
discovered_by = indian 
discovered_by = chinese


terrain = mountain

capital = "Leh"

trade_goods =cloth

citysize = 	6174
manpower = 	0.1
base_tax = 	1
smd_prepped = 	no
hre = 	no
#dev level = 	Underdeveloped
temple = 	no
workshop = 	no
regimental_camp = 	no
constable = 	no
courthouse = 	no
marketplace = 	no
customs_house = 	no
fort1 = 	no
fort2 = 	no
fort3 = 	no
fort4 = 	no
fort5 = 	no
fort6 = 	no


2000.1.1 = {
	owner = IN9
	controller = IN9
	add_core = KH9
	add_core = PK9
	add_core = IN9
}