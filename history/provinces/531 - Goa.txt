# 531 - Goa
discovered_by = HAT
discovered_by = latin
discovered_by = eastern
discovered_by = muslim
discovered_by = indian
discovered_by = chinese
culture = india_culture
terrain = forested_plain
capital = "Jodhpur"
#OLDREL religion =
religion = reactionary_r
#religion =  hinduism
trade_goods = spices
citysize = 460000
manpower = 4.6
base_tax = 4
smd_prepped = no
hre = no
# dev level = 	Underdeveloped
temple = yes
workshop = no
regimental_camp = no
constable = no
courthouse = no
marketplace = no
customs_house = no
fort1 = yes
fort2 = no
fort3 = no
fort4 = no
fort5 = no
fort6 = no
################################################################################
# Anything typed here will be added to all selected province history files
################################################################################
2000.1.1 = { owner = IN9 controller = IN9 add_core = IN9 } 