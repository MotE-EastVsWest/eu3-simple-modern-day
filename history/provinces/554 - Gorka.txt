#554 - Gorka


culture = nepal_culture  
#OLDREL religion =
religion = socialist_r
#religion =  hinduism   

discovered_by = HAT 
discovered_by = latin 
discovered_by = eastern 
discovered_by = muslim 
discovered_by = indian 
discovered_by = chinese


terrain = mountain

capital = "Pokhara"

trade_goods =grain

citysize = 	119901
manpower = 	1.2
base_tax = 	2
smd_prepped = 	no
hre = 	no
#dev level = 	Underdeveloped
temple = 	no
workshop = 	no
regimental_camp = 	no
constable = 	no
courthouse = 	no
marketplace = 	no
customs_house = 	no
fort1 = 	yes
fort2 = 	no
fort3 = 	no
fort4 = 	no
fort5 = 	no
fort6 = 	no

2000.1.1 = {
	owner = NE8
	controller = NE8
	add_core = NE9
	add_core = NE8
}