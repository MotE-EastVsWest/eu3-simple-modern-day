#557 - Katmandu

culture = nepal_culture
#OLDREL religion =
religion = protestant
#religion =  hinduism   

discovered_by = HAT 
discovered_by = latin 
discovered_by = eastern 
discovered_by = muslim 
discovered_by = indian 
discovered_by = chinese


terrain = mountain

capital = "Kathmandu"

trade_goods =wool

citysize = 	580000
manpower = 	5.8
base_tax = 	4
smd_prepped = 	no
hre = 	no
#dev level = 	Underdeveloped
temple = 	yes
workshop = 	no
regimental_camp = 	no
constable = 	no
courthouse = 	no
marketplace = 	no
customs_house = 	no
fort1 = 	yes
fort2 = 	no
fort3 = 	no
fort4 = 	no
fort5 = 	no
fort6 = 	no


2000.1.1 = {
	owner = NE9
	controller = NE9
	add_core = NE9
	add_core = NE8
}