# 562 - Koch
culture = india_culture
#OLDREL religion =
religion = socialist_r
#religion =  catholic
discovered_by = HAT
discovered_by = latin
discovered_by = eastern
discovered_by = muslim
discovered_by = indian
discovered_by = chinese
capital = "Sambalpur"
terrain = forested_plain
trade_goods = cotton
citysize = 67152
manpower = 0.7
base_tax = 2
smd_prepped = no
hre = no
# dev level = 	Underdeveloped
temple = no
workshop = no
regimental_camp = no
constable = no
courthouse = no
marketplace = no
customs_house = no
fort1 = yes
fort2 = no
fort3 = no
fort4 = no
fort5 = no
fort6 = no
################################################################################
# Anything typed here will be added to all selected province history files
################################################################################
2000.1.1 = { owner = IN9 controller = IN9 add_core = IN9 } 