#577 - Quetta


culture = balochistan_culture 
#OLDREL religion =
religion = reactionary_r
#religion =  sunni


discovered_by = HAT 
discovered_by = latin 
discovered_by = eastern 
discovered_by = muslim 
discovered_by = indian 
discovered_by = chinese

capital = "Quetta"

terrain = desert


trade_goods =grain


citysize = 	200241
manpower = 	2
base_tax = 	3
smd_prepped = 	no
hre = 	no
#dev level = 	Underdeveloped
temple = 	no
workshop = 	no
regimental_camp = 	no
constable = 	no
courthouse = 	no
marketplace = 	no
customs_house = 	no
fort1 = 	yes
fort2 = 	no
fort3 = 	no
fort4 = 	no
fort5 = 	no
fort6 = 	no

2000.1.1 = {
	owner = PK9
	controller = PK9
	add_core = PK9
	add_core = BL9
}
