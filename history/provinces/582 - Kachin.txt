#582 - Kachin

culture = kachin_culture    
#OLDREL religion =
religion = protestant
#religion =  buddhism  


discovered_by = HAT 
discovered_by = latin 
discovered_by = eastern 
discovered_by = muslim 
discovered_by = indian 
discovered_by = chinese

capital = "Myitkyina"

terrain = mountain


trade_goods =grain


citysize = 	162190
manpower = 	1.6
base_tax = 	2
smd_prepped = 	no
hre = 	no
#dev level = 	Underdeveloped
temple = 	no
workshop = 	no
regimental_camp = 	no
constable = 	no
courthouse = 	no
marketplace = 	no
customs_house = 	no
fort1 = 	yes
fort2 = 	no
fort3 = 	no
fort4 = 	no
fort5 = 	no
fort6 = 	no

2000.1.1 = {
	owner = YA9
	controller = YA9
	add_core = K29
	add_core = YA9
}