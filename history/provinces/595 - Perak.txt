#595 - Perak


culture = malaysia_culture  
#OLDREL religion =
religion = protestant
#religion =  buddhism

discovered_by = HAT 
discovered_by = latin 
discovered_by = eastern 
discovered_by = muslim 
discovered_by = indian 
discovered_by = chinese

capital = "George Town"


terrain = jungle

trade_goods =spices

citysize = 	566791
manpower = 	5.7
base_tax = 	6
smd_prepped = 	no
hre = 	no
#dev level = 	Developing
temple = 	yes
workshop = 	no
regimental_camp = 	yes
constable = 	yes
courthouse = 	yes
marketplace = 	yes
customs_house = 	no
fort1 = 	yes
fort2 = 	yes
fort3 = 	no
fort4 = 	no
fort5 = 	no
fort6 = 	no

2000.1.1 = {
	owner = MY9
	controller = MY9
	add_core = MY9
}