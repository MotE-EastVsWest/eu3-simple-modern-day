#599 - Kelantan



culture = malaysia_culture  
#OLDREL religion =
religion = protestant
#religion =  sunni  

discovered_by = HAT 
discovered_by = latin 
discovered_by = eastern 
discovered_by = muslim 
discovered_by = indian 
discovered_by = chinese

capital = "Kuala Terengganu"


terrain = forested_plain

trade_goods =fish

citysize = 	195085
manpower = 	2
base_tax = 	2
smd_prepped = 	no
hre = 	no
#dev level = 	Underdeveloped
temple = 	no
workshop = 	no
regimental_camp = 	no
constable = 	no
courthouse = 	no
marketplace = 	no
customs_house = 	no
fort1 = 	yes
fort2 = 	no
fort3 = 	no
fort4 = 	no
fort5 = 	no
fort6 = 	no

2000.1.1 = {
	owner = MY9
	controller = MY9
	add_core = MY9
}