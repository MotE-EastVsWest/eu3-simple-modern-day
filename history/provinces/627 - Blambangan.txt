# 627 - Blambangan
culture = indonesia_culture
#OLDREL religion =
religion = protestant
#religion =  sunni
discovered_by = HAT
discovered_by = latin
discovered_by = eastern
discovered_by = muslim
discovered_by = indian
discovered_by = chinese
capital = "Malang"
terrain = mountain
trade_goods = cotton
citysize = 732738
manpower = 7.3
base_tax = 5
smd_prepped = no
hre = no
# dev level = 	Underdeveloped
temple = yes
workshop = no
regimental_camp = no
constable = no
courthouse = no
marketplace = no
customs_house = no
fort1 = yes
fort2 = no
fort3 = no
fort4 = no
fort5 = no
fort6 = no
################################################################################
# Anything typed here will be added to all selected province history files
################################################################################
2000.1.1 = { owner = ID9 controller = ID9 add_core = ID9 } 