# 629 - Demak
culture = indonesia_culture
#OLDREL religion =
religion = protestant
#religion =  sunni
trade_goods = fish
discovered_by = HAT
discovered_by = latin
discovered_by = eastern
discovered_by = muslim
discovered_by = indian
discovered_by = chinese
capital = "Semarang"
terrain = forested_plain
citysize = 636703
manpower = 6.4
base_tax = 6
smd_prepped = no
hre = no
# dev level = 	Developing
temple = yes
workshop = no
regimental_camp = yes
constable = yes
courthouse = yes
marketplace = yes
customs_house = no
fort1 = yes
fort2 = yes
fort3 = no
fort4 = no
fort5 = no
fort6 = no
################################################################################
# Anything typed here will be added to all selected province history files
################################################################################
2000.1.1 = { owner = ID9 controller = ID9 add_core = ID9 } 