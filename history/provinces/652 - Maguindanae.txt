# 652 - Maguindanao
culture = philippines_culture
#OLDREL religion =
religion = protestant
#religion =  catholic
discovered_by = HAT
discovered_by = latin
discovered_by = eastern
discovered_by = muslim
discovered_by = indian
discovered_by = chinese
capital = "Davao City"
terrain = jungle
trade_goods = iron
citysize = 554134
manpower = 5.5
base_tax = 6
smd_prepped = no
hre = no
# dev level = 	Developing
temple = yes
workshop = no
regimental_camp = yes
constable = yes
courthouse = yes
marketplace = yes
customs_house = no
fort1 = yes
fort2 = yes
fort3 = no
fort4 = no
fort5 = no
fort6 = no
################################################################################
# Anything typed here will be added to all selected province history files
################################################################################
2000.1.1 = { owner = PH9 controller = PH9 add_core = PH9 } 