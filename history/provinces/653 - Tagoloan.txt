# 653 - Tagoloan
culture = moroland_culture
#OLDREL religion =
religion = reactionary_r
#religion =  sunni
discovered_by = HAT
discovered_by = latin
discovered_by = eastern
discovered_by = muslim
discovered_by = indian
discovered_by = chinese
capital = "Cagayan de Oro"
terrain = mountain
trade_goods = iron
citysize = 990000
manpower = 9.9
base_tax = 6
smd_prepped = no
hre = no
# dev level = 	Underdeveloped
temple = yes
workshop = no
regimental_camp = no
constable = no
courthouse = no
marketplace = yes
customs_house = no
fort1 = yes
fort2 = yes
fort3 = no
fort4 = no
fort5 = no
fort6 = no

cot = yes
################################################################################
# Anything typed here will be added to all selected province history files
################################################################################
2000.1.1 = { owner = PH9 controller = REB add_core = DA9 add_core = PH9 
	revolt = {
		type = nationalist_rebels
		size = 1
	}
} 