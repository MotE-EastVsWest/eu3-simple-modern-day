trade_goods = wine
culture = germany_culture
#OLDREL religion =
religion = protestant
#religion =  protestant
terrain = forested_plain
discovered_by = HAT
discovered_by = latin
discovered_by = eastern
discovered_by = muslim
discovered_by = indian
discovered_by = chinese
capital = "Nuremberg"
citysize = 711530
manpower = 7.1
base_tax = 9
smd_prepped = no
hre = no
# dev level = 	Developed
temple = yes
workshop = yes
regimental_camp = yes
constable = yes
courthouse = yes
marketplace = yes
customs_house = yes
fort1 = yes
fort2 = yes
fort3 = yes
fort4 = yes
fort5 = no
fort6 = no
################################################################################
# Anything typed here will be added to all selected province history files
################################################################################
2000.1.1 = { owner = DE9 controller = DE9 add_core = DE9 } 