# 720 - Tshiyet Noyan
culture = mongolia_culture
#OLDREL religion =
religion = protestant
#religion =  buddhism
discovered_by = HAT
discovered_by = latin
discovered_by = eastern
discovered_by = muslim
discovered_by = indian
discovered_by = chinese
terrain = desert
capital = "Dalanzadgad"
trade_goods = tea
citysize = 13129
manpower = 0.1
base_tax = 1
smd_prepped = no
hre = no
# dev level = 	Underdeveloped
temple = no
workshop = no
regimental_camp = no
constable = no
courthouse = no
marketplace = no
customs_house = no
fort1 = no
fort2 = no
fort3 = no
fort4 = no
fort5 = no
fort6 = no
################################################################################
# Anything typed here will be added to all selected province history files
################################################################################
2000.1.1 = { owner = MO9 controller = MO9 add_core = MO9 } 