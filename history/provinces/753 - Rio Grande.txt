# 753 - Rio Grande
culture = brazil_culture
#OLDREL religion =
religion = protestant
#religion =  catholic
discovered_by = HAT
discovered_by = latin
discovered_by = eastern
discovered_by = muslim
discovered_by = indian
discovered_by = chinese
capital = "Fortaleza"
trade_goods = sugar
citysize = 833580
manpower = 8.3
base_tax = 7
smd_prepped = no
hre = no
# dev level = 	Developing
temple = yes
workshop = yes
regimental_camp = yes
constable = yes
courthouse = yes
marketplace = yes
customs_house = yes
fort1 = yes
fort2 = yes
fort3 = yes
fort4 = no
fort5 = no
fort6 = no
################################################################################
# Anything typed here will be added to all selected province history files
################################################################################
2000.1.1 = { owner = BR9 controller = BR9 add_core = BR9 } 