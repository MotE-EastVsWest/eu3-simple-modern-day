# 756 - Bahia
culture = brazil_culture
#OLDREL religion =
religion = protestant
#religion =  catholic
discovered_by = HAT
discovered_by = latin
discovered_by = eastern
discovered_by = muslim
discovered_by = indian
discovered_by = chinese
capital = "Aracaju"
terrain = forested_plain
trade_goods = sugar
citysize = 132982
manpower = 1.3
base_tax = 3
smd_prepped = no
hre = no
# dev level = 	Developing
temple = yes
workshop = no
regimental_camp = no
constable = no
courthouse = no
marketplace = yes
customs_house = no
fort1 = yes
fort2 = no
fort3 = no
fort4 = no
fort5 = no
fort6 = no
################################################################################
# Anything typed here will be added to all selected province history files
################################################################################
2000.1.1 = { owner = BR9 controller = BR9 add_core = BR9 } 