#788 - Cuyo



culture = argentina_culture
#OLDREL religion =
religion = protestant
#religion =  catholic
discovered_by = HAT 
discovered_by = latin 
discovered_by = eastern 
discovered_by = muslim 
discovered_by = indian 
discovered_by = chinese

capital = "Mendoza"


terrain = mountain

trade_goods =grain

citysize = 	206600
manpower = 	2.1
base_tax = 	4
smd_prepped = 	no
hre = 	no
#dev level = 	Developing
temple = 	yes
workshop = 	no
regimental_camp = 	no
constable = 	no
courthouse = 	no
marketplace = 	no
customs_house = 	no
fort1 = 	yes
fort2 = 	yes
fort3 = 	no
fort4 = 	no
fort5 = 	no
fort6 = 	no


2000.1.1 = {
	owner = AG9
	controller = AG9
	add_core = AG9
}