#819 - Guayaquil

culture = ecuador_culture 
#OLDREL religion =
religion = protestant
#religion =  catholic
discovered_by = HAT 
discovered_by = latin 
discovered_by = eastern 
discovered_by = muslim 
discovered_by = indian 
discovered_by = chinese


terrain = forested_plain

capital = "Guayaquil"

trade_goods =grain

citysize = 	622740
manpower = 	6.2
base_tax = 	6
smd_prepped = 	no
hre = 	no
#dev level = 	Developing
temple = 	yes
workshop = 	no
regimental_camp = 	yes
constable = 	yes
courthouse = 	yes
marketplace = 	yes
customs_house = 	no
fort1 = 	yes
fort2 = 	yes
fort3 = 	no
fort4 = 	no
fort5 = 	no
fort6 = 	no

2000.1.1 = {
	owner = EC9
	controller = EC9
	add_core = EC9
}