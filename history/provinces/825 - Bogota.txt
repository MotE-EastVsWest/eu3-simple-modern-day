#825 - Bogota

culture = colombia_culture 
#OLDREL religion =
religion = protestant
#religion =  catholic
discovered_by = HAT 
discovered_by = latin 
discovered_by = eastern 
discovered_by = muslim 
discovered_by = indian 
discovered_by = chinese

capital = "Bogota"

terrain = mountain


trade_goods =coffee

citysize = 	990000
manpower = 	9.9
base_tax = 	9
smd_prepped = 	no
hre = 	no
#dev level = 	Developing
temple = 	yes
workshop = 	yes
regimental_camp = 	yes
constable = 	yes
courthouse = 	yes
marketplace = 	yes
customs_house = 	yes
fort1 = 	yes
fort2 = 	yes
fort3 = 	yes
fort4 = 	yes
fort5 = 	no
fort6 = 	no

cot = yes


2000.1.1 = {
	owner = CO9
	controller = CO9
	add_core = CO9
	add_core = CO8
}