culture = germany_culture
#OLDREL religion =
religion = protestant
#religion =  protestant
trade_goods = cloth
discovered_by = HAT
discovered_by = latin
discovered_by = eastern
discovered_by = muslim
discovered_by = indian
discovered_by = chinese
terrain = forested_plain
capital = "Düsseldorf"
citysize = 244000
manpower = 2.4
base_tax = 6
smd_prepped = no
hre = no
# dev level = 	Developed
temple = yes
workshop = yes
regimental_camp = yes
constable = yes
courthouse = yes
marketplace = yes
customs_house = no
fort1 = yes
fort2 = yes
fort3 = yes
fort4 = no
fort5 = no
fort6 = no
################################################################################
# Anything typed here will be added to all selected province history files
################################################################################
2000.1.1 = { owner = DE9 controller = DE9 add_core = DE9 } 