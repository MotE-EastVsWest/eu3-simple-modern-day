#862 - Yaqui


culture = mexico_culture
#OLDREL religion =
religion = protestant
#religion =  catholic 

discovered_by = HAT 
discovered_by = latin 
discovered_by = eastern 
discovered_by = muslim 
discovered_by = indian 
discovered_by = chinese

capital = "Hermosillo" 
mexico = no


terrain = desert

trade_goods =wool

citysize = 	176855
manpower = 	1.8
base_tax = 	3
smd_prepped = 	no
hre = 	no
#dev level = 	Developing
temple = 	yes
workshop = 	no
regimental_camp = 	no
constable = 	no
courthouse = 	no
marketplace = 	no
customs_house = 	no
fort1 = 	yes
fort2 = 	no
fort3 = 	no
fort4 = 	no
fort5 = 	no
fort6 = 	no


2000.1.1 = {
	owner = MX9
	controller = MX9
	add_core = MX9
}