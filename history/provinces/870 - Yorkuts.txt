#870 - Yorkuts



culture = california_culture
#OLDREL religion =
religion = socialist_r
#religion =  catholic


discovered_by = HAT 
discovered_by = latin 
discovered_by = eastern 
discovered_by = muslim 
discovered_by = indian 
discovered_by = chinese


terrain = forested_plain


capital = "Yorkuts" 

trade_goods = fish

citysize = 	990000
manpower = 	9.9
base_tax = 	10
hre = 	no
#dev level = 	Developed
temple = 	yes
workshop = 	yes
regimental_camp = 	yes
constable = 	yes
courthouse = 	yes
marketplace = 	yes
customs_house = 	yes
fort1 = 	yes
fort2 = 	yes
fort3 = 	yes
fort4 = 	yes
fort5 = 	no
fort6 = 	no

cot = yes

2000.1.1 = {
	owner = US9
	controller = US9
	add_core = US9
	add_core = CL9
}