#907 - Dakota


culture = united_states_of_america_culture
#OLDREL religion =
religion = protestant
#religion =  protestant
discovered_by = HAT 
discovered_by = latin 
discovered_by = eastern 
discovered_by = muslim 
discovered_by = indian 
discovered_by = chinese

capital = "Sioux Falls"


terrain = steppe

trade_goods =grain

citysize = 	76392
manpower = 	0.8
base_tax = 	3
#smd_prepped = 	no
hre = 	no
#dev level = 	Developed
temple = 	yes
workshop = 	no
regimental_camp = 	no
constable = 	no
courthouse = 	no
marketplace = 	yes
customs_house = 	no
fort1 = 	yes
fort2 = 	no
fort3 = 	no
fort4 = 	no
fort5 = 	no
fort6 = 	no


2000.1.1 = {
	owner = US9
	controller = US9
	add_core = US9
}
