# 93 Li�ge - Principal cities: Li�ge & Namur
culture = wallonia_culture
#OLDREL religion =
religion = protestant
#religion =  catholic
discovered_by = HAT
discovered_by = latin
discovered_by = eastern
discovered_by = muslim
discovered_by = indian
discovered_by = chinese
terrain = forested_plain
capital = "Li�ge"
trade_goods = iron
citysize = 221398
manpower = 2.2
base_tax = 6
# smd_prepped = 	no
hre = no
# dev level = 	Developed
temple = yes
workshop = yes
regimental_camp = yes
constable = yes
courthouse = yes
marketplace = yes
customs_house = no
fort1 = yes
fort2 = yes
fort3 = yes
fort4 = no
fort5 = no
fort6 = no
################################################################################
# Anything typed here will be added to all selected province history files
################################################################################
2000.1.1 = { owner = BG9 controller = BG9 add_core = BG9 add_core = WL9 } 