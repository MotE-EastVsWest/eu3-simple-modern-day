#931 - Chiaha


culture = united_states_of_america_culture
#OLDREL religion =
religion = conservative_r
#religion =  reformed
discovered_by = HAT 
discovered_by = latin 
discovered_by = eastern 
discovered_by = muslim 
discovered_by = indian 
discovered_by = chinese

capital = "Atlanta"
confederacy = no

terrain = forested_plain

trade_goods =fur

citysize = 	990000
manpower = 	9.9
base_tax = 	10
#smd_prepped = 	no
hre = 	no
#dev level = 	Developed
temple = 	yes
workshop = 	yes
regimental_camp = 	yes
constable = 	yes
courthouse = 	yes
marketplace = 	yes
customs_house = 	yes
fort1 = 	yes
fort2 = 	yes
fort3 = 	yes
fort4 = 	yes
fort5 = 	no
fort6 = 	no

cot = yes


2000.1.1 = {
	owner = US9
	controller = US9
	add_core = US9
}