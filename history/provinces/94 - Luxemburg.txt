# 94 Luxembourg - Principal cities: Luxembourg


culture = luxembourg_culture 
#OLDREL religion =
religion = protestant
#religion =  catholic     

discovered_by = HAT 
discovered_by = latin 
discovered_by = eastern 
discovered_by = muslim 
discovered_by = indian 
discovered_by = chinese


terrain = forested_plain

capital = "Luxemburg"

trade_goods =iron

citysize = 	128788
manpower = 	1.3
base_tax = 	4
smd_prepped = 	no
hre = 	no
#dev level = 	Developed
temple = 	yes
workshop = 	no
regimental_camp = 	no
constable = 	no
courthouse = 	no
marketplace = 	yes
customs_house = 	no
fort1 = 	yes
fort2 = 	yes
fort3 = 	no
fort4 = 	no
fort5 = 	no
fort6 = 	no


2000.1.1 = {
	owner = LX9
	controller = LX9
	add_core = LX9
}