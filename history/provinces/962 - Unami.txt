#962 - Unami

culture = united_states_of_america_culture
#OLDREL religion =
religion = protestant
#religion =  catholic


discovered_by = HAT 
discovered_by = latin 
discovered_by = eastern 
discovered_by = muslim 
discovered_by = indian 
discovered_by = chinese

capital = "Newark"

terrain = forested_plain

trade_goods =fur

citysize = 	202310
manpower = 	2
base_tax = 	6
#smd_prepped = 	no
hre = 	no
#dev level = 	Developed
temple = 	yes
workshop = 	yes
regimental_camp = 	yes
constable = 	yes
courthouse = 	yes
marketplace = 	yes
customs_house = 	no
fort1 = 	yes
fort2 = 	yes
fort3 = 	yes
fort4 = 	no
fort5 = 	no
fort6 = 	no


2000.1.1 = {
	owner = US9
	controller = US9
	add_core = US9
}