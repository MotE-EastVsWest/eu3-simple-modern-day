#969 - Pocumtuk

culture = united_states_of_america_culture

discovered_by = HAT 
discovered_by = latin 
discovered_by = eastern 
discovered_by = muslim 
discovered_by = indian 
discovered_by = chinese

capital = "Burlington"


terrain = mountain

trade_goods = fur
#OLDREL religion =
religion = protestant
#religion =  protestant

citysize = 	129413
manpower = 	1.3
base_tax = 	4
#smd_prepped = 	no
hre = 	no
#dev level = 	Developed
temple = 	yes
workshop = 	no
regimental_camp = 	no
constable = 	no
courthouse = 	no
marketplace = 	yes
customs_house = 	no
fort1 = 	yes
fort2 = 	yes
fort3 = 	no
fort4 = 	no
fort5 = 	no
fort6 = 	no

2000.1.1 = {
	owner = US9
	controller = US9
	add_core = US9
}
