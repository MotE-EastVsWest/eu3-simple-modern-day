#976 - Tlingit

#OLDREL religion =
religion = protestant
#religion =  protestant

culture = united_states_of_america_culture


discovered_by = HAT 
discovered_by = latin 
discovered_by = eastern 
discovered_by = muslim 
discovered_by = indian 
discovered_by = chinese
terrain = mountain

capital = "Juneau"

trade_goods = gold

citysize = 	6451
manpower = 	1
base_tax = 	2
smd_prepped = 	no
hre = no
#dev level = 	Developed


2000.1.1 = {
	owner = US9
	controller = US9
	add_core = US9
}