# 99 Gelre - Principal cities: Arnhem, Zwolle & Nijmegen
culture = netherlands_culture
#OLDREL religion =
religion = protestant
#religion =  reformed
discovered_by = HAT
discovered_by = latin
discovered_by = eastern
discovered_by = muslim
discovered_by = indian
discovered_by = chinese
terrain = forested_plain
capital = "Arnhem"
trade_goods = grain
citysize = 72210
manpower = 0.7
base_tax = 3
smd_prepped = no
hre = no
# dev level = 	Developed
temple = yes
workshop = no
regimental_camp = no
constable = no
courthouse = no
marketplace = yes
customs_house = no
fort1 = yes
fort2 = no
fort3 = no
fort4 = no
fort5 = no
fort6 = no
################################################################################
# Anything typed here will be added to all selected province history files
################################################################################
2000.1.1 = { owner = ND9 controller = ND9 add_core = ND9 } 