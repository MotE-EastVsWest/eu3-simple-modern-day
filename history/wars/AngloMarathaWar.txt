name = "Anglo-Maratha War"

1777.3.7 = {
	add_attacker = GBR
	add_defender = MAR
}

# Treaty of Phadnis
1783.2.1 = {
	rem_attacker = GBR
	rem_defender = MAR
}
