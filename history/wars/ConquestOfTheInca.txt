name = "Conquest of the Inca"

1531.1.1 = {
	add_attacker = SPA
	add_defender = INC
}

1531.4.1 = {
	battle = {
		name = "Pun�"
		location = 816
		attacker = {
#			leader =	# Francisco Pizarro
			infantry = 160
			cavalry = 30
			losses = 1	# percent
		}
		defender = {
#			leader =	#
			infantry = 3000
			losses = 14	# percent
		}
		result = win
	}
}

1532.11.16 = {
	battle = {
		name = "Cajamarca"
		location = 813
		attacker = {
#			leader =	# Francisco Pizarro
			infantry = 110
			cavalry = 70
			artillery = 5
			losses = 1	# percent
		}
		defender = {
#			leader =	# Atahualpa
			infantry = 6000
			losses = 70	# percent
		}
		result = win
	}
}

# The death of Atahualpa
1533.8.29 = {
	rem_attacker = SPA
	rem_defender = INC
}
