name = "Dutch-Portuguese War"

1588.1.1 = {
	add_attacker = NED
	add_defender = POR
}

# The Pernambuco Insurrection
1649.2.18 = {
	battle = {
		name = "Guararapes"
		location = 755
		attacker = {
#			leader =	# Johan van den Bricken
			infantry = 4000
			artillery = 5
			losses = 38	# percent
		}
		defender = {
#			leader =	# Francisco Barreto de Menezes
			infantry = 3000
			losses = 10	# percent
		}
		result = loss
	}
}

1654.1.1 = {
	rem_attacker = NED
	rem_defender = POR
}
