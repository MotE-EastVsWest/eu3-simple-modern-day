name = "Eighty Years War"

# Union of Utrecht
1579.1.23 = {
	add_attacker = SPA
	add_defender = NED
}

1597.1.23 = {
	battle = {
		name = "Turnhout"
		location = 90
		attacker = {
#			leader =	# Count of Varas
			infantry = 4600
			cavalry = 550
			artillery = 5
			losses = 38	# percent
		}
		defender = {
#			leader =	# Maurice de Nassau
			infantry = 5900
			cavalry = 820
			artillery = 5
			losses = 1	# percent
		}
		result = loss
	}
}

1600.7.2 = {
	battle = {
		name = "Nieuwpoort"
		location = 90
		attacker = {
#			leader =	# Albrecht of Austria
			infantry = 6000
			cavalry = 1200
			artillery = 10
			losses = 36	# percent
		}
		defender = {
#			leader =	# Maurits of Nassau
			infantry = 9000
			cavalry = 1500
			artillery = 15
			losses = 15	# percent
		}
		result = loss
	}
}

1607.4.25 = {
	battle = {
		name = "Gibraltar"
		location = 1293
		attacker = {
#			leader =	# Juan �lvarez de �vila
			big_ship = 10
			light_ship = 11
			losses = 100	# percent
		}
		defender = {
#			leader =	# Jacob van Heemskerk
			light_ship = 26
			losses = 2	# percent
		}
		result = loss
	}
}

1639.10.31 = {
	battle = {
		name = "Downs"
		location = 1271
		attacker = {
#			leader =	# Antonio de Oquendo
			big_ship = 60
			light_ship = 10
			losses = 86	# percent
		}
		defender = {
#			leader =	# Maarten Tromp
			big_ship = 60
			light_ship = 70
			losses = 1	# percent
		}
		result = loss
	}
}

# Treaty of M�nster
1648.1.30 = {
	rem_attacker = SPA
	rem_defender = NED
}
