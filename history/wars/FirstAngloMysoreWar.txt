name = "First Anglo-Mysore War"

1766.1.1 = {
	add_attacker = GBR
	add_defender = MYS
}

1769.1.1 = {
	rem_attacker = GBR
	rem_defender = MYS
}
