name = "First Muscovite-Lithuanian War"

1486.1.1 = {
	add_attacker = MOS
	add_defender = LIT
}

# Marriage between Helena and Alexander
1495.1.1 = {
	rem_attacker = MOS
	rem_defender = LIT
}
