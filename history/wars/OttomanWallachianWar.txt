name = "Ottoman-Wallachian War"

1594.9.1 = {
	add_attacker = TUR
	add_defender = WAL
}

1595.8.13 = {
	battle = {
		name = "Calugareni"
		location = 161
		attacker = {
#			leader =	# Sinan Pasha
			infantry = 86000
			cavalry = 42000
			losses = 10	# percent
		}
		defender = {
#			leader =	# Michael the Brave
			infantry = 15000
			cavalry = 8000
			losses = 5	# percent
		}
		result = loss
	}
}

1601.1.1 = {
	rem_attacker = TUR
	rem_defender = WAL
}
