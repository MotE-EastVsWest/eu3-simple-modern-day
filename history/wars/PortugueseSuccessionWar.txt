name = "Portuguese Succession War"
succession = POR

1580.7.24 = {
	add_attacker = POR
	add_attacker = ENG
	add_defender = SPA
}

1580.8.25 = {
	battle = {
		name = "Battle of Alcantara"
		location = 227
		attacker = {
#			leader =	# Ant�nio I
			infantry = 18000
			cavalry = 6000
			losses = 80	# percent
		}
		defender = {
#			leader =	# Duque de Alba
			infantry = 15000
			cavalry = 5000
			losses = 20	# percent
		}
		result = loss
	}
}


1580.8.26 = {
	rem_attacker = POR
	rem_attacker = ENG
	rem_defender = SPA
}
