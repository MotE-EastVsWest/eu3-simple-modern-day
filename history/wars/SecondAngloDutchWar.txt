name = "Second Anglo-Dutch War"

1665.3.4 = {
	add_attacker = ENG
	add_defender = NED
}

1665.6.13 = {
	battle = {
		name = "Lowestoft"
		location = 1270
		attacker = {
#			leader =	# James Stuart
			big_ship = 109
			losses = 1	# percent
		}
		defender = {
#			leader =	# Jacob van Wassenaer Obdam
			big_ship = 103
			losses = 16	# percent
		}
		result = loss
	}
}

1666.6.1 = {
	battle = {
		name = "North Foreland"
		location = 1271
		attacker = {
#			leader =	# George Monck
			big_ship = 79
			losses = 13	# percent
		}
		defender = {
#			leader =	# Michiel de Ruyter
			big_ship = 84
			losses = 5	# percent
		}
		result = loss
	}
}

# Treaty of Breda
1667.7.31 = {
	rem_attacker = ENG
	rem_defender = NED
}
