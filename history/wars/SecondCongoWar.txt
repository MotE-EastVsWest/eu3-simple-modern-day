name = "Second Congo War"

2000.1.1 = {
	add_attacker = KI8
	add_attacker = RW9
	add_attacker = BW9
	add_attacker = UN9
	add_defender = KI9
	add_defender = AN9
	add_defender = CN9
}


2001.1.1 = {
	rem_attacker = KI8
	rem_attacker = RW9
	rem_attacker = BW9
	rem_attacker = UN9
	rem_defender = KI9
	rem_defender = AN9
	rem_defender = CN9
}
