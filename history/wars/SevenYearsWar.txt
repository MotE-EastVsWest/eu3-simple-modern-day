name = "Seven Years War"

1756.5.15 = {
	add_attacker = HAB
	add_attacker = RUS
	add_attacker = SAX
	add_attacker = FRA
	add_defender = PRU
	add_defender = GBR
	add_defender = HAN
	add_defender = HES
	add_defender = BRU
}

1756.10.1 = {
	battle = {
		name = "Lobositz"
		location = 266
		attacker = {
#			leader =	# Maximilian Ulysses & Reichsgraf von Browne
			infantry = 24000
			cavalry = 10000
			losses = 8	# percent
		}
		defender = {
#			leader =	# Frederick the Great
			infantry = 16000
			cavalry = 13000
			artillery = 60
			losses = 10	# percent
		}
		result = loss
	}
}


# The Pomeranian War
1757.3.1 = {
	add_attacker = SWE
}

1757.4.21 = {
	battle = {
		name = "Reichenberg"
		location = 266
		attacker = {
#			leader =	# Count K�nigsegg
			infantry = 8000
			cavalry = 2000
			losses = 10	# percent
		}
		defender = {
#			leader =	# Marshal von Bevern
			infantry = 10000
			cavalry = 6000
			losses = 5	# percent
		}
		result = loss
	}
}

1757.5.6 = {
	battle = {
		name = "Prague"
		location = 266
		attacker = {
#			leader =	# Charles of Lorraine & Reichsgraf von Browne
			infantry = 37000
			cavalry = 25000
			losses = 14	# percent
		}
		defender = {
#			leader =	# Frederick the Great
			infantry = 40000
			cavalry = 25000
			losses = 22	# percent
		}
		result = loss
	}
}

1757.6.18 = {
	battle = {
		name = "Kolin"
		location = 266
		attacker = {
#			leader =	# Leopold Josef
			infantry = 32000
			cavalry = 12000
			losses = 20	# percent
		}
		defender = {
#			leader =	# Frederick the Great
			infantry = 23000
			cavalry = 9000
			losses = 45	# percent
		}
		result = win
	}
}

1757.7.26 = {
	battle = {
		name = "Hastenbeck"
		location = 57
		attacker = {
#			leader =	# Louis Charles d'Estr�es
			infantry = 50000
			cavalry = 10000
			artillery = 70
			losses = 5	# percent
		}
		defender = {
#			leader =	# William Augustus
			infantry = 29000
			cavalry = 5000
			artillery = 30
			losses = 4	# percent
		}
		result = win
	}
}

1757.8.30 = {
	battle = {
		name = "Gross-J�gersdorf"
		location = 41
		attacker = {
#			leader =	# Stepan Fedorovich Apraksin
			infantry = 20000
			cavalry = 5000
			artillery = 30
			losses = 7	# percent
		}
		defender = {
#			leader =	# Hans von Lehwaldt
			infantry = 60000
			cavalry = 10000
			artillery = 80
			losses = 20	# percent
		}
		result = win
	}
}

1757.11.5 = {
	battle = {
		name = "Rossbach"
		location = 58
		attacker = {
#			leader =	# Charles de Rohan
			infantry = 38000
			cavalry = 16000
			artillery = 160
			losses = 10	# percent
		}
		defender = {
#			leader =	# Frederick the Great
			infantry = 15000
			cavalry = 6000
			artillery = 60
			losses = 3	# percent
		}
		result = loss
	}
}

1757.11.22 = {
	battle = {
		name = "Breslau"
		location = 264
		attacker = {
#			leader =	# Charles de Lorraine
			infantry = 58000
			cavalry = 20000
			losses = 22	# percent
		}
		defender = {
#			leader =	# August Wilhelm
			infantry = 20000
			cavalry = 7000
			losses = 6	# percent
		}
		result = loss
	}
}

1757.12.5 = {
	battle = {
		name = "Leuthen"
		location = 264
		attacker = {
#			leader =	# Charles de Lorraine
			infantry = 65000
			artillery = 210
			losses = 11	# percent
		}
		defender = {
#			leader =	# Frederick the Great
			infantry = 35000
			artillery = 170
			losses = 14	# percent
		}
		result = loss
	}
}

1758.6.23 = {
	battle = {
		name = "Krefeld"
		location = 84
		attacker = {
#			leader =	# Le Comte de Clermont
			infantry = 45000
			losses = 9	# percent
		}
		defender = {
#			leader =	# Ferdinand of Brunswick
			infantry = 32000
			losses = 6	# percent
		}
		result = loss
	}
}

1758.8.25 = {
	battle = {
		name = "Zorndorf"
		location = 49
		attacker = {
#			leader =	# Willem Fermor
			infantry = 33000
			cavalry = 10000
			artillery = 200
			losses = 40	# percent
		}
		defender = {
#			leader =	# Frederick the Great
			infantry = 24000
			cavalry = 11000
			artillery = 150
			losses = 35	# percent
		}
		result = loss
	}
}

1759.8.12 = {
	battle = {
		name = "Kunersdorf"
		location = 81
		attacker = {
#			leader =	# Ernst von Laudon
			infantry = 48000
			cavalry = 12000
			losses = 14	# percent
		}
		defender = {
#			leader =	# Frederick the Great
			infantry = 35000
			cavalry = 15000
			losses = 20	# percent
		}
		result = win
	}
}

1760.11.3 = {
	battle = {
		name = "Torgau"
		location = 58
		attacker = {
#			leader =	# Leopold Josef Graf Daun
			infantry = 36000
			cavalry = 18000
			artillery = 350
			losses = 30	# percent
		}
		defender = {
#			leader =	# Frederick the Great
			infantry = 32000
			cavalry = 18000
			artillery = 340
			losses = 33	# percent
		}
		result = loss
	}
}

1762.1.1 = {
	add_attacker = SPA
	add_defender = POR
}


# Treaty of St. Petersburg
1762.5.5 = {
	rem_attacker = RUS
	add_defender = RUS
	rem_attacker = SWE
}

# Treaty of Paris
1763.2.10 = {
	rem_attacker = FRA
	rem_attacker = SPA
	rem_defender = GBR
	rem_defender = HAN
	rem_defender = HES
	rem_defender = BRU
	rem_defender = POR
}

# Treaty of Hubertusburg
1763.2.15 = {
	rem_defender = PRU
	rem_defender = RUS
	rem_attacker = HAB
	rem_attacker = SAX
}
