name = "Swedish-Danish War"

1497.1.1 = {
	add_attacker = DAN
	add_defender = SWE
}

1497.10.6 = {
	rem_attacker = DAN
	rem_defender = SWE
}
