name = "War of Lombardy"

1425.5.4 = {
	add_attacker = MLO
	add_defender = VEN
}

# Treaty of Lodi
1454.4.9 = {
	rem_attacker = MLO
	rem_defender = VEN
}
