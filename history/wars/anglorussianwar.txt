name = "Anglo-Russian War"

1807.11.1 = {
	add_attacker = RUS
	add_defender = GBR
}

# Peace
1812.4.1 = {
	rem_attacker = RUS
	rem_defender = GBR
}
