name = "Burmese-Chinese War"

1766.1.1 = {
	add_attacker = MCH
	add_defender = TAU
}

# Peace of Kaungton
1770.1.1 = {
	rem_attacker = MCH
	rem_defender = TAU
}
