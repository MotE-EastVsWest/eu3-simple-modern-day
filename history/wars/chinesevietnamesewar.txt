name = "Chinese-Vietnamese War"

1407.1.1 = {
	add_attacker = MNG
	add_defender = DAI
}

# Vietnamese Defeat
1413.1.1 = {
	rem_attacker = MNG
	rem_defender = DAI
}
