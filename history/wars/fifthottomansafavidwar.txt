name = "Fifth Ottoman-Safavid War" 

1578.1.1 = {
	add_attacker = TUR
	add_defender = PER
}

# Peace of Istanbul
1590.3.21 = {
	rem_attacker = TUR
	rem_defender = PER
}
