name = "Fifth Siamese-Burmese War"

1764.1.1 = {
	add_attacker = TAU
	add_defender = AYU
}

# Singu Mu welcomes peace
1776.6.10 = {
	rem_attacker = TAU
	rem_defender = AYU
}
