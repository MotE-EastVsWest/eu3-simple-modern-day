name = "First Ottoman-Venetian War"

1423.1.1 = {
	add_attacker = TUR
	add_defender = VEN
	add_defender = CEP
}

1430.3.29 = {
	rem_attacker = TUR
	rem_defender = VEN
	rem_defender = CEP
}
