name = "Gollub War"

1422.7.1 = {
	add_attacker = POL
	add_attacker = LIT
	add_defender = TEU
}

# Treaty of Melno
1422.9.27 = {
	rem_attacker = POL
	rem_attacker = LIT
	rem_defender = TEU
}
