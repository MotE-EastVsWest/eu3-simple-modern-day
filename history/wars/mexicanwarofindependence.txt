name = "Mexican War of Independence"

1810.9.18  = {
	add_attacker = MEX
	add_defender = SPA
}

# Treaty of Cordoba
1821.10.24 = {
	rem_attacker = MEX
	rem_defender = SPA
}
