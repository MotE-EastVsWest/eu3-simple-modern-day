name = "Ottoman-Polish War"

1497.1.1 = {
	add_attacker = POL
	add_defender = TUR
	add_defender = MOL
}

# Battle of Cosmin Forest
1497.10.26 = { 
	battle = { 
		name = "Cosmin Forest" 
		location = 268
		attacker = { 
#			leader = 			# Jan I Olbracht of Poland
			infantry = 55000
			cavalry =  20000
			losses = 7			# percent 
		} 
		defender = { 
#			leader =			# Stephen III of Moldavia
			infantry = 25000
			cavalry = 7000
			losses = 3			# percent 
		} 
		result = loss
	} 
} 

# Truce
1499.7.1 = {
	rem_attacker = POL
	rem_defender = TUR
	rem_defender = MOL
}
