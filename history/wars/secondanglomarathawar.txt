name = "Second Anglo-Maratha War"

1803.8.1 = {
	add_attacker = GBR
	add_defender = MAR
	add_defender = BHO
}

1803.9.11 = {
	battle = {
		name = "Delhi"
		location = 522
		attacker = {
#			leader =	# Gerard Lake
			infantry = 4500
			losses = 7	# percent
		}
		defender = {
#			leader =	# Bourquin
			infantry = 19000
			losses = 40	# percent
		}
		result = win
	}
}

1803.9.23 = {
	battle = {
		name = "Assaye"
		location = 545
		attacker = {
#			leader =	# Arthur Wellesley
			infantry = 4500
			cavalry = 2000
			losses = 56	# percent
		}
		defender = {
#			leader =	# Ragojee Bhonsla
			infantry = 50000
			artillery = 100
			losses = 12	# percent
		}
		result = win
	}
}

# Treaty of Deogaon
1803.12.17 = {
	rem_defender = BHO
}

# Treaty of Rajpurghat
1805.12.25 = {
	rem_attacker = GBR
	rem_defender = MAR
}
