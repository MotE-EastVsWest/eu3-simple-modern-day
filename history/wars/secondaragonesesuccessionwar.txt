name = "Second Aragonese Succession War"

succession = ARA

1466.6.30 = {
	add_attacker = ARA
	add_defender = PRO
}

1472.10.16 = {
	rem_attacker = ARA
	rem_defender = PRO
}
