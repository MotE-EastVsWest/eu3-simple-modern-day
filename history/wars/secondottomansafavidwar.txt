name = "Second Ottoman-Safavid War" 

1533.10.1 = {
	add_attacker = TUR
	add_defender = PER
}

# Peace
1536.1.1 = {
	rem_attacker = TUR
	rem_defender = PER
}
