name = "Seventh Ottoman-Safavid War" 

1615.5.22 = {
	add_attacker = PER
	add_defender = TUR
}

# Peace of Erdebil
1618.9.26 = {
	rem_attacker = PER
	rem_defender = TUR
}
