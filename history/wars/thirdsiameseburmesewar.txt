name = "Third Siamese-Burmese War"

1613.1.1 = {
	add_attacker = TAU
	add_defender = AYU
	add_defender = LNA
}

1618.1.1 = {
	rem_attacker = TAU
	rem_defender = AYU
	rem_defender = LNA
}
