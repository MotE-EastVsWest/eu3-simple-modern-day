name = "War of the Burgundian Succession"

1477.1.5 = {
	add_attacker = FRA
	add_defender = HAB
}

# Treaty of Arras
1482.12.23 = {
	rem_attacker = FRA
	rem_defender = HAB
}
