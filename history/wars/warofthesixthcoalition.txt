name = "War of the Sixth Coalition"

1812.6.21  = {
	add_attacker = RFR
	add_attacker = ITA
	add_attacker = NAP
	add_attacker = WES
	add_attacker = BAV
	add_attacker = SAX
	add_attacker = BAD
	add_attacker = HES
	add_attacker = ANH
	add_attacker = WUR
	add_attacker = WBG
	add_attacker = MKL
	add_attacker = SWI
	add_attacker = DNZ
	add_attacker = POL
	add_defender = RUS
}

1812.7.25 = {
	battle = {
		name = "Ostrovno"
		location = 276
		attacker = {
#			leader =	# Joachim Murat
			infantry = 25000
			losses = 13	# percent
		}
		defender = {
#			leader =	# Alexander Ostermann-Tolstoy
			infantry = 16500
			losses = 18	# percent
		}
		result = loss
	}
}

1812.7.30 = {
	battle = {
		name = "Klyatitsy"
		location = 276
		attacker = {
#			leader =	# Nicolas Oudinot
			infantry = 25000
			losses = 20	# percent
		}
		defender = {
#			leader =	# Peter Wittgenstein
			infantry = 20000
			losses = 24	# percent
		}
		result = loss
	}
}

1812.8.17 = {
	battle = {
		name = "Smolensk"
		location = 293
		attacker = {
#			leader =	# Napol�on Bonaparte
			infantry = 175000
			losses = 5	# percent
		}
		defender = {
#			leader =	# Peter Wittgenstein
			infantry = 20000
			losses = 8	# percent
		}
		result = win
	}
}

1812.8.18 = {
	battle = {
		name = "Polotsk"
		location = 275
		attacker = {
#			leader =	# Nicolas Oudinot
			infantry = 18000
			artillery = 120
			losses = 20	# percent
		}
		defender = {
#			leader =	# Peter Wittgenstein
			infantry = 22000
			artillery = 135	
			losses = 33	# percent
		}
		result = win
	}
}

1812.8.18 = {
	battle = {
		name = "Valutino"
		location = 293
		attacker = {
#			leader =	# Ney Junot
			infantry = 30000
			losses = 23	# percent
		}
		defender = {
#			leader =	# Peter Wittgenstein
			infantry = 40000
			losses = 15	# percent
		}
		result = win
	}
}

1812.9.17 = {
	battle = {
		name = "Borodino"
		location = 276
		attacker = {
#			leader =	# Napol�on Bonaparte
			infantry = 82500
			cavalry = 27000
			artillery = 590
			losses = 47	# percent
		}
		defender = {
#			leader =	# Mikhail Kutuzov
			infantry = 72000
			cavalry = 17300
			artillery = 640
			losses = 42	# percent
		}
		result = win
	}
}

1812.10.18 = {
	battle = {
		name = "Tarutino"
		location = 296
		attacker = {
#			leader =	# Joachim Murat
			infantry = 26000
			losses = 12	# percent
		}
		defender = {
#			leader =	# Levin August von Bennigsen
			infantry = 36000
			losses = 3	# percent
		}
		result = loss
	}
}

1812.10.20 = {
	battle = {
		name = "Polotsk"
		location = 275
		attacker = {
#			leader =	# Laurent Gouvion Saint-Cyr
			infantry = 25000
			losses = 32	# percent
		}
		defender = {
#			leader =	# Peter Wittgenstein
			infantry = 50000
			losses = 22	# percent
		}
		result = win
	}
}

1812.10.24 = {
	battle = {
		name = "Maloyaroslavets"
		location = 296
		attacker = {
#			leader =	# Mikhail Illarionovich Kutuzov
			infantry = 12000
			cavalry = 3000
			artillery = 85
			losses = 40	# percent
		}
		defender = {
#			leader =	# Eug�ne de Beauharnais
			infantry = 20000
			losses = 25	# percent
		}
		result = win
	}
}

1812.10.31 = {
	battle = {
		name = "Chashniki"
		location = 276
		attacker = {
#			leader =	# Claude Victor-Perrin
			infantry = 14000
			losses = 9	# percent
		}
		defender = {
#			leader =	# Peter Wittgenstein
			infantry = 11000
			losses = 4	# percent
		}
		result = loss
	}
}

1812.11.3 = {
	battle = {
		name = "Vyazma"
		location = 293
		attacker = {
#			leader =	# Louis Nicolas Davout
			infantry = 24000
			losses = 33	# percent
		}
		defender = {
#			leader =	# Mikhail Miloradovich
			infantry = 26500
			losses = 7	# percent
		}
		result = loss
	}
}

1812.11.13 = {
	battle = {
		name = "Smoliani"
		location = 276
		attacker = {
#			leader =	# Claude Victor
			infantry = 20000
			losses = 15	# percent
		}
		defender = {
#			leader =	# Peter Wittgenstein
			infantry = 30000
			losses = 10	# percent
		}
		result = loss
	}
}

1812.11.15 = {
	battle = {
		name = "Krasnoi"
		location = 293
		attacker = {
#			leader =	# Napol�on Bonaparte
			infantry = 81000
			losses = 12	# percent
		}
		defender = {
#			leader =	# Mikhail Illarionovich Kutuzov
			infantry = 70000
			losses = 7	# percent
		}
		result = loss
	}
}

1812.11.26 = {
	battle = {
		name = "Berezina"
		location = 276
		attacker = {
#			leader =	# Napol�on Bonaparte
			infantry = 75000
			losses = 47	# percent
		}
		defender = {
#			leader =	# Mikhail Illarionovich Kutuzov
			infantry = 64000
			losses = 21	# percent
		}
		result = loss
	}
}

# Treaty of �rebro, Sweden joins the sixth coalition
1813.3.3 = {
	add_defender = SWE
}

1813.3.17 = {
	add_defender = PRU
}

1813.5.21 = {
	battle = {
		name = "Bautzen"
		location = 61
		attacker = {
#			leader =	# Napol�on Bonaparte
			infantry = 115000
			losses = 17	# percent
		}
		defender = {
#			leader =	# Gebhard von Bl�cher
			infantry = 96000
			losses = 21	# percent
		}
		result = win
	}
}

1813.8.12 = {
	add_defender = HAB
}

1813.8.26 = {
	battle = {
		name = "Katzbach"
		location = 264
		attacker = {
#			leader =	# �tienne MacDonald
			infantry = 102000
			losses = 15	# percent
		}
		defender = {
#			leader =	# Gebhard von Bl�cher
			infantry = 114000
			losses = 4	# percent
		}
		result = loss
	}
}

1813.8.27 = {
	battle = {
		name = "Dresden"
		location = 61
		attacker = {
#			leader =	# Napol�on Bonaparte
			infantry = 135000
			losses = 7	# percent
		}
		defender = {
#			leader =	# Karl Philipp
			infantry = 214000
			losses = 18	# percent
		}
		result = win
	}
}

1813.8.30 = {
	battle = {
		name = "Kulm"
		location = 266
		attacker = {
#			leader =	# Dominique Vandamme
			infantry = 32000
			losses = 16	# percent
		}
		defender = {
#			leader =	# Gebhard von Bl�cher
			infantry = 54000
			losses = 20	# percent
		}
		result = loss
	}
}

# Treaty of Ried
1813.10.8 = {
	rem_attacker = BAV
	add_defender = BAV
}

1813.10.13 = {
	rem_attacker = WES
}

1813.10.16 = {
	battle = {
		name = "Leipzig"
		location = 62
		attacker = {
#			leader =	# Napol�on Bonaparte
			infantry = 191000
			losses = 20	# percent
		}
		defender = {
#			leader =	# Karl Philipp
			infantry = 330000
			losses = 16	# percent
		}
		result = loss
	}
}

# Saxony switches sides
1813.10.18 = {
	rem_attacker = SAX
	add_defender = SAX
	rem_attacker = ANH
	add_defender = ANH
}

# Baden and Hesse switch side
1813.10.20 = {
	rem_attacker = BAD
	add_defender = BAD
	rem_attacker = HES
	add_defender = HES
}

# Wurzburg switches side
1813.10.26 = {
	rem_attacker = WBG
	add_defender = WBG
}

1813.10.30 = {
	battle = {
		name = "Hanau"
		location = 81
		attacker = {
#			leader =	# Napol�on Bonaparte
			infantry = 17000
			losses = 26	# percent
		}
		defender = {
#			leader =	# Karl Philipp von Wrede
			infantry = 43000
			losses = 21	# percent
		}
		result = win
	}
}

# W�rttemberg switches side
1813.11.2 = {
	rem_attacker = WUR
	add_defender = WUR
}

1813.11.18 = {
	rem_attacker = SWI
}

# Naples switches side
1814.1.11 = {
	rem_attacker = NAP
	add_defender = NAP
}

1814.2.1 = {
	battle = {
		name = "La Rothi�re"
		location = 187
		attacker = {
#			leader =	# Napol�on Bonaparte
			infantry = 40000
			losses = 15	# percent
		}
		defender = {
#			leader =	# Gebhard von Bl�cher
			infantry = 110000
			losses = 5	# percent
		}
		result = loss
	}
}

1814.3.20 = {
	battle = {
		name = "Arcis-sur-Aube"
		location = 186
		attacker = {
#			leader =	# Napol�on Bonaparte
			infantry = 25000
			losses = 12	# percent
		}
		defender = {
#			leader =	# Karl Philipp
			infantry = 56000
			losses = 7	# percent
		}
		result = loss
	}
}

1814.3.30 = {
	battle = {
		name = "Paris"
		location = 183
		attacker = {
#			leader =	# Joseph Bonaparte
			infantry = 50000
			losses = 8	# percent
		}
		defender = {
#			leader =	# Karl Philipp
			infantry = 100000
			losses = 8	# percent
		}
		result = loss
	}
}

# Treaty of Fontainebleu, Napoleon abdicates unconditionally
1814.4.11 = {
	rem_attacker = RFR
	rem_attacker = POL
	rem_attacker = MKL
	rem_attacker = DNZ
	rem_attacker = ITA
	rem_defender = SWE
	rem_defender = RUS
	rem_defender = HAB
	rem_defender = HES
	rem_defender = WUR
	rem_defender = BAD
	rem_defender = PRU
	rem_defender = BAV
	rem_defender = SAX
	rem_defender = ANH
	rem_defender = WBG
	rem_defender = NAP
}
